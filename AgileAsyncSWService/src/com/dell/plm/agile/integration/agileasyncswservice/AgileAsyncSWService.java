package com.dell.plm.agile.integration.agileasyncswservice;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.agile.api.APIException;
import com.agile.api.AgileSessionFactory;
import com.agile.api.IAgileSession;
import com.agile.api.IChange;
import com.agile.api.IDataObject;
import com.agile.api.IItem;
import com.dell.plm.agile.aic.common.cryptography.FileEncryptorUtil;
import com.dell.plm.agile.aic.common.cryptography.StringEncryptor;
import com.dell.plm.agile.aic.common.exception.ValidationException;
import com.dell.plm.agile.aic.common.util.AgileCommonUtil;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.agileasyncswservice.bean.AutoReGenBean;
import com.dell.plm.agile.integration.agileasyncswservice.bean.DFFileDownloadRequest;
import com.dell.plm.agile.integration.agileasyncswservice.bean.FileDetailsRequest;
import com.dell.plm.agile.integration.agileasyncswservice.bean.PartDetails;
import com.dell.plm.agile.integration.agileasyncswservice.bean.FileChangeInfo;
import com.dell.plm.agile.integration.agileasyncswservice.bo.CreateAutoReGenerationWPCO;
import com.dell.plm.agile.integration.agileasyncswservice.bo.CreateDSCR;
import com.dell.plm.agile.integration.agileasyncswservice.bo.RequestDUPFileActionValidations;
import com.dell.plm.agile.integration.agileasyncswservice.bo.ValidatePSR;
import com.dell.plm.agile.integration.agileasyncswservice.dao.AddServiceQueueDAO;
import com.dell.plm.agile.integration.agileasyncswservice.dao.ServicePLMAppUserDAO;
import com.dell.plm.agile.integration.agileasyncswservice.model.Response;
import com.dell.plm.agile.integration.agileasyncswservice.model.ResponseStatus;
import com.dell.plm.agile.integration.agileasyncswservice.util.Constants;
import com.dell.plm.agile.integration.agileasyncswservice.util.Utility;
import com.dell.plm.agile.integration.agileasyncswservice.bo.ProcessEsupportAckBO;
import com.dell.plm.agile.integration.agileasyncswservice.model.AckUpdateResponse;

//enum Status{Success, Failure};

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class AgileAsyncSWService
{

	public AgileAsyncSWService() {
		super();
	}

	public static String DB_PROPERTIES = "";
	private static final Logger logger = Logger.getLogger(AgileAsyncSWService.class);
	//private static IAgileSession agileSession = null;
	private Connection connection = null;
	private static String configBase = null;
	public static String psrUser = "";

	
	/**
	 * This method is download request exposed to downstream
	 * 
	 * @param messageID Unique MessageID to identify the request
	 * @param downstreamName Name of the DownStream
	 * @param downStreamUserName UserName of the DownStream
	 * @param swb_Number Software Bundle Number in Agile
	 * @param swb_Rev Sotware Bundle Revision
	 * @param folderNumber FolderNumber
	 * @param folderVersion Version
	 * @param fileName  Name of the file   
	 * @param fileTypeCode File Type Code
	 * @return Response Object with Success,ObjectId and ValidationError Attributes
	 * @throws ValidationException
	 * @throws Exception
	 */
	public Response requestFileDownload(String messageID, String downstreamName,String downStreamUserName,
			String swb_Number, String swb_Rev, String folderNumber,
			String folderVersion, String fileName, String fileTypeCode
	) throws Exception 
	{

		return requestRCDFileDownload(messageID,downstreamName,downStreamUserName, swb_Number,swb_Rev,folderNumber, folderVersion, fileName, fileTypeCode, 0, "", "");
		
	}

	/**
	 * This method is for download request for RCD
	 * 
	 * @param messageID Unique MessageID to identify the request
	 * @param downstreamName Name of the DownStream
	 * @param swb_Number Software Bundle Number in Agile
	 * @param swb_Rev Software Bundle Revision 
	 * @param folderNumber FolderNumber
	 * @param folderVersion Version
	 * @param fileName Name of the file
	 * @param fileTypeCode File Type Code
	 * @param dependentqueueid QueueId dependent on another QueueId of Scheduler
	 * @return Response Object with Success,ObjectId and ValidationError Attributes
	 * @throws ValidationException
	 * @throws Exception
	 */
	public Response requestRCDFileDownload(String messageID, String downstreamName,String downStreamUserName,
			String swb_Number, String swb_Rev, String folderNumber,
			String folderVersion, String fileName, String fileTypeCode,
			int dependentqueueid, String batch_process_Id, String batch_process) throws Exception {

		StringBuffer stringBuffer = new StringBuffer("");
		CallableStatement insertReqStmt = null;
		Response response = new Response();
		String action = Constants.FILEDOWNLOAD_ACTION;
		
		/** Code to handle action for RCD downstream*/
		/*if(downstreamName != null && downstreamName.equalsIgnoreCase(Constants.RCD)){
				action = Constants.RCD_ACTION;
		}else{
			action = Constants.FILEDOWNLOAD_ACTION;
		}*/
		
		try {

			if (StringUtils.isEmpty(messageID)) {
				stringBuffer.append("MessageID is Mandatory. \n");
			}

			if (StringUtils.isEmpty(downstreamName)) {
				stringBuffer.append("DownStreamName is Mandatory. \n");
			}
			else
			{
				logger.debug("loading properties..");
				Properties applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
				String dscrDownStreamNames = applicationProperties.getProperty("DSCR_DOWNSTREAM_NAMES").trim();
				logger.debug("downStreamNames:" + dscrDownStreamNames);

				/**Code to handle to check valid downstream name*/
				Utility util = new Utility();
				if (util.isValidDownStream(dscrDownStreamNames, downstreamName)) {
					logger.debug("DownStream name is Valid:" + downstreamName);
				} else {
					stringBuffer.append("INVALID DownStreamName "+ downstreamName + "\n");
				}
			}

			if (StringUtils.isEmpty(downStreamUserName)) {
				stringBuffer.append("DownStreamUserName is Mandatory. \n");
			}

			if (StringUtils.isEmpty(swb_Number)) {
				stringBuffer.append("swb_Number is Mandatory. \n");
			}

			/*if (StringUtils.isEmpty(swb_Rev)) {
				stringBuffer.append("swb_Rev is Mandatory. \n");
			}*/
			
			if (StringUtils.isEmpty(fileTypeCode)) {
				stringBuffer.append("FileTypeCode is Mandatory. \n");
			}
			
			if (StringUtils.isEmpty(folderNumber)) {
				stringBuffer.append("FolderNumber is Mandatory. \n");
			}
			if (StringUtils.isEmpty(folderVersion)) {
				stringBuffer.append("FolderVersion is Mandatory. \n");
			}
			if (StringUtils.isEmpty(fileName)) {
				stringBuffer.append("FileName is Mandatory. \n");
			}

			if (stringBuffer.length() > 0) 
			{
				/*String str = "<Response><Status>Failed</Status><ValidationError>Invalid Parameter:"
						+ stringBuffer.toString()
						+ "</ValidationError></Response>";*/
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				throw new ValidationException(str);
			} 
			else 
			{
				AddServiceQueueDAO dao = new AddServiceQueueDAO();
				//dao.initailizeLogs();
				configureLog4J();
				//encryptFile();
				if(connection == null)
					connection = dao.getDBConnection();
				logger.debug("Start Executing Stored Procedure"+connection);
				System.out.println("Start Executing Stored Procedure");
				try
				{
					//AgileCommonUtil agileCommonUtil = new AgileCommonUtil();
					insertReqStmt = dao.addServiceQueue(connection, action, dependentqueueid, messageID, downstreamName, swb_Number, swb_Rev, folderNumber, folderVersion, fileName, fileTypeCode, "", downStreamUserName, batch_process_Id, batch_process);
				}catch(Exception ex){
					String error = "Exception at addServiceQueue:"+AgileCommonUtil.exception2String(ex);
					logger.error(error);
					throw new Exception(error);
				}
				System.out.println("End Executing Stored Procedure"+insertReqStmt);
				insertReqStmt.executeQuery();
				logger.debug("End Executing Stored Procedure.");
				logger.debug("Inserting the Record has been Completed.");
				// response = "Request Consider.";
				//response.setStatus(Status.Success);
				response.setStatus("Success");
				response.setObjectId("");
				response.setValidationError("");
				logger.debug(response.toString());

			}
		} catch(ValidationException ve){
			//response.setStatus(Status.Failure);
			logger.error("Validation Exception :"+ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		}
		finally {
			try{
				if (insertReqStmt != null) {
					insertReqStmt.close();
				}
				if(connection != null){
					logger.debug("closing the connection");
					connection.close();
					logger.debug("nullifying the connection");
					connection = null;
				}
			} catch (SQLException sqlException) {
				System.out.println("Exception Occured:"
						+ AgileCommonUtil.exception2String(sqlException));
			}
		}
		return response;
	}
	
	
	public Response requestDFFileDownload(DFFileDownloadRequest downloadRequest) throws Exception 
	{
		

		logger.debug("Started the request for DF FileDownload method");
		StringBuffer stringBuffer = new StringBuffer("");
		Response response = new Response();
		CallableStatement insertReqStmt = null;
		PreparedStatement partsStatement= null;
		String action = null;
		Properties applicationProperties=null;
		PartDetails pdArray[]= null;
		PartDetails partDetails=null;
		String partNumber=null , partRev=null, msgID=null;
		
		logger.debug("loading aic_filedownload properties..");
		applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
		
		action= applicationProperties.getProperty("QUEUE_SERVICE_ACTION");
		String tempInsertQuery= "insert into "+Constants.DEE_TEMP_TABLE+" (action, messageid, part_number, part_revision, creation_date) values(?, ?, ?, ?, current_timestamp)";
		try 
		{

			msgID=downloadRequest.getMsgID();
			pdArray=downloadRequest.getPartDetails();
				
			if (StringUtils.isEmpty(msgID)) {
				stringBuffer.append("MessageID is Mandatory. \n");
			}
			
			for(int k=0;k<pdArray.length; k++)
			{
				partDetails = pdArray[k];
				partNumber= partDetails.getPartNumber();
				partRev= partDetails.getPartRevision();
				
				if(StringUtils.isEmpty(partNumber)){
					stringBuffer.append("One Part Number is Mandatory. \n");
				}
				if(StringUtils.isEmpty(partRev)){
					stringBuffer.append("One Revision is Mandatory. \n");
				}
			}
			
			if (stringBuffer.length() > 0) 
			{
				/*String str = "<Response><Status>Failed</Status><ValidationError>Invalid Parameter:"
						+ stringBuffer.toString()
						+ "</ValidationError></Response>";*/
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				throw new ValidationException(str);
			} 
			else
			{
				
				
			
				logger.debug("Message Id: "+ msgID);
				AddServiceQueueDAO dao = new AddServiceQueueDAO();
				configureLog4J();
				//encryptFile(); // Ajit--> Consolidation to properties file
				if(connection == null)
					connection = dao.getDBConnection();
				
			try
				{	
						AgileCommonUtil agileCommonUtil = new AgileCommonUtil();
						logger.debug("Start Executing Stored Procedure"+connection);
						for(int l=0;l<pdArray.length; l++)
						{
							partDetails = pdArray[l];
							partNumber= partDetails.getPartNumber();
							partRev= partDetails.getPartRevision();
						
							logger.debug("Part Number["+ l +"] :"+ partNumber);
							logger.debug("Revision ["+ l +"] :" + partRev);
							logger.debug("Queue Service Action :"+action);
							insertReqStmt = dao.addServiceQueue(connection, action, 0, msgID, "" ,partNumber, partRev, "", "", "", "", "", "", "", "");
							
							partsStatement=connection.prepareStatement(tempInsertQuery);
							partsStatement.setString(1, action);
							partsStatement.setString(2, msgID);
							partsStatement.setString(3, partNumber);
							partsStatement.setString(4,partRev);
						
							
						
							logger.debug("Record has been inserted...");
							if(insertReqStmt!=null && partsStatement !=null)
							{
								insertReqStmt.executeQuery();
								partsStatement.executeQuery();
								connection.commit();
							}
							else
								logger.debug("Get an error while inserting the record in AddServiceQueue or Dee Temp ServiceQueue.....");
						}
						
					response.setStatus("Success");
					response.setObjectId("");
					response.setValidationError("");
				}
			catch(Exception ex)
			{
					String error = "Exception at addServiceQueue:"+AgileCommonUtil.exception2String(ex);
					logger.error(error);
					throw new Exception(error);
			}
				logger.debug("End Executing Stored Procedure.");
				logger.debug("Inserting the Record has been Completed.");
				logger.debug(response.toString());

			}
		} catch(ValidationException ve){
			//response.setStatus(Status.Failure);
			logger.error("Validation Exception :"+ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		}
		finally {
			try{
				if (insertReqStmt != null) {
					insertReqStmt.close();
				}
				if(connection != null){
					logger.debug("closing the connection");
					connection.close();
					logger.debug("nullifying the connection");
					connection = null;
				}
			} catch (SQLException sqlException) {
				System.out.println("Exception Occured:"
						+ AgileCommonUtil.exception2String(sqlException));
			}
		}
		return response;
		}

	
	
	

	/** Start createRCDBuild Method*/
	/**
	 * This method is to Create Batch Request for RCD
	 *  
	 * @param messageID Unique MessageID to identify the request
	 * @param batch_process_Id Batch Process ID unique id to identify the transaction
	 * @param batch_process Batch Process is 'Yes' or 'No'
	 * @return Response Object with Success,ObjectId and ValidationError Attributes
	 * @throws Exception
	 */
	public Response createRCDBuild(String messageID,String batch_process_Id, String batch_process) throws Exception 
	{

		CallableStatement insertReqStmt = null;
		StringBuffer stringBuffer = new StringBuffer("");
		Response response = new Response();
		String action = Constants.RCD_ACTION;
		try {

			if (StringUtils.isEmpty(messageID)) {
				stringBuffer.append("MessageID is Mandatory. \n");
			}

			if (StringUtils.isEmpty(batch_process_Id)) {
				stringBuffer.append("Batch Process ID is Mandatory. \n");
			}
			if (StringUtils.isEmpty(batch_process)) {
				stringBuffer.append("Batch Process is Mandatory. \n");
			}

			if (stringBuffer.length() > 0) 
			{
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				throw new ValidationException(str);
			} 
			else
			{
				AddServiceQueueDAO dao = new AddServiceQueueDAO();
				//dao.initailizeLogs();
				configureLog4J();
				//encryptFile(); // Ajit--> Consolidation to properties file
				if(connection == null)
					connection = dao.getDBConnection();
				logger.debug("Start Executing Stored Procedure");
				try{
					//AgileCommonUtil agileCommonUtil = new AgileCommonUtil();
					insertReqStmt = dao.addServiceQueue(connection, action, 0, messageID, Constants.RCD_REQUESTOR, "", "", "", "", "", "", "", "", batch_process_Id, batch_process);
				}catch(Exception ex){
					String error = "Exception at addServiceQueue:"+AgileCommonUtil.exception2String(ex);
					logger.error(error);
					throw new Exception(error);
				}
				insertReqStmt.executeQuery();
				logger.debug("End Executing Stored Procedure.");
				logger.debug("Inserting the Record has been Completed.");
				//response = "Request Consider.";
				//response.setStatus(Status.Success);
				response.setStatus("Success");
				response.setObjectId("");
				response.setValidationError("");
				logger.debug(response.toString());
			}
		} catch(ValidationException ve){
			//response.setStatus(Status.Failure);
			logger.error("Validation Exception :"+ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		}
		/*catch (Exception e) {
			response = "Exception Occurred.."+ exception2String(e);
			System.out.println("Exception Occured:" + exception2String(e));
		}*/ finally {
			try {
				if (insertReqStmt != null) {
					insertReqStmt.close();
				}if(connection != null){
					logger.debug("closing the connection");
					connection.close();
					logger.debug("nullifying the connection");
					connection = null;
				}
			} catch (SQLException sqlException) {
				System.out.println("Exception Occured:"
						+ AgileCommonUtil.exception2String(sqlException));
			}
		}
		return response;
	}
	/** End createRCDBuild Method*/

	/** Start RequestRCDDownloadAck Method*/
	/**
	 * @param messageID
	 * @param batch_process_Id
	 * @param batch_process
	 * @return
	 * @throws Exception
	 */
	public Response requestRCDDownloadAck(String messageID,String batch_process_Id, String batch_process) throws Exception 
	{

		CallableStatement insertReqStmt = null;
		StringBuffer stringBuffer = new StringBuffer("");
		Response response = new Response();
		String action = Constants.RCD_DOWNLOAD_ACK;
		try {

			if (StringUtils.isEmpty(messageID)) {
				stringBuffer.append("MessageID is Mandatory. \n");
			}

			if (StringUtils.isEmpty(batch_process_Id)) {
				stringBuffer.append("Batch Process ID is Mandatory. \n");
			}
			if (StringUtils.isEmpty(batch_process)) {
				stringBuffer.append("Batch Process is Mandatory. \n");
			}

			if (stringBuffer.length() > 0) 
			{
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				throw new ValidationException(str);
			} 
			else
			{
				AddServiceQueueDAO dao = new AddServiceQueueDAO();
				//dao.initailizeLogs();
				configureLog4J();
				//encryptFile(); // Ajit--> Consolidation to properties file
				if(connection == null)
					connection = dao.getDBConnection();
				logger.debug("Start Executing Stored Procedure");
				try{
					insertReqStmt = dao.addServiceQueue(connection, action, 0, messageID, Constants.RCD_REQUESTOR, "", "", "", "", "", "", "", "", batch_process_Id, batch_process);
				}catch(Exception ex){
					String error = "Exception at addServiceQueue:"+AgileCommonUtil.exception2String(ex);
					logger.error(error);
					throw new Exception(error);
				}
				insertReqStmt.executeQuery();
				logger.debug("End Executing Stored Procedure.");
				logger.debug("Inserting the Record has been Completed.");
				//response = "Request Consider.";
				//response.setStatus(Status.Success);
				response.setStatus("Success");
				response.setObjectId("");
				response.setValidationError("");
				logger.debug(response.toString());
			}
		} catch(ValidationException ve){
			//response.setStatus(Status.Failure);
			logger.error("Validation Exception :"+ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		}
		/*catch (Exception e) {
			response = "Exception Occurred.."+ exception2String(e);
			System.out.println("Exception Occured:" + exception2String(e));
		}*/ finally {
			try {
				if (insertReqStmt != null) {
					insertReqStmt.close();
				}if(connection != null){
					logger.debug("closing the connection");
					connection.close();
					logger.debug("nullifying the connection");
					connection = null;
				}
			} catch (SQLException sqlException) {
				System.out.println("Exception Occured:"
						+ AgileCommonUtil.exception2String(sqlException));
			}
		}
		return response;
	}
	/** End RequestRCDDownloadAck Method*/
	
	/**createDSCR Method */
	/**
	 * This method is to submit DSFOrm
	 * 
	 * @param messageID Unique MessageID to identify the request
	 * @param downStreamName Name of the DownStream
	 * @param downStreamUserName User Name of the DownStream
	 * @param xmlFileName Name of the xml file to attach DSCR
	 * @return Response Object with Success,ObjectId and ValidationError Attributes
	 * @throws Exception
	 */
	public Response createDSCR(String messageID, String downstreamName, String downStreamUserName, String xmlFileName) throws Exception
	{
		CallableStatement insertCallableStmt = null;
		StringBuffer stringBuffer = new StringBuffer("");
		Response response = new Response();
		String action = Constants.DSFORM_ACTION;
		AddServiceQueueDAO dao = new AddServiceQueueDAO();
		String batch_process_Id = "";
		String batch_process = "N";
		//IAgileSession agileSession = null;
		try 
		{
			if (StringUtils.isEmpty(messageID)) {
				stringBuffer.append("MessageID is Mandatory. \n");
			}
			if (StringUtils.isEmpty(downstreamName)) {
				stringBuffer.append("DownStreamName ID is Mandatory. \n");
			} 
			else 
			{
				logger.debug("loading properties..");
				Properties applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
				String dscrDownStreamNames = applicationProperties.getProperty("DSCR_DOWNSTREAM_NAMES").trim();
				logger.debug("dscrDownStreamNames:" + dscrDownStreamNames);

				/**Code to handle to check valid downstream name*/
				Utility util = new Utility();
				if (util.isValidDownStream(dscrDownStreamNames, downstreamName)) {
					logger.debug("dscrDownStream is Valid:" + downstreamName);
				} else {
					stringBuffer.append("INVALID DownStreamName "+ downstreamName + "\n");
				}
			}
			if (StringUtils.isEmpty(downStreamUserName)) {
				stringBuffer.append("DownStreamUserName is Mandatory. \n");
			}
			if (StringUtils.isEmpty(xmlFileName)) {
				stringBuffer.append("FileName is Mandatory. \n");
			}
			if(stringBuffer.length()> 0)
			{
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				throw new ValidationException(str);
			}
			else
			{
				String dscrNumber = "";
				CreateDSCR createDSCR = new CreateDSCR();
				//dao.initailizeLogs();
				configureLog4J();
				//encryptFile(); // Ajit--> Consolidation to properties file
				logger.debug("call createSession method");
				IAgileSession agileSession = createSession();
				if(agileSession != null)
				{
					dscrNumber = createDSCR.getDSCRNumber(agileSession, messageID, downstreamName, downStreamUserName);
					batch_process_Id = dscrNumber;
				}
				if(dscrNumber == null || dscrNumber.equalsIgnoreCase("")){
					String error = "dscr_number not created"+dscrNumber;
					logger.error(error);
					//response = "<Response><Status>Failed</Status><ObjectID></ObjectID><ValidationError>dscr_number not created"+dscrNumber+"</ValidationError></Response>";
					//return response;
					throw new ValidationException(error);
				}
				logger.debug("DB Connection.");
				if(connection == null)
					connection = dao.getDBConnection();
				try{
					//AgileCommonUtil agileCommonUtil = new AgileCommonUtil();
					insertCallableStmt = dao.addServiceQueue(connection, action, 0, messageID, downstreamName, "", "", "", "", xmlFileName, "", dscrNumber, downStreamUserName, batch_process_Id, batch_process);
				}catch(Exception ex){
					String error = "Exception at addServiceQueue:"+AgileCommonUtil.exception2String(ex);
					logger.error(error);
					throw new Exception(error);
				}
				System.out.println("End Executing Stored Procedure"+insertCallableStmt);
				insertCallableStmt.executeQuery();
				logger.debug("End Executing Stored Procedure.");
				//response.setStatus(Status.Success);
				response.setStatus("Success");
				response.setObjectId(dscrNumber);
				response.setValidationError("");
				/**close the agileSession*/
				agileSession.close();
			}
		} 
		catch(ValidationException ve){
			logger.error("Validation Exception :"+ve.getMessage());
			//response.setStatus(Status.Failure);
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		}
		/*catch (SQLException e) {
			response = "<Response><Status>Failed</Status><Error>SQL Exception Occurred.."+ exception2String(e)+"</Error></Response>";
			System.out.println(e.getMessage());
		}*/
		finally
		{
			try{
				if (insertCallableStmt != null) {
					insertCallableStmt.close();
				}
				if(connection != null){
					logger.debug("closing the connection");
					connection.close();
					logger.debug("nullifying the connection");
					connection = null;
				}
			} catch (SQLException sqlException) {
				System.out.println("Exception Occured:"
						+ AgileCommonUtil.exception2String(sqlException));
			}

		}
		return response;
	}

	

	/**End of createDSCR Method */

	/** Start GetPCID Method*/
	/**
	 * This method is to get PCI_ID details.
	 *  
	 * @param messageID Unique MessageID to identify the request
	 * @param objectInfo Object Info of PCI
	 * @return Response Object with Success,ObjectId and ValidationError Attributes
	 * @throws Exception
	 */
	public Response createPCID(String messageID,String swb_Number,String downstreamName, String downStreamUserName) throws Exception 
	{

		CallableStatement insertReqStmt = null;
		Response response = new Response();
		String action = Constants.PCI_ID_ACTION;
		StringBuffer stringBuffer = new StringBuffer("");
		AddServiceQueueDAO dao = new AddServiceQueueDAO();

		try {

			configureLog4J();
			//encryptFile(); // Ajit--> Consolidation to properties file
			if (StringUtils.isEmpty(messageID)) {
				stringBuffer.append("MessageID is Mandatory. \n");
			}
			if (StringUtils.isEmpty(swb_Number)) {
				stringBuffer.append("swb_Number ID is Mandatory. \n");
			}

			if (StringUtils.isEmpty(downstreamName)) {
				stringBuffer.append("DownStreamName is Mandatory. \n");
			}
			if (StringUtils.isEmpty(downStreamUserName)) {
				stringBuffer.append("DownStreamUserName is Mandatory. \n");
			}
			/**Code to handle to check valid downstream name*/
			logger.debug("loading properties..");
			Properties applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
			String dscrDownStreamNames = applicationProperties.getProperty("DSCR_DOWNSTREAM_NAMES").trim();
			logger.debug("dscrDownStreamNames:" + dscrDownStreamNames);
			Utility util = new Utility();
			if (util.isValidDownStream(dscrDownStreamNames, downstreamName)) {
				logger.debug("dscrDownStream is Valid:" + downstreamName);
			} else {
				stringBuffer.append("INVALID DownStreamName "+ downstreamName + "\n");
			}



			if(stringBuffer.length()> 0)
			{
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				throw new ValidationException(str);
			}
			else
			{
				if(connection == null)
					connection = dao.getDBConnection();
				logger.debug("Start Executing Stored Procedure");
				try{
					//AgileCommonUtil agileCommonUtil = new AgileCommonUtil();
					insertReqStmt = dao.addServiceQueue(connection, action,0,messageID, downstreamName, swb_Number, "", "", "", "", "", "", downStreamUserName, "", "");
				}catch(Exception ex){
					String error = "Exception at addServiceQueue:"+AgileCommonUtil.exception2String(ex);
					logger.error(error);
					throw new Exception(error);
				}
				insertReqStmt.executeQuery();
				logger.debug("End Executing Stored Procedure.");
				System.out.println("End Executing Stored Procedure.");
				logger.debug("Inserting the Record has been Completed.");
				//response = "Request Consider.";
				//response.setStatus(Status.Success);
				response.setStatus("Success");
				response.setObjectId("");
				response.setValidationError("");
				logger.debug(response.toString());
			}

		}catch(ValidationException ve){
			logger.error("Validation Exception :"+ve.getMessage());
			//response.setStatus(Status.Failure);
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		} 

		/*catch (Exception e) {
			response = "Exception Occurred.."+ exception2String(e);
			System.out.println("Exception Occured:" + exception2String(e));
		}*/ finally {
			try {
				if (insertReqStmt != null) {
					insertReqStmt.close();
				}
				if(connection != null){
					logger.debug("closing the connection");
					connection.close();
					logger.debug("nullifying the connection");
					connection = null;
				}
			} catch (SQLException sqlException) {
				System.out.println("Exception Occured:"
						+ AgileCommonUtil.exception2String(sqlException));
			}
		}
		return response;
	}
	/** End Get PCID Method*/
	/** Start CreateCFGReader Method*/
	/**
	 * This method is to get PCI_ID details.
	 *  
	 * @param messageID Unique MessageID to identify the request
	 * @param objectInfo Object Info of PCI
	 * @return Response Object with Success,ObjectId and ValidationError Attributes
	 * @throws Exception
	 */
	public Response createCFGReader(String messageID, String swb_Number, String downstreamName, String downStreamUserName) throws Exception 
	{

		CallableStatement insertReqStmt = null;
		Response response = new Response();
		String action = Constants.CFGREADER_ACTION;
		StringBuffer stringBuffer = new StringBuffer("");
		AddServiceQueueDAO dao = new AddServiceQueueDAO();

		try {

			configureLog4J();
			//encryptFile(); // Ajit--> Consolidation to properties file
			if (StringUtils.isEmpty(messageID)) {
				stringBuffer.append("MessageID is Mandatory. \n");
			}
			if (StringUtils.isEmpty(swb_Number)) {
				stringBuffer.append("swb_Number ID is Mandatory. \n");
			}

			if (StringUtils.isEmpty(downstreamName)) {
				stringBuffer.append("DownStreamName is Mandatory. \n");
			}
			if (StringUtils.isEmpty(downStreamUserName)) {
				stringBuffer.append("DownStreamUserName is Mandatory. \n");
			}
			/**Code to handle to check valid downstream name*/
			logger.debug("loading properties..");
			Properties applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
			String dscrDownStreamNames = applicationProperties.getProperty("DSCR_DOWNSTREAM_NAMES").trim();
			logger.debug("dscrDownStreamNames:" + dscrDownStreamNames);
			Utility util = new Utility();
			if (util.isValidDownStream(dscrDownStreamNames, downstreamName)) {
				logger.debug("dscrDownStream is Valid:" + downstreamName);
			} else {
				stringBuffer.append("INVALID DownStreamName "+ downstreamName + "\n");
			}

			if(stringBuffer.length()> 0)
			{
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				throw new ValidationException(str);
			}
			else
			{
				if(connection == null)
					connection = dao.getDBConnection();
				logger.debug("Start Executing Stored Procedure");
				try{
					//AgileCommonUtil agileCommonUtil = new AgileCommonUtil();
					insertReqStmt = dao.addServiceQueue(connection, action,0,messageID, downstreamName, swb_Number, "", "", "", "", "", "", downStreamUserName, "", "");
				}catch(Exception ex){
					String error = "Exception at addServiceQueue:"+AgileCommonUtil.exception2String(ex);
					logger.error(error);
					throw new Exception(error);
				}
				insertReqStmt.executeQuery();
				logger.debug("End Executing Stored Procedure.");
				System.out.println("End Executing Stored Procedure.");
				logger.debug("Inserting the Record has been Completed.");
				response.setStatus("Success");
				response.setObjectId("");
				response.setValidationError("");
				logger.debug(response.toString());
			}

		}catch(ValidationException ve){
			logger.error("Validation Exception :"+ve.getMessage());
			//response.setStatus(Status.Failure);
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		} 

		/*catch (Exception e) {
			response = "Exception Occurred.."+ exception2String(e);
			System.out.println("Exception Occured:" + exception2String(e));
		}*/ finally {
			try {
				if (insertReqStmt != null) {
					insertReqStmt.close();
				}
				if(connection != null){
					logger.debug("closing the connection");
					connection.close();
					logger.debug("nullifying the connection");
					connection = null;
				}
			} catch (SQLException sqlException) {
				System.out.println("Exception Occured:"
						+ AgileCommonUtil.exception2String(sqlException));
			}
		}
		return response;
	}
	/** End CreateCFGReader Method*/	
	
	
	/**
	 * This method is get the SRV details for requested SWB Number
	 * @param messageID Unique MessageID to identify the request
	 * @param downstreamName Name of the DownStream
	 * @param downStreamUserName of the DownStream
	 * @param swb_Number Software Bundle Number in Agile
	 * @return Response Object with Success,ObjectId and ValidationError Attributes
	 * @throws ValidationException
	 * @throws Exception
	 */
    public Response requestSRVDetailsForSWB(String messageID, String downstreamName, String downStreamUserName,
				String swb_Number) throws Exception {

		StringBuffer stringBuffer = new StringBuffer("");
		CallableStatement insertReqCallableStmt = null;
		Response response = new Response();
		String action = Constants.SRV_DETAILS_FOR_SWB_ACTION;
			
		try {
			configureLog4J();
			logger.debug("Log4j initializion completed.");
			//encryptFile(); // Ajit--> Consolidation to properties file
			
			if (StringUtils.isEmpty(messageID)) {
				stringBuffer.append("MessageID is Mandatory. \n");
			}

			if (StringUtils.isEmpty(downstreamName)) {
				stringBuffer.append("DownStreamName is Mandatory. \n");
			}
			else
			{
				logger.debug("loading aic_filedownload_application.properties file...");
				Properties applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
				String dscrDownStreamNames = applicationProperties.getProperty("DSCR_DOWNSTREAM_NAMES").trim();
				logger.debug("downStreamNames:" + dscrDownStreamNames);

				/**Code to handle to check valid downstream name*/
				Utility util = new Utility();
				if (util.isValidDownStream(dscrDownStreamNames, downstreamName)) {
					logger.debug("DownStream name is Valid:" + downstreamName);
				} else {
					stringBuffer.append("INVALID DownStreamName "+ downstreamName + "\n");
				}
			}

			if (StringUtils.isEmpty(downStreamUserName)) {
				stringBuffer.append("DownStreamUserName is Mandatory. \n");
			}

			if (StringUtils.isEmpty(swb_Number)) {
				stringBuffer.append("swb_Number is Mandatory. \n");
			}

			if (stringBuffer.length() > 0) 
			{
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				throw new ValidationException(str);
			} 
			else 
			{
				AddServiceQueueDAO dao = new AddServiceQueueDAO();
				if(connection == null){
					connection = dao.getDBConnection();
					logger.debug("Got the db connection:"+connection);
				}
				
				try
				{
					logger.debug("Start calling addServiceQueue method...");
					insertReqCallableStmt = dao.addServiceQueue(connection, action, 0, messageID, downstreamName, swb_Number, "", "", "", "", "", "", downStreamUserName, "", "");
					logger.debug("End of calling addServiceQueue method...");
				}catch(Exception ex){
					String error = "Exception at addServiceQueue: "+AgileCommonUtil.exception2String(ex);
					logger.error(error);
					throw new Exception(error);
				}
				logger.debug("Start Executing Stored Procedure.");
				insertReqCallableStmt.executeQuery();
				logger.debug("End of Executing Stored Procedure.");
				logger.debug("Inserting the record has been completed.");
				response.setStatus("Success");
				response.setObjectId("");
				response.setValidationError("");
				logger.debug(response.toString());
			}
		} catch(ValidationException ve){
			logger.error("Validation Exception :"+ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		}
		finally {
			try{
				if (insertReqCallableStmt != null) {
					insertReqCallableStmt.close();
					logger.debug("Closing the callable statement");
				}
				if(connection != null){
					logger.debug("closing the connection");
					connection.close();
					logger.debug("nullifying the connection");
					connection = null;
				}
			} catch (SQLException sqlException) {
				logger.error("Exception Occured:"
						+ AgileCommonUtil.exception2String(sqlException));
			}
		}
		return response;
	}
	
	/**
	 * This method will return the ResponseStatus object contains information about file name, status and logMessage for given input
	 * @param messageID - Unique Message ID
	 * @param downstreamName - Downstream Name
	 * @param swb_Number - Software Bundle Number
	 * @return ResponseStatus Object
	 * @throws Exception
	 */
    public ResponseStatus getDUPFileStatus(String messageID, String downstreamName, String swb_Number) throws Exception 
    {
		StringBuffer stringBuffer = new StringBuffer("");
		CallableStatement callableStmt = null;
		ResponseStatus response = new ResponseStatus();
		String procName = "";
		ResultSet resultSet = null;
		Properties applicationProperties = null;
		ServicePLMAppUserDAO dao = null;
		String queueStatusId = null;
		String fileName = null;
		String logMessage = null;
		
		FileChangeInfo fileStat = null;
		ArrayList<FileChangeInfo> fileStatuses = null;
		FileChangeInfo fileStatusArr[] = null;
		
		FileChangeInfo changeStatus = null;
		
		boolean flag = false;
		
		/** Start - Logic for OverallStatus Code */
		Set<String> overAllStatusSet = null;
		String overAllStatus = "";
		String errorNumber = "";
		String successNumber = "";
		String inProgressNumber = "";
		/** End - Logic for OverallStatus Code*/
		
		try {
			configureLog4J();
			logger.debug("Log4j initializion completed.");
			
			logger.debug("loading aic_filedownload_application.properties file...");
			applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");

			if (StringUtils.isEmpty(messageID)) {
				stringBuffer.append(applicationProperties.getProperty("DUP_File_Status_Err_Msg_1").trim()+" \n");
			}

			if (StringUtils.isEmpty(downstreamName)) {
				stringBuffer.append(applicationProperties.getProperty("DUP_File_Status_Err_Msg_2").trim()+" \n");
			}
			else
			{
				String dupDownStreamNames = applicationProperties.getProperty("DUP_FILE_DOWNSTREAM_NAMES").trim();
				logger.debug("dupDownStreamNames:" + dupDownStreamNames);

				/** Code to handle to check valid downstream name */
				Utility util = new Utility();
				if (util.isValidDownStream(dupDownStreamNames, downstreamName)) {
					logger.debug("DownStream name is Valid:" + downstreamName);
				} else {
					stringBuffer.append(applicationProperties.getProperty("DUP_File_Status_Err_Msg_3").trim()+ "\n");
				}
			}
			
			if (StringUtils.isEmpty(swb_Number)) {
				stringBuffer.append(applicationProperties.getProperty("DUP_File_Status_Err_Msg_4").trim()+" \n");
			}

			if (stringBuffer.length() > 0){
				String str = stringBuffer.toString();
				throw new ValidationException(str);
			}
			else
			{
				dao = new ServicePLMAppUserDAO();
				if(connection == null){
					connection = dao.getServicePlmUserDBConnection();
					logger.debug("Got the db connection:"+connection);
				}
				
				try	{
					logger.debug("Start Invoking DUP Get File Status Procedure");
					procName = applicationProperties.getProperty(Constants.DUP_GET_FILE_STATUS_PROC).trim();
					callableStmt = connection.prepareCall("{call "+procName+"(?,?,?)}");
					callableStmt.setString(1, messageID);
					callableStmt.setString(2, swb_Number.trim().toUpperCase());
					callableStmt.registerOutParameter(3, OracleTypes.CURSOR);
					logger.debug("Start Executing Stored Procedure.");
					callableStmt.execute();
					logger.debug("End of Executing Stored Procedure.");
					callableStmt.getResultSet();
					resultSet = (ResultSet) callableStmt.getObject(3);
					logger.debug("End of Invoking DUP Get File Status Procedure");
					
					fileStatuses = new ArrayList<FileChangeInfo>();
					
					overAllStatusSet = new HashSet<String>();
					changeStatus = new FileChangeInfo();
					
					if(resultSet!=null){
						while(resultSet.next()) {
							queueStatusId = resultSet.getString("QUEUESTATUSID");
							fileName = resultSet.getString("FILENAME");
							logMessage = resultSet.getString("LOGMESSAGE");
							logger.debug(queueStatusId+"\t"+fileName+"\t"+logMessage);
							
							fileStat = new FileChangeInfo();
														
							if (queueStatusId != null) {
								
								overAllStatusSet.add(queueStatusId);
								String status = applicationProperties.getProperty(queueStatusId).trim();
								logger.debug("status ==>"+status);
								
								/** WPCO Record information */
								if (fileName == null){
									fileName = applicationProperties.getProperty("DUP_Get_File_Wpco_Name").trim();
									logger.debug("fileName ==>"+fileName);
									
									changeStatus.setName(fileName);
									changeStatus.setStatus(status);
									
									if (status.equalsIgnoreCase(applicationProperties.getProperty("DUP_Get_File_Queue_Status_Error_Name").trim())) {
										int logMsgSizeLimit=Integer.parseInt(applicationProperties.getProperty(Constants.LOG_MSG_SIZE_LIMIT).trim());
										if(logMessage!=null && logMessage.length() > logMsgSizeLimit)
												logMessage = logMessage.substring(0, logMsgSizeLimit);
										
										changeStatus.setMessage(logMessage);
										
									} else {
											if(fileName.equalsIgnoreCase("WPCO") && queueStatusId.equalsIgnoreCase("3") ){
												String message[]=logMessage.split(":");
												changeStatus.setName(fileName+":"+message[0]);
												changeStatus.setMessage("");
											}else {
												changeStatus.setMessage("");
											}
									}
									flag = true;
								} 
								else {
									fileStat.setName(fileName);
									fileStat.setStatus(status);
									if (status.equalsIgnoreCase(applicationProperties.getProperty("DUP_Get_File_Queue_Status_Error_Name").trim())) {
										int logMsgSizeLimit=Integer.parseInt(applicationProperties.getProperty(Constants.LOG_MSG_SIZE_LIMIT).trim());
										if(logMessage!=null && logMessage.length() > logMsgSizeLimit)
												logMessage = logMessage.substring(0, logMsgSizeLimit);
										
										fileStat.setMessage(logMessage);
										
									} else {
										fileStat.setMessage("");
									}
									fileStatuses.add(fileStat);
								}
							} else {
								logger.debug("Records not present for messageID==>"+messageID+"\t"+"swb_Number==>"+swb_Number+" in DB");
								fileStat.setName("");
								fileStat.setMessage("");
								fileStat.setStatus("");
								fileStatuses.add(fileStat);
								
								changeStatus.setName("");
								changeStatus.setMessage("");
								changeStatus.setStatus("");
								
								flag = true;
							}
						}
					}
					
					if(!flag){
						changeStatus.setName("");
						changeStatus.setMessage("");
						changeStatus.setStatus("");
					}
					
					logger.debug("Files size is ==>"+fileStatuses.size());
					fileStatusArr = new FileChangeInfo[fileStatuses.size()];
					for(int i=0; i<fileStatuses.size();i++){
						fileStatusArr[i]=fileStatuses.get(i);
					}
					
					
					/** Start - Logic for OverallStatus Code */
					inProgressNumber = applicationProperties.getProperty(Constants.IN_PROGRESS_NUMBER).trim();
					logger.debug("inProgressNumber ==>"+inProgressNumber);
					
					successNumber = applicationProperties.getProperty(Constants.SUCCESS_NUMBER).trim();
					logger.debug("successNumber ==>"+successNumber);
					
					errorNumber = applicationProperties.getProperty(Constants.ERROR_NUMBER).trim();
					logger.debug("errorNumber ==> "+errorNumber);
										
					if(overAllStatusSet.isEmpty()){
						overAllStatus = "";
					}
					else if(overAllStatusSet.contains(errorNumber)){
						overAllStatus = applicationProperties.getProperty(errorNumber).trim();
					}
					else if(overAllStatusSet.size()==1 && overAllStatusSet.contains(successNumber)){
						overAllStatus = applicationProperties.getProperty(successNumber).trim();
					} else{
						overAllStatus = applicationProperties.getProperty(inProgressNumber).trim();
					}
					logger.debug("overAllStatus ==>"+overAllStatus);
					/** End - Logic for OverallStatus Code */
					
					logger.debug("Fetching the records has been completed.");
					
				}catch(Exception ex){
					String error = "Exception at getDUPFileStatus Method: "+AgileCommonUtil.exception2String(ex);
					logger.error(error);
					throw new Exception(error);
				}
				response.setSwbMessageId(swb_Number+"_"+messageID);
				response.setStatus("Success");
				response.setChangeStatus(changeStatus);
				response.setFileStatus(fileStatusArr);
				response.setObjectId("");
				response.setValidationError("");
				response.setOverallStatus(overAllStatus);
				logger.debug(response.toString());
			}
		} catch(ValidationException ve){
			logger.error("Validation Exception :"+ve.getMessage());
			response.setSwbMessageId("");
			response.setStatus("Failure");
			response.setFileStatus(null);
			response.setChangeStatus(null);
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
			response.setOverallStatus("");
		}
		catch(Exception e){
			logger.error("Exception :"+e.getMessage());
			response.setSwbMessageId("");
			response.setStatus("Failure");
			response.setFileStatus(null);
			response.setChangeStatus(null);
			response.setObjectId("");
			response.setValidationError(e.getMessage());
			response.setOverallStatus("");
		}
		finally {
			try{
				if(resultSet!=null){
					logger.debug("Closing the ResultSet");
					resultSet.close();
					logger.debug("Nullyfying the ResultSet");
					resultSet = null;
				}
				if (callableStmt != null) {
					logger.debug("Closing the callable statement");
					callableStmt.close();
					logger.debug("Closing the callable statement");
					callableStmt=null;
				}
				if(connection != null){
					logger.debug("closing the connection");
					connection.close();
					logger.debug("nullifying the connection");
					connection = null;
				}
			} catch (SQLException sqlException) {
				logger.error("Exception Occured:"
						+ AgileCommonUtil.exception2String(sqlException));
			}
			
			stringBuffer = null;
			procName = null;
			applicationProperties = null;
			dao = null;
			queueStatusId = null;
			fileName = null;
			logMessage = null;
			
			overAllStatus = null;
			if(overAllStatusSet!=null && overAllStatusSet.size()>0){
				overAllStatusSet.clear();
				overAllStatusSet = null;
			} else {
				overAllStatusSet = null;
			}
			
			if(fileStat!=null){
				fileStat = null;
			}
			
			if(fileStatuses!=null && fileStatuses.size()>0){
				fileStatuses.clear();
				fileStatuses = null;
			} else{
				fileStatuses = null;
			}
			
			if(fileStatusArr!=null){
				fileStatusArr = null;
			}
			
			if(changeStatus!=null){
				changeStatus = null;
			}
			
			errorNumber = null;
			successNumber = null;
			inProgressNumber = null;
		}
		return response;
}

  
    /**
     * The Auto ReGeneratation WPCO for given part number and downstream name will create WPCO number.
     * @param downstreamName
     * @param swb_Number
     * @return Response
     * @throws Exception
     */
    public Response createAutoReGenerationWPCO(String downstreamName, String swb_Number) throws Exception
	{
		StringBuffer stringBuffer = new StringBuffer("");
		Response response = new Response();
		Properties applicationProperties = null;
		Properties agilePCProperties = null;
		IAgileSession agileSession = null;
		CreateAutoReGenerationWPCO createWPCO = null;
		AutoReGenBean autoReGenBean = null;
		IChange wpcoChange = null;
		HashMap<IItem, String> validation = null;
		Set<IItem> set = null;
		Iterator<IItem> it = null;
		IItem swbItem = null;
		String val = "";
		
		try 
		{
			configureLog4J();
			logger.debug("Log4j initializion completed.");
			
			logger.debug("loading aic_filedownload_application properties");
			applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
			logger.debug("loaded aic_filedownload_application properties");
			
			if (StringUtils.isEmpty(downstreamName)) {
				stringBuffer.append(applicationProperties.getProperty("DUP_AutoGen_Err_Msg_1").trim()+" \n");
			}
			else 
			{
				String dupDownStreamNames = applicationProperties.getProperty("DUP_FILE_DOWNSTREAM_NAMES").trim();
				logger.debug("dupDownStreamNames:" + dupDownStreamNames);

				/** Code to handle check about valid downstream name */
				Utility util = new Utility();
				if (util.isValidDownStream(dupDownStreamNames, downstreamName)) {
					logger.debug("dupDownStreamNames is Valid:" + downstreamName);
				} else {
					stringBuffer.append(applicationProperties.getProperty("DUP_AutoGen_Err_Msg_2").trim()+" \n");
				}
			}
			
			if (StringUtils.isEmpty(swb_Number)) {
				stringBuffer.append(applicationProperties.getProperty("DUP_AutoGen_Err_Msg_3").trim()+" \n");
			}
			
			if (stringBuffer.length() > 0) {
				String str = stringBuffer.toString();
				throw new ValidationException(str);
			}
			else {
				//encryptFile(); // Ajit--> Consolidation to properties file
				logger.debug("Calling createSession Method");
				agileSession = createSession();
				logger.debug("Agile Session Got Created");
				agilePCProperties = loadApplicationProperties("Agile_PC_References.properties");
				createWPCO = new CreateAutoReGenerationWPCO();
				
				if(agileSession != null)
				{
					validation = createWPCO.basicSWBValidations(agileSession, swb_Number, applicationProperties, agilePCProperties);
					set = validation.keySet();
					it = set.iterator();
					if( it.hasNext()){
						swbItem = it.next();
						val = validation.get(swbItem);
					}
					
					if(val.length()> 0){
						String str = val;
						throw new ValidationException(str);
					} else{
						autoReGenBean = createWPCO.processWPCOAutoReGeneration(agileSession, swbItem, applicationProperties,agilePCProperties);
						if(autoReGenBean!=null){
							wpcoChange = autoReGenBean.getWpcoChange();
							if(autoReGenBean.isAutoGenDone() && wpcoChange.getStatus().getName().equalsIgnoreCase("Released")){
								createWPCO.insertRequestforSWBPromotion(downstreamName.toUpperCase(), swbItem.getName(), wpcoChange.getName(),applicationProperties);
							} else {
								String error = applicationProperties.getProperty("DUP_AutoGen_Err_Msg_4").trim();
								logger.error(error);
								throw new ValidationException(error);
							}
						} else {
							String error = applicationProperties.getProperty("DUP_AutoGen_Err_Msg_4").trim();
							logger.error(error);
							throw new ValidationException(error);
						}
					}
				}
				response.setStatus("Success");
				response.setObjectId(wpcoChange.getName());
				response.setValidationError("");
			}
		} 
		catch (ValidationException ve) {
			logger.error("Validation Exception :" + ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		}
		catch (Exception e) {
			logger.error("Exception :" + e.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(e.getMessage());
		}
		finally {
			if(agileSession!=null){
				agileSession.close();
				agileSession = null;
			}
			
			stringBuffer = null;
			applicationProperties = null;
			agilePCProperties = null;
			createWPCO = null;
			autoReGenBean = null;
			wpcoChange = null;
			if(validation!=null && validation.size()>0){
				validation.clear();
				validation = null;
			}else{
				validation = null;
			}
			set = null;
			it = null;
			swbItem = null;
			val = null;
			
		}
		return response;
	}
    
    /**
    * This method will take the request from downstream for file action - Add,Delete,and/or Replace and Auto Regeneration
    * @param messageID - Unique Message ID
    * @param downstreamName - Downstream Name
    * @param swb_Number - Software Bundle Number
    * @param swb_Rev - Software Bundle Part Revision
    * @param fileDetailsRequest  - FileDetailsRequest Object
    * @param autoRegeneration - Auto Regeneration value is Y or N
    * @return Response - Response Object
    * @throws Exception
    */
   public Response requestDUPFileAction(String messageID, String downstreamName,String swb_Number,String swb_Rev, FileDetailsRequest fileDetailsRequest, String autoRegeneration) throws Exception 
   {
		Response response = new Response();
		Properties applicationProperties = null;
		Properties agilePCProperties = null;
		RequestDUPFileActionValidations fileActionRequest = null;
		String basicValidation = "";
		String fileDataValidation = "";
		IAgileSession agileSession = null;
			
		try
		{
			configureLog4J();
			logger.debug("Log4j initializion completed.");

			//encryptFile(); // Ajit--> Consolidation to properties file

			logger.debug("loading aic_filedownload_application.properties file");
			applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");

			logger.debug("loading Agile_PC_References.properties file");
			agilePCProperties = loadApplicationProperties("Agile_PC_References.properties");
			
			fileActionRequest = new RequestDUPFileActionValidations();
			basicValidation = fileActionRequest.basicValidations(messageID, downstreamName, swb_Number, swb_Rev, fileDetailsRequest, autoRegeneration, applicationProperties, agilePCProperties);
			
			if (basicValidation.length() > 0) {
				throw new ValidationException(basicValidation);
			}
			else {
				agileSession = createSession();
				logger.debug("Agile Session is Created");
				
				if(agileSession != null){
					fileDataValidation = fileActionRequest.fileDataValidations(agileSession, messageID, downstreamName, swb_Number, swb_Rev,
							fileDetailsRequest, autoRegeneration, applicationProperties, agilePCProperties); 
					
					if(fileDataValidation.length() > 0){
						throw new ValidationException(fileDataValidation);
					}
				}
			}
			response.setStatus("Success");
			response.setObjectId("");
			response.setValidationError("");
		}
		catch(ValidationException ve) {
			logger.error("Validation Exception :" + ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		}
		catch(Exception e) {
			logger.error("Exception :" + e.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(e.getMessage());
		}
		finally 
		{
			if (agileSession != null) {
				agileSession.close();
				agileSession = null;
			}
			if (applicationProperties != null)
				applicationProperties = null;

			if (agilePCProperties != null)
				agilePCProperties = null;

			basicValidation = null;
			fileDataValidation = null;

			if (fileActionRequest != null)
				fileActionRequest = null;
		}
		return response;
   }
   
   /**
	 * This method takes request from authorized downstreams for Ack processing 
	 * @param storeId
	 * @param changeNumber
	 * @param downStreamName
	 * @return AckUpdateResponse
	 * @throws Exception
	 */
	public AckUpdateResponse processEsupportAck(String agileIdentifier, String storeId, String downStreamName) throws Exception 
	{
		logger.debug("Inside processEsupportAck Method---");
		AckUpdateResponse response = new AckUpdateResponse();
		StringBuffer stringBuffer = new StringBuffer("");
		Properties applicationProperties = null;
		boolean isAllowedDownStream = false;
		IAgileSession session = null;
		IDataObject dObject = null;
		Properties stdSysconfigProperties = null;
		try 
		{
			
			configureLog4J();
			logger.debug("Log4j initializion completed.");

			//encryptFile(); // Ajit--> Consolidation to properties file

			logger.debug("loading aic_filedownload_application.properties file");
			applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
			
			 /**Loading std_system_config properties*/   // Ajit-->Consolidation to properties file
			String std_sysconfig_propfilepath = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
			//std_sysconfig_propfilepath = configBase+"\\"+std_sysconfig_propfilepath;
			logger.debug("loading std_system_config properties from::"+std_sysconfig_propfilepath);
			stdSysconfigProperties = FilePropertyUtility.loadProperties(std_sysconfig_propfilepath);
			logger.debug("Loaded system config properties.");
						
			if (StringUtils.isEmpty(downStreamName) || downStreamName.trim().equals("?")) 
			{
				stringBuffer.append(applicationProperties.getProperty("DOWNSTREAM_MISSING_ERROR_MSG"));
			}
			else
			{
				String allowedDownStreams[] = applicationProperties.getProperty("ALLOWED_DOWNSTREAMS").split(applicationProperties.getProperty("ALLOWED_DOWNSTREAM_SEPARATOR"));
				for(int i=0;i<allowedDownStreams.length;i++)
				{
					if (allowedDownStreams[i].trim().equalsIgnoreCase(downStreamName.toUpperCase().trim())) 
					{
						isAllowedDownStream = true;
						break;
					}
				}
				if(!isAllowedDownStream)
				{
					stringBuffer.append("'"+downStreamName +"' " +(applicationProperties.getProperty("UNAUTHORIZED_DOWNSTREAM_ERROR_MSG")));
				}
			}
			if (StringUtils.isEmpty(storeId) || storeId.trim().equals("?")) 
			{
				stringBuffer.append(applicationProperties.getProperty("STOREID_MISSING_ERROR_MSG"));
			}
			if (StringUtils.isEmpty(agileIdentifier) || agileIdentifier.trim().equals("?")) 
			{
				stringBuffer.append(applicationProperties.getProperty("AIDENTIFIER_MISSING_ERROR_MSG"));
			}
			else
			{
				/*String username = applicationProperties.getProperty("ESUPPORT_ACK_UPDATE_USER");*/ // Ajit-->Consolidation to properties file
				String username = stdSysconfigProperties.getProperty("PLATFORMPROMOTION_USERNAME");
				/*String password = StringEncryptor.decrypt(applicationProperties.getProperty("ESUPPORT_ACK_UPDATE_PASS"));*/ // Ajit-->Consolidation to properties file
				String password = StringEncryptor.decrypt(stdSysconfigProperties.getProperty("PLATFORMPROMOTION_PASSWORD"));
				/*String url = applicationProperties.getProperty("AGILE.URL");*/ // Ajit-->Consolidation to properties file
				String url = stdSysconfigProperties.getProperty("AGILE_URL");
				session = Utility.createSession(url,username,password);
				logger.debug("Getting Agile Object---");
				dObject = (IDataObject) session.getObject(IChange.OBJECT_TYPE, agileIdentifier);
						
				if(dObject == null)
				{
					stringBuffer.append("Object '"+agileIdentifier+"' doesn't exist in Agile!");
				}
			}

			if(stringBuffer.length()> 0)
			{
				String str = applicationProperties.getProperty("INVALID_PARAMS_ERROR_MSG") + stringBuffer.toString();
				logger.debug(str);
				throw new ValidationException(str);
			}
			else
			{
				ProcessEsupportAckBO ackBo = new ProcessEsupportAckBO();
				logger.debug("Calling doAckProcessing method---");
				response = ackBo.doAckProcessing(agileIdentifier.toUpperCase(), storeId, downStreamName.toUpperCase(), response, applicationProperties);
			}
		}
		catch(ValidationException ve)
		{
			logger.error("Validation Exception: "+ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setFailureReason(ve.getMessage());
		}
		finally
		{
			session = null;
			dObject = null;
			
			if (applicationProperties != null)
				applicationProperties = null;

		}
		logger.debug("---End of processEsupportAck method");
		return response;
	}
	
	/**
	 * This method is for PSR File download request (PQM Attachment Project)
	 * 
	 * @param messageID
	 *            Unique MessageID to identify the request
	 * @param psr_Number
	 *            Software Bundle Number in Agile
	 * @param fileName
	 *            Name of the file
	 * @param fileTypeCode
	 *            File Type Code
	 * @param downstreamName
	 *            Name of the DownStream
	 * @return Response Object with Success,ObjectId and ValidationError
	 *         Attributes
	 * @throws ValidationException
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public Response psrFileDownload(String messageID, String downstreamName, String psr_Number, String fileName,
			String fileExtension) throws Exception {

		StringBuffer stringBuffer = new StringBuffer("");
		CallableStatement insertReqStmt = null;
		Response response = new Response();
		/*Properties agileapplicationProperties = loadApplicationProperties("aic_filedownload_agile.properties");*/ // Ajit-->Consolidation to properties file
		
		//Loading here std_system_config properties// Ajit-->Consolidation to properties file
		
		Properties filedwnloadapplicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
		String stdSys_config_path = filedwnloadapplicationProperties.getProperty("DBPROP_FILEPATH").trim();
		//stdSys_config_path = configBase+"\\"+stdSys_config_path;
		logger.debug("loading std_system_config properties ::"+stdSys_config_path);
		Properties sysConfigProperties = FilePropertyUtility.loadProperties(stdSys_config_path);
		logger.debug("Loaded system config properties.");
		
		/*String agileurl = agileapplicationProperties.getProperty("AGILE_PSR_URL").trim();*/ // Ajit-->Consolidation to properties file
		String agileurl = sysConfigProperties.getProperty("AGILE_URL").trim();
		String agileusername = sysConfigProperties.getProperty("AGILE_PSR_USERNAME").trim();
		Utility util = new Utility();
		// logger.debug("encrypted passWord:"+passWord);
		String agilepassword = sysConfigProperties.getProperty("AGILE_PSR_PASSWORD").trim();
		agilepassword = util.getDecryptPwd(agilepassword);

		IAgileSession agileSession = Utility.createSession(agileurl, agileusername, agilepassword);
		String action = Constants.PSR_FILE_DOWNLOAD_ACTION;
		String valMsg = "";
		boolean userFlag = false;

		if (messageID == null) {
			messageID = "";
		}
		if (downstreamName == null) {
			downstreamName = "";
		}
		if (psr_Number == null) {
			psr_Number = "";
		}
		if (fileName == null) {
			fileName = "";
		}
		if (fileExtension == null) {
			fileExtension = "";
		}

		try {

			logger.debug("PSR Logged In User ::: " + psrUser);
			Properties applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
			
			String sValidUsers = applicationProperties.getProperty("PSR_VALID_USERS").trim();
			StringTokenizer st = new StringTokenizer(sValidUsers, ",");
			logger.debug("currently logged in user :: " + psrUser);
			while (st.hasMoreTokens()) {
				String sUserName = st.nextToken();
				logger.debug("User name from properties file for PSR :: " + sUserName);
				if (psrUser.equalsIgnoreCase(sUserName)) {
					userFlag = true;
				}
			}
			
			if (StringUtils.isEmpty(messageID)) {
				stringBuffer.append("MessageID is mandatory. \n");
			}

			if (StringUtils.isEmpty(downstreamName)) {
				stringBuffer.append("DownStreamName is mandatory. \n");
			} else {
				logger.debug("loading properties..#");
				
				String dscrDownStreamNames = applicationProperties.getProperty("PSR_DOWNSTREAM_NAMES").trim();
				logger.debug("downStreamNames:" + dscrDownStreamNames);

				/** Code to handle to check valid downstream name */
				if (util.isValidDownStream(dscrDownStreamNames, downstreamName)) {
					logger.debug("DownStream name is Valid:" + downstreamName);
				} else {
					stringBuffer
							.append("Please select the valid downstream name, available downstream names for PSR Attachment download are "
									+ dscrDownStreamNames + "\n");
				}
			}
			
			
			
			if (userFlag == false) {
				stringBuffer.append(psrUser + " is not a valid user to download PSR Files \n");
			}

			if (StringUtils.isEmpty(psr_Number)) {
				stringBuffer.append("PSR Number is mandatory. \n");
			}

			if (StringUtils.isEmpty(fileExtension) && StringUtils.isEmpty(fileName)) {
				stringBuffer.append("Either File name or File extension is mandatory. \n");
			}

			logger.debug("msg id :::: " + messageID);
			logger.debug("downstreamName id :::: " + downstreamName);
			logger.debug("psr_Number id :::: " + psr_Number);
			
			ValidatePSR valpsrObj = new ValidatePSR();
			valMsg = valpsrObj.psrTypeValidation(agileSession, psr_Number, fileName, fileExtension);
			logger.debug(" valMsg " + valMsg);
			

			if (!"".equals(valMsg)) {
				stringBuffer.append(valMsg + " \n");
			}

			if (stringBuffer.length() > 0) {

				String str = "Validation Errors :: " + stringBuffer.toString();
				throw new ValidationException(str);
			} else {

				AddServiceQueueDAO dao = new AddServiceQueueDAO();
				configureLog4J();
				//encryptFile(); // Ajit--> Consolidation to properties file
				if (connection == null)
					connection = dao.getDBConnection();
				logger.debug("Start Executing Stored Procedure" + connection);
				System.out.println("Start Executing Stored Procedure");
				try {
					
						insertReqStmt = dao.addServiceQueue(connection, action, 0, messageID, downstreamName,
								psr_Number, "", "", "", fileName, fileExtension, "", "", "", "");
					
				} catch (Exception ex) {
					String error = "Exception at addServiceQueue:" + AgileCommonUtil.exception2String(ex);
					logger.error(error);
					throw new Exception(error);
				}
				System.out.println("End Executing Stored Procedure" + insertReqStmt);
				insertReqStmt.executeQuery();
				logger.debug("End Executing Stored Procedure.");
				logger.debug("Inserting the Record has been Completed.");
				response.setStatus("Success");
				response.setObjectId("");
				response.setValidationError("");
				logger.debug(response.toString());

			}
		} catch (ValidationException ve) {
			logger.error("Validation Exception :" + ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		} finally {
			try {
				if (insertReqStmt != null) {
					insertReqStmt.close();
				}
				if (connection != null) {
					logger.debug("closing the connection");
					connection.close();
					logger.debug("nullifying the connection");
					connection = null;
				}
				psrUser = null;
			} catch (SQLException sqlException) {
				System.out.println("Exception Occured:" + AgileCommonUtil.exception2String(sqlException));
			}
		}

		return response;
	}

    /**
	 * @param message
	 */
	/*private static void traceLog(String message){
		logger.debug(message);
	}*/

	/**
	 * Configures the log4j 
	 */
	private void configureLog4J()
	{
		
		try
		{
			getAicConfigBasePath("asynchwebservice");
			StringBuffer log4jPath = new StringBuffer(configBase);
			log4jPath.append("\\config").append("\\filedownloadservice_log4j.xml");
			//InputStream iStream = getClass().getResource(name)sourceAsStream("/filedownloadservice_log4j.xml");
			//URL url = getClass().getResource("/filedownloadservice_log4j.xml");
			DOMConfigurator.configure(log4jPath.toString());
		}catch(Exception ex){
			System.out.println("Unable to Configure Log4j"+ex.getMessage());
		}
	}

	/**
	 * Loads application properties.
	 * @param propertiesFileName
	 * @return
	 */
	private Properties loadApplicationProperties(String propertiesFileName)
	{
		logger.debug("loading properties.."+propertiesFileName);
		Properties	applicationProperties = null;
		try {
			getAicConfigBasePath("asynchwebservice");
			//applicationProperties = fu.loadWebAppProperties("/"+propertiesFileName);
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\"+propertiesFileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return applicationProperties;
	}

	/**
	 * 
	 */
	// Ajit--> Consolidation to properties file
	/*private void encryptFile()
	{
		String AGILE_PROPERTIES = "";
		logger.debug("loading properties..");
		getAicConfigBasePath("asynchwebservice");
		Properties applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
		AGILE_PROPERTIES = applicationProperties.getProperty("AGILEPROP_FILEPATH").trim();
		AGILE_PROPERTIES = configBase+"\\"+AGILE_PROPERTIES;
		logger.debug("Encrypt the File "+AGILE_PROPERTIES+ "Started");
		FileEncryptorUtil.encryptFile(AGILE_PROPERTIES);
		logger.debug("Encrypt the File done");
	}*/
	
	/**
	 * Creates Agile Session 
	 * 
	 * @return IAgile session object 
	 * 
	 * @throws APIException
	 */
	public IAgileSession createSession() throws Exception 
	{
		IAgileSession agileSession = null;
		logger.debug("loading properties..");
		/*Properties applicationProperties = loadApplicationProperties("aic_filedownload_agile.properties");*/ // Ajit-->Consolidation to properties file
		
		//Loading Standard_System_Configs property file // Ajit-->Consolidation to properties file
		
		Properties applicationProperties = loadApplicationProperties("aic_filedownload_application.properties");
		String standard_system_configsPath = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
		//standard_system_configsPath = configBase+"\\"+standard_system_configsPath;
		logger.debug("loading std_system_config properties from::"+standard_system_configsPath);
		Properties stdSysconfigProperties = FilePropertyUtility.loadProperties(standard_system_configsPath);
		logger.debug("Loaded system config properties.");
		
		/*String url = applicationProperties.getProperty("AGILE_DSFORM_URL").trim();*/ // Ajit-->Consolidation to properties file
		String url = stdSysconfigProperties.getProperty("AGILE_URL").trim();
		logger.debug("url:*************************************"+url);
		/*String userName = applicationProperties.getProperty("AGILE_USERNAME").trim();*/  // Ajit-->Consolidation to properties file
		String userName = stdSysconfigProperties.getProperty("DSCR_USERNAME").trim();
		/*logger.debug("userName:**********************************"+userName);*/
		/*String passWord = applicationProperties.getProperty("AGILE_PASSWORD").trim();*/ // Ajit-->Consolidation to properties file
		String passWord = stdSysconfigProperties.getProperty("DSCR_PASSWORD").trim();
		/*logger.debug("passWord:*************************************"+passWord);*/
		Utility util = new Utility();
		//logger.debug("encrypted passWord:"+passWord);
		passWord = util.getDecryptPwd(passWord);
		/*logger.debug("passWord:*************************************"+passWord);*/
		logger.debug("url:"+url+"userName:"+userName);
		//logger.debug("decrypted passWord:"+passWord);
		if(url == null || url.equalsIgnoreCase("") || userName == null || userName.equalsIgnoreCase("") || passWord == null || passWord.equalsIgnoreCase("")){
			logger.error("Properties file Not Loaded .."+"url:"+url+"userName:"+userName);
			throw new APIException(null, null, "Can't load properties file");
		}
		logger.debug("Getting New Agile Session.");
		AgileSessionFactory instance = AgileSessionFactory.getInstance(url);
		HashMap<Integer, String> params = new HashMap<Integer, String>();
		params.put(AgileSessionFactory.USERNAME, userName);
		params.put(AgileSessionFactory.PASSWORD, passWord);
		agileSession = instance.createSession(params);
		logger.debug("agileSession created"+agileSession);
		return agileSession;
	}
	
	
	/**
	 * Returns the config base of the type passed
	 * @param type
	 * @return
	 */
	private String getAicConfigBasePath(final String type)
	{
		if(configBase == null)
		{
			configBase = FilePropertyUtility.getConfigBasePath(type);
		}
		return configBase;
	}

}