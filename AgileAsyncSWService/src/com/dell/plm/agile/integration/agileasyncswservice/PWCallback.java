package com.dell.plm.agile.integration.agileasyncswservice;

import java.io.IOException;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.apache.ws.security.WSPasswordCallback;

import com.dell.plm.agile.aic.common.cryptography.FileEncryptorUtil;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.agileasyncswservice.ldap.LDAPAuthenticationImpl;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class PWCallback implements CallbackHandler {
    
	Logger logger = null;
	/* (non-Javadoc)
	 * @see javax.security.auth.callback.CallbackHandler#handle(javax.security.auth.callback.Callback[])
	 */
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException 
    {
				
		String configBase = FilePropertyUtility.getConfigBasePath("asynchwebservice");
		
		StringBuffer log4jPath = new StringBuffer(configBase);
		log4jPath.append("\\config").append("\\filedownloadservice_log4j.xml");
		
		//URL url = getClass().getResource("/filedownloadservice_log4j.xml");
		DOMConfigurator.configure(log4jPath.toString());
		logger = Logger.getLogger(PWCallback.class);
		logger.debug("Log4j is configured successfully ..");
		
		FilePropertyUtility fu = new FilePropertyUtility();
        /*String AGILE_PROPERTIES = "";*/  // Ajit--> consolidation to properties file
		String DB_PROPERTIES = "";
        String ldap_prop_filepath = null;
        /**Loading Application properties*/
        Properties aicfiledownloadProperties = null;
        logger.debug("loading aic_filedownload_application properties..");
        
        StringBuffer configBaseLoc = new StringBuffer(configBase);
        configBaseLoc.append("\\config").append("\\aic_filedownload_application.properties");
        aicfiledownloadProperties = FilePropertyUtility.loadProperties(configBaseLoc.toString());
        
        /*AGILE_PROPERTIES = aicfiledownloadProperties.getProperty("AGILEPROP_FILEPATH").trim();*/ // Ajit-->consolidation to properties file
        DB_PROPERTIES = aicfiledownloadProperties.getProperty("DBPROP_FILEPATH").trim();
       /* AGILE_PROPERTIES = configBase+"\\"+AGILE_PROPERTIES;*/ // Ajit-->consolidation to properties file
        //DB_PROPERTIES = configBase+"\\"+DB_PROPERTIES;
        ldap_prop_filepath = aicfiledownloadProperties.getProperty("LDAP_PROP_FILEPATH").trim();
        ldap_prop_filepath = configBase+"\\"+ldap_prop_filepath;
        
/*      logger.debug("Encryption for file "+AGILE_PROPERTIES+ " Started");*/ // Ajit-->consolidation to properties file
        //logger.debug("Encryption for file "+DB_PROPERTIES+ " Started");
/*		FileEncryptorUtil.encryptFile(AGILE_PROPERTIES);*/ // Ajit-->consolidation to properties file
		//FileEncryptorUtil.encryptFile(DB_PROPERTIES); Ajit-->consolidation to properties file
/*		logger.debug("Encryption done for::"+AGILE_PROPERTIES);*/ // Ajit-->consolidation to properties file
		//logger.debug("Encryption done for::"+DB_PROPERTIES); Ajit-->consolidation to properties file
		
        /**Loading Application properties*/
        //Properties applicationProperties = null;
        logger.debug("loading aic_filedownload_agile properties..");
		//applicationProperties = FilePropertyUtility.loadProperties(AGILE_PROPERTIES);
		
		//Loading ldap properties
		 Properties ldapProperties = null;
		 logger.debug("loading ldapProperties properties from::"+ldap_prop_filepath);
		 if(null!= ldap_prop_filepath)
		 {
			 ldapProperties = FilePropertyUtility.loadProperties(ldap_prop_filepath);
			 logger.debug("Loaded ldap properties.");
		 }
		 
		//loadProperties("/aic_filedownload_application.properties");
		//String user = applicationProperties.getProperty("WSSECURITY_USER").trim();
		//String userDomain = null;
		//String userName = null;
		
		//logger.debug("WSSECURITY_USER :"+user);
		//String password = applicationProperties.getProperty("WSSECURITY_PASSWORD").trim();
				
		//Utility util = new Utility();
		//password = util.getDecryptPwd(password);
		 int len = callbacks.length;
        for (int i = 0; i < len; i++)
        {
            if (callbacks[i] instanceof WSPasswordCallback)
            {
                WSPasswordCallback pc = (WSPasswordCallback) callbacks[i];
                String userNameLogged = pc.getIdentifier();
                logger.debug("userName logged: "+userNameLogged+", usage: "+pc.getUsage());
                
                //LDAP authentication , if the username has domain and userID seperated with seperator
                //else will do the authentication with WSSECURITY_USER
               if(userNameLogged.contains("\\") ||userNameLogged.contains("/"))
                {
            	   LDAPAuthenticationImpl ldapImpl = new LDAPAuthenticationImpl();
              	   ldapImpl.doLDAPAuthentication(pc,ldapProperties);
                }
                
               else
                {
 	                if (pc.getUsage() == WSPasswordCallback.USERNAME_TOKEN)
	                {
	                    
 	                	logger.debug("unknown user: "+pc.getIdentifier());
 	                	throw new IOException("unknown user: "+pc.getIdentifier());
 	                	// for passwords sent in digest mode we need to provide the password,
	                    // because the original one can't be un-digested from the message
	
	                    // we can throw either of the two Exception types if authentication fails
	                   /* if (! user.equals(pc.getIdentifier()))
	                        throw new IOException("unknown user: "+pc.getIdentifier());*/
	                	
	                    // this will throw an exception if the passwords don't match
	                   // pc.setPassword(password);
	
	                } else if (pc.getUsage() == WSPasswordCallback.USERNAME_TOKEN_UNKNOWN)
	                {
	                	logger.debug("unknown user: "+pc.getIdentifier());
 	                	throw new IOException("unknown user: "+pc.getIdentifier());
	                	// for passwords sent in cleartext mode we can compare passwords directly
	
	                   /* if (! user.equals(pc.getIdentifier()))
	                        throw new IOException("unknown user: "+pc.getIdentifier());
	                    password=pc.getPassword();*/
	
	                    // we can throw either of the two Exception types if authentication fails
	                    /*if (! password.equals(pc.getPassword()))
	                        throw new IOException("password incorrect for user: "+pc.getIdentifier());*/
	                }
                }
 
            }            
            else
            	
            {
                throw new UnsupportedCallbackException(callbacks[i], "Unrecognized Callback");
            }
        }
    }

}