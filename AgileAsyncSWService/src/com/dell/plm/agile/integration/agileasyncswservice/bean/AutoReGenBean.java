package com.dell.plm.agile.integration.agileasyncswservice.bean;

import com.agile.api.IChange;

public class AutoReGenBean {
	
	private IChange wpcoChange;
	private boolean autoGenDone;
	
	public IChange getWpcoChange() {
		return wpcoChange;
	}
	
	public void setWpcoChange(IChange wpcoChange) {
		this.wpcoChange = wpcoChange;
	}
	
	public boolean isAutoGenDone() {
		return autoGenDone;
	}
	
	public void setAutoGenDone(boolean autoGenDone) {
		this.autoGenDone = autoGenDone;
	}
}
