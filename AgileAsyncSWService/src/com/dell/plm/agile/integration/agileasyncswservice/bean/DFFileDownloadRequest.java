package com.dell.plm.agile.integration.agileasyncswservice.bean;

import com.dell.plm.agile.integration.agileasyncswservice.bean.PartDetails;

public class DFFileDownloadRequest {
	String msgID="";
	PartDetails partDetails[];
	
	public String getMsgID() {
		return msgID;
	}
	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}
	public PartDetails[] getPartDetails() {
		return partDetails;
	}
	public void setPartDetails(PartDetails[] partDetails) {
		this.partDetails = partDetails;
	}
	
	
}
