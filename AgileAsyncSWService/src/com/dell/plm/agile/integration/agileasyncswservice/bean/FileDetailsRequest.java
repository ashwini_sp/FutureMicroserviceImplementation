package com.dell.plm.agile.integration.agileasyncswservice.bean;

public class FileDetailsRequest {

	FileDtls fileDtls[] = null;

	public FileDtls[] getFileDtls() {
		return fileDtls;
	}

	public void setFileDtls(FileDtls[] fileDtls) {
		this.fileDtls = fileDtls;
	}

}
