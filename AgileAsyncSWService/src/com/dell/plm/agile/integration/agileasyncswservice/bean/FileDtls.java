package com.dell.plm.agile.integration.agileasyncswservice.bean;

public class FileDtls {

	private String fileName;
	private String fileDescription;
	private String fileTypeCode;
	private String fileActionType;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileDescription() {
		return fileDescription;
	}

	public void setFileDescription(String fileDescription) {
		this.fileDescription = fileDescription;
	}

	public String getFileTypeCode() {
		return fileTypeCode;
	}

	public void setFileTypeCode(String fileTypeCode) {
		this.fileTypeCode = fileTypeCode;
	}

	public String getFileActionType() {
		return fileActionType;
	}

	public void setFileActionType(String fileActionType) {
		this.fileActionType = fileActionType;
	}

}
