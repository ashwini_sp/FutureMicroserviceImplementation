package com.dell.plm.agile.integration.agileasyncswservice.bean;

public class PartDetails {
	String partNumber;
	String partRevision;
	
	public String getPartNumber() {
		return partNumber;
	}
	
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	
	public String getPartRevision() {
		return partRevision;
	}
	
	public void setPartRevision(String partRevision) {
		this.partRevision = partRevision;
	}
	
}
