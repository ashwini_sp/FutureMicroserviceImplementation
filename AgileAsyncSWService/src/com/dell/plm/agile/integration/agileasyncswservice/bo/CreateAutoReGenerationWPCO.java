package com.dell.plm.agile.integration.agileasyncswservice.bo;


import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.ChangeConstants;
import com.agile.api.ExceptionConstants;
import com.agile.api.IAgileClass;
import com.agile.api.IAgileSession;
import com.agile.api.IAutoNumber;
import com.agile.api.IChange;
import com.agile.api.IItem;
import com.agile.api.IRow;
import com.agile.api.IStatus;
import com.agile.api.ITable;
import com.agile.api.IWorkflow;
import com.agile.api.ItemConstants;
import com.dell.plm.agile.aic.common.exception.ValidationException;
import com.dell.plm.agile.aic.common.util.AgileCommonUtil;
import com.dell.plm.agile.integration.agileasyncswservice.bean.AutoReGenBean;
import com.dell.plm.agile.integration.agileasyncswservice.dao.ServicePLMAppUserDAO;
import com.dell.plm.agile.integration.agileasyncswservice.util.Constants;



public class CreateAutoReGenerationWPCO {
	
	private static final Logger logger = Logger.getLogger(CreateAutoReGenerationWPCO.class);
			
	/**
	 * This method will check basic SWB validations on the Software Bundle Part.
	 * @param agilesession
	 * @param swb_Number
	 * @param applicationProperties
	 * @param agilepcProperties
	 * @return HashMap<IItem, String> - Contains IItem as Key and String as value.
	 * @throws APIException
	 */
	public HashMap<IItem, String> basicSWBValidations(IAgileSession agilesession, String swb_Number, Properties applicationProperties, Properties agilePCProperties) throws APIException 
	{
		StringBuffer sb = null;
		String swb_bundle_subclass_name = "";
		String dupExternalFileTypeCodes = "";
		HashMap<IItem, String> hMap = null;
		IItem item =  null;
		String ePromotedPlatforms = "";
		String str = "";
		String lifeCyclePhase ="";
		try {
			sb = new StringBuffer("");
			swb_bundle_subclass_name = agilePCProperties.getProperty("Parts_Subclass_SWB").trim();
			logger.debug("swb_bundle_subclass_name==>"+swb_bundle_subclass_name);
			
			dupExternalFileTypeCodes = applicationProperties.getProperty("DUP_AUTO_REGENERATION_EXTERNAL_FILE_TYPE_CODES");
			logger.debug("dupExternalFileTypeCodes==>"+dupExternalFileTypeCodes);
			
			lifeCyclePhase = applicationProperties.getProperty("SWB_A_REV_LIFE_CYCLE").trim();
			logger.debug("lifeCyclePhase==>"+lifeCyclePhase);
			
			hMap = new HashMap<IItem, String> ();
			item = loadSWBItem(agilesession, swb_Number, swb_bundle_subclass_name, agilePCProperties);
						
			if (item == null) {
				str = applicationProperties.getProperty("DUP_AutoGen_Err_Msg_5").replace("%0", swb_Number);
				logger.debug(str);
				sb.append(str+" \n");
			} else if (!(item.getValue("Title Block.Lifecycle Phase").toString().equalsIgnoreCase(lifeCyclePhase))) {
				str = applicationProperties.getProperty("DUP_AutoGen_Err_Msg_6").replace("%0", swb_Number);
				logger.debug(str);
				sb.append(str+" \n");
			} else {
				ePromotedPlatforms = item.getValue(Integer.valueOf(agilePCProperties.getProperty("Parts_SWB_PageThree_ePromoted_Platforms"))).toString().trim();
				
				if (ePromotedPlatforms == null || ePromotedPlatforms.length() == 0) {
					str = applicationProperties.getProperty("DUP_AutoGen_Err_Msg_7").replace("%0", swb_Number);
					logger.debug(str);
					sb.append(str+" \n");
				} else {
					boolean ftcExist = isFileTypeCodeExistInSWBundle(item, dupExternalFileTypeCodes);
					if (!ftcExist) {
						str = applicationProperties.getProperty("DUP_AutoGen_Err_Msg_8").replace("%0", swb_Number);
						logger.debug(str);
						sb.append(str+" \n");
					}
				}
			}
			hMap.put(item, sb.toString());
		} 
		catch(APIException ape){
			logger.debug("API Exception in basicSWBValidations Method ==>"+ape.getMessage());
			throw ape;
		}
		finally{
			ePromotedPlatforms = null;
			swb_bundle_subclass_name = null;
			str = null;
			lifeCyclePhase=null;
		}
		return hMap;
	}
	
	/**
	 * This method load part from Agile and validate whether it is Software Bundle or Not
	 * @param agilesession
	 * @param swb_Number
	 * @param swb_bundle_subclass_name
	 * @param agilePCProperties of Properties
	 * @return IItem
	 * @throws APIException
	 */
	public IItem loadSWBItem(IAgileSession agilesession, String swb_Number, String swb_bundle_subclass_name, Properties agilePCProperties) throws APIException
	{
		IItem item = null;
		String partClass = "";
		String partType = "";
		try{
			item = (IItem)agilesession.getObject(IItem.OBJECT_TYPE, swb_Number);
			logger.info("item loaded is: "+ item);
			if(item!=null){
				/** Checking whether loaded part type is Software bundle */
				partClass= item.getValue(Integer.valueOf(agilePCProperties.getProperty("Parts_Title_Block_Part_Class"))).toString().trim();
				logger.info("partClass:"+partClass);
				partType= item.getValue(Integer.valueOf(agilePCProperties.getProperty("Parts_Title_Block_Part_Type"))).toString().trim();
				logger.info("partType:"+partType);
				
				if (partClass.equalsIgnoreCase(swb_bundle_subclass_name) && partType.equalsIgnoreCase(swb_bundle_subclass_name)) {
					return item;
				}else{
					return null;
				}
			}else{
				return null;
			}
		}catch (APIException ae) {
			logger.error(AgileCommonUtil.exception2String(ae));
			return null;
		}
		finally{
			partClass = null;
			partType = null;
		}
	}
	
	
	/**
	 * This method will check at least one external file type code of attachment of Tab of SWB should present in dupExternalfileTypeCodes
	 * @param swbItem
	 * @param dupExternalFileTypeCodes
	 * @return boolean - true or false
	 * @throws APIException
	 */
	public boolean isFileTypeCodeExistInSWBundle(IItem swbItem, String dupExternalFileTypeCodes) throws APIException
	{
		ITable attTable = null;
		String fileTypeCode = "";
		boolean ftcExist = false;
		
		try {
			attTable = swbItem.getTable(ItemConstants.TABLE_ATTACHMENTS);
			if (!attTable.isEmpty()) {
				Iterator it = attTable.iterator();
				while (it.hasNext()) {
					IRow row = (IRow) it.next();
					fileTypeCode = row.getValue("Attachments.FileTypeCode").toString().trim();
					if (dupExternalFileTypeCodes.contains(fileTypeCode)) {
							ftcExist = true;
							break;
					}
				}
			}
		}
		catch (APIException ape){
			logger.debug("API Exception in isFileTypeCodeExistInSWBundle Method ==>"+ape.getMessage());
			throw ape;
		} finally {
			if(attTable!= null){
				attTable = null;
				fileTypeCode = null;
			}
			
		}
		return ftcExist;
	}
	
	
	/**
	 * This Method create the WPCO, move the work flow to Released from Pending status. 
	 * @param agileSession
	 * @param swbItem
	 * @param agilePCProperties
	 * @return AutoGenBean
	 * @throws Exception
	 */
	public AutoReGenBean processWPCOAutoReGeneration(IAgileSession agileSession, IItem swbItem, Properties applicationProperties,Properties agilePCProperties) throws Exception
	{
		boolean isWebPromotionWFSet = false;
		boolean isWPCOAutoReGenerationDone = false;
		boolean isValidationException = false;
		IChange wpcoChange = null;
		AutoReGenBean autoReGenBean = null;
		IWorkflow[] wfs = null;
		String dell_analyst = "";
		String reasonForChange = "";
		HashMap<Integer, String> params = null;
		ITable wpcoAffectedItemsTab = null;
	
		try {
			agileSession.disableAllWarnings();
			IAgileClass cls = agileSession.getAdminInstance().getAgileClass(agilePCProperties.getProperty("MfrOrders_Subclass_WebPromotion"));
			IAutoNumber auto[] = cls.getAutoNumberSources();
			String sAutoumber = null;
			if (auto != null && auto.length > 0)
				sAutoumber = auto[0].getNextNumber();
			else {
				logger.debug("Error while generating autonumber");
			}
			logger.debug("Creating WPCO - " + sAutoumber);
			wpcoChange = (IChange) agileSession.createObject(cls, sAutoumber);
			wpcoChange.refresh();

			wpcoAffectedItemsTab = wpcoChange.getTable(ChangeConstants.TABLE_AFFECTEDITEMS);
			wpcoAffectedItemsTab.createRow(swbItem);
			wpcoChange.refresh();
			
			wfs = wpcoChange.getWorkflows();
			if(wfs.length==0){
				throw new ValidationException(applicationProperties.getProperty("DUP_AutoGen_Err_Msg_9").replace("%0",wpcoChange.getName()));
			}
			
			dell_analyst =agilePCProperties.getProperty("DUP_AutoRegen_Dell_Analyst").trim();
			logger.debug("dell_analyst==>"+dell_analyst);
			
			reasonForChange =agilePCProperties.getProperty("DUP_Reason_For_Change").trim();
			logger.debug("reasonForChange==>"+reasonForChange);
			
			params = new HashMap <Integer, String>();
			params.put(ChangeConstants.ATT_COVER_PAGE_COMPONENT_ENGINEER, dell_analyst);
			params.put(Integer.valueOf(agilePCProperties.getProperty("MFRChangeOrders_WebPromotion_PageThree_WebPromotion")), agilePCProperties.getProperty("Webpromotion_PageThree_Auto_Repromotion").trim());
			params.put(ChangeConstants.ATT_COVER_PAGE_REASON_FOR_CHANGE, reasonForChange);
			wpcoChange.setValues(params);
						
			for (int i = 0; i < wfs.length; i++) {
				String wfName = wfs[i].getName();
				logger.debug("Workflow list: " + wfName);

				if (wfName.equals("Web Promotion WF")) {
					wpcoChange.setWorkflow(wfs[i]);	
					wpcoChange.refresh();
					while(true){
						try{
							IStatus targetState = getStatus(wpcoChange, "Released");
							wpcoChange.changeStatus(targetState, true,"", true, true, null, wpcoChange.getApprovers(targetState), wpcoChange.getObservers(targetState), false, "");
							logger.debug("WPCO status changed to Released");
						}
						catch (APIException ae){
							try {
								if(ae.getErrorCode().equals(ExceptionConstants.API_SEE_MULTIPLE_ROOT_CAUSES)){
									Throwable[] causes = ae.getRootCauses();
									for (int a = 0; a < causes.length; a++) {
										agileSession.disableWarning(
												(Integer)((APIException)causes[a]).getErrorCode()
										);
									}
								} else {
									agileSession.disableWarning((Integer)ae.getErrorCode());
								}
							} catch (Exception e) {
								throw ae;
							}
							continue;
						}
						break;
					}
					isWebPromotionWFSet = true;
					break;
				}
			}

			if(!isWebPromotionWFSet){
				isValidationException = true;
				throw new ValidationException(applicationProperties.getProperty("DUP_AutoGen_Err_Msg_10").replace("%0",wpcoChange.getName()));
			}
			isWPCOAutoReGenerationDone = true;
			autoReGenBean = new AutoReGenBean(); 
			autoReGenBean.setWpcoChange(wpcoChange);
			autoReGenBean.setAutoGenDone(isWPCOAutoReGenerationDone);
			
			if(isValidationException){
				wpcoChange.changeStatus(getStatus(wpcoChange, "Cancelled"), true,"", true, true, null, null, null, false, "");
				logger.debug("WPCO status changed to Cancelled");
				throw new ValidationException("Web Promotion workflow cannot be found for - "+wpcoChange.getName());
			}
		}	
		finally 
		{
			try {
				agileSession.enableAllWarnings();
			} catch (APIException e) {
				logger.debug("Exception while enabling warnings for session - "
						+ wpcoChange + " "
						+ AgileCommonUtil.exception2String(e));
			}
			
			if(params!=null && params.size() >0){
				params.clear();
				params = null;
			} else{
				params = null;
			}
			
			if(wpcoAffectedItemsTab!=null){
				wpcoAffectedItemsTab = null;
			}
		}
		return autoReGenBean;
	}
	
	/**
	 * This method gets the status of given change order
	 * @param dataObj change object
	 * @param workflowStatus status of workflow
	 * @return IStatus status of workflow
	 * @throws APIException API Exception
	 */
	private IStatus getStatus(IChange dataObj, String workflowStatus) throws APIException
	{
		logger.debug("Start of getStatus Method");
		IStatus status = null;
		IStatus[] statuses = null;
		try{
			statuses = dataObj.getWorkflow().getStates();
			for (int i = 0; i < statuses.length; i++) {
				if (statuses[i].getName().equals(workflowStatus)) {
					status = statuses[i];
					break;
				}
			}
			logger.debug("Status "+dataObj.getName()+" to be moved:" + status);
		} catch (APIException ape){
			logger.debug("The APIException in getStatus Method ==>"+ape.getMessage());
			throw ape;
		} finally {
			if(statuses != null){
				statuses = null;
			}
		}
		logger.debug("End of getStatus method");
		return status;
	}

	/**
	 * This Method will insert action 7 and 10 records in ServiceQueue table of Agile Services Schema from Service PLM App Schema
	 * @param downstreamName
	 * @param swbItem
	 * @param wpcoNumber
	 * @param applicationProperties - Application Properties Object
	 * @throws Exception
	 */
	public void insertRequestforSWBPromotion(String downstreamName, String swbItem, String wpcoNumber,Properties applicationProperties) throws Exception 
	{
		logger.debug("Start of insertRequestForSWBAutoRegeneration Method");
		CallableStatement insertCallableStmt = null;
		Connection connection = null;
		ServicePLMAppUserDAO dao = null;
		String servername = ""; 
		String procName = "";
		String createByUser = "";
		String requesterUserName = "";
			
		try {
				servername = InetAddress.getLocalHost().getHostName();
								
				logger.debug("Start of getting ServicePLMApp DB Connection");
				dao = new ServicePLMAppUserDAO();
				
				if(connection == null)
					connection = dao.getServicePlmUserDBConnection();
				logger.debug("End of getting ServicePLMApp DB Connection");	
																
				procName = applicationProperties.getProperty(Constants.DUP_SP_RT_WPCO_TRK_PROC).trim();
				logger.debug("procName ==>"+procName);
								
				createByUser = applicationProperties.getProperty(Constants.DUP_CREATE_BY_USER).trim();
				logger.debug("createByUser ==>"+createByUser);
				
				requesterUserName = applicationProperties.getProperty(Constants.DUP_REQUESTOR_USER_NAME).trim();
				logger.debug("requesterUserName ==>"+requesterUserName);
				
				insertCallableStmt = connection.prepareCall("{call "+procName+"(?,?,?,?,?,?)}");
				
				/** Setting WPCO Number*/
				insertCallableStmt.setString(1, wpcoNumber);
							
				/** Setting SWB Files copy to FTP(Always Y) */
				insertCallableStmt.setString(2, "Y");
							
				/** Setting Created By User */
				insertCallableStmt.setString(3, createByUser);
							
				/** Setting Down Stream Name */
				insertCallableStmt.setString(4, downstreamName);
							
				/** Setting Downstream User Name - Requester User Name */
				insertCallableStmt.setString(5, requesterUserName); 
							
				/** Setting Server Name */
				insertCallableStmt.setString(6, servername);
				
				logger.debug("Start Executing Sp_Rt_wpco_Trk Stored Procedure");		
				insertCallableStmt.execute();
				logger.debug("End Executing Stored Procedure");

			}catch(Exception ex){
				String error = "Exception at insertRequestForSWBAutoRegeneration Method:"+AgileCommonUtil.exception2String(ex);
				logger.error(error);
				throw new Exception(error);
			}
			finally {
				try{
					if (insertCallableStmt != null) {
						insertCallableStmt.close();
						insertCallableStmt = null;
					}
					if(connection != null){
						logger.debug("closing the connection");
						connection.close();
						logger.debug("nullifying the connection");
						connection = null;
					}
					
					if(dao!=null){
						dao = null;
					}
					
					servername = null; 
					procName = null;
					createByUser = null;
					requesterUserName = null;
					
				} catch (SQLException sqlException) {
					logger.debug("Exception Occured insertRequestforSWBPromotion Method:"
							+ AgileCommonUtil.exception2String(sqlException));
				}
			}
			logger.debug("End of insertRequestForSWBAutoRegeneration Method");
	}
}
