package com.dell.plm.agile.integration.agileasyncswservice.bo;

import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.ChangeConstants;
import com.agile.api.IAdmin;
import com.agile.api.IAgileClass;
import com.agile.api.IAgileSession;
import com.agile.api.IAutoNumber;
import com.agile.api.IChange;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class CreateDSCR 
{
	
	private static HashMap<Integer, String> params = null;
	//Resource bundle
	
	private static Properties agilepcProperties = null;
	
	//Subclasses names
	private static String changeReq_SuclassName_DSCR = null;
			
	//Page Three attributes of DS Change Request
	private static int dscr_page_three_downstream_user_name = 0;
	private static int dscr_page_three_downstream_name = 0;
	private static int dscr_page_three_unique_iD = 0;
	
	private static final Logger logger = Logger.getLogger(CreateDSCR.class);
	
	
	/**
	 * @param session
	 * @return
	 */
	public String getDSCRNumber(IAgileSession session, String messageID, String downStreamName, String downStreamUserName) throws Exception
	{
		String dscr_change_num = "";
		try{			
			//agilepcProperties = FilePropertyUtility.loadProperties(agilePCReferencePropsFilePath);
			FilePropertyUtility fu = new FilePropertyUtility();
			logger.debug("loading properties..");
			String configBase = FilePropertyUtility.getConfigBasePath("asynchwebservice");
			agilepcProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\Agile_PC_References.properties");
			//agilepcProperties = fu.loadWebAppProperties("/Agile_PC_References.properties");
						
			//Calling method for creating Change Request object
			changeReq_SuclassName_DSCR = String.valueOf(agilepcProperties.getProperty("ChangeRequests_Subclass_DSCR"));
			dscr_change_num = createChangeReq(session, changeReq_SuclassName_DSCR, messageID, downStreamName, downStreamUserName);
			
		}/*catch(Exception e){
			logger.error(e.getMessage());
			//System.out.println(e.getMessage());
		}*/
		finally {
			params = null;
			
		}
		return dscr_change_num;
	}
	
	/**
     * Creates IChange object 
     * 
     * @param agileSession The agile session
     * @param ChangeReq_suclass_name The change subclass name
     * @return IChange object 
     * 
     * @throws APIException
     */
	public static String createChangeReq(IAgileSession agileSession, String ChangeReq_suclass_name, String messageID, String downStreamName, String downStreamUserName) throws APIException {
		
		IAutoNumber[] numSources;
		String nextAvailableAutoNumber = null;
		IChange dscr_change = null;
		
		//Getting next autonumber for DSCR subclass
		IAdmin admin = agileSession.getAdminInstance();
		//Changed to Integers ...
		IAgileClass cls = admin.getAgileClass(ChangeReq_suclass_name);
		numSources = cls.getAutoNumberSources();
		if (numSources!=null) {
			nextAvailableAutoNumber = numSources[0].getNextNumber(cls);
			//Creating DSCR object
			//loading base Ids
			dscr_page_three_downstream_user_name = Integer.valueOf(agilepcProperties.getProperty("ChangeRequests_DSCR_PageThree_Downstream_User_Name"));
			dscr_page_three_downstream_name = Integer.valueOf(agilepcProperties.getProperty("ChangeRequests_DSCR_PageThree_Downstream_Name"));
			dscr_page_three_unique_iD = Integer.valueOf(agilepcProperties.getProperty("ChangeRequests_DSCR_PageThree_Unique_ID"));
						
			//Putting attribute values into Hashmap
			params = new HashMap <Integer, String>();
			params.put(ChangeConstants.ATT_COVER_PAGE_NUMBER, nextAvailableAutoNumber);
			params.put(dscr_page_three_downstream_name,downStreamName);
			params.put(dscr_page_three_downstream_user_name,downStreamUserName);
			params.put(dscr_page_three_unique_iD,messageID);
			dscr_change = (IChange)agileSession.createObject(cls, params);
			//System.out.println("DSCR Change Request Created =="+dscr_change);			
		}
		return nextAvailableAutoNumber;				
	}//end of createChangeReqmethod	
	
	/**
	 * @throws Exception
	 *//*
	private void loadProperties(String fileName) throws Exception 
	{
		if(agilepcProperties == null){
			agilepcProperties = new Properties();
		} else{
			agilepcProperties.clear();
		}
		//String fileName="/Agile_PC_References.properties";
        InputStream inputStream = getClass().getResourceAsStream(fileName);
        System.out.println("InputStream is: " + inputStream);
        logger.debug("InputStream is: " + inputStream);
        try {
        	agilepcProperties.load(inputStream);
        } catch (IOException ex) {
            System.out.println("Cannot read logging properties"+ ex);
        }
	}*/
}
