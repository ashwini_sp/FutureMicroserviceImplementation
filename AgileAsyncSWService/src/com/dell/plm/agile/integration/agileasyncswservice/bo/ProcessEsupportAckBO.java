package com.dell.plm.agile.integration.agileasyncswservice.bo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.log4j.Logger;
import com.dell.plm.agile.aic.common.util.AgileCommonUtil;
import com.dell.plm.agile.integration.agileasyncswservice.dao.AddServiceQueueDAO;
import com.dell.plm.agile.integration.agileasyncswservice.model.AckUpdateResponse;

public class ProcessEsupportAckBO 
{
	private static Logger logger = Logger.getLogger(ProcessEsupportAckBO.class);
	StringBuffer failureReason = new StringBuffer("");

	/**
	 * This method returns response after inserting record into Service Queue
	 * @param changeNumber
	 * @param storeId
	 * @param response
	 * @return AckUpdateResponse
	 * @throws Exception
	 */
	public AckUpdateResponse doAckProcessing(String changeNumber, String storeId, String downStreamName, AckUpdateResponse response, Properties applicationProperties) throws Exception 
	{
		logger.debug("Inside doAckProcessing method---");
		synchronized(this)
		{
			Connection wsDbConnection = null;
			AddServiceQueueDAO addSQ = new AddServiceQueueDAO();
			
			try
			{
				try
				{
					wsDbConnection =  addSQ.getDBConnection();
					logger.info("Got connection: " +wsDbConnection);
				}
				catch(FileNotFoundException fx)
				{
					logger.error("FileNotFoundException while getting the connection using properties file :"+AgileCommonUtil.exception2String(fx));
					failureReason.append("\n" + "FileNotFoundException: " +fx);
				}
				catch(IOException ix)
				{
					logger.error("IOException while getting the connection using properties file:"+AgileCommonUtil.exception2String(ix));
					failureReason.append("\n" + "IOException: " +ix);
				}
				catch(SQLException qx)
				{
					logger.error("SQLException while getting the connection using properties file:"+AgileCommonUtil.exception2String(qx));
					failureReason.append("\n" + "SQLException: " +qx);
				}

				logger.debug("Calling insertRecordIntoServiceQueue method---");
				insertRecordIntoServiceQueue(changeNumber, applicationProperties.getProperty("ESUPPORT_ACK_ACTION_NUMBER"), storeId, downStreamName, applicationProperties.getProperty("ESUPPORT_BATCH_PROCESS"), wsDbConnection);

				if(failureReason.length()> 0)
				{
					response.setStatus("Failure");
					logger.info("Setting status as 'Failure' in AckUpdateResponse");
					response.setObjectId(changeNumber);
					response.setFailureReason(failureReason.toString());
					logger.info("Failure reason: " +failureReason.toString());
				}
				else
				{
					response.setStatus("Success");
					logger.info("Setting status as 'Success' in AckUpdateResponse");
					response.setObjectId(changeNumber);
					response.setFailureReason("");
				}
				logger.info(response.toString());
			}
			catch(Exception e)
			{
				logger.error("Exception:"+AgileCommonUtil.exception2String(e));
				logger.error("Processing Ack failed as: "+e.getMessage());
				response.setStatus("Failure");
				response.setObjectId(changeNumber);
				response.setFailureReason(e.getMessage());
			}
			finally
			{
				if(wsDbConnection != null)
				{
					try 
					{
						wsDbConnection.close();
						logger.debug("DB connection closed");
					} 
					catch (SQLException e) 
					{
						logger.error("Exception while closing connection :"+AgileCommonUtil.exception2String(e));
					}
				}
			}
		}
		logger.info("---End of doAckProcessing method");	
		return response;
	}

	/**
	 * This method inserts eSupport Ack record into ServiceQueue table
	 * @param changeNumber
	 * @param actionType
	 * @param storeId
	 * @param connection
	 * @throws Exception
	 */
	private void insertRecordIntoServiceQueue(String changeNumber, String actionType, String storeId, String downStreamName, String batchProcess, Connection connection) throws Exception 
	{
		logger.debug("Inside insertRecordIntoServiceQueue method---");
		CallableStatement cs = null;
		int dependentqueueid = 0;
		AddServiceQueueDAO addSQ = new AddServiceQueueDAO();

		try
		{
			if(connection == null || connection.isClosed())
			{
				logger.info("Connection is null and closed! Getting connection again---");
				connection = addSQ.getDBConnection();
			}

			if(connection != null)
			{
				logger.debug("Got connection: " + connection);
				cs = addSQ.addServiceQueue(connection, actionType, dependentqueueid, storeId, downStreamName, "", "", "", "", "", "", changeNumber, "", "", batchProcess);
				cs.executeUpdate();
				logger.info("Record for Change: " +changeNumber +" has been inserted into Servicequeue table with Action: " +actionType);
			}
		}
		catch(Exception e)
		{
			logger.error("Exception : "+AgileCommonUtil.exception2String(e));
			failureReason.append("\n" + "Exception: " +e);
			throw e;
		}
		finally
		{
			if(cs != null)
			{
				cs.close();
			}
		}
		logger.info("---End of insertRecordIntoServiceQueue method");	
	}
}

