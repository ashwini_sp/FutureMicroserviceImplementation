package com.dell.plm.agile.integration.agileasyncswservice.bo;

import java.io.File;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.agile.api.APIException;
import com.agile.api.IAgileClass;
import com.agile.api.IAgileSession;
import com.agile.api.IAttachmentRow;
import com.agile.api.IAttribute;
import com.agile.api.IItem;
import com.agile.api.IRow;
import com.agile.api.ITable;
import com.agile.api.ITwoWayIterator;
import com.agile.api.ItemConstants;
import com.dell.plm.agile.aic.common.util.AgileCommonUtil;
import com.dell.plm.agile.integration.agileasyncswservice.bean.FileDetailsRequest;
import com.dell.plm.agile.integration.agileasyncswservice.bean.FileDtls;
import com.dell.plm.agile.integration.agileasyncswservice.dao.AddServiceQueueDAO;
import com.dell.plm.agile.integration.agileasyncswservice.util.Constants;
import com.dell.plm.agile.integration.agileasyncswservice.util.Utility;


public class RequestDUPFileActionValidations {
	
	private static final Logger logger = Logger.getLogger(RequestDUPFileActionValidations.class);
	String fileExtMain = ""; 
		
	/**
	 * This method do all the file data validation based on the scenarios.
	 * @param agileSession - IAgileSession Object
	 * @param messageID - Unique Message ID
	 * @param downstreamName - Downstream Name
	 * @param swb_Number  - Software Bundle Number
	 * @param swb_Rev - Software Bundle Revision
	 * @param fileDetailsRequest - FileDetailsRequest Object
	 * @param autoRegeneration - value is Y or N
	 * @param applicationProperties - Properties Object
	 * @param agilePCProperties - Properties Object
	 * @return String
	 * @throws Exception
	 */
	public String fileDataValidations(IAgileSession agileSession, String messageID, String downstreamName, String swb_Number, String swb_Rev,
			FileDetailsRequest fileDetailsRequest, String autoRegeneration,Properties applicationProperties, Properties agilePCProperties) throws Exception {
		
		logger.debug("Starting of fileDataValidations Method");
		HashMap<IItem, String> swbValidation = null;
		Set<IItem> set = null;
		Iterator<IItem> it = null;
		IItem item = null;
		String swbVal = "";
		String partRevisionVal = "";
				
		FileDtls fileDtls[] = null;
		String fileName = "";
		String fileDescription = "";
		String fileTypeCode = "";
		String fileActionType = "";
		
		String loc = "";
		String folderName = "";
		String folderPath = "";
		File folderdir = null;
		String lifecycle = "";
		String lifeCyclePhase = "";
		
		String fileExistVal = "";
		String fileActionVal = "";
		
		String autoReGenValidation= "";
		boolean autoRegenerationFlag = false;
		HashMap<String, String> fileTypeCodeMap = null;
		ITable attTable = null;
		String downstreamNameForFolder = "";
		
		try {
			/** Start - Software Bundle Validation */
			swbValidation = swbValidation(agileSession, swb_Number, applicationProperties, agilePCProperties);
			set = swbValidation.keySet();
			it = set.iterator();
			if( it.hasNext()){
				item = it.next();
				swbVal = swbValidation.get(item);
			}
			
			if(swbVal.length()> 0){
				return swbVal;
			} 
			/** End - Software Bundle Validation */
			
			
			/** Software Bundle Part Revision Validation */
			partRevisionVal = validatePartRevision(item, swb_Rev, applicationProperties);
			if(!partRevisionVal.equalsIgnoreCase("Success")){
					return partRevisionVal;
			} else {
				fileDtls = fileDetailsRequest.getFileDtls();
				logger.debug("File Details Request count is:" + fileDtls.length);
					
				loc = applicationProperties.getProperty("DUP_UNCSHARE_FILEPATH").trim();
				logger.debug("loc: "+loc);
		
				downstreamNameForFolder = downstreamName;
				logger.debug("downstreamNameForFolder: "+downstreamNameForFolder);
				
				folderName = applicationProperties.getProperty(downstreamNameForFolder.toUpperCase()+Constants.FOLDER_NAME);
				if(folderName==null)
					folderName = "";
				logger.debug("folderName: "+folderName);
				
				if (folderName != null && folderName.length() > 0) {
					logger.debug("folderName: "+folderName);
									
					folderPath =  loc + File.separator + folderName;
					logger.debug("folderPath: "+folderPath);
				
					/** Start - Downstream folder Validation */
					folderdir = new File(folderPath);
					logger.debug("folder exists : "+folderdir.exists());
					if(!folderdir.exists()){
						String errorMsg = applicationProperties.getProperty("DUP_File_Error_Msg_28").replace("%0", folderPath);
						return errorMsg;
					}
					/** End - Downstream folder Validation */
				} else{
					String errorMsg = applicationProperties.getProperty("DUP_File_Error_Msg_43").replace("%0", downstreamName);
					return errorMsg;
				}
				
				lifecycle = item.getCell(ItemConstants.ATT_TITLE_BLOCK_LIFECYCLE_PHASE).toString();
				logger.debug("lifecycle: "+lifecycle);
				
				lifeCyclePhase = applicationProperties.getProperty("SWB_A_REV_LIFE_CYCLE").trim();
				logger.debug("lifeCyclePhase==>"+lifeCyclePhase);
				
				/** A Revision Validation - SWB life cycle is A-Revision and fileActionType type should be Add or Replace (not Delete)*/
				if(lifecycle.equalsIgnoreCase(lifeCyclePhase)){
					String errMsg = validateARevDelete(fileDtls, applicationProperties);
					if(!errMsg.equalsIgnoreCase("Success")){
						return errMsg;
					}
				}
				
				/** AutoRegeneration Validation - SWB life cycle is A-Revision and fileActionType type should be Replace (not Add or Delete)*/
				if(autoRegeneration.equalsIgnoreCase("Y")){
					   autoReGenValidation = processAutoRegenerationWPCO(fileDetailsRequest, autoRegeneration, item, applicationProperties, agilePCProperties);
					if (autoReGenValidation.length() > 0)
						    return autoReGenValidation;
					   else{
						   autoRegenerationFlag = true;
					   }
				}
				
				/** Start - File Validation Code */
				if (fileDtls.length > 0) {
					fileTypeCodeMap = getAicAgileFileTypeCodeTableData(applicationProperties);
					item.setRevision(swb_Rev);
					attTable = item.getAttachments();
					
					for (FileDtls fDtls : fileDtls) {
						fileName = fDtls.getFileName().trim();
						fileDescription = fDtls.getFileDescription().trim();
						fileTypeCode = fDtls.getFileTypeCode().trim();
						fileActionType = fDtls.getFileActionType().trim();
						logger.debug(fileName + "\t" + fileDescription + "\t"+ fileTypeCode + "\t" + fileActionType);
							
						fileActionVal = validateFileAction(item, swb_Rev, fileActionType, fileName, fileTypeCode, lifecycle, applicationProperties, agilePCProperties, fileTypeCodeMap, attTable);
						if(!fileActionVal.equalsIgnoreCase("Success")){
							return fileActionVal;
						} 
						
						fileExistVal = validateFileExist(folderPath, fileName, fileActionType, applicationProperties);
						if(!fileExistVal.equalsIgnoreCase("Success")){
							return fileExistVal;
						}
					}
				}
				/** End - File Validation Code */
				String result = insertRecordsForFileUploadAutoRegeneration(messageID, downstreamName.toUpperCase(), item.getName().toUpperCase(), swb_Rev.toUpperCase(),fileDtls, autoRegeneration, applicationProperties, autoRegenerationFlag);
				if(!result.equalsIgnoreCase("Success")){
					return result;
				}
			}
			logger.debug("End of fileDataValidations Method");
			return "";
		}catch (Exception e){
			logger.debug("Exception in allInputDataValidations Method:"+e.getMessage());
			throw e;
		} finally {
			if(set!=null && set.size()>0)
				set = null;
			else
				set = null;
			if(it!=null)
				it = null;
			
			if(fileDtls!=null && fileDtls.length >0)
					fileDtls = null;
			else
				fileDtls = null;
			
			fileName = null;
			fileDescription = null;
			fileTypeCode = null;
			fileActionType = null;
			
			loc = null;
			folderName = null;
			folderPath = null;
			if(folderdir!=null)
				folderdir = null;
			
			lifecycle = null;
			lifeCyclePhase = null;
			
			if(fileTypeCodeMap!=null && fileTypeCodeMap.size()>0){	
				fileTypeCodeMap.clear();
				fileTypeCodeMap = null;
				
			} else{
				fileTypeCodeMap = null;
			}
			
			if(attTable!=null)
				attTable = null;
			
			downstreamNameForFolder = null;
		}
	}
		
	/**
	 * This method will do the basic validations of the input request
	 * @param messageID - Unique message id
	 * @param downstreamName - Downstream name should be OPM or CPG
	 * @param swb_Number - Software Bundle Number
	 * @param swb_Rev - Software Bundle Revision should be X-Revision or A-Revision
	 * @param fileDetailsRequest - FileDetailsRequest Object
	 * @param autoRegeneration - value is Y/N
	 * @param applicationProperties - Application Properties Object
	 * @param agilePCProperties - Agile PC Properties Object
	 * @return String
	 * @throws Exception
	 */
	public String basicValidations(String messageID, String downstreamName, String swb_Number, String swb_Rev,
			FileDetailsRequest fileDetailsRequest, String autoRegeneration,Properties applicationProperties,Properties agilePCProperties) throws Exception {
		
		logger.debug("Starting basicValidations Method");
		String swbPartRevs = "";
		String fileTypeCodes = "";
		String fileActionTypes = "";
		String autoRegenAllowedValues = "";
		Integer messageIdMaxLength;
		String dupDownStreamNames = "";
		Utility util = null;
		FileDtls fileDtls[] = null;
		Integer fileDtlsMaxLength;
		StringBuffer sb = new StringBuffer("");
		
		try	
		{
			swbPartRevs = applicationProperties.getProperty("DUP_ALLOWABLE_REVISIONS").trim();
			logger.debug("swbPartRevs:" + swbPartRevs);
			
			fileTypeCodes = applicationProperties.getProperty("DUP_ALLOWED_FILETYPE_CODES").trim();
			logger.debug("fileTypeCodes:" + fileTypeCodes);
			
			fileActionTypes = applicationProperties.getProperty("DUP_FILE_ACTION_TYPE_ALLOWED_VALUES").trim();
			logger.debug("fileActionTypes:" + fileActionTypes);
			
			autoRegenAllowedValues = applicationProperties.getProperty("DUP_AUTO_REGENERATION_ALLOWED_VALUES").trim();
			logger.debug("autoRegenAllowedValues:" + autoRegenAllowedValues);
			
			messageIdMaxLength = Integer.parseInt(applicationProperties.getProperty("MESSAGE_ID_MAX_LENGTH").trim());
			logger.debug("messageIdMaxLength:" + messageIdMaxLength);
			
			fileDtlsMaxLength = Integer.parseInt(applicationProperties.getProperty("FILE_DETAILS_MAX_LENGTH_ALLOWED").trim());
			logger.debug("fileDtlsMaxLength:" + fileDtlsMaxLength);
			
						
			if (StringUtils.isEmpty(messageID.trim())) {
				sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_1").trim()+" \n");
			} 
			else if(messageID.trim().length()> messageIdMaxLength){
				sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_2").trim().replace("%0", String.valueOf(messageIdMaxLength))+" \n");
			}
			
			if (StringUtils.isEmpty(downstreamName.trim())) {
				sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_3").trim()+" \n");
			}
			else {
				dupDownStreamNames = applicationProperties.getProperty("DUP_FILE_DOWNSTREAM_NAMES").trim();
				logger.debug("dupDownStreamNames:" + dupDownStreamNames);
				
				/** Code to handle to check valid downstream name */
				util = new Utility();
				if (util.isValidDownStream(dupDownStreamNames, downstreamName)) {
					logger.debug("downstreamName is valid:" + downstreamName);
				} 
				else {
					sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_4").trim()+" \n");
				}
			}
			
			if (StringUtils.isEmpty(swb_Number.trim())) {
				sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_5")+" \n");
			}
			
			if (StringUtils.isEmpty(swb_Rev.trim())) {
				sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_6").trim()+" \n");
			} 
			else if(!isValidRevision(swbPartRevs, swb_Rev.trim())){
				sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_7").trim()+" \n");
			}
		
			fileDtls = fileDetailsRequest.getFileDtls();
			logger.debug("File Details Request provided count is:" + fileDtls.length);
					
			if(fileDtls.length > 0 && fileDtls.length <=fileDtlsMaxLength){
				for(FileDtls fDtls:fileDtls){
			
					if (StringUtils.isEmpty(fDtls.getFileName().trim())) {
						sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_8")+" \n");
					}
					
					if (StringUtils.isEmpty(fDtls.getFileDescription().trim())) {
						sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_9")+" \n");
					}
					
					
					if (StringUtils.isEmpty(fDtls.getFileTypeCode().trim())) {
						sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_10")+" \n");
					}
					else if(isValidFileTypeCode(fileTypeCodes, fDtls.getFileTypeCode().trim())){
						logger.debug("fileTypeCode is Valid for:" +fDtls.getFileName());
					}else {
						sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_11").replace("%0", fDtls.getFileTypeCode().trim())+" \n");
					}
					
					
					if (StringUtils.isEmpty(fDtls.getFileActionType().trim())) {
						sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_12").trim()+" \n");
					} 
					else if(!isValidActionType(fileActionTypes, fDtls.getFileActionType().trim())){
						sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_13").trim()+" \n");
					}
				}
			} 
			else {
				if(fileDtls.length==0)
					sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_14").trim()+" \n");
				else
					sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_22").trim()+" \n");
			}
		
			/* Auto Regeneration basic validation */ 
			if (StringUtils.isEmpty(autoRegeneration.trim())) {
				sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_15").trim()+" \n");
			} 
			else if(!isValidAutoRegenerationValue(autoRegenAllowedValues, autoRegeneration.trim())){
				sb.append(applicationProperties.getProperty("DUP_File_Error_Msg_16").trim()+" \n");
			}
		}
		catch(Exception e) {
			logger.error("Validation Exception :" + e.getMessage());
			throw e;
		}
		finally	{
			swbPartRevs = null;
			fileTypeCodes = null;
			fileActionTypes = null;
			autoRegenAllowedValues = null;
			dupDownStreamNames = null;
			util = null;
			if (fileDtls != null && fileDtls.length > 0) {
				fileDtls = null;
			} else {
				fileDtls = null;
			}
		}
		logger.debug("End of basicValidations Method");
		return sb.toString();
	}
	
	/**
	 * This method will check given revision is allowed revision file action
	 * @param swbPartRevs - Allowed revisions from the property files
	 * @param swb_Rev - Given SWB Revision
	 * @return boolean - true/false
	 * @throws Exception
	 */
	 public boolean isValidRevision(String swbPartRevs, String swb_Rev) throws Exception	{
		logger.debug("Starting of isValidRevision Method -"+swb_Rev);
		boolean isFound = false;
		String swbPartRevsValid[] = null;
		try 
		{
			if(swbPartRevs!=null){
				swbPartRevsValid= swbPartRevs.split(",");
				for(String containdVal:swbPartRevsValid){
					if(StringUtils.startsWithIgnoreCase(swb_Rev.trim(),containdVal.trim())){
						isFound = true;
						logger.debug("Given Value Found is ==>"+containdVal.trim());
					}
				}
			}
		}catch(Exception e){
			logger.error("Exception in isValidRevision Method:"+e.getMessage());
			throw e;
		} finally {
			swbPartRevsValid = null;
		}
		logger.debug("End of isValidRevision Method");
		return isFound;
	}
			
	/**
	 * This method will check fileTypeCode given is present in properties fileTypeCodes.
	 * @param fileTypeCodes
	 * @param fileTypeCode
	 * @return boolean - true is existing/false is not existing
	 * @throws Exception
	 */
	public boolean isValidFileTypeCode(String fileTypeCodes, String fileTypeCode) throws Exception
	{
		logger.debug("Starting of isValidFileTypeCode Method -"+fileTypeCode);
		boolean isFounnd = false;
		String ftypeCodes[] = null;
		try {
			if(fileTypeCodes!=null) {
				ftypeCodes= fileTypeCodes.split(";");
				for(String ftypeCode:ftypeCodes){
					if(ftypeCode.trim().equals(fileTypeCode.trim())) {
						isFounnd = true;
						logger.debug("File Type Code Found is ==>"+ftypeCode);
						break;
					}
				}
			}
		}
		catch(Exception e){
			logger.error("Exception in isValidFileTypeCode Method:"+e.getMessage());
			throw e;
		}
		finally {
			ftypeCodes = null;
		}
		logger.debug("End of isValidFileTypeCode Method");
		return isFounnd;
	}
		
	/**
	 * This method will check given file action type present in properties valid action types.
	 * @param validActionTypes
	 * @param actionType - file action type
	 * @return boolean - true/false
	 * @throws Exception
	 */
	public boolean isValidActionType(String validActionTypes, String actionType) throws Exception
	{
		logger.debug("Starting of isValidActionType Method -"+actionType);
		boolean isFound = false;
		String validActionTypeValues[] = null;
		try 
		{
			if(validActionTypes!=null){
				validActionTypeValues= validActionTypes.split(",");
				for(String actionVal:validActionTypeValues){
					if(actionVal.trim().equalsIgnoreCase(actionType))	{
						isFound = true;
						logger.debug("Given Value Found is ==>"+actionType);
						break;
					}
				}
			}
		}catch(Exception e){
			logger.error("Exception in isValidActionType Method: "+e.getMessage());
			throw e;
		} finally {
			validActionTypeValues = null;
		}
		logger.debug("End of isValidActionType Method -"+isFound);
		return isFound;
	}
	
	/**
	 * This method will check given auto regeneration value is valid values in the property files.
	 * @param autoRegenerationValues - Values from the property files
	 * @param autoRegenerationValue - Input provided by the user
	 * @return boolean - true/false
	 * @throws Exception
	 */
	public boolean isValidAutoRegenerationValue(String autoRegenerationValues, String autoRegenerationValue) throws Exception
	{
		logger.debug("Starting of isValidAutoRegenerationValue Method -"+autoRegenerationValue);
		boolean isFound = false;
		String validAutoRegenerationValues[] = null;
		try 
		{
			if(autoRegenerationValues!=null){
				validAutoRegenerationValues= autoRegenerationValues.split(",");
				for(String autoRegenVal:validAutoRegenerationValues){
					if(autoRegenVal.trim().equalsIgnoreCase(autoRegenerationValue))	{
						isFound = true;
						logger.debug("Given Value Found is ==>"+autoRegenerationValue);
						break;
					}
				}
			}
		}catch(Exception e){
			logger.error("Exception in isValidAutoRegenerationValue Method: "+e.getMessage());
			throw e;
		} finally {
			validAutoRegenerationValues = null;
		}
		logger.debug("End of isValidAutoRegenerationValue Method -"+isFound);
		return isFound;
	}
	
	/**
	 * This method will check SWB validations on the Software Bundle Part.
	 * @param agilesession
	 * @param swb_Number
	 * @param applicationProperties
	 * @param agilePCProperties
	 * @return HashMap<IItem, String> - Contains IItem as Key and String as value.
	 * @throws APIException
	 */
	public HashMap<IItem, String> swbValidation(IAgileSession agilesession, String swb_Number, Properties applicationProperties, Properties agilePCProperties) throws APIException 
	{
		logger.debug("Starting of swbValidation Method");
		StringBuffer sb = null;
		String swb_bundle_subclass_name = "";
		HashMap<IItem, String> hMap = null;
		IItem item =  null;
		String str = "";
		
		try {
			sb = new StringBuffer("");
			swb_bundle_subclass_name = agilePCProperties.getProperty("Parts_Subclass_SWB").trim();
			logger.debug("swb_bundle_subclass_name==>"+swb_bundle_subclass_name);
			
			hMap = new HashMap<IItem, String> ();
			item = loadSWBItem(agilesession, swb_Number, swb_bundle_subclass_name, agilePCProperties);
						
			if (item == null) {
				str = applicationProperties.getProperty("DUP_File_Error_Msg_17").replace("%0", swb_Number);
				logger.debug(str);
				sb.append(str+" \n");
			} 
			hMap.put(item, sb.toString());
		} 
		catch(APIException ape){
			logger.debug("API Exception in swbValidation Method ==>"+ape.getMessage());
			throw ape;
		}
		finally{
			swb_bundle_subclass_name = null;
			str = null;
		}
		logger.debug("End of swbValidation Method");
		return hMap;
	}
		
	/**
	 * This method load part from Agile and validate whether it is Software Bundle or Not
	 * @param agilesession
	 * @param swb_Number
	 * @param swb_bundle_subclass_name
	 * @param agilePCProperties of Properties
	 * @return IItem
	 * @throws APIException
	 */
	public IItem loadSWBItem(IAgileSession agilesession, String swb_Number, String swb_bundle_subclass_name, Properties agilePCProperties) throws APIException
	{
		logger.debug("Starting of loadSWBItem Method");
		IItem item = null;
		String partClass = "";
		String partType = "";
		try{
			item = (IItem)agilesession.getObject(IItem.OBJECT_TYPE, swb_Number);
			logger.info("item loaded is: "+ item);
			if(item!=null){
				/** Checking whether loaded part type is Software bundle */
				partClass= item.getValue(Integer.valueOf(agilePCProperties.getProperty("Parts_Title_Block_Part_Class"))).toString().trim();
				logger.info("partClass:"+partClass);
				
				partType= item.getValue(Integer.valueOf(agilePCProperties.getProperty("Parts_Title_Block_Part_Type"))).toString().trim();
				logger.info("partType:"+partType);
				
				if (partClass.equalsIgnoreCase(swb_bundle_subclass_name) && partType.equalsIgnoreCase(swb_bundle_subclass_name)) {
					return item;
				}else{
					return null;
				}
			}else{
				return null;
			}
		}catch (APIException ae) {
			logger.error(AgileCommonUtil.exception2String(ae));
			return null;
		}
		finally{
			partClass = null;
			partType = null;
		}
	}
	
	/**
	 * This method will validate the given inputs will be eligible for Auto Regeneration
	 * @param fileDetailsRequest - FileDetailsRequest Object
	 * @param autoRegeneration - value is Y/N
	 * @param swbItem - IItem Object
	 * @param applicationProperties - Properties Object for Application
	 * @param agilePCProperties - Properties Object for PC
	 * @return String - Contains validations
	 * @throws Exception
	 */
	public String processAutoRegenerationWPCO(FileDetailsRequest fileDetailsRequest, String autoRegeneration, IItem swbItem, Properties applicationProperties, Properties agilePCProperties) throws Exception 
	{
		logger.debug("Starting of processAutoRegenerationWPCO Method");
		StringBuffer sb = new StringBuffer("");
		FileDtls fileDtls[] = null;
		String lifeCyclePhase = "";
		String dupExternalFileTypeCodes = "";
		String ePromotedPlatforms = "";
		String swb_Number = swbItem.getName();
		String str = "";
		
		HashMap<String, Boolean> hashMap = null;
		Set<String> keySet = null;
		Iterator<String> it = null;
		String key = null;
		
		try 
		{
			if(autoRegeneration.equalsIgnoreCase("Y")) {
				fileDtls = fileDetailsRequest.getFileDtls();
				logger.debug("File Details Request provided count is:" + fileDtls.length);
				
				lifeCyclePhase = applicationProperties.getProperty("SWB_A_REV_LIFE_CYCLE").trim();
				logger.debug("lifeCyclePhase==>"+lifeCyclePhase);
				
				dupExternalFileTypeCodes = applicationProperties.getProperty("DUP_AUTO_REGENERATION_EXTERNAL_FILE_TYPE_CODES");
				logger.debug("dupExternalFileTypeCodes==>"+dupExternalFileTypeCodes);
				
				if (!(swbItem.getValue("Title Block.Lifecycle Phase").toString().equalsIgnoreCase(lifeCyclePhase))) {
					str = applicationProperties.getProperty("DUP_File_Error_Msg_18").replace("%0", swb_Number);
					logger.debug(str);
					sb.append(str);
				} else if(!validateActionType(fileDtls)){
					str = applicationProperties.getProperty("DUP_File_Error_Msg_19").trim();
					logger.debug(str);
					sb.append(str);
				} else {
					ePromotedPlatforms = swbItem.getValue(Integer.valueOf(agilePCProperties.getProperty("Parts_SWB_PageThree_ePromoted_Platforms"))).toString().trim();
					
					if (ePromotedPlatforms == null || ePromotedPlatforms.length() == 0) {
						str = applicationProperties.getProperty("DUP_File_Error_Msg_20").replace("%0", swb_Number);
						logger.debug(str);
						sb.append(str);
						
					} else {
						hashMap=validateFileTypeCdForAutoRegen(fileDtls, dupExternalFileTypeCodes);
						keySet = hashMap.keySet();
						it = keySet.iterator();
						if(it.hasNext()){
							key = it.next();
							boolean ftcExist= hashMap.get(key);
							if (!ftcExist) {
								str = applicationProperties.getProperty("DUP_File_Error_Msg_21").replace("%0", key);
								logger.debug(str);
								sb.append(str);
							}
						}
					}
				}
			} 
		} catch(APIException ape){
			logger.debug("API Exception in processAutoRegenerationWPCO Method==>"+ ape.getMessage());
			throw new Exception(ape);
		} 
		finally {
			if(fileDtls!=null && fileDtls.length>0){
				fileDtls = null;
			}
			lifeCyclePhase = null;
			dupExternalFileTypeCodes = null;
			ePromotedPlatforms = null;
			swb_Number = null;
			str = null;
			
			if (hashMap != null && hashMap.size() > 0) {
				hashMap.clear();
				hashMap = null;
			} else {
				hashMap = null;
			}
			if (keySet != null && keySet.size() > 0) {
				keySet.clear();
				keySet = null;
			} else {
				keySet = null;
			}
			it = null;
			key = null;
		}
		logger.debug("End of processAutoRegenerationWPCO Method");
		return sb.toString();
	}
			
	/**
	 * This method will validate given File Action Type is Replace for all the files given input
	 * @param fileDtls - Array of FileDtls Object
	 * @return boolean - true/false
	 */
	public boolean validateActionType(FileDtls fileDtls[]){
		logger.debug("Starting validateActionType Method");
		if(fileDtls.length > 0){
			for(FileDtls fDtls:fileDtls){
				String actionType = fDtls.getFileActionType().trim();
				if(!actionType.equalsIgnoreCase("Replace")){
					return false;
				}
			}
		} else{
			return false;
		}
		return true;
	}
	
	/**
	 * This method will validate all input provided file type codes exist in auto regeneration properties
	 * @param fileDtls
	 * @param dupExternalFileTypeCodes
	 * @return HashMap<String, Boolean> contains fileTypeCode and Boolean value
	 */
	public HashMap<String, Boolean> validateFileTypeCdForAutoRegen(FileDtls fileDtls[], String dupExternalFileTypeCodes) throws Exception
	{
		logger.debug("Starting of validateFileTypeCdForAutoRegen Method");
		Boolean autoRegenFileTypeCdExist = false;
		HashMap<String, Boolean> hashMap = null;
		String fileTypeCd = "";
		StringBuffer sb = null;
		
		try {
			sb = new StringBuffer("");
			hashMap = new HashMap<String, Boolean> ();
			if(fileDtls.length > 0){
				for(FileDtls fDtls:fileDtls){
					fileTypeCd = fDtls.getFileTypeCode().trim();
					sb.append(fileTypeCd+",");
					if(dupExternalFileTypeCodes.contains(fileTypeCd)){
						autoRegenFileTypeCdExist = true;
						logger.debug("fileTypeCd==>"+fileTypeCd);
						break;
					}
				}
			} else{
				autoRegenFileTypeCdExist=false;
			}
			
			if(sb.length()>0){
				sb.deleteCharAt(sb.length()-1);
			}
			
			hashMap.put(sb.toString(), autoRegenFileTypeCdExist);
		}
		catch(Exception e){
			logger.debug("Exception in validateFileTypeCdForAutoRegen Method:"+e.getMessage());
			throw e;
		}
		finally {
			fileTypeCd = null;
			sb = null;
		}
		logger.debug("End of validateFileTypeCdForAutoRegen Method");
		return hashMap;
	}
			
	/**
	 * This method will be validating part revision of given SWB
	 * @param item - IItem Object
	 * @param part_rev - Software Bundle Revision
	 * @param applicationProperties - Application Properties
	 * @return String - "Success" or Error message
	 * @throws APIException
	 */
	public String validatePartRevision(IItem item, String part_rev, Properties applicationProperties) throws APIException 
	{
		logger.debug("Starting of validatePartRevision Method");
		String returnMsg = "Success";
		boolean validRev = false;
		Map revs = null;
		String lifecycle = null;
		String currentRev = "";
	
		try {
			logger.debug("Part Number is : "+item.getName());
			
			lifecycle = item.getCell(ItemConstants.ATT_TITLE_BLOCK_LIFECYCLE_PHASE).toString();
			logger.debug("lifecycle is :"+lifecycle);	
			
			if(lifecycle.equals("X Revision")) {
				revs = item.getRevisions();
		        logger.debug("Revs for the part:"+item.getName()+" are : "+revs);
		       
		        if(revs.containsValue(part_rev)) {
		        	currentRev = item.getRevision();
		        	logger.debug("currentRev: "+currentRev);
		        		    		 
			        if(!currentRev.equals(part_rev)) {
			        	returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_23").trim().replace("%0", item.getName());
			        	logger.debug(returnMsg);
			        	
			        } else {
			        	validRev = validateRevision(part_rev, item);
			        	if(!validRev){
				        	returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_24").trim().replace("%0", item.getName());
				        	logger.debug(returnMsg);
			        	} 
			        }
		        }
		        else if (revs.containsValue("("+part_rev+")"))
		        {
		        	validRev = validateRevision(part_rev,item);
			        if(!validRev){
				       	returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_25").trim().replace("%0", item.getName());
				       	logger.debug(returnMsg);
			        }
		        	
		        } else {
		        	returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_26").trim().replace("%0", item.getName());
		        	logger.debug(returnMsg);	        	
		        }	        	
			}
			else if (lifecycle.equals("A Revision"))
			{
				currentRev = item.getRevision();
				logger.debug("Current Rev: "+currentRev);
				    
				if(currentRev.equals(part_rev)) {
				   logger.debug("The currentRev is equal to the part revision");
				} else {
				  returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_23").trim().replace("%0", item.getName());
				  logger.debug(returnMsg);
				}	
			}
			else {
				/** Incorrect Life Cycle Phase */
				returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_27").trim().replace("%0", lifecycle).replace("%1", item.getName());
				logger.debug(returnMsg);
			}
			
		} catch (APIException ape) {
			logger.debug("APIException in validatePartRevision Method:"+ape.getMessage());
			throw ape;
		} finally {
			
			if(revs!=null && revs.size()>0){
				revs.clear();
				revs = null;
			} else {
				revs = null;
			}
			lifecycle = null;
			currentRev = null;
		}
		logger.debug("End of validatePartRevision Method");
		return returnMsg;
	}
	
	/**
	 * This method will validate part revision for given Software Bundle Number
	 * @param inputRevision
	 * @param item - IItem Object
	 * @return boolean 
	 * @throws APIException
	 */
	public boolean validateRevision(String inputRevision, IItem item) throws APIException 
	{
		logger.debug("Starting of validateRevision Method");
		HashMap<String, String> hp = null;
		HashMap<String, String> hp1 = null;
		boolean isValid = false;
		boolean hasReleasedArev = false;
		logger.debug("inputRevision: " + inputRevision + " for item - "	+ item.getName());
		String prefixTypes[] = { Constants.PENDING_AREV_CHAR,Constants.RELEASED_AREV_CHAR, Constants.RELEASED_XREV_CHAR };
		
		try {
			if (startsWithIgnoreWhitespaces(prefixTypes[0],	inputRevision)){
				isValid = false;
			} 
			else if (startsWithIgnoreWhitespaces(prefixTypes[1], inputRevision)){
				isValid = false;
			}
			else if (startsWithIgnoreWhitespaces(prefixTypes[2], inputRevision))
			{
				hp =  readChangeHistoryTable(item);
				if(hp.get(Constants.XREVISION)!= null && hp.get(Constants.XREVISION).equalsIgnoreCase(inputRevision))
				{
					logger.debug("Change History Latest X Revision equal to input revision. Its a latest X rev");
					
					/** Code change to consider Released A revs ( in other than Pending / Submitted To Block/ ECO status) */
					hp1 =  readChangeHistoryTableForReleasedARevChanges(item);
					logger.debug("A Revision : "+hp1.get(Constants.AREVISION));
					
					logger.debug("A Revision : "+hp1.get(Constants.AREVISION));
					if(hp1.get(Constants.AREVISION)!=null && hp1.size()>0){
						logger.debug("Item has Released A Rev. Change Status not in Pending / Submitted to Block, Cancelled Status" );
						isValid =false;
						hasReleasedArev=true;
					}

					/** Check if the item has already Released A rev . Then no need to go to Pending Changes. */
					if(!hasReleasedArev){
						logger.debug("There are no Relased A rev for Item :"+item.getName()+". So Validating from Pending Change table");
						isValid = readPendingChangeTable(item);
					}
				}
				else {
					/** if there is no X Revision or InputRevision is not equal to Latest X Revision. */
					isValid = true;
				}
			} 
			else {
				isValid = true;
			}
		} catch (APIException ape){
			logger.debug("APIException in validateRevision Method:"+ape.getMessage());
			throw ape;
		} 
		finally {
			if (hp != null && hp.size() > 0) {
				hp.clear();
				hp = null;
			} else {
				hp = null;
			}

			if (hp1 != null && hp1.size() > 0) {
				hp1.clear();
				hp1 = null;
			} else {
				hp1 = null;
			}
			prefixTypes = null;
		}
		logger.debug("isValid: "+isValid);
		logger.debug("End of validateRevision Method");
		return isValid;
	}
			
	/**
	 * This method reads the change history table of given part
	 * @param item - IItem Object
	 * @return HashMap<String, String> - 
	 * @throws APIException
	 */
	public HashMap<String, String> readChangeHistoryTable(IItem item) throws APIException
	{
		logger.debug("Starting of readChangeHistoryTable Method");
		HashMap<String, String> hp = null;
		ITable historyChangeTable = null;
		IAgileClass cls = null;
		IAttribute attr = null;
		ITable.ISortBy sortByNumber = null;
		ITwoWayIterator histChgTblItr = null;
		IRow histRow = null;
		String changeHistoryRevision = "";
		String changeType="";
		String changeStatus = "";
		String changeNumber = "";
		
		try {
			hp = new HashMap<String, String>();
			historyChangeTable = item.getTable(ItemConstants.TABLE_CHANGEHISTORY);
			
			if(historyChangeTable != null && historyChangeTable.size()> 0)
			{
				cls = item.getAgileClass();
				attr = cls.getAttribute(ItemConstants.ATT_CHANGE_HISTORY_REL_DATE);
				
				/** Specify the sort attribute for the table iterator*/
				sortByNumber = historyChangeTable.createSortBy(attr, ITable.ISortBy.Order.DESCENDING);
				
				/** Create a sorted table iterator */
				histChgTblItr = historyChangeTable.getTableIterator(new ITable.ISortBy[] { sortByNumber });
			
				while(histChgTblItr.hasNext())
				{
					histRow = (IRow)histChgTblItr.next();
					changeType = histRow.getValue(ItemConstants.ATT_CHANGE_HISTORY_TYPE).toString();
					changeStatus = histRow.getValue(ItemConstants.ATT_CHANGE_HISTORY_STATUS).toString();
					hp.put(Constants.CHANGE_STATUS, changeStatus);
					
					if(changeStatus.equalsIgnoreCase(Constants.CANCELLED)||changeStatus.equalsIgnoreCase(Constants.HOLD)){
						continue;
					}
					
					if(changeType.equalsIgnoreCase(Constants.PNR) || changeType.equalsIgnoreCase(Constants.PNCR)) 
					{
						changeHistoryRevision = histRow.getValue(ItemConstants.ATT_CHANGE_HISTORY_REV).toString();
						
						if(startsWithIgnoreWhitespaces(Constants.RELEASED_XREV_CHAR, changeHistoryRevision)){
							hp.put(Constants.XREVISION, changeHistoryRevision);
							changeNumber = histRow.getValue(ItemConstants.ATT_CHANGE_HISTORY_NUMBER).toString();
							hp.put(Constants.CHANGE_NUMBER, changeNumber);
							break;
						}else {
							continue;
						}

					}
					else {
						/** this is not PNR/PNCR */
						logger.debug("The change Type is not PNR/PNCR: "+changeType);
						continue;
					}
				}
				logger.debug("XRev: "+hp.get(Constants.XREVISION));
				logger.debug("changeNumber: "+hp.get(Constants.CHANGE_NUMBER));
				logger.debug("changeStatus: "+hp.get(Constants.CHANGE_STATUS));
			}
			else {
				logger.debug("Change History Table is empty");
			}
		} catch (APIException ape){
			logger.debug("APIException in readChangeHistoryTable:"+ ape.getMessage());
			throw ape;
		} finally {
			if(historyChangeTable!=null)
				historyChangeTable = null;
			
			if(cls!= null)
				cls = null;
			
			if(attr!=null)
				attr = null;
			if(sortByNumber!=null)
				sortByNumber = null;
			
			if(histChgTblItr!=null)
				histChgTblItr = null;
			
			if(histRow!=null)
				histRow = null;
			
			changeHistoryRevision = null;
			changeType = null;
			changeStatus = null;
			changeNumber = null;
		}
		logger.debug("readChangeHistoryTable end..");
		return hp;
	}
		
	/**
	 * This method will be validating any released A-Revisions on the SWB
	 * @param item
	 * @return boolean
	 * @throws APIException
	 */
	public HashMap<String, String> readChangeHistoryTableForReleasedARevChanges(IItem item) throws APIException
	{
		logger.debug("Starting readChangeHistoryTableForReleasedARevChanges Method");
		HashMap<String, String> hp = null;
		ITable historyChangeTable = null;
		IAgileClass cls = null;
		IAttribute attr = null;
		ITable.ISortBy sortByNumber = null;
		ITwoWayIterator histChgTblItr = null;
		IRow histRow = null;
		String changeHistoryRevision = "";
		String changeType="";
		String changeStatus = "";
		String changeNumber = "";
		
		try {
			hp = new HashMap<String, String>();
			historyChangeTable = item.getTable(ItemConstants.TABLE_CHANGEHISTORY);
			
			if(historyChangeTable != null && historyChangeTable.size()> 0)
			{
				cls = item.getAgileClass();
				attr = cls.getAttribute(ItemConstants.ATT_CHANGE_HISTORY_REL_DATE);
				
				/** Specify the sort attribute for the table iterator*/
				sortByNumber = historyChangeTable.createSortBy(attr, ITable.ISortBy.Order.DESCENDING);
				
				/** Create a sorted table iterator */
				histChgTblItr = historyChangeTable.getTableIterator(new ITable.ISortBy[] { sortByNumber });
				
				while(histChgTblItr.hasNext())
				{
					histRow = (IRow)histChgTblItr.next();
					changeType = histRow.getValue(ItemConstants.ATT_CHANGE_HISTORY_TYPE).toString();
					changeStatus = histRow.getValue(ItemConstants.ATT_CHANGE_HISTORY_STATUS).toString();
					hp.put(Constants.CHANGE_STATUS, changeStatus);
					
					if(changeStatus.equalsIgnoreCase(Constants.CANCELLED) || changeStatus.equalsIgnoreCase(Constants.HOLD)){
						continue;
					}

					/** Code change begins to consider other than Pending and Submit To Block */
					if(changeType.equalsIgnoreCase(Constants.ECO)){
						logger.debug("changeType: "+changeType);
						changeHistoryRevision = histRow.getValue(ItemConstants.ATT_CHANGE_HISTORY_REV).toString();
						logger.debug("changeHistoryRevision :"+changeHistoryRevision);
						
						if(startsWithIgnoreWhitespaces(Constants.RELEASED_AREV_CHAR, changeHistoryRevision))
						{
							logger.debug("Latest Released A Rev");
							hp.put(Constants.AREVISION, changeHistoryRevision);
							changeNumber = histRow.getValue(ItemConstants.ATT_CHANGE_HISTORY_NUMBER).toString();
							hp.put(Constants.CHANGE_NUMBER, changeNumber);
							break;
						}else{
							continue;
						}
					}
					else {
						/** this is not ECO*/
						logger.debug("The change Type is not ECO."+changeType);
						continue;
					}
				}
				logger.debug("ARev: "+hp.get(Constants.AREVISION));
				logger.debug("changeNumber: "+hp.get(Constants.CHANGE_NUMBER));
				logger.debug("changeStatus: "+hp.get(Constants.CHANGE_STATUS));
			}
			else {
				logger.debug("Change History Table is empty");
			}
		} catch (APIException ape) {
			logger.debug("APIException in readChangeHistoryTableForReleasedARevChanges:"+ ape.getMessage());
			throw ape;
		} finally {
			if(historyChangeTable!=null)
				historyChangeTable = null;
			
			if(cls!=null)
				cls = null;
			
			if(attr!=null)
				attr = null;
			
			if(sortByNumber!=null)
				sortByNumber = null;
			
			if(histChgTblItr!=null)
				histChgTblItr = null;
			
			if(histRow!=null)
				histRow = null;
			
			changeHistoryRevision = null;
			changeType = null;
			changeStatus = null;
			changeNumber = null;
		}
		logger.debug("End of readChangeHistoryTableForReleasedARevChanges Method");
		return hp;
	}
		
	/**
	 * This method will be validating changeStatus other than Pending/Submit to Block/Cancelled
	 * @param item - IItem Object
	 * @return boolean - true or false
	 * @throws APIException
	 */
	public boolean readPendingChangeTable(IItem item) throws APIException
	{
		logger.debug("Starting readPendingChangeTable Method");
		ITable pendingChangeTable = null;
		IAgileClass cls = null;
		IAttribute attr = null;
		ITable.ISortBy sortByNumber = null;
		ITwoWayIterator pendChgTblItr = null;
		IRow pendRow = null;
		String changeType = "";
		String changeStatus = "";
		boolean isECOStatus = false;

		try {
			pendingChangeTable = item.getTable(ItemConstants.TABLE_PENDINGCHANGES);
			
			if(pendingChangeTable != null && pendingChangeTable.size()> 0)
			{
				cls = item.getAgileClass();
				attr = cls.getAttribute(ItemConstants.ATT_PENDING_CHANGES_DATE_ORIGINATED);
				
				/** Specify the sort attribute for the table iterator */
				sortByNumber = pendingChangeTable.createSortBy(attr, ITable.ISortBy.Order.DESCENDING);
				
				/** Create a sorted table iterator */
				pendChgTblItr = pendingChangeTable.getTableIterator(new ITable.ISortBy[] { sortByNumber });
			
				while(pendChgTblItr.hasNext())
				{
					pendRow = (IRow)pendChgTblItr.next();
					changeType = pendRow.getValue(ItemConstants.ATT_PENDING_CHANGES_TYPE).toString();
					changeStatus = pendRow.getValue(ItemConstants.ATT_PENDING_CHANGES_STATUS).toString();

					if(changeStatus.equalsIgnoreCase(Constants.UNASSIGNED) 
							|| changeStatus.equalsIgnoreCase(Constants.CANCELLED) 
							|| changeStatus.equalsIgnoreCase(Constants.HOLD)){
						continue;
					}
					
					if(changeType.equalsIgnoreCase(Constants.ECO)) 
					{
						/**Check the changeStatus other than �Pending�/�Submit to Block�/�Cancelled�*/
						if(changeStatus.equalsIgnoreCase(Constants.CHANGE_PENDING_STATUS)
								|| changeStatus.equalsIgnoreCase(Constants.CHANGE_SUBMIT2BLOCK_STATUS)
								|| changeStatus.equalsIgnoreCase(Constants.CHANGE_CANCELLED_STATUS))
						{
							isECOStatus = true;
							continue;
						}else{
							isECOStatus = false;
							break;
						}
					}
					else {
						/** this is not ECO*/
						logger.debug("The change Type is not ECO."+changeType);
						isECOStatus = true;
						continue;
					}
				}
			}
			else{
				logger.debug("pending History Table is empty.");
				isECOStatus = true;
			}
		} catch (APIException ape) {
			
		} finally {
			
			if (pendingChangeTable != null)
				pendingChangeTable = null;

			if (cls != null)
				cls = null;

			if (attr != null)
				attr = null;

			if (sortByNumber != null)
				sortByNumber = null;

			if (pendChgTblItr != null)
				pendChgTblItr = null;

			if (pendRow != null)
				pendRow = null;

			changeType = null;
			changeStatus = null;
		}
		logger.debug("End readPendingChangeTable Method");
		return isECOStatus;
	}
	
	/**
	 * This method will check given prefix value exist in string value
	 * @param prefix
	 * @param string
	 * @return boolean true/false
	 */
	public boolean startsWithIgnoreWhitespaces(String prefix, String string)
	{
	    int index1 = 0;
	    int index2 = 0;
	    int length1 = prefix.length();
	    int length2 = string.length();
	    char ch1 = ' ';
	    char ch2 = ' ';
	    while ((index1 < length1) && (index2 < length2)) {
	      while ((index1 < length1) && (Character.isWhitespace(ch1 = prefix.charAt(index1)))) {
	        index1++;
	      }
	      while ((index2 < length2) && (Character.isWhitespace(ch2 = string.charAt(index2)))) {
	        index2++;
	      }
	      if ((index1 == length1) && (index2 == length2)) {
	        return true;
	      }
	      if (ch1 != ch2) {
	        return false;
	      }
	      index1++;
	      index2++;
	    }
	    if ((index1 < length1) && (index2 >= length2))
	      return false;
	    return true;
	  }
	
	/**
	 * This method will validation life cycle phase of the part is A-Revision and Action is other than Delete
	 * @param fileDtls - FileDtls Array
	 * @param applicationProperties - Application Properties Object
	 * @return String - Success or error message
	 * @throws Exception
	 */
	public String validateARevDelete(FileDtls fileDtls[], Properties applicationProperties) throws Exception
	{
		logger.debug("Starting of validateARevDelete Method");
		String returnMsg = "Success";
		String fileActionType = "";
		try{
			if(fileDtls.length > 0){
				for (FileDtls fDtls : fileDtls) {
					fileActionType = fDtls.getFileActionType().trim();
					logger.debug("fileActionType:"+fileActionType);
					if(fileActionType.equalsIgnoreCase("Delete")){
						returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_29").trim();
						logger.debug(returnMsg);
				       	return returnMsg;
					}
				}
			}
		} 
		catch (Exception e){
			logger.debug("Exception in validateARevDelete Method:"+ e.getMessage());
			throw e;
		} 
		finally {
			fileActionType = null;
		}
		logger.debug("End of validateFileExist Method");
		return returnMsg;			
	}
	
	/**
	 * This method validate file exist in downstream shared folder path and size of file should be greater than zero
	 * @param folderPath - Downstream folder path
	 * @param fileName - Name of the file
	 * @param action_type - File Action Type
	 * @param applicationProperties - Application Properties Object
	 * @return String - Success or error message
	 * @throws Exception
	 */
	public String validateFileExist(String folderPath, String fileName, String action_type, Properties applicationProperties) throws Exception
	{
		logger.debug("Starting of validateFileExist Method");
		String returnMsg = "Success";
		File attachFile = null;
			
		try{
			/** Checking if file exists in DS UNC Sharepath for Add and replace File action */
			if(action_type.equalsIgnoreCase("Add") ||action_type.equalsIgnoreCase("Replace")) 
			{
				attachFile = new File(folderPath + File.separator + fileName);
				
				if (!attachFile.exists()) {	
					returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_30").trim().replace("%0", folderPath+"\\"+fileName);
					logger.debug(returnMsg);
			       	return returnMsg;
				}
					
				if (attachFile.length() == 0) {	
					returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_31").trim().replace("%0", folderPath+"\\"+fileName);
					logger.debug(returnMsg);
			       	return returnMsg;
				}				
			} 
		} 
		catch (Exception e){
			logger.debug("Exception in validateFileExist Method:"+ e.getMessage());
			throw e;
		} 
		finally {
			if(attachFile!=null){
				attachFile = null;
			}
		}
		logger.debug("End of validateFileExist Method");
		return returnMsg;			
	}
	
	/**
	 * This method will validate file type code, file and file action based on the given inputs 
	 * @param item - IItem Object
	 * @param part_rev - SWB Part Revision
	 * @param action_type - File Action Type
	 * @param fileName - File Name
	 * @param fileTypeCode - File Type Code
	 * @param lifecycle - Life Cycle of SWB
	 * @param applicationProperties - Application Properties Object
	 * @param agilePCProperties - PC Properties Object
	 * @param fileTypeCodeMap - HashMap<String, String> Object
	 * @param attTable - ITable Object
	 * @return String - Success means valid, other wise error
	 * @throws APIException
	 * @throws Exception
	 */
	public String validateFileAction(IItem item, String part_rev, String action_type, String fileName, String fileTypeCode, 
			String lifecycle, Properties applicationProperties, Properties agilePCProperties, HashMap<String, String> fileTypeCodeMap, ITable attTable) throws APIException, Exception{
		logger.debug("Starting of validateFileAction Method");
		String returnMsg = "Success";
		logger.debug("part_number is : "+item.getName());
		String ftc = "";
				
		if(action_type.equalsIgnoreCase("Add")){
			ftc = applicationProperties.getProperty("FILETYPECODE_NOT_ALLOWED_ADD").trim();
			logger.debug("Disallowed file type codes for Add action : "+ftc);
			
			if(ftc.contains(fileTypeCode.trim())){
				returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_32").trim().replace("%0", item.getName()).replace("%1", fileName).replace("%2", fileTypeCode);
				logger.debug(returnMsg);
				return returnMsg;
			}
						
			if(lifecycle.equals("X Revision")){
				ftc = applicationProperties.getProperty("FILETYPECODE_ALLOWED_ADD_FOR_X_REV").trim();
				if(!ftc.contains(fileTypeCode.trim())){
					returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_33").trim().replace("%0", lifecycle).replace("%1", item.getName()).replace("%2", fileName).replace("%3", fileTypeCode);
					logger.debug(returnMsg);
					return returnMsg;
				}
			}
			else if(lifecycle.equals("A Revision")){
				ftc = applicationProperties.getProperty("FILETYPECODE_ALLOWED_ADD_FOR_A_REV").trim();
				if(!ftc.contains(fileTypeCode.trim())){
					returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_33").trim().replace("%0", lifecycle).replace("%1", item.getName()).replace("%2", fileName).replace("%3", fileTypeCode);
					logger.debug(returnMsg);
					return returnMsg;
				}
				
			}
					
			/** Checking valid file extension for a FTC */
			if (!chkFileExt(fileTypeCode, fileName, fileTypeCodeMap)){
				returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_34").trim().replace("%0", item.getName()).replace("%1", fileName).replace("%2", fileTypeCode).replace("%3", fileExtMain);
				logger.debug(returnMsg);
				return returnMsg;
			}
			
			if(isFileExistInSWB(item, fileName, attTable)){
				returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_35").trim().replace("%0", item.getName()).replace("%1", fileName);
				logger.debug(returnMsg);
				return returnMsg;
			}

			if(isFileTypeCodeExistInSWBundle(item, fileTypeCode, attTable)){
				returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_36").trim().replace("%0", item.getName()).replace("%1", fileTypeCode);
				logger.debug(returnMsg);
				return returnMsg;
			}
		}
		
		
		if(action_type.equalsIgnoreCase("Replace"))
		{
			ftc = applicationProperties.getProperty("FILETYPECODE_NOT_ALLOWED_REPLACE").trim();
			logger.debug("Disallowed file type codes : "+ftc);
			
			if(ftc.contains(fileTypeCode.trim())){
				returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_37").trim().replace("%0", item.getName()).replace("%1", fileName).replace("%2", fileTypeCode);
				logger.debug(returnMsg);
				return returnMsg;
			}
			
			if(lifecycle.equals("X Revision")){
				ftc = applicationProperties.getProperty("FILETYPECODE_ALLOWED_REPLACE_FOR_X_REV").trim();
				if(!ftc.contains(fileTypeCode.trim())){
					returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_38").trim().replace("%0", lifecycle).replace("%1", item.getName()).replace("%2", fileName).replace("%3",fileTypeCode);
					logger.debug(returnMsg);
					return returnMsg;
				}
			}else if(lifecycle.equals("A Revision")){
				ftc = applicationProperties.getProperty("FILETYPECODE_ALLOWED_REPLACE_FOR_A_REV").trim();
				if(!ftc.contains(fileTypeCode.trim())){
					returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_38").trim().replace("%0", lifecycle).replace("%1", item.getName()).replace("%2", fileName).replace("%3",fileTypeCode);
					logger.debug(returnMsg);
					return returnMsg;
				}
			}
					
			/** Checking valid file extension for a FTC */
			if (!chkFileExt(fileTypeCode, fileName, fileTypeCodeMap)){
				returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_39").trim().replace("%0", item.getName()).replace("%1", fileName).replace("%2", fileTypeCode).replace("%3", fileExtMain); 
				logger.debug(returnMsg);
				return returnMsg;
			}
			
			/** Checking file type code validation */
			if(!isFileTypeCodeExistInSWBundle(item, fileTypeCode, attTable)){
				returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_44").trim().replace("%0", item.getName()).replace("%1", fileTypeCode);
				logger.debug(returnMsg);
				return returnMsg;
			}
		}
			
		if(action_type.equalsIgnoreCase("Delete")){
			ftc = applicationProperties.getProperty("FILETYPECODE_NOT_ALLOWED_DELETE").trim();
			logger.debug("Disallowed file type codes : "+ftc);
			
			if(ftc.contains(fileTypeCode.trim())){
				returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_41").trim().replace("%0", item.getName()).replace("%1", fileName).replace("%2", fileTypeCode);
				logger.debug(returnMsg);
				return returnMsg;
			}
			
			/** Checking file with file type code validation for Delete */
			if(!isFileAndFileTypeCodeExistInSWB(item, fileName, fileTypeCode, attTable)){
				returnMsg = applicationProperties.getProperty("DUP_File_Error_Msg_42").trim().replace("%0", item.getName()).replace("%1", fileName).replace("%2", fileTypeCode);
				logger.debug(returnMsg);
				return returnMsg;
			}
		}
		return returnMsg;
	}

	/**
	 * This method will validate file exist on the SWB
	 * @param partNumItem - IItem Object
	 * @param fileName - File Name
	 * @param attTable - ITable Object
	 * @return boolean - true(file exist)/ false - file not exist or table is empty
	 * @throws APIException
	 */
	public boolean isFileExistInSWB(IItem partNumItem, String fileName, ITable attTable) throws APIException
	{
		logger.debug("Starting of isFileExistInSWB Method");
		Iterator it = null;
		IAttachmentRow attachRow = null;
		String rowFileName = "";
		boolean isFound = false;

		try {
			if (attTable.size() > 0) {
				it = attTable.iterator();
				while(it.hasNext()){
					attachRow = (IAttachmentRow)it.next();
					rowFileName = attachRow.getCell(ItemConstants.ATT_ATTACHMENTS_FILE_NAME).toString();
					logger.debug("rowFileName: "+rowFileName);
					if(rowFileName.trim().equalsIgnoreCase(fileName)){
						logger.debug("File found in attachments table is: "+fileName);	
						isFound = true;
						break;
					}				
				}
			}else{
				isFound = false;
			}
		} 
		catch (APIException ape) {
			logger.debug("APIException in isFileExistInSWB Method -"
					+ ape.getMessage());
			throw ape;
		} 
		finally {
			if (it != null)
				it = null;

			if (attachRow != null)
				attachRow = null;

			attachRow = null;
			rowFileName = null;
		}
		logger.debug("End of isFileExistInSWB Method");
		return isFound;
	}
	
	/**
	 * This method will check file type code exist in SWB 
	 * @param item - IItem Object
	 * @param fileTypeCode - File Type Code
	 * @param attTable - ITable Object
	 * @return boolean - true or false
	 * @throws APIException
	 */
	public boolean isFileTypeCodeExistInSWBundle(IItem item, String fileTypeCode, ITable attTable) throws APIException 
	{
		logger.debug("Starting of isFileTypeCodeExistInSWBundle Method");
		IAttachmentRow attachRow=null;
		String rowFileTypeCode=null;
		Iterator<IAttachmentRow> it = null;
		boolean isFound = false;
		
		try {
			if (attTable.size() > 0) {
				it = attTable.iterator();
				while(it.hasNext()){
					attachRow = it.next();
					rowFileTypeCode = attachRow.getCell("Attachments.FileTypeCode").toString();
					logger.debug("Attachment File Details: "+rowFileTypeCode);

					if(rowFileTypeCode.trim().equals(fileTypeCode)){
						logger.debug("File Type Code found in attachments table:"+fileTypeCode);	
						isFound = true;
						break;
					}				
				}
			}
			else {
				isFound = false;
			}
		}
		catch(APIException ape){
			logger.debug("APIException in isFileTypeCodeExistInSWBundle Method:"+ape.getMessage());
			throw ape;
		} 
		finally 
		{
			if(it!=null)
				it = null;
		
			if(attachRow!=null)
				attachRow = null;
		
			rowFileTypeCode = null;
		}
		logger.debug("End of isFileTypeCodeExistInSWBundle Method");
		return isFound;
	}

	/**
	 * This method will validate file name with respective file type code in the SWB attachments table  
	 * @param partNumItem - IItem Object
	 * @param fileName
	 * @param fileTypeCode
	 * @param attTable - ITable object
	 * @return boolean - true or false
	 * @throws APIException
	 */
	public boolean isFileAndFileTypeCodeExistInSWB(IItem partNumItem, String fileName, String fileTypeCode, ITable attTable) throws APIException
	{
		logger.debug("Starting of isFileAndFileTypeCodeExistInSWB Method");
		Iterator it = null;
		IAttachmentRow attachRow = null;
		String rowFileName = "";
		String rowFileTypeCode = "";
		boolean isFound = false;

		try {
			if (attTable.size() > 0) {
				it = attTable.iterator();
				while(it.hasNext()){
					attachRow = (IAttachmentRow)it.next();
					rowFileName = attachRow.getCell(ItemConstants.ATT_ATTACHMENTS_FILE_NAME).toString().trim();
					logger.debug("rowFileName: "+rowFileName);
					
					rowFileTypeCode = attachRow.getCell("Attachments.FileTypeCode").toString().trim();
					logger.debug("rowFileTypeCode: "+rowFileTypeCode);
					
					if(rowFileName.equalsIgnoreCase(fileName) && rowFileTypeCode.equals(fileTypeCode) ){
						logger.debug("File and File Type Code found in attachments table is: "+fileName+" , "+rowFileTypeCode);	
						isFound = true;
						break;
					}				
				}
			}else{
				isFound = false;
			}
		} 
		catch (APIException ape) {
			logger.debug("APIException in isFileAndFileTypeCodeExistInSWB Method -"
					+ ape.getMessage());
			throw ape;
		} 
		finally {
			if (it != null)
				it = null;

			if (attachRow != null)
				attachRow = null;

			attachRow = null;
			rowFileName = null;
			rowFileTypeCode = null;
		}
		logger.debug("End of isFileAndFileTypeCodeExistInSWB Method");
		return isFound;
	}
	
	/**
	 * This Method will insert action 30 and 31 records into Service Queue table of Agile Services Schema
	 * @param messageID - Unique Message ID
	 * @param downstreamName - Downstream User Name
	 * @param swb_Number - Software Bundle Number
	 * @param swb_Rev - Software Bundle Revision
	 * @param fileDtls - FileDtls Array
	 * @param autoRegeneration - Y or N
	 * @param applicationProperties - Application Properties Object
	 * @param autoRegenerationFlag - true or false
	 * @return String - Success or empty
	 * @throws Exception
	 */
	public String insertRecordsForFileUploadAutoRegeneration(String messageID, String downstreamName, String swb_Number, String swb_Rev, FileDtls fileDtls[], String autoRegeneration, Properties applicationProperties, boolean autoRegenerationFlag) throws Exception 
	{
		logger.debug("Starting of insertRecordForFileAutoRegenAction Method");
		CallableStatement insertCallableStmt = null;
		int dependentqueueid = 0;
		Connection connection = null;
		AddServiceQueueDAO dao = null;
		String downStreamUserName = "";
		String dupSWBFileAction = "";
		String dupSWBFileBatchAction = "";
		String batchProcessId = "";
		String result = "";
		
		try {
			logger.debug("Start of getting DB Connection");
			dao = new AddServiceQueueDAO();
			if (connection == null)
				connection = dao.getDBConnection();
			logger.debug("End of getting DB Connection");
			
			downStreamUserName = applicationProperties.getProperty("DOWNSTREAM_USER_NAME").trim().toUpperCase();
			logger.debug("downStreamUserName ==>" + downStreamUserName);
			
			dupSWBFileAction = applicationProperties.getProperty(Constants.DUP_SWB_FILE_ACTION).trim();
			logger.debug("dupSWBFileAction ==>" + dupSWBFileAction);
			
			batchProcessId = messageID+"_"+swb_Number;
			logger.debug("batchProcessId ==>" + batchProcessId);
			
			logger.debug("Start Executing File Action Procedure");
			if(fileDtls.length > 0){
				for(FileDtls fileDtlsObj: fileDtls){
					insertCallableStmt = dao.addServiceQueue(connection, dupSWBFileAction, dependentqueueid, messageID, downstreamName, swb_Number, swb_Rev, fileDtlsObj.getFileActionType().trim(),autoRegeneration,fileDtlsObj.getFileName().trim(), fileDtlsObj.getFileTypeCode().trim(), fileDtlsObj.getFileDescription().trim(),downStreamUserName, batchProcessId, "N");
					insertCallableStmt.executeQuery();
				}
			}
			logger.debug("End Executing File Action Procedure");
			
			if(autoRegenerationFlag){
				logger.debug("Start Executing File Action Batch Procedure");
				dupSWBFileBatchAction = applicationProperties.getProperty(Constants.DUP_SWB_FILE_BATCH_ACTION).trim();
				logger.debug("dupSWBFileBatchAction ==>" + dupSWBFileBatchAction);
				
				insertCallableStmt = dao.addServiceQueue(connection, dupSWBFileBatchAction, dependentqueueid, messageID, downstreamName, swb_Number, swb_Rev, "", "", "", "", "",
					downStreamUserName, batchProcessId, "Y");
				insertCallableStmt.executeQuery();
				logger.debug("End Executing File Action Batch Procedure");
			}
			result = "Success";
		}
		catch(Exception ex){
			String error = "Exception at insertRecordForFileAutoRegenAction Method:"
					+ AgileCommonUtil.exception2String(ex);
			logger.error(error);
			throw new Exception(error);
		}
		finally {
			try {
				if (insertCallableStmt != null) {
					insertCallableStmt.close();
					insertCallableStmt = null;
				}
				if (connection != null) {
					logger.debug("closing the connection");
					connection.close();
					logger.debug("nullifying the connection");
					connection = null;
				}

				if (dao != null) {
					dao = null;
				}

				downStreamUserName = null;
				dupSWBFileBatchAction = null;
				dupSWBFileAction = null;
				batchProcessId = null;

			} catch (SQLException sqlException) {
				logger.debug("SQLException Occured:"
						+ AgileCommonUtil.exception2String(sqlException));
			}
		}
		logger.debug("End of insertRecordForFileAutoRegenAction Method");
		return result;
	}
	
	/**
	 * This method will validate file name extension and fileTypeCode corresponding extension in DB for AIC_AGILE_FILETYPECODES table
	 * @param fileTypeCode
	 * @param fileName
	 * @param fileTypeCodeMap
	 * @return boolean - true or false
	 * @throws Exception
	 */
	public boolean chkFileExt(String fileTypeCode, String fileName, HashMap<String, String> fileTypeCodeMap) throws Exception 
	{
		logger.debug("Starting of chkFileExt Method: "+fileTypeCode);
		String fileExt = "";
		String ext = "";
		String fileDBExtArr[] = null;
		try {
			fileExt = fileTypeCodeMap.get(fileTypeCode);
			if(fileExt==null)
				fileExt = "";
			logger.debug("File extension from database: "+fileExt);
			fileExtMain = fileExt;
			
			if(fileName.lastIndexOf(".")!=-1){
				ext = fileName.substring(fileName.lastIndexOf("."), fileName.length());
				if(ext==null)
					ext = "";
				logger.debug("File extension of file: "+ext);
					
				if(!(fileExt.trim().equalsIgnoreCase(".all")))
				{
					fileDBExtArr = fileExt.split(",");
					for(String fileDBExtension: fileDBExtArr){
						if(fileDBExtension.trim().equalsIgnoreCase(ext.trim())){
							logger.debug("File extension is correct");
							return true;
						}
					}
					logger.debug("File extension is not correct");
					return false;
										
				}else{
					return true;
				}
			} else {
				return false;
			}
			
		} catch (Exception e) {
			logger.debug("Exception in chkFileExt Method: "+e.getMessage());
			throw e;
		} finally {
			fileExt = null;
			ext = null;
			fileDBExtArr = null;
		}
	}
	
	/**
	 * This method will fetch the records from AIC_AGILE_FILETYPECODES from Agile Services Schema
	 * @param applicationProperties - Properties Object
	 * @return HashMap<String, String> - Contains Code and Extension values
	 * @throws SQLException
	 * @throws Exception
	 */
	private HashMap<String, String> getAicAgileFileTypeCodeTableData(Properties applicationProperties) throws SQLException, Exception 
	{
		logger.debug("Starting of getAicAgileFileTypeCodeTableData Method");
		Connection connection = null;
		String AIC_TABLE_COLUMN_CODE = "";
		String AIC_TABLE_COLUMN_EXTENSION = "";
		String procName = "";
		AddServiceQueueDAO dao = null;
		CallableStatement callableStatement = null;
		ResultSet rs = null;
		String code = "";
		String extension = "";
		HashMap<String, String> fileTypeCodeMap = null;
				
		try{ 
			fileTypeCodeMap = new HashMap<String, String>();
			
			dao = new AddServiceQueueDAO();
			if(connection == null){
				connection = dao.getDBConnection();
				logger.debug("Got the db connection:"+connection);
			}
			
			AIC_TABLE_COLUMN_CODE = applicationProperties.getProperty(Constants.AIC_TABLE_COLUMN_CODE).trim();
			logger.debug("AIC_TABLE_COLUMN_CODE: " + AIC_TABLE_COLUMN_CODE);
			
			AIC_TABLE_COLUMN_EXTENSION = applicationProperties.getProperty(Constants.AIC_TABLE_COLUMN_EXTENSION).trim();
			logger.debug("AIC_TABLE_COLUMN_EXTENSION: " + AIC_TABLE_COLUMN_EXTENSION);
				
			procName = applicationProperties.getProperty(Constants.AIC_AGILE_FILETYPECODES_PROC).trim();
			logger.debug("procName: " + procName);
			
			logger.debug("Start executing stored procedure");
			callableStatement = connection.prepareCall("{call "+procName+"(?)}");
			callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
			callableStatement.execute();
			logger.debug("End executing stored procedure");
			
			rs = (ResultSet) callableStatement.getObject(1);

			if(rs != null){
				while (rs.next()){
					code = rs.getString(AIC_TABLE_COLUMN_CODE);
					if(code==null){
						code = "";
					}
					
					extension = rs.getString(AIC_TABLE_COLUMN_EXTENSION);
					if(extension==null){
						extension = "";
					}
					//logger.debug(code+"\t"+extension);
					fileTypeCodeMap.put(code, extension);
				}
			}
		}
		catch(SQLException sqe){
			logger.error("SQLException in getAicAgileFileTypeCodeTableData Method: "+ sqe.getMessage());
			throw sqe;						
		}
		catch(Exception e){
			logger.error("Exception in getAicAgileFileTypeCodeTableData Method: "+ e.getMessage());
			throw e;						
		}
		finally{
			if(dao!=null)
				dao = null;
			
			if(rs!=null)
				rs = null;

			if(callableStatement!=null)
				callableStatement = null;
			
			if(connection!=null){
				connection.close();
				connection = null;
			}
			procName = null;
			code = null;
			extension = null;
		}
		logger.debug("End of getAicAgileFileTypeCodeTableData Method");
		return fileTypeCodeMap;
	}
 }
