package com.dell.plm.agile.integration.agileasyncswservice.bo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.log4j.Logger;
import com.agile.api.APIException;
import com.agile.api.IAgileSession;
import com.agile.api.IDataObject;
import com.agile.api.IRow;
import com.agile.api.ITable;
import com.agile.api.ServiceRequestConstants;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;

/**
 * @author Manoj_Dugaya
 *
 */

public class ValidatePSR {

	private static final Logger logger = Logger.getLogger(ValidatePSR.class);
	private static IDataObject item = null;
	private static StringTokenizer st = null;
	private static Iterator<?> itr = null;

	/**
	 * @param session
	 * @param psrName
	 * @param fileName
	 * @param fileTypeCode
	 * @return String object with success or validation error msg
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String psrTypeValidation(IAgileSession session, String psrName, String fileName, String fileTypeCode)
			throws FileNotFoundException, IOException {
		
		String msg = "";
		String psrType = "";
		boolean validPSRType = false;

		logger.debug("loading properties..");
		String configBase = FilePropertyUtility.getConfigBasePath("asynchwebservice");
		Properties applicationProperties = FilePropertyUtility.loadProperties(configBase + "\\config\\aic_filedownload_application.properties");

		if (fileName == null) {
			fileName = "";
		}
		if (fileTypeCode == null) {
			fileTypeCode = "";
		}

		try {

			item = (IDataObject) session
					.getObject(ServiceRequestConstants.CLASS_PRODUCT_SERVICE_REQUEST_BASE_CLASS, psrName);
			logger.debug(" item " + item);
			
			if(item!=null)
			{
				psrType = item.getCell(ServiceRequestConstants.ATT_COVER_PAGE_PSR_TYPE).toString();
			}

			logger.debug("The PSR Object type is ::" + psrType);
			String sPSRTypes= applicationProperties.getProperty("PSR_VALID_TYPES").trim();
			st = new StringTokenizer(sPSRTypes, ",");
			while (st.hasMoreElements()) {
				String psrTypeFromProp = (String) st.nextElement();
				if ((psrType).equalsIgnoreCase(psrTypeFromProp)) {
					
					validPSRType = true;
				}
			}
			if (validPSRType==false) {
				msg = "File Download utility is available only for " + sPSRTypes + " PSR Types";
				return msg;
			
			} else {

				ITable attchmentTable = item.getTable(ServiceRequestConstants.TABLE_ATTACHMENTS);
				itr = attchmentTable.iterator();
				Vector<String> fileNames = new Vector<String>();
				while (itr.hasNext()) {

					IRow row = (IRow) itr.next();
					String sAttachmentName = row.getValue(ServiceRequestConstants.ATT_ATTACHMENTS_FILENAME).toString();
					sAttachmentName = sAttachmentName.toLowerCase();
					logger.debug("The PSR Object's Attachment Name ::" + sAttachmentName);
					fileName = fileName.toLowerCase();
					logger.debug("File Name provided in by user ::" + fileName);
					fileTypeCode = fileTypeCode.toLowerCase();
					logger.debug("File Type Code provided in by user ::" + fileTypeCode);
					
					if (sAttachmentName.equalsIgnoreCase(fileName)) {
						fileNames.addElement(sAttachmentName);
					}

					st = new StringTokenizer(fileTypeCode, ",");
					while (st.hasMoreElements()) {

						String fileExtension = (String) st.nextElement();
						logger.debug("fileExtension after trimming the file type code ::" + fileExtension);
						
						if (fileExtension.contains("*") || fileExtension.contains(".")) {
							fileExtension = fileExtension.substring(fileExtension.indexOf("."), fileExtension.length());
						}

						if ((sAttachmentName.contains(fileExtension))) {
							fileNames.addElement(sAttachmentName);
						}
					}
				}
				logger.debug("fileNames vector ::" + fileNames);
				if (fileNames.isEmpty()) {
					msg = "PSR Does not contains the given file name or file type";
				}
			}
		} catch (APIException ex) {
			logger.error("An exception has occured while validating the PSR type: " + ex.getLocalizedMessage());
		}
		finally{
			logger.debug(":: Nullyfying the variable in Validate PSR::");
			item = null;
			st = null;
			itr=null;
		}

		return msg;
	}

}
