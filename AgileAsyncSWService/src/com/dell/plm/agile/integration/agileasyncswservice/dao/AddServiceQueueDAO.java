/**
 * 
 */
package com.dell.plm.agile.integration.agileasyncswservice.dao;

import java.net.InetAddress;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.agile.api.APIException;
import com.dell.plm.agile.aic.common.cryptography.FileEncryptorUtil;
import com.dell.plm.agile.aic.common.dao.DBConnectionManagerSingleton;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.agileasyncswservice.util.Utility;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class AddServiceQueueDAO 
{
	private static Logger logger = Logger.getLogger(AddServiceQueueDAO.class);
	
	public static String DB_PROPERTIES = "";
	private static Properties applicationProperties = null;
	private static String configBase = null;
	
	public void initailizeLogs()
	{
		/**Initialize the logs */
		try
		{
			getAicConfigBasePath("asynchwebservice");
			//URL url = getClass().getResource("/filedownloadservice_log4j.xml");
			StringBuffer log4jPath = new StringBuffer(configBase);
			log4jPath.append("\\config").append("\\filedownloadservice_log4j.xml");
			DOMConfigurator.configure(log4jPath.toString());
		}catch(Exception ex){
			System.out.println("Unable to Configure Log4j"+ex.getMessage());
		}
	}

	
	/**
	 * @throws Exception
	 */
	public Connection getDBConnection() throws Exception 
	{

		initailizeLogs();
		Connection connection = null;
		
		


		/*if (connection == null) {
			connection = DBInit.getDBConnection(Constants.AGILE_SERVICES, DB_PROPERTIES);
			System.out.println("Connection created..");
			logger.debug("Connection created..");
		}*/
		
		
		String DB_PROPERTIES = "";
		/**Loading Application properties*/
		//FilePropertyUtility fu = new FilePropertyUtility();
		logger.debug("loading properties..");
		getAicConfigBasePath("asynchwebservice");
		
		applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_filedownload_application.properties");
		//loadProperties("/aic_filedownload_application.properties");
		DB_PROPERTIES = applicationProperties.getProperty("DBPROP_FILEPATH").trim(); 
		//DB_PROPERTIES = configBase+"\\"+DB_PROPERTIES; // Ajit-->Consolidation to properties file		
		System.out.println("DB Properties Path :" + DB_PROPERTIES);
		logger.debug("DB Properties Path :" + DB_PROPERTIES);
		
		
		//FileEncryptorUtil.encryptFile(DB_PROPERTIES);
		logger.debug("Encrypt the File done");
		Properties asyncDBProperties = FilePropertyUtility.loadProperties(DB_PROPERTIES);
		//Properties asyncDBProperties = fu.loadWebAppProperties("/asyncdb.properties");
		/*String dbUrl = asyncDBProperties.getProperty("DB_URL").trim();*/ //********************** Ajit-->Consolidation to properties file
		String dbUrl = asyncDBProperties.getProperty("ESB_DB_URL").trim();
		/*String dbUserName = asyncDBProperties.getProperty("DB_USERNAME").trim();*/ // ********************* Ajit-->Consolidation to properties file
		String dbUserName = asyncDBProperties.getProperty("ESB_DB_SCHEMA_AGILE_SERVICES_USERNAME").trim();
		/*String dbPassword = asyncDBProperties.getProperty("DB_PASSWORD").trim();*/ // ************************ Ajit-->Consolidation to properties file
		String dbPassword = asyncDBProperties.getProperty("ESB_DB_SCHEMA_AGILE_SERVICES_PASSWORD").trim();
		Utility util = new Utility();
		dbPassword = util.getDecryptPwd(dbPassword);
		logger.debug("url:"+dbUrl+"userName:"+dbUserName);
		if(dbUrl == null || dbUrl.equalsIgnoreCase("") || dbUserName == null || dbUserName.equalsIgnoreCase("") || dbPassword == null || dbPassword.equalsIgnoreCase(""))
		{
			logger.error("Properties file Not Loaded .."+"url:"+dbUrl+"userName:"+dbUserName);
			throw new APIException(null, null, "Can't load properties file");
		}
		
		//logger.debug("after decrypt:"+dbPassword);
		DBConnectionManagerSingleton dms = DBConnectionManagerSingleton.getInstance();
		connection = dms.getConnection(dbUrl, dbUserName, dbPassword);
		logger.debug("getDBConnection():" + connection);
		return connection;
	}
	
		
	/**
	 * @param connection This is Database Connection
	 * @param action     Action number for example FileDownload Action is 1(check servicequeue_actionlist table)
	 * @param dependentqueueid This is internal logic
	 * @param messageID  Unique MessageID to identify the message
	 * @param downstreamName Name of the DownStream
	 * @param swb_Number  Software Bundle Part Number
	 * @param swb_Rev     Software Bundle Revision
	 * @param folderNumber Folder Number
	 * @param folderVersion Folder Version
	 * @param fileName      FileName
	 * @param fileTypeCode  File Type Code
	 * @param objectInfo    Object Info (like WPCO Numberj, DSCR Number etc.)
	 * @param downStreamUserName User Name of the DownStream
	 * @param batch_process_Id This is to identify the batch process
	 * @param batch_process This is batch process 'Y' or 'N'
	 * @return CallableStatement
	 * @throws Exception
	 */
	public CallableStatement addServiceQueue(Connection connection, String action, int dependentqueueid, String messageID, String downstreamName,
			String swb_Number, String swb_Rev, String folderNumber,
			String folderVersion, String fileName, String fileTypeCode,String objectInfo,String downStreamUserName,
			String batch_process_Id, String batch_process) throws Exception
	{
		System.out.println("Enter into addServiceQueue++++1");
		logger.debug("add ServiceQueue Method Start");
		CallableStatement insertReqStmt = null;
		String servername = InetAddress.getLocalHost().getHostName();
		logger.debug("servername"+servername);		
		insertReqStmt = connection.prepareCall("{call AIC_QUEUESERVICE_PKG.prc_addServiceQueue(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
		// insertReqStmt.setString(1,action);
		/**1 is Action Parameter (PCID Action, FileDownLoad Action=1, ZIPACK Action= 4 )*/
		insertReqStmt.setString(1, action);
		/**2 is dependentqueueid parameter*/
		insertReqStmt.setInt(2, dependentqueueid);
		/**3 is messageID */
		insertReqStmt.setString(3, messageID);
		/**4 is requestor (downStreamName)*/
		insertReqStmt.setString(4, downstreamName);
		/**5 is part_number (swb_number)*/
		insertReqStmt.setString(5, swb_Number);
		/**6 is part_revision (swb_rev)*/
		insertReqStmt.setString(6, swb_Rev);
		/**7 is folderNumber*/
		insertReqStmt.setString(7, folderNumber);
		/**8 is folderversion*/
		insertReqStmt.setString(8, folderVersion);
		//System.out.println("Enter into addServiceQueue++++2");
		/**9 is filename*/
		insertReqStmt.setString(9, fileName);
		/**10 is filetypecode*/
		insertReqStmt.setString(10, fileTypeCode);
		/**11 is queuestatusid.(NEW=1,INPROGRESS=2,COMPLETED=3,FAIL=4*/
		insertReqStmt.setString(11, "1");
		/**12 is objectinfo.*/
		insertReqStmt.setString(12, objectInfo);
		/**13 is requestor_username.*/
		insertReqStmt.setString(13, downStreamUserName);
		/**14 is batch_process_id.*/
		insertReqStmt.setString(14, batch_process_Id);
		/**15 is batch_process.*/
		insertReqStmt.setString(15, batch_process);
		/**16 is servername.*/
		insertReqStmt.setString(16, servername);
		//System.out.println("Enter into addServiceQueue++++3");
		logger.debug("add ServiceQueue Method End"+insertReqStmt);
		System.out.println("End addServiceQueue...");
		return insertReqStmt;
		
	}
	
	/**
	 * Returns the config base of the type passed
	 * @param type
	 * @return
	 */
	private String getAicConfigBasePath(final String type)
	{
		if(configBase == null)
		{
			configBase = FilePropertyUtility.getConfigBasePath(type);
		}
		return configBase;
	}
}
