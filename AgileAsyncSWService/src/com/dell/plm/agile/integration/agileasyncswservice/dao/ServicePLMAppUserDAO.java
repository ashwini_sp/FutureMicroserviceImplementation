package com.dell.plm.agile.integration.agileasyncswservice.dao;

import java.sql.Connection;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import com.dell.plm.agile.aic.common.cryptography.FileEncryptorUtil;
import com.dell.plm.agile.aic.common.dao.DBConnectionManagerSingleton;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.agileasyncswservice.util.Utility;

/**
 * @author Vumesh_Gundapuneni
 *
 */
public class ServicePLMAppUserDAO {
	private static Logger logger = Logger.getLogger(ServicePLMAppUserDAO.class);
	private static String configBase = null;
	
	/**
	 * Initializing log4j properties
	 */
	public void initailizeLogs(){
		try
		{
			getAicConfigBasePath("asynchwebservice");
			StringBuffer log4jPath = new StringBuffer(configBase);
			log4jPath.append("\\config").append("\\filedownloadservice_log4j.xml");
			DOMConfigurator.configure(log4jPath.toString());
		}catch(Exception ex){
			System.out.println("Unable to Configure Log4j"+ex.getMessage());
		}
	}
	
	/**
	 * This method will get the connection for Service PLM App User
	 * @return Connection - connection object
	 * @throws Exception
	 */
	public Connection getServicePlmUserDBConnection() throws Exception 
	{
		Connection connection = null;
		String DB_PROPERTIES = "";
		String svcPlmDBUrl = "";
		String svcPlmDBUserName = "";
		String svcPlmDBPassword = "";
		DBConnectionManagerSingleton dms = null;
		Properties srvicePLMUserDBProperties = null;
		Properties applicationProperties = null;
		Utility util = null;
		
		try {
			initailizeLogs();
			getAicConfigBasePath("asynchwebservice");
			
			logger.debug("loading application properties...");
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_filedownload_application.properties");
			
			DB_PROPERTIES = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
			//DB_PROPERTIES = configBase+"\\"+DB_PROPERTIES; // Ajit-->Consolidation to properties file
			logger.debug("DB Properties Path :" + DB_PROPERTIES);
					
			//FileEncryptorUtil.encryptFile(DB_PROPERTIES); // Ajit-->
			logger.debug("Encrypt the File done");
			
			logger.debug("loading db properties...");
			srvicePLMUserDBProperties = FilePropertyUtility.loadProperties(DB_PROPERTIES);
			
			/*svcPlmDBUrl = srvicePLMUserDBProperties.getProperty("SERVICEPLM_DB_URL").trim();*/ //Ajit-->Consolidation to properties file
			svcPlmDBUrl = srvicePLMUserDBProperties.getProperty("ESB_DB_URL").trim();
			logger.debug("svcPlmDBUrl ==>"+svcPlmDBUrl);
			
			/*svcPlmDBUserName = srvicePLMUserDBProperties.getProperty("SERVICEPLM_DB_USERNAME").trim();*/ //Ajit-->Consolidation to properties file
			svcPlmDBUserName = srvicePLMUserDBProperties.getProperty("ESB_DB_SCHEMA_SERVICEPLM_APPUSER_USERNAME").trim();
			logger.debug("svcPlmDBUserName ==>"+svcPlmDBUserName);
			
			/*svcPlmDBPassword = srvicePLMUserDBProperties.getProperty("SERVICEPLM_DB_PASSWORD").trim();*/ // Ajit-->Consolidation to properties file
			svcPlmDBPassword = srvicePLMUserDBProperties.getProperty("ESB_DB_SCHEMA_SERVICEPLM_APPUSER_PASSWORD").trim();
			
			util = new Utility();
			svcPlmDBPassword = util.getDecryptPwd(svcPlmDBPassword);
			
			if(svcPlmDBUrl==null || svcPlmDBUrl.equalsIgnoreCase("")||svcPlmDBUserName == null||svcPlmDBUserName.equalsIgnoreCase("")||svcPlmDBPassword == null||svcPlmDBPassword.equalsIgnoreCase(""))	{
				logger.error("Properties file Not Loaded .."+"url:"+svcPlmDBUrl+"userName:"+svcPlmDBUrl);
				throw new Exception("Can't load properties files");
			}
						
			dms = DBConnectionManagerSingleton.getInstance();
			connection = dms.getConnection(svcPlmDBUrl, svcPlmDBUserName, svcPlmDBPassword);
			logger.debug("getDBConnection():" + connection);
		}
		catch(Exception e){
			logger.error("Exception in getDBConnection Method==>"+e.getMessage());
			throw e;
		} finally {
			DB_PROPERTIES = null;
			svcPlmDBUrl = null;
			svcPlmDBUserName = null;
			svcPlmDBPassword = null;
			srvicePLMUserDBProperties=null;
			applicationProperties=null;
			if(dms!=null)
				dms = null;
			util = null;
		}
		return connection;
	}
	
	/**
	 * Getting the base path
	 * @param type
	 * @return
	 */
	private String getAicConfigBasePath(final String type){
		if(configBase == null){
			configBase = FilePropertyUtility.getConfigBasePath(type);
		}
		return configBase;
	}
}
