/*
 * LDAPAuthenticationImpl.java  
 * 
 * Version information
 * Date: 16-Sept-2011
 * 
 * Copyright : DELL confidential
 * 
 */
package com.dell.plm.agile.integration.agileasyncswservice.ldap;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.security.sasl.AuthenticationException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.ws.security.WSPasswordCallback;

import com.dell.plm.agile.integration.agileasyncswservice.AgileAsyncSWService;
import com.dell.plm.agile.integration.agileasyncswservice.ldap.constants.LDAPConstants;
import com.dell.plm.agile.integration.agileasyncswservice.ldap.vo.LDAPConfigVO;


/**
 * Authenticates the user with LDAP depending upon the domain passed . LDAP properties are configured in properties file.
 * 
 * 
 * 		-------     ----------   		 -----------------		-------------------------
 * 		Version 	  Author	 		 Last modified date			Remarks(Modification)
 * 		------      ----------			 ------------------		------------------------
 * 		1.0			Yathiraja_Lingadahal	16-Sept-2011  			Created this class.	
 * 		1.1     	Yathiraja_Lingadahal    19-Sept-2011           Added validation with allowed production service/process user accounts 
 * 		
 */
public class LDAPAuthenticationImpl {
	
	Logger logger = Logger.getLogger(LDAPAuthenticationImpl.class);
	
    /**
     * Validates the user with ldap.
     *  
     * @param vo
     * @return
     */
   private  boolean validateUser(final LDAPConfigVO ldapConfigVO) throws NamingException {
	   
	   logger.info("starts validating user");
	   
        String ctxt = ldapConfigVO.getLadapContextFactory();
         String provider = ldapConfigVO.getProvider();
        String principal = ldapConfigVO.getPrincipal();
        String password = ldapConfigVO.getLdapPassword();
                
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, ctxt);
        env.put(Context.PROVIDER_URL, provider);
        env.put(Context.SECURITY_PRINCIPAL,principal);
        env.put(Context.SECURITY_CREDENTIALS, password);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");

        DirContext dctxt = new InitialDirContext(env);
        dctxt.close();
        logger.info("ends validating user");
        return true;
    }
   
   /**
     * Authenticates the user against ldap
	 * @param vo
	 * @throws AuthenticationException
	 */
	private void authenticateUser(final LDAPConfigVO vo) throws AuthenticationException 
	{
		try {
			 boolean flag = validateUser(vo);

           if (flag) {
           	logger.info("**********LDAP authentication is passed************");
            }
       } catch (NamingException e) {
            throw new AuthenticationException(e.getCause()+":"+ e.getMessage(), e);
       }

	}
	
	/**
	 * Sets properties LDAPConfigVO with the configured properties.
	 * @param vo
	 * @param applicationProperties
	 */
	private void setLDAPConfigVO(final LDAPConfigVO vo,final Properties ldapProperties) {
		
		logger.info("starts forming LDAPConfigVO with configured properties");

		String ctxt = ldapProperties.getProperty(LDAPConstants.CONTEXT_FACTORY_LDAP);
		vo.setLadapContextFactory(ctxt);

		String protocol = ldapProperties.getProperty(LDAPConstants.PROTOCOL_LDAP);
		String port = ldapProperties.getProperty(LDAPConstants.PORT_LDAP);
		String baseDN = null;
		String principal = null;
		String host = null;
		String keyStore = null;

		if ("AMERICAS".equalsIgnoreCase(vo.getDomain())) {
			host = ldapProperties.getProperty(LDAPConstants.HOST_AMERICAS).trim();
			baseDN = ldapProperties.getProperty(LDAPConstants.BASEDN_AMERICAS).trim();
			principal = ldapProperties.getProperty(LDAPConstants.PRINCIPAL_AMERICAS);

		} else if ("ASIA-PACIFIC".equalsIgnoreCase(vo.getDomain())) {
			host = ldapProperties.getProperty(LDAPConstants.HOST_ASIA_PACIFIC).trim();
			baseDN = ldapProperties.getProperty(LDAPConstants.BASEDN_ASIA_PACIFIC).trim();
			principal = ldapProperties.getProperty(LDAPConstants.PRINCIPAL_ASIA_PACIFIC).trim();
		}
		keyStore = ldapProperties.getProperty(LDAPConstants.KEYSTORE);
		vo.setPort(port);
		vo.setHost(host);
		vo.setBaseDN(baseDN);
		vo.setKeystore(keyStore);

		String userId = vo.getLdapUser();
		String formattedPrincipal = MessageFormat.format(principal, userId);
		vo.setPrincipal(formattedPrincipal);

		if (vo.isEnableSSL()) {
			// env.put(Context.SECURITY_PROTOCOL, "ssl");
			System.setProperty("javax.net.ssl.trustStore", vo.getKeystore());
			protocol += "s";
		}

		String provider = protocol + "://" + vo.getHost() + ":" + vo.getPort();

		vo.setProtocol(protocol);

		if (vo.getBaseDN() != null) {
			provider = provider + "/" + vo.getBaseDN();
		}
		vo.setProvider(provider);
		
		logger.info("ends forming LDAPConfigVO with configured properties");
	}
	
	/**
	 * Does ldap authentication.
	 * @param pc
	 * @param ldapProperties
	 * @throws IOException 
	 */
	public void doLDAPAuthentication(WSPasswordCallback pc,Properties ldapProperties) throws IOException
	{
		String userDomain = null;
		String userName = null;
		String password = null;
		String userNameLoggedIn = pc.getIdentifier();
 	   logger.info("***********Authenticating user with LDAP*************");
 	   	LDAPConfigVO ldapConfigVO = new LDAPConfigVO();
 
 	   	if(null==ldapProperties)
 	   	{
 	   		logger.error("LDAP property file is not loaded:ldapProperties is "+ldapProperties);
 	   		throw new IOException("LDAP property file is not loaded.");
 	   	}
 	   	setUserAccountsMap(ldapConfigVO,ldapProperties);
 	   	
     	String enabledSSL = ldapProperties.getProperty(LDAPConstants.ENABLED_SSL);
     	String msg = null;
     	if(enabledSSL == null )
     	{
     		enabledSSL = "false";
     	}
     	boolean isEnabledSSL = Boolean.parseBoolean(enabledSSL.trim());
         ldapConfigVO.setEnableSSL(isEnabledSSL);
         if(userNameLoggedIn.contains("\\"))
         {
         	 
        	 boolean accountAllowed = isAccountAllowed(userNameLoggedIn,ldapConfigVO);
        	 if(accountAllowed)
        	 {
	        	 userDomain = StringUtils.substringBefore(userNameLoggedIn, "\\");
	             userName = StringUtils.substringAfter(userNameLoggedIn, "\\");
	             //Added by Manoj Dugaya for PSR File Download (PQM Attachments) Start.
	              AgileAsyncSWService.psrUser = userName;
	             //Added by Manoj Dugaya for PSR File Download (PQM Attachments) End. 
	              boolean isService = StringUtils.containsIgnoreCase(userName, "service");
	              boolean isProcess = StringUtils.containsIgnoreCase(userName, "process");
	              
	              if(!(isService || isProcess))
	              {
	             	 msg = ldapProperties.getProperty("MSG_INVALID_SERVICE_USERNAME");
	             	 logger.error(msg);
	             	 throw new IOException(msg);
	              }
	              
	              ldapConfigVO.setLdapUser(userName);
	              ldapConfigVO.setDomain(userDomain);
	              String domain_amer = ldapProperties.getProperty(LDAPConstants.DOMAIN_AMERICAS);
	              String domain_asia = ldapProperties.getProperty(LDAPConstants.DOMAIN_ASIA_PACIFIC);
	              if(!userDomain.equalsIgnoreCase(domain_amer) && !userDomain.equalsIgnoreCase(domain_asia))
	              {
	             	 msg = ldapProperties.getProperty("MSG_INVALID_DOMAIN");
	             	 logger.error(msg);
	             	 throw new IOException(msg);
	              }
        	 }
        	 else
        	 {
        		 msg = ldapProperties.getProperty("MSG_USER_ACCOUNT_NOTALLOWED");
             	 logger.error(msg);
             	 throw new IOException(msg);
        	 }
         }
         else
         {
         	msg = ldapProperties.getProperty("MSG_INVALID_USERNAME");
         	logger.error(msg);
         	throw new IOException(msg);
         }
        
         password= pc.getPassword();
         
         if(null == password)
         {
         	msg = ldapProperties.getProperty("MSG_PASSWORD_TYPE");
         	logger.error(msg);
         	throw new IOException(msg);
         }
         ldapConfigVO.setLdapPassword(password);
        
         setLDAPConfigVO(ldapConfigVO,ldapProperties);
         
         try{
         	authenticateUser(ldapConfigVO);
         	logger.info("***********Done authenticating user with LDAP*************");
         }catch(AuthenticationException e)
         {
         	String exception = e.getMessage();
          	logger.error("LDAP authentication failed::"+exception);
         	logger.error("Cause ::"+e.getCause());
         	msg = ldapProperties.getProperty("MSG_LDAP_AUTH_FAIL");
          	throw new IOException(msg);
       
         }
     
	}
	
	/**
	 * Checks wheteher current user account is allowed or not
	 * @param userAccount
	 * @param ldapConfigVO
	 * @return
	 */
	private boolean isAccountAllowed(final String userAccount,final LDAPConfigVO ldapConfigVO)
	{
		boolean accountAllowed= false;
		Map<String, String> userAccountsMap = ldapConfigVO.getUserAccountsMap();
		
		if(null != userAccountsMap && userAccountsMap.size()>0)
		{
			Set<String> accountKeySet = userAccountsMap.keySet();
			Iterator<String> accountIter = accountKeySet.iterator();
			while(accountIter.hasNext())
			{
				String accKey = accountIter.next();
				String userAcc = userAccountsMap.get(accKey);
				if(userAcc.equalsIgnoreCase(userAccount))
				{
					accountAllowed = true;
					break;
				}
			}
		}
		return accountAllowed;
	}
	
	/**
	 * Sets allowed user accounts for LDAP authentication which are configured in ldap properties .
	 * @param ldapConfigVO
	 * @param ldapProperties
	 */
	private void setUserAccountsMap(LDAPConfigVO ldapConfigVO,Properties ldapProperties)
	{
		Map<String, String> userAccountsMap = new HashMap<String, String>();
		
		Set accountKeySet = ldapProperties.keySet();
		Iterator accountIter = accountKeySet.iterator();
		while(accountIter.hasNext())
		{
			String accKey = (String)accountIter.next();
			String userAcc = null;
			if(accKey.startsWith("ACC_DOWNSTREAM"))
			{
				userAcc = ldapProperties.getProperty(accKey);
				userAccountsMap.put(accKey.trim(), userAcc.trim());
			}
		}
		
		ldapConfigVO.setUserAccountsMap(userAccountsMap);
	}

	
}
