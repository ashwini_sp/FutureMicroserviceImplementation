package com.dell.plm.agile.integration.agileasyncswservice.model;

public class AckUpdateResponse 
{
	private String status;
	private String objectId;
	private String failureReason;
	private String comments;	

	public String getFailureReason() {
		return failureReason;
	}

	public void setFailureReason(String failureReason) {
		this.failureReason = failureReason;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getStatus() 
	{
		return status;
	}

	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getObjectId() 
	{
		return objectId;
	}

	public void setObjectId(String objectId) 
	{
		this.objectId = objectId;
	}
}
