package com.dell.plm.agile.integration.agileasyncswservice.model;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class Response 
{
	private String status;
	private String objectId;
	private String validationError;
	//private Status status;

	
	/**
	 * @return
	 */
	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getValidationError() {
		return validationError;
	}

	public void setValidationError(String validationError) {
		this.validationError = validationError;
	}
	
	public String toString(){
		return "status:"+status+"objectId:"+objectId+"validationError:"+validationError;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/*public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}*/
}
