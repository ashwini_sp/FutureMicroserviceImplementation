package com.dell.plm.agile.integration.agileasyncswservice.model;

import com.dell.plm.agile.integration.agileasyncswservice.bean.FileChangeInfo;

public class ResponseStatus {

	private String status;
	private String objectId;
	private String validationError;
	private FileChangeInfo fileStatus[];
	private FileChangeInfo changeStatus;
	private String overallStatus;
	private String swbMessageId;
	
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return the objectId
	 */
	public String getObjectId() {
		return objectId;
	}
	
	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	
	/**
	 * @return the validationError
	 */
	public String getValidationError() {
		return validationError;
	}
	
	/**
	 * @param validationError the validationError to set
	 */
	public void setValidationError(String validationError) {
		this.validationError = validationError;
	}
	
	/**
	 * @return the fileStatus
	 */
	public FileChangeInfo[] getFileStatus() {
		return fileStatus;
	}

	/**
	 * @param fileStatus the fileStatus to set
	 */
	public void setFileStatus(FileChangeInfo[] fileStatus) {
		this.fileStatus = fileStatus;
	}

	/**
	 * @return the changeStatus
	 */
	public FileChangeInfo getChangeStatus() {
		return changeStatus;
	}

	/**
	 * @param changeStatus the changeStatus to set
	 */
	public void setChangeStatus(FileChangeInfo changeStatus) {
		this.changeStatus = changeStatus;
	}

	/**
	 * @return the overallStatus
	 */
	public String getOverallStatus() {
		return overallStatus;
	}

	/**
	 * @param overallStatus the overallStatus to set
	 */
	public void setOverallStatus(String overallStatus) {
		this.overallStatus = overallStatus;
	}

	/**
	 * @return the swbMessageId
	 */
	public String getSwbMessageId() {
		return swbMessageId;
	}

	/**
	 * @param swbMessageId the swbMessageId to set
	 */
	public void setSwbMessageId(String swbMessageId) {
		this.swbMessageId = swbMessageId;
	}
}
