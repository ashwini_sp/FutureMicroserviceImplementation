package com.dell.plm.agile.integration.agileasyncswservice.util;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class Constants
{
	   public static final String RCD = "rcd";
	   public static final String RCD_REQUESTOR = "RCD";
	   public static final String FILEDOWNLOAD_ACTION = "1";
	   public static final String RCD_ACTION = "2";
	   public static final String RCD_DOWNLOAD_ACK = "3";
	   public static final String DSFORM_ACTION = "5";
	   public static final String PCI_ID_ACTION = "8";
	   public static final String CFGREADER_ACTION = "14";
	   
	   // This action added as part of DBT(Drivers By Tag) Project  
	   public static final String SRV_DETAILS_FOR_SWB_ACTION = "17";
	   
	   public static final String DEE_TEMP_TABLE="DEE_TEMP_SERVICEQUEUE";
	   
	   /** Added for getDUPFileStatus Method */
	   public static final String DUP_GET_FILE_STATUS_PROC="DUP_GET_FILE_STATUS_PROC";
	   public static final String LOG_MSG_SIZE_LIMIT="LOG_MSG_SIZE_LIMIT";
	   public static final String IN_PROGRESS_NUMBER = "IN_PROGRESS_NUMBER";
	   public static final String SUCCESS_NUMBER = "SUCCESS_NUMBER";
	   public static final String ERROR_NUMBER = "ERROR_NUMBER";
	   
	   
	   /** Added for createAutoReGenerationWPCO Method */
	   public static final String DUP_SP_RT_WPCO_TRK_PROC="DUP_SP_RT_WPCO_TRK_PROC";
	   public static final String DUP_CREATE_BY_USER="DUP_CREATE_BY_USER";
	   public static final String DUP_REQUESTOR_USER_NAME="DUP_REQUESTOR_USER_NAME";
	   
	   /** Added for requestDUPFileAction Web Method */
	   public static final String DUP_SWB_FILE_ACTION = "DUP_SWB_FILE_ACTION";
	   public static final String DUP_SWB_FILE_BATCH_ACTION = "DUP_SWB_FILE_BATCH_ACTION";
	   public static final String PENDING_AREV_CHAR  = "(A";
	   public static final String RELEASED_AREV_CHAR = "A";
	   public static final String RELEASED_XREV_CHAR = "X";
	   public static final String XREVISION = "XRevision";
	   public static final String AREVISION  = "ARevision";
	   public static final String CHANGE_NUMBER = "changeNumber";
	   public static final String CHANGE_STATUS = "changeStatus";
	   public static final String CANCELLED = "Canceled";
	   public static final String HOLD = "Hold";
	   public static final String PNR = "PNR";
	   public static final String PNCR = "PNCR";
	   public static final String ECO = "ECO";
	   public static final String UNASSIGNED= "Unassigned";
	   public static final String CHANGE_PENDING_STATUS = "Pending";
	   public static final String CHANGE_SUBMIT2BLOCK_STATUS = "Submit to Block";
	   public static final String CHANGE_CANCELLED_STATUS = "Cancelled";
	   public static final String AIC_TABLE_COLUMN_CODE = "AIC_TABLE_COLUMN_CODE";
	   public static final String AIC_TABLE_COLUMN_EXTENSION = "AIC_TABLE_COLUMN_EXTENSION";
	   public static final String AIC_AGILE_FILETYPECODES_PROC = "AIC_AGILE_FILETYPECODES_PROC";
	   public static final String FOLDER_NAME ="_FOLDER_NAME";
	   
	   /**This action being added for PSR (BTO-FACE/CFIPR) File Download **/ 
	   public static final String PSR_FILE_DOWNLOAD_ACTION = "34";
}

