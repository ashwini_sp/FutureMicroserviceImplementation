package com.dell.plm.agile.integration.agileasyncswservice.util;

import java.util.HashMap;
import java.util.StringTokenizer;

import com.agile.api.APIException;
import com.agile.api.AgileSessionFactory;
import com.agile.api.IAgileSession;
import com.dell.plm.agile.aic.common.cryptography.StringEncryptor;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class Utility 
{
	
	//private static Logger logger = Logger.getLogger(Utility.class);
	
	/**
	 * @param dscrDownStreamNames
	 * @param downStreamName
	 * @return boolean
	 */
	public boolean isValidDownStream(String dscrDownStreamNames, String downStreamName) 
	{
		boolean isFound = false;
		String token = "";
		StringTokenizer tokenizer = new StringTokenizer(dscrDownStreamNames, ",");
		while (tokenizer.hasMoreElements()) 
		{
			token = tokenizer.nextToken(); 
			if (token.equalsIgnoreCase(downStreamName)) 
			{
				isFound = true;
			} 
		}
		return isFound;
	}
	
	/**
	 * @param ecryptPwd
	 * @return
	 */
	public String getDecryptPwd(String ecryptPwd)
	{
		String decryptPwd = ""; 
		decryptPwd = StringEncryptor.decrypt(ecryptPwd);
		return decryptPwd;
	}
	
	/**
	 * This method creates Agile session
	 * @param url
	 * @param username
	 * @param password
	 * @return IAgileSession
	 * @throws APIException
	 */
	
	public static IAgileSession createSession(String url, String username, String password) throws APIException 
	{
		IAgileSession session = null;
		
		
		AgileSessionFactory factory = AgileSessionFactory.getInstance(url);
		
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(AgileSessionFactory.USERNAME, username);
		map.put(AgileSessionFactory.PASSWORD, password);
		
		session = factory.createSession(map);
		return session;
	}
	
}
