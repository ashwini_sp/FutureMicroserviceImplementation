package com.dell.plm.agile.integration.agilesyncwservice;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.agile.api.APIException;
import com.agile.api.IAgileSession;
import com.agile.api.IAttribute;
import com.agile.api.IItem;
import com.agile.api.IQuery;
import com.agile.api.IRow;
import com.agile.api.ITable;
import com.agile.api.ItemConstants;
import com.agile.api.QueryConstants;
import com.dell.plm.agile.aic.common.cryptography.FileEncryptorUtil;
import com.dell.plm.agile.aic.common.cryptography.StringEncryptor;
import com.dell.plm.agile.aic.common.exception.ValidationException;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.bean.SWBCreateRequest;
import com.dell.plm.agile.integration.bean.SWBFirstLevelWURequest;
import com.dell.plm.agile.integration.bo.afpupdate.AFPUpdateBO;
import com.dell.plm.agile.integration.bo.dpk.DpkPartretriveBO;
import com.dell.plm.agile.integration.bo.osfiltering.OsToFileTypeCodeBO;
import com.dell.plm.agile.integration.bo.oslang.OsLangMatrixBO;
import com.dell.plm.agile.integration.bo.partAttribute.UpdatePartAttributeBO;
import com.dell.plm.agile.integration.bo.partDetails.SWBDetailsBO;
import com.dell.plm.agile.integration.bo.partDetails.SWBStatusBO;
//D&D change 1
//import com.dell.plm.agile.integration.bo.predecessors.SWBPredecessorBO;
import com.dell.plm.agile.integration.bo.predecessorsuccessor.SWBPredecessorSuccessorBO;
//D&D changes over
import com.dell.plm.agile.integration.bo.prereqcoreq.SWBPrereqCoreqBO;
import com.dell.plm.agile.integration.bo.swbAutoCreation.SWBAutoGeneration;
import com.dell.plm.agile.integration.dto.AFPResponse;
import com.dell.plm.agile.integration.dto.PartResponse;
import com.dell.plm.agile.integration.dto.Response;
import com.dell.plm.agile.integration.dto.SWBAutoCreationResponse;
import com.dell.plm.agile.integration.dto.SWBFirstLevelWUResponse;
import com.dell.plm.agile.integration.dto.SWBResponse;
import com.dell.plm.agile.integration.dto.SuccessorResponse;
import com.dell.plm.agile.integration.util.Constants;
import com.dell.plm.agile.integration.util.Utility;

//enum Status{Success, Failure};

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class AgileSyncSWService
{

	public AgileSyncSWService() {
		super();
	}

	public static String DB_PROPERTIES = "";
	public static String Username = "";
	private static final Logger logger = Logger.getLogger(AgileSyncSWService.class);

	private static String configBase = null;

	/**
	 * This method returns following details of the Part Number passed:
	 * 1)Predecessor chain
	 * 2)Successor chain 
	 * 3)OS lang matrix
	 * @param partNumber
	 * @return
	 * @throws Exception
	 */
	public SWBResponse getSWBDetails(String partNumber) throws Exception 
	{
		logger.debug("Inside getSWBDetails method---");
		SWBResponse response = new SWBResponse();
		StringBuffer stringBuffer = new StringBuffer("");
		IAgileSession session = null;
		IItem item = null;
		String partType=null;
		String partClass=null;
		Properties dbProperties = null;// Ajit--> Consolidation to properties file
		try 
		{
			configureLog4J();
			logger.debug("Log4j configured.");			

			if (StringUtils.isEmpty(partNumber) || partNumber.trim().equals("?")) 
			{
				stringBuffer.append("Part Number is mandatory.\n");
			}
			else
			{
				Properties applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
				
				//Loading here std.system config properties file path  // Ajit--> Consolidation to properties file
				String stdSys_config_prop_filepath = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
				dbProperties = FilePropertyUtility.loadProperties(stdSys_config_prop_filepath);
				/*String username = applicationProperties.getProperty("AGILE.USERNAME");*/ // Ajit--> Consolidation to properties file
				String username = dbProperties.getProperty("AIC_DM_USERNAME");
				/*String password = StringEncryptor.decrypt(applicationProperties.getProperty("AGILE.PASSWORD"));*/ // Ajit--> Consolidation to properties file
				String password = StringEncryptor.decrypt(dbProperties.getProperty("AIC_DM_PASSWORD"));
				/*String url = applicationProperties.getProperty("AGILE.URL");*/ // Ajit--> Consolidation to properties file
				String url = dbProperties.getProperty("AGILE_BIGIP_URL");
				session = Utility.createSession(url,username,password);
				logger.debug("Get Item Object");
				item= (IItem) session.getObject(IItem.OBJECT_TYPE, partNumber);
				if(item == null)
				{
					stringBuffer.append("Part '"+partNumber+"' doesn't exist in Agile");
				}
				else
				{
					partClass = item.getValue(ItemConstants.ATT_TITLE_BLOCK_PART_TYPE).toString();
					partType=item.getValue(ItemConstants.ATT_TITLE_BLOCK_PART_CATEGORY).toString();
					if(!"Software Bundle".equalsIgnoreCase(partClass) && !"Software Bundle".equalsIgnoreCase(partType))
					{
						stringBuffer.append("Part '"+partNumber+"' is  not a Software Bundle");
					}
				}
			}
			if(stringBuffer.length()> 0)
			{
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				logger.debug(str);
				throw new ValidationException(str);
			}
			else
			{
				SWBDetailsBO partDetailsBo = new SWBDetailsBO();
				logger.debug("Calling getPartDetailsXMLResponse method---");
				response = partDetailsBo.getPartDetailsXMLResponse(partNumber.toUpperCase(), response);
			}
		}
		catch(ValidationException ve)
		{
			logger.error("Validation Exception: "+ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		}
		finally
		{
			session = null;
			item = null;
			partType=null;
			partClass=null;
		}
		logger.debug("End of getSWBDetails Method.");
		return response;
	}
	
	/**
	 * This method returns the Status for a Software Bundle as 1.Active 2.Invalid 3.Demoted
	 * @param partNumber
	 * @return
	 * @throws Exception
	 */
	public AFPResponse getSWBStatus(String partNumber) throws Exception 
	{
		logger.debug("Method getSWBStatus invoked with input Part Number: " +partNumber);
		AFPResponse response = new AFPResponse();
		try 
		{
			configureLog4J();
			logger.debug("--Log4j configured ---");			

			//String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			Properties applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
			
			//Setting SWB Status as 'Invalid' if the input for Part Number is given as empty or '?'and throw Validation Error
			if (StringUtils.isEmpty(partNumber) || partNumber.trim().equals("?")) 
			{
				String str = applicationProperties.getProperty("INVALID_PARAM_PART_NUMBER");
				logger.debug(str);
				throw new ValidationException(str);
			}
			else
			{
				SWBStatusBO swbStatusBo = new SWBStatusBO();
				//Starting processing to determine Status of the Part Number given as input
				logger.debug("Calling setSwbStatus method---");
				response = swbStatusBo.setSwbStatus(partNumber.toUpperCase(), response);
			}
		}
		catch(ValidationException ve)
		{
			logger.error("Validation Exception: "+ve.getMessage());
			response.setStatus("Invalid");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
			logger.info("SWB Status-->" +response.getStatus() + "::: Reason-->" +response.getValidationError());
		}
		logger.info("Returning Response: " +response.toString());
		logger.debug("End of getSWBStatus method.");
		return response;
	}

	/**
	 * Returns the OS lang matrix of the part number passed in.
	 * @param part_number
	 * @return
	 * @throws Exception
	 */
	public Response getOSLangMatrix(String swbPartNumber) throws Exception 
	{
		Response response = new Response();
		StringBuffer stringBuffer = new StringBuffer("");
		try {
			configureLog4J();
			//encryptFile();

			if (StringUtils.isEmpty(swbPartNumber)) {
				stringBuffer.append("swbPartNumber is mandatory. \n");
			}

			/**Code to handle to check valid downstream name*/
			logger.debug("loading properties..");

			if(stringBuffer.length()> 0)
			{
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				logger.debug(str);
				throw new ValidationException(str);
			}
			else
			{
				OsLangMatrixBO osLangMatrix = new OsLangMatrixBO();
				logger.debug("getOSLangXMLResponse method call..");
				response = osLangMatrix.getOsLangXMLResponse(swbPartNumber, response);
			}

		}catch(ValidationException ve){
			logger.error("Validation Exception :"+ve.getMessage());
			//response.setStatus(Status.Failure);
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		} 

		return response;
	}

	/**
	 * Returns the all the predecessors of the swbNumber passed in.
	 * @param swbNumber 
	 * @return
	 * @throws Exception
	 */
	public Response getPredecessorsForSwbNumber(String swbNumber) throws Exception 
	{
		Response response = new Response();
		StringBuffer stringBuffer = new StringBuffer("");
		try {
			configureLog4J();
			logger.debug("Log4j configured...");			
			logger.debug("Starting getPredecessorsForSwbNumber() Method...");

			if (StringUtils.isEmpty(swbNumber)) {
				stringBuffer.append("swbNumber is mandatory. \n");
			}

			if(stringBuffer.length()> 0)
			{
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				logger.debug(str);
				throw new ValidationException(str);
			}
			else
			{
				//D&D change 2
				//SWBPredecessorBO swbPredecessorBo = new SWBPredecessorBO();
				SWBPredecessorSuccessorBO swbPredecessorBo = new SWBPredecessorSuccessorBO();
				//D&D change over

				logger.debug("Calling getPredecessorXMLResponse method ...");
				response = swbPredecessorBo.getPredecessorXMLResponse(swbNumber,response);
			}

		}catch(ValidationException ve){
			logger.error("Validation Exception :"+ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		} 
		logger.debug("End of getPredecessorsForSwbNumber() Method...");
		return response;
	}
	//D&D change 3
	/**
	 * Returns the all the successors of the swbNumber passed in.
	 * @param swbNumber 
	 * @return
	 * @throws Exception
	 */
	public SuccessorResponse getSuccessorsForSwbNumber(String swbNumber) throws Exception 
	{
		SuccessorResponse succResponse = new SuccessorResponse();
		StringBuffer stringBuffer = new StringBuffer("");
		try {
			configureLog4J();
			logger.debug("Log4j configured...");			
			logger.debug("Starting getSuccessorForSwbNumber() Method...");

			if (StringUtils.isEmpty(swbNumber)) {
				stringBuffer.append("swbNumber is mandatory. \n");
			}

			if(stringBuffer.length()> 0)
			{
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				logger.debug(str);
				throw new ValidationException(str);
			}
			else
			{
				SWBPredecessorSuccessorBO swbSuccessorBo = new SWBPredecessorSuccessorBO();

				logger.debug("Calling getSuccessorXMLResponse method ...");
				succResponse = swbSuccessorBo.getSuccessorXMLResponse(swbNumber,succResponse);
			}

		}catch(ValidationException ve){
			logger.error("Validation Exception :"+ve.getMessage());
			succResponse.setStatus("Failure");
			succResponse.setObjectId("");
			succResponse.setValidationError(ve.getMessage());
		} 
		logger.debug("End of getSuccessorForSwbNumber() Method...");
		return succResponse;
	}

	//D&D change over

	/**
	 * Returns the Prereq & Coreq Details of the Candidate Software Bundle Number passed in as Input
	 * to the method.
	 * @param  swbNumber
	 * @return Response
	 * @throws Exception
	 */
	public Response getPrereqCoreqDetails(String swbNumber) throws Exception
	{
		Response response = new Response();
		StringBuffer stringBuffer = new StringBuffer("");
		try {
			configureLog4J();
			logger.debug("Log4j configured for getPrereqCoreqDetails Method...");

			logger.debug("Starting getPrereqCoreqDetails() Method...");

			if (StringUtils.isEmpty(swbNumber)) {
				stringBuffer.append("swbNumber is mandatory. \n");
				logger.debug("The input Software Bundle Number is empty: "+swbNumber);
			}

			if(stringBuffer.length()> 0){
				String str = "Invalid Parameter:"+ stringBuffer.toString();
				logger.debug(str);
				throw new ValidationException(str);
			}
			else{
				SWBPrereqCoreqBO swbPrereqCoreqBo = new SWBPrereqCoreqBO();
				logger.debug("Invoking getPrereqCoreqXMLResponse method.");
				response = swbPrereqCoreqBo.getPrereqCoreqXMLResponse(swbNumber,response);
			}

		}catch(ValidationException ve){
			logger.error("Validation Exception: "+ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		} 
		logger.debug("End of getPrereqCoreqDetails() Method...");
		return response;
	}


	/**
	 * This web method will give File Type Code to OS 60 Code mapping from the mapping table from RCD or Agile Service Schema	
	 * @return Response
	 * @throws Exception
	 */
	public Response getOsToFileTypeCodeMapping() throws Exception
	{
		Response response = new Response();
		try{
			configureLog4J();
			logger.debug("Log4j Configured For getOsToFileTypeCodeMapping Method.");

			logger.debug("Starting getOsToFileTypeCodeMapping() Method.");
			OsToFileTypeCodeBO osToFileTypeCodeBo = new OsToFileTypeCodeBO();
			logger.debug("Invoking getOsToFileTypeCodeXMLResponse method.");
			response = osToFileTypeCodeBo.getOsToFileTypeCodeXMLResponse(response);
		}
		catch(Exception ve){
			logger.error("Exception: "+ve.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(ve.getMessage());
		} 
		logger.debug("End of getOsToFileTypeCodeMapping() Method.");
		return response;
	}


	public PartResponse updatePartAttribute(String swbNumber, String revision,String attributeName, String attributeValue){
		PartResponse response = new PartResponse();
		StringBuffer stringBuffer = new StringBuffer("");
		boolean flag = false;
		IAgileSession session = null;
		IItem item = null;
		UpdatePartAttributeBO updatePartAttributeBO = new UpdatePartAttributeBO();
		logger.info("Input Parameter-- SWB Number:"+swbNumber+" Revision:"+revision+" Attribute Name:"+attributeName+" Attribute Value:"+attributeValue);
		try{

			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			logger.debug("loading properties..");

			Properties applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
			String allowedUserDetail[] = applicationProperties.getProperty("AllowedUsers").split(",");
			/*String username = applicationProperties.getProperty("AGILE.USERNAME");*/ // Ajit-->Consolidation to properties file
			
			//Loading Std system config path--> Ajit-->Consolidation to properties file
			String stdSysConfigPath = applicationProperties.getProperty("DBPROP_FILEPATH");
			Properties dbProperties = FilePropertyUtility.loadProperties(stdSysConfigPath);
			String username = dbProperties.getProperty("AIC_DM_USERNAME");			
			/*String password = StringEncryptor.decrypt(applicationProperties.getProperty("AGILE.PASSWORD"));*/
			String password = StringEncryptor.decrypt(dbProperties.getProperty("AIC_DM_PASSWORD"));
			/*String url = applicationProperties.getProperty("AGILE.URL");*/// Ajit-->Consolidation to properties file
			String url = dbProperties.getProperty("AGILE_BIGIP_URL");
			String propertyName = attributeName.replace(" ", "");
			String agileAttributeName = applicationProperties.getProperty(propertyName);
			session = updatePartAttributeBO.createSession(url,username,password);
			String subClass = "";
			for(int i=0;i<allowedUserDetail.length;i++){

				logger.debug("Allowed User Invoked:"+allowedUserDetail[i]);
				logger.debug("Invoked User:"+Username);
				if (allowedUserDetail[i].trim().equalsIgnoreCase(Username.trim())) {
					flag = true;
					configureLog4J();
					if (swbNumber.trim().isEmpty() || swbNumber.trim().equals("?")) {
						stringBuffer.append("Software Bundle Number is mandatory");
					}else{
						logger.debug("Get Item Object");
						item= (IItem) session.getObject(ItemConstants.CLASS_ITEM_BASE_CLASS, swbNumber);

						if(item == null)
							stringBuffer.append("Part ("+swbNumber+") Doesn't Exist in Agile");
						else
							subClass = item.getValue(ItemConstants.ATT_TITLE_BLOCK_ITEM_TYPE).toString();
					}
					if (revision.trim().isEmpty() || revision.trim().equals("?")) {
						if(stringBuffer.length()==0)
							stringBuffer.append("Revision is mandatory");
						else
							stringBuffer.append(", Revision is mandatory");
					}

					if (attributeName.trim().isEmpty() || attributeName.trim().equals("?")) {
						if(stringBuffer.length()==0)
							stringBuffer.append("Attribute Name is mandatory");
						else
							stringBuffer.append(", Attribute Name is mandatory");
					}else{
						if(!subClass.isEmpty()){
							IAttribute attribute = session.getAdminInstance().getAgileClass(subClass).getAttribute(attributeName);
							if(attribute !=null){
								if(agileAttributeName == null){
									if(stringBuffer.length()==0)
										stringBuffer.append("User Doesn't have Privilege to Update the Attirbute ("+attributeName+")");
									else
										stringBuffer.append(", User Doesn't have Privilege to Update the Attirbute ("+attributeName+")");
								}
							}else{
								if(stringBuffer.length()==0)
									stringBuffer.append("Attribute ("+attributeName+") Doesn't Exist corresponding to the Part");
								else
									stringBuffer.append(", Attribute ("+attributeName+") Doesn't Exist corresponding to the Part");
							}
						}else{
							if(stringBuffer.length()==0)
								stringBuffer.append("Attribute ("+attributeName+") corresponding to the Part is Incorrect");
							else
								stringBuffer.append(", Attribute ("+attributeName+") corresponding to the Part is Incorrect");
						}
					}
					if (attributeValue.trim().isEmpty() || attributeValue.trim().equals("?")) {
						if(stringBuffer.length()==0)
							stringBuffer.append("Attribute Value is mandatory");
						else
							stringBuffer.append(", Attribute Value is mandatory");
					}
					/*			else{
						IAttribute attribute = session.getAdminInstance().getAgileClass("Software Bundle").getAttribute(agileAttributeName);
						Object obj[] = attribute.getChildren();
						boolean attributeFlag = false;
						for(int j=0;j<obj.length;j++){
							IAgileList list = (IAgileList)obj[j];
							if(list.getValue().toString().equalsIgnoreCase(attributeValue))
								attributeFlag = true;
						}

						if(!attributeFlag){
							if(stringBuffer.length()==0)
								stringBuffer.append("Attribute value doesn exist corresponding to the Part");
							else
								stringBuffer.append(", Attribute doesnot exist corresponding to the Part");
						}
					}*/
					if (stringBuffer.length() > 0) {
						String error = "Invalid Parameter --"
								+ stringBuffer.toString();
						logger.error("Error Message --" + error);
						throw new ValidationException(error);
					} else {

						logger.debug("Updating Attribute Process Begins");
						item.setRevision(revision);
						String destination = item.getValue(applicationProperties.getProperty("DESTINATION")).toString();
						if(destination.contains(applicationProperties.getProperty("FACTORYINSTALL"))){
							response = updatePartAttributeBO
									.updatePartAttributeResponse(
											swbNumber, agileAttributeName,
											attributeValue, applicationProperties,allowedUserDetail[i],
											response,session,item);
							flag = true;
						}else{
							response.setStatus("Failure");
							response.setObjectId("");
							response.setValidationError("Item ("+swbNumber+") doesn't have 'Factory Install' as Destination.");
						}
					}

					break;
				}
			}

			if(!flag){
				response.setStatus("Failure");
				response.setObjectId("");
				response.setValidationError("The User ("+Username+") doesn't have Access to Modify Attributes.");
			}
		}catch(ValidationException e){
			logger.error("Validation Exception: "+e.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(e.getMessage());
		}catch(APIException e){
			logger.error("API Exception: "+e.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(e.getMessage());
		}catch(Exception e){
			logger.error("Exception: "+e.getMessage());
			response.setStatus("Failure");
			response.setObjectId("");
			response.setValidationError(e.getMessage());
		}
		return response;
	}

	public PartResponse getDpkPartAttribute(String partNumber) throws Exception{
		PartResponse response = new PartResponse();
		try
		{
			logger.debug("Starting dpkPartAttribute() Method...");
			DpkPartretriveBO dpk=new DpkPartretriveBO();
			logger.debug("Invoking retrivePartAttributeResponse method.");
			response= dpk.retrivePartAttributeResponse(partNumber, response);
		}
		catch(Exception e){
			logger.error("Exception: "+e.getMessage());
		} 
		logger.debug("End of dpkPartAttribute() Method...");
		return response;
	}

	public SWBAutoCreationResponse createAutoSWBPart(SWBCreateRequest swbCreateRequest) throws Exception{					
		    SWBAutoCreationResponse swbAutoCreationResponse =new SWBAutoCreationResponse();
	        long lStartTime_1 = System.currentTimeMillis();
			String[] swbNumberDetails=new String[2];
				try{
					configureLog4J();
			        logger.debug("Log4j configured ---");
					logger.debug("Starting createCPGSWBPart Method...");
					logger.debug("message id=="+swbCreateRequest.getMessageId());
					logger.debug("downstreamName=="+swbCreateRequest.getDownStreamName());
					
					Properties applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
					Utility.validateMandatoryAttributes("MessageId",swbCreateRequest.getMessageId(),applicationProperties);
					Utility.validateMandatoryAttributes("DownStreamName",swbCreateRequest.getDownStreamName(),applicationProperties);
					String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
					String standard_system_configsPath = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
					logger.debug("loading std_system_config properties from::"+standard_system_configsPath);
					Properties stdSysconfigProperties = FilePropertyUtility.loadProperties(standard_system_configsPath);
					logger.debug("Reference  swb number"+swbCreateRequest.getReferenceSWBNum());
					String xmlFilepath=configBase+"\\config\\"+Constants.CONFIG_PNR_AUTOMATION;
					IAgileSession session=Utility.createSession(stdSysconfigProperties.getProperty("AGILE_URL"), stdSysconfigProperties.getProperty("SWB_AUTOMATION_USERNAME"),Utility.getDecryptPwd(stdSysconfigProperties.getProperty("SWB_AUTOMATION_PASWORD")));
					logger.debug("session created");
					session.disableAllWarnings();
					Utility.isCurrentUserValid(session,swbCreateRequest.getSwbOwner(),applicationProperties);
					 
				     SWBAutoGeneration createSWB = new SWBAutoGeneration();
					
					logger.debug("agile URL=="+stdSysconfigProperties.getProperty("AGILE_URL"));
					logger.debug("validator Username"+stdSysconfigProperties.getProperty("SWB_AUTOMATION_USERNAME"));
			        /*logger.debug("validator password=="+stdSysconfigProperties.getProperty("SWB_AUTOMATION_PASWORD"));*/

					if(StringUtils.isEmpty(swbCreateRequest.getReferenceSWBNum())){
						logger.debug("==independent SWB==");
						swbNumberDetails=createSWB.createIndependentSWB(session, swbCreateRequest,applicationProperties);
					}else if(createSWB.isValidSWB(swbCreateRequest.getReferenceSWBNum(),session,applicationProperties,swbCreateRequest.getIsSuccessor())){
						swbNumberDetails=createSWB.createSuccessorSWB(session, swbCreateRequest,applicationProperties,xmlFilepath);
					}				
					logger.debug("swb_number=="+swbNumberDetails[0]);
					logger.debug("Error message=="+swbNumberDetails[1]);
					swbAutoCreationResponse.setStatus("Success");
					swbAutoCreationResponse.setNewSWBNumber(swbNumberDetails[0]);
					swbAutoCreationResponse.setErrorMessage(swbNumberDetails[1]);
					swbAutoCreationResponse.setErrorCode("");
					swbAutoCreationResponse.setReferneceSWBNumber(swbCreateRequest.getReferenceSWBNum());
					swbAutoCreationResponse.setMessageId(swbCreateRequest.getMessageId());
			        long lEndTime = System.currentTimeMillis();
			        long reponsetime=(lEndTime-lStartTime_1)/1000;
	                logger.info("response time=="+reponsetime);
	                session.enableAllWarnings();
	                /**/
				}catch(ValidationException exp){
					logger.error("Validation Exception :" + exp.getMessage());
					swbAutoCreationResponse.setStatus("Failure");
					swbAutoCreationResponse.setNewSWBNumber("");
					swbAutoCreationResponse.setErrorMessage(exp.getMessage());
					swbAutoCreationResponse.setErrorCode("");
					swbAutoCreationResponse.setReferneceSWBNumber(swbCreateRequest.getReferenceSWBNum());
					swbAutoCreationResponse.setMessageId(swbCreateRequest.getMessageId());
				}catch(Exception exp){
					logger.error(" Exception :" + exp.getMessage());
	                exp.printStackTrace();
				}
				return swbAutoCreationResponse;
		}
	public SWBFirstLevelWUResponse getSWBFirstLevelWhereUsed(SWBFirstLevelWURequest swbFirstLevelWURequest) throws Exception{
		     SWBFirstLevelWUResponse swbFirstLevelWUResponse =new SWBFirstLevelWUResponse();
            //ArrayList whereUsedArr = new ArrayList();
            IQuery whereused = null;
            ITable results = null;
            Iterator itr = null;
            IRow row = null;
            StringBuffer parentPartNumbers=new StringBuffer();
            
            try {
				configureLog4J();
		        logger.debug("Log4j configured ---");
				String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
				Properties applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
				String standard_system_configsPath = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
				logger.debug("loading std_system_config properties from::"+standard_system_configsPath);
				Properties stdSysconfigProperties = FilePropertyUtility.loadProperties(standard_system_configsPath);
				String partNum=swbFirstLevelWURequest.getSwbNumber();
				logger.debug(" swb number"+partNum);
				Utility.validateMandatoryAttributes("MessageId",swbFirstLevelWURequest.getMessageId(),applicationProperties);
				Utility.validateMandatoryAttributes("SWB",partNum,applicationProperties);
				Utility.validateMandatoryAttributes("DownStreamName",swbFirstLevelWURequest.getDownStreamName(),applicationProperties);

				IAgileSession session=Utility.createSession(stdSysconfigProperties.getProperty("AGILE_URL"), stdSysconfigProperties.getProperty("SWB_AUTOMATION_USERNAME"),Utility.getDecryptPwd(stdSysconfigProperties.getProperty("SWB_AUTOMATION_PASWORD")));
				SWBAutoGeneration createSWB = new SWBAutoGeneration();
				if(!createSWB.isValidSWB(swbFirstLevelWURequest.getSwbNumber(),session,applicationProperties)){
					throw new ValidationException("Not a Valid Software Bundle");
				}
                whereused = (IQuery)session.createObject(IQuery.OBJECT_TYPE, ItemConstants.CLASS_ITEM_BASE_CLASS);
                whereused.setSearchType(QueryConstants.WHERE_USED_ONE_LEVEL_LATEST_RELEASED); 
                whereused.setCriteria("[Title Block.Number] like '"+partNum+"'"); 
                results = whereused.execute();
                logger.debug("query=="+whereused.toString());
                
                
                itr = results.iterator();
                while(itr.hasNext()) {
                     row = (IRow)itr.next();
                     //whereUsedArr.add(row.getValue(ItemConstants.ATT_TITLE_BLOCK_NUMBER).toString().trim());
                     parentPartNumbers.append(row.getValue(ItemConstants.ATT_TITLE_BLOCK_NUMBER).toString().trim()+",");
                     //logger.debug("parentPartNumbers ==="+parentPartNumbers);
                     row = null;
                }
                
                logger.debug(" after retrieving parentPartNumbers==="+parentPartNumbers.toString());
               if(parentPartNumbers.length()>0){
                	//parentPartNumbers.setCharAt(parentPartNumbers.length()-1,'\0');
                	 parentPartNumbers.deleteCharAt(parentPartNumbers.length()-1);
                }
                

            logger.debug("After iteration"+new SimpleDateFormat("yyyy.MM.dd HH.mm.ss").format(new Date()));
            swbFirstLevelWUResponse.setStatus("Success");
   			swbFirstLevelWUResponse.setSwbNumber(swbFirstLevelWURequest.getSwbNumber());
   			swbFirstLevelWUResponse.setMessageId(swbFirstLevelWURequest.getMessageId());
   			swbFirstLevelWUResponse.setParentPartNumbers(parentPartNumbers.toString());
            
		}catch(ValidationException exp){
			logger.error("Validation Exception :" + exp.getMessage());
			swbFirstLevelWUResponse.setStatus("Failure");
			swbFirstLevelWUResponse.setSwbNumber(swbFirstLevelWURequest.getSwbNumber());
			swbFirstLevelWUResponse.setErrorMessage(exp.getMessage());
			swbFirstLevelWUResponse.setErrorCode("");
			swbFirstLevelWUResponse.setMessageId(swbFirstLevelWURequest.getMessageId());
		}catch(Exception exp){
			logger.error("Validation Exception :" + exp.getMessage());
			exp.printStackTrace();
			swbFirstLevelWUResponse.setStatus("Failure");
			swbFirstLevelWUResponse.setSwbNumber(swbFirstLevelWURequest.getSwbNumber());
			swbFirstLevelWUResponse.setErrorMessage(exp.getMessage());
			swbFirstLevelWUResponse.setErrorCode("");
			swbFirstLevelWUResponse.setMessageId(swbFirstLevelWURequest.getMessageId());
		} finally {
            whereused = null;
            results = null;
            itr = null;
            row = null;
       }
            /*catch(Exception exp){
			logger.error(" Exception :" + exp.getMessage());
            exp.printStackTrace();
		}*/

		return swbFirstLevelWUResponse;
	}
	 //AFP Update Changes

	/**
	 * This web method accepts swbNumber,afpSrvMap and userId as input parameters
	 * and validate them as per business logic. Then updates the AFP values for
	 * the SWB part with respect to the SRV. It returns success response in case of
	 * the update is successful. Otherwise returns error response with an appropriate 
	 * error message.
	 * @param  swbNumber
	 * @param  AFPSRVMap
	 * @param  userId
	 * @return AFPResponse
	 * @throws Exception
	 */
	public AFPResponse updateAFPNumbers(String swbNumber, HashMap<String,String>AFPSRVMap, 
			String userId) throws Exception
	{
		logger.debug("Starts AgileSyncSWService::updateAFPNumbers() Method...");
		
		AFPResponse afpResponse = new AFPResponse();
		IAgileSession agileSession = null;
		IItem item = null;
		
		try 
		{
			//Loads property file to read access details
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			Properties appProp = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
			AFPUpdateBO afpUpdBo = new AFPUpdateBO();
			//Validate Service Account
			afpUpdBo.checkServiceAccValid(appProp,Username);
			//Input parameters validation
			afpUpdBo.checkInputParamMandate(swbNumber,AFPSRVMap,userId,appProp);
			//Get Agile Session
			agileSession = afpUpdBo.createAgileSession(appProp);
			//Validate User
			afpUpdBo.checkUserValid(agileSession,userId,appProp);
			//Get a input Part item
			item= (IItem) agileSession.getObject(IItem.OBJECT_TYPE, swbNumber);
			//Validate SWB Number
			afpUpdBo.checkSwbValid(agileSession,swbNumber,item,appProp);
			//Validate AFP Numbers
			StringBuffer invalAfpBuff = new StringBuffer("");
			afpUpdBo.removeInvalidAfpNums(AFPSRVMap,invalAfpBuff,appProp);
			//Validate SRV Numbers
			StringBuffer invalSrvBuff = new StringBuffer("");
			afpUpdBo.removeInvalidSrvValues(agileSession,AFPSRVMap,invalSrvBuff,invalAfpBuff,appProp);
			//Update AFP Numbers
			afpUpdBo.updateAfpValues(agileSession,item,AFPSRVMap,appProp);
			//Update History Table
			afpUpdBo.updateHistoryTable(item,userId,invalSrvBuff,invalAfpBuff,appProp);
			//Set response attributes for successful update
			afpResponse.setStatus("Success");
			afpResponse.setObjectId(swbNumber);
			afpResponse.setValidationError("");
			logger.debug(afpResponse.toString());
			
		}
		catch(ValidationException valEx)
		{
			logger.error("Validation Exception Occurred: "+valEx.getMessage());
			afpResponse.setStatus("Failure");
			afpResponse.setObjectId("");
			afpResponse.setValidationError(valEx.getMessage());
		}
		catch(APIException apiEx)
		{
			logger.error("API Exception Occurred: "+apiEx.getMessage());
			afpResponse.setStatus("Failure");
			afpResponse.setObjectId("");
			afpResponse.setValidationError(apiEx.getMessage());
		}
		catch(Exception ex)
		{
			logger.error("Exception Occurred: "+ex.getMessage());
			afpResponse.setStatus("Failure");
			afpResponse.setObjectId("");
			afpResponse.setValidationError(ex.getMessage());
		}
		finally
		{
			agileSession = null;
		}
		logger.debug("Ends AgileSyncSWService::updateAFPNumbers() Method...");
		return afpResponse;
	}

	/**
	 * @param message
	 */
	/*private static void traceLog(String message){
		logger.debug(message);
	}*/

	/**
	 * Configures the log4j 
	 */
	private void configureLog4J()
	{
		try
		{
			getAicConfigBasePath("synchwebservice");
			StringBuffer log4jPath = new StringBuffer(configBase);
			log4jPath.append("\\config").append("\\syncswservice_log4j.xml");

			//InputStream iStream = getClass().getResource(name)sourceAsStream("/filedownloadservice_log4j.xml");
			DOMConfigurator.configure(log4jPath.toString());
		}catch(Exception ex){
			System.out.println("Unable to Configure Log4j"+ex.getMessage());
		}
	}

	/**
	 * Loads application properties.
	 * @param propertiesFileName
	 * @return
	 */
	private Properties loadApplicationProperties(String propertiesFileName)
	{
		logger.debug("loading properties.."+propertiesFileName);
		getAicConfigBasePath("synchwebservice");
		Properties	applicationProperties = null;
		try {
			//applicationProperties = fu.loadWebAppProperties("/"+propertiesFileName);
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\"+propertiesFileName);
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			logger.debug("loading properties..");


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return applicationProperties;
	}

	/**
	 * 
	 */
	private void encryptFile()
	{
		String AGILE_PROPERTIES = "";
		logger.debug("loading properties..");
		getAicConfigBasePath("synchwebservice");
		Properties applicationProperties = loadApplicationProperties("aic_syncswservice_application.properties");
		AGILE_PROPERTIES = applicationProperties.getProperty("AGILEPROP_FILEPATH").trim();
		AGILE_PROPERTIES = configBase+"\\"+AGILE_PROPERTIES;
		logger.debug("Encrypt the File "+AGILE_PROPERTIES+ "Started");
		FileEncryptorUtil.encryptFile(AGILE_PROPERTIES);
		logger.debug("Encrypt the File done");
	}

	/**
	 * Returns the config base of the type passed
	 * @param type
	 * @return
	 */
	private String getAicConfigBasePath(final String type)
	{
		if(configBase == null)
		{
	
			configBase = FilePropertyUtility.getConfigBasePath(type);
		}
		return configBase;
	}
}