package com.dell.plm.agile.integration.agilesyncwservice;

import java.io.IOException;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.apache.ws.security.WSPasswordCallback;

import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.ldap.LDAPAuthenticationImpl;

public class PWCallback implements CallbackHandler {
	Logger logger = null;
		
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException 
    {
		
		String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
		StringBuffer log4jPath = new StringBuffer(configBase);
		log4jPath.append("\\config").append("\\syncswservice_log4j.xml");
		DOMConfigurator.configure(log4jPath.toString());
		
		logger = Logger.getLogger(PWCallback.class);
		logger.debug("Log4j is configured successfully ..");
		//Utility util = new Utility();
       
        //String AGILE_PROPERTIES = "";
        String ldap_prop_filepath = null;
        /**Loading Application properties*/
        Properties aicfiledownloadProperties = null;
        
        logger.debug("loading aic_filedownload_application properties..");
        StringBuffer configBaseLoc = new StringBuffer(configBase);
        configBaseLoc.append("\\config").append("\\aic_syncswservice_application.properties");
        aicfiledownloadProperties = FilePropertyUtility.loadProperties(configBaseLoc.toString());

        ldap_prop_filepath = aicfiledownloadProperties.getProperty("LDAP_PROP_FILEPATH").trim();
        ldap_prop_filepath = configBase+"\\"+ldap_prop_filepath;
        //logger.debug("Encryption for file "+AGILE_PROPERTIES+ " Started");
		//FileEncryptorUtil.encryptFile(AGILE_PROPERTIES);
		logger.debug("Properties are loaded from  aic_syncswservice_application.properties");
		
        /**Loading Application properties*/
        //Properties applicationProperties = null;
        //logger.debug("loading.."+AGILE_PROPERTIES);
		//applicationProperties = FilePropertyUtility.loadProperties(AGILE_PROPERTIES);
		
		//Loading ldap properties
		 Properties ldapProperties = null;
		 logger.debug("loading ldapProperties properties from::"+ldap_prop_filepath);
		 if(null!= ldap_prop_filepath)
		 {
			 ldapProperties = FilePropertyUtility.loadProperties(ldap_prop_filepath);
			 
			 logger.debug("Loaded ldap Properties");
		 }
		
		//String user = applicationProperties.getProperty("WSSECURITY_USER").trim();

		//logger.debug("WSSECURITY_USER :"+user);
		//String password = applicationProperties.getProperty("WSSECURITY_PASSWORD").trim();
		//password = util.getDecryptPwd(password);
		
        for (int i = 0; i < callbacks.length; i++)
        {
            if (callbacks[i] instanceof WSPasswordCallback)
            {
                WSPasswordCallback pc = (WSPasswordCallback) callbacks[i];
                String userNameLogged = pc.getIdentifier();
                AgileSyncSWService.Username = userNameLogged;
                logger.debug("userNameLogged: "+userNameLogged+", usage: "+pc.getUsage());
                               
                //LDAP authentication , if the username has domain and userID seperated with seperator
                //else will do the authentication with WSSECURITY_USER
                if(userNameLogged.contains("\\") ||userNameLogged.contains("/"))
                {
                	LDAPAuthenticationImpl ldapImpl = new LDAPAuthenticationImpl();
                	ldapImpl.doLDAPAuthentication(pc,ldapProperties);
                }
                
                else
                {
	                if (pc.getUsage() == WSPasswordCallback.USERNAME_TOKEN)
	                {
	                	logger.debug("Unknown user: "+pc.getIdentifier());
	                	throw new IOException("unknown user: "+pc.getIdentifier());
	                	// for passwords sent in digest mode we need to provide the password,
	                    // because the original one can't be un-digested from the message
	
	                    // we can throw either of the two Exception types if authentication fails
	                    /*if (! user.equals(pc.getIdentifier()))
	                    {
	                    	logger.debug("unknown user: "+pc.getIdentifier());
	                    	throw new IOException("unknown user: "+pc.getIdentifier());
	                       
	                    }*/
	
	                    // this will throw an exception if the passwords don't match
	                    //pc.setPassword(password);
	
	                } else if (pc.getUsage() == WSPasswordCallback.USERNAME_TOKEN_UNKNOWN)
	                {
	                    // for passwords sent in cleartext mode we can compare passwords directly
	                	logger.debug("unknown user:"+pc.getIdentifier());
	                	throw new IOException("unknown user: "+pc.getIdentifier());
	
	                   /* if (! user.equals(pc.getIdentifier()))
	                    {
	                    	logger.debug("unknown user: "+pc.getIdentifier());
	                    	throw new IOException("unknown user: "+pc.getIdentifier());
	                    }*/
	
	                    // we can throw either of the two Exception types if authentication fails
	                   /* if (! password.equals(pc.getPassword()))
	                    {
	                    	logger.debug("password incorrect for user: "+pc.getIdentifier());
	                    	throw new IOException("password incorrect for user: "+pc.getIdentifier());
	                    }*/
	                }
                
                } //End of else
                
                
            } else
            {
                throw new UnsupportedCallbackException(callbacks[i], "Unrecognized Callback");
            }
        }
    }
}