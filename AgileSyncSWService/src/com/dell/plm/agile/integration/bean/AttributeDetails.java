package com.dell.plm.agile.integration.bean;

public class AttributeDetails {
		String attributeName;
		public String getAttributeName() {
			return attributeName;
		}
		public void setAttributeName(String attributeName) {
			this.attributeName = attributeName;
		}
		String attributeValue;
		public String getAttributeValue() {
			return attributeValue;
		}
		public void setAttributeValue(String attributeValue) {
			this.attributeValue = attributeValue;
		}
}
