package com.dell.plm.agile.integration.bean;


public class SWBCreateRequest {
	String referenceSWBNum;
	String devicePartNums;
	String swbOwner;
	String isSuccessor;
	String messageId;
	String downStreamName;
    public String getDownStreamName() {
		return downStreamName;
	}
	public void setDownStreamName(String downStreamName) {
		this.downStreamName = downStreamName;
	}
	AttributeDetails attributeDetails[];
    public String getReferenceSWBNum() {
		return referenceSWBNum;
	}
	public void setReferenceSWBNum(String referenceSWBNum) {
		this.referenceSWBNum = referenceSWBNum;
	}
	public String getDevicePartNums() {
		return devicePartNums;
	}
	public void setDevicePartNums(String devicePartNums) {
		this.devicePartNums = devicePartNums;
	}
	public String getSwbOwner() {
		return swbOwner;
	}
	public void setSwbOwner(String swbOwner) {
		this.swbOwner = swbOwner;
	}
	public String getIsSuccessor() {
		return isSuccessor;
	}
	public void setIsSuccessor(String isSuccessor) {
		this.isSuccessor = isSuccessor;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public AttributeDetails[] getAttributeDetails() {
		return attributeDetails;
	}
	public void setAttributeDetails(AttributeDetails[] attributeDetails) {
		this.attributeDetails = attributeDetails;
	}
	
}
