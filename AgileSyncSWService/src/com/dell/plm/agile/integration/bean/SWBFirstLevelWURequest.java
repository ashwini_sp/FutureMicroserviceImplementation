package com.dell.plm.agile.integration.bean;


public class SWBFirstLevelWURequest {
	String messageId;
	String swbNumber;
	String downStreamName;
	public String getDownStreamName() {
		return downStreamName;
	}
	public void setDownStreamName(String downStreamName) {
		this.downStreamName = downStreamName;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getSwbNumber() {
		return swbNumber;
	}
	public void setSwbNumber(String swbNumber) {
		this.swbNumber = swbNumber;
	}
}
