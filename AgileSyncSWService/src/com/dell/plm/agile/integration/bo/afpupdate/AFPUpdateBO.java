package com.dell.plm.agile.integration.bo.afpupdate;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.AgileSessionFactory;
import com.agile.api.IAgileSession;
import com.agile.api.IItem;
import com.agile.api.IUser;
import com.agile.api.ItemConstants;
import com.agile.api.UserConstants;
import com.dell.plm.agile.aic.common.cryptography.StringEncryptor;
import com.dell.plm.agile.aic.common.exception.ValidationException;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;

/**
 * This BO class is being used by AFP values update service to validate
 * the input parameters and update the AFP numbers and History table if
 * the validation success. Otherwise throws error messages based on the
 * type of failure.
 * @Project AFP Update
 * @author Siva_Ramaraj
 * @Date October 6,2016
 */
public class AFPUpdateBO 
{

	private static final Logger logger = Logger.getLogger(AFPUpdateBO.class);
	private static final String SOFTWARE_BUNDLE= "Software Bundle";
	private static final String SERVICE_INSTALL= "Service Install";
	private static final String ACTIVE= "Active";
	
	
	/**
	 * This method validates the service account to access the service.
	 * Throws ValidationException if it is invalid.
	 * @param  appProp
	 * @param username
	 * @throws ValidationException
	 */
	public void checkServiceAccValid(Properties appProp, String username) throws ValidationException
	{
		logger.debug("Starts AFPUpdateBO::checkServiceAccValid()..");
		List<String> usersAllowedList = Arrays.asList(appProp.getProperty("USERS_ALLOWED").split(","));
		boolean valFlag = false;
		logger.debug("usersAllowedList::"+usersAllowedList);
		for(String userAllowed : usersAllowedList)
		{	
			logger.debug("userAllowed::"+userAllowed);
			if(userAllowed.equalsIgnoreCase(username))
			{
				valFlag = true;
				break;
			}
		}
		if(!valFlag)
		{
			String errorMsg = appProp.getProperty("ERROR_MSG_SER_ACC");
			throw new ValidationException(MessageFormat.format(errorMsg, username));
		}
		logger.debug("Ends AFPUpdateBO::checkServiceAccValid()..");
		
	}
	
	
	/**
	 * This method validates the input parameters required to update AFP
	 * values. Throws ValidationException in case of failure.
	 * @param  swbNumber
	 * @param  afpSrvMap
	 * @param  userId
	 * @param  appProp 
	 * @throws ValidationException
	 */
	public void checkInputParamMandate(String swbNumber, HashMap<String, String> afpSrvMap, 
			String userId, Properties appProp) throws ValidationException 
	{
		logger.debug("Starts AFPUpdateBO::checkInputParamMandate()..");
		StringBuffer strBuff = new StringBuffer("");
		boolean valFlag = true;
		//Part number input parameter validation
		if (StringUtils.isEmpty(swbNumber) || swbNumber.trim().equals("?")) 
		{
			valFlag = false;
			strBuff.append(appProp.getProperty("ERROR_MSG_INPUT_PART"));
		}
		//AFP(SRV) attribute input parameter validation
		if(afpSrvMap.isEmpty())
		{
			valFlag = false;
			if(strBuff.length()> 0)
			{
				strBuff.append(",");
			}
			strBuff.append(appProp.getProperty("ERROR_MSG_INPUT_AFP"));
		}
		//User ID input parameter validation
		if(StringUtils.isEmpty(userId) || userId.trim().equals("?"))
		{
			valFlag = false;
			if(strBuff.length()> 0)
			{
				strBuff.append(",");
			}
			strBuff.append(appProp.getProperty("ERROR_MSG_INPUT_USER"));
		}
		if(!valFlag)
		{
			//Throws ValidationException if any one of validations fails
			throw new ValidationException(strBuff.toString());
		}
		
		logger.debug("Ends AFPUpdateBO::checkInputParamMandate()..");
	}

	/**
	 * This method reads user name , password and url from property file
	 * and creates  AgileSession form AgileSessionFactory.
	 * @param appProp 
	 * @return agileSession
	 * @throws APIException, IOException
	 */
/*	public IAgileSession createAgileSession(Properties appProp) 
			throws APIException,IOException*/ //   Ajit--> Consolidation to properties file
	
	public IAgileSession createAgileSession(Properties appProp) 
			throws APIException,IOException
	{
		logger.debug("Starts AFPUpdateBO::createAgileSession()..");
		IAgileSession agileSession = null;
		//Loads property file to read access details
		/*String username = appProp.getProperty("AGILE.USERNAME");*/ // Ajit--> Consolidation to properties file
		
		String dbPropsFilePath = appProp.getProperty("DBPROP_FILEPATH");
		Properties dbProps = FilePropertyUtility.loadProperties(dbPropsFilePath);
		/*String username = appProp.getProperty("AGILE.USERNAME");*/ //Ajit--> Consolidation to properties file
		String username = dbProps.getProperty("AIC_DM_USERNAME"); 
		/*String password = StringEncryptor.decrypt(appProp.getProperty("AGILE.PASSWORD"));*/
		String password = StringEncryptor.decrypt(dbProps.getProperty("AIC_DM_PASSWORD"));
		/*String url = appProp.getProperty("AGILE.URL");*/ //Ajit--> Consolidation to properties file
		String url = dbProps.getProperty("AGILE_BIGIP_URL");
		//Get an instance of AgileSessionFactory
		AgileSessionFactory factory = AgileSessionFactory.getInstance(url);
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(AgileSessionFactory.USERNAME, username);
		map.put(AgileSessionFactory.PASSWORD, password);
		//Get Agile session from AgileSessionFactory
		agileSession = factory.createSession(map);
		logger.debug("Ends AFPUpdateBO::createAgileSession()..");
		return agileSession;
	}


	/**
	 * This method checks the user ID's validity against agile.
	 * Throws ValidationException in case of failure.
	 * @param agileSession
	 * @param userId
	 * @param appProp 
	 * @throws APIException
	 * @throws ValidationException
	 */
	public void checkUserValid(IAgileSession agileSession, String userId, Properties appProp) 
			throws APIException,ValidationException 
	{
		logger.debug("Starts AFPUpdateBO::checkUserValid()..");
		//StringBuffer strBuff = new StringBuffer("");
		String userStatus = null;
		String errorMsg = null;
		boolean valFlag = true;
		//Get IUser item by passing input user Id 
		IUser user = (IUser) agileSession.getObject(IUser.OBJECT_TYPE, userId);
		if(user == null)
		{
			valFlag = false;
			errorMsg = appProp.getProperty("ERROR_MSG_INVAL_USER");
		}
		else
		{
			userStatus = user.getValue(UserConstants.ATT_GENERAL_INFO_STATUS).toString();
			if(!ACTIVE.equalsIgnoreCase(userStatus))
			{
				valFlag = false;
				errorMsg = appProp.getProperty("ERROR_MSG_INACT_USER");
			}
		}
		if(!valFlag)
		{
			//Throw ValidationException if user does not exist or active in Agile
			throw new ValidationException(MessageFormat.format(errorMsg, userId));
		}
		logger.debug("Ends AFPUpdateBO::checkUserValid()..");
	}

	/**
	 * This method checks if the part number presents in agile and its
	 * type is Software Bundle.Throws ValidationException in case of failure.
	 * @param agileSession
	 * @param swbNumber
	 * @param partItem
	 * @param appProp 
	 * @throws APIException
	 * @throws ValidationException
	 */
	public void checkSwbValid(IAgileSession agileSession, String swbNumber,
			IItem partItem, Properties appProp) throws APIException,ValidationException
	{
		logger.debug("Starts AFPUpdateBO::checkSwbValid()..");
		String partType=null;
		String partClass=null;
		String errorMsg = null;
		boolean valFlag = true;
		//StringBuffer strBuff = new StringBuffer("");
		logger.debug("Input part item::"+partItem);
		//Check if input part item exists in Agile
		if(partItem == null)
		{
			valFlag = false;
			errorMsg = appProp.getProperty("ERROR_MSG_INVAL_PART");
		}
		else
		{
			//Get Part class and Part Type attribute of Part
			partClass = partItem.getValue(ItemConstants.ATT_TITLE_BLOCK_PART_TYPE).toString();
			logger.debug("partClass::" + partClass);
			partType=partItem.getValue(ItemConstants.ATT_TITLE_BLOCK_PART_CATEGORY).toString();
			logger.debug("partType::" + partType);
			//Check if the Part is Software Bundle
			if(!SOFTWARE_BUNDLE.equalsIgnoreCase(partClass) || !SOFTWARE_BUNDLE.equalsIgnoreCase(partType))
			{
				valFlag = false;
				errorMsg = appProp.getProperty("ERROR_MSG_INVAL_PART_TYPE");
			}
		}
		if(!valFlag)
		{
			//Throw ValidationException if validation fails
			throw new ValidationException(MessageFormat.format(errorMsg, swbNumber));
		}
		logger.debug("Ends AFPUpdateBO::checkSwbValid()..");
	}

	/**
	 * This method validates if the input AFP values are numeric.
	 * Removes the entry in HashMap in case of non-numeric.
	 * Throws ValidationException in case of all AFP values are non-numeric..
	 * @param afpSrvMap
	 * @param invalAfpBuff
	 * @param appProp 
	 * @throws ValidationException 
	 */
	public void removeInvalidAfpNums(HashMap<String, String> afpSrvMap, StringBuffer invalAfpBuff, 
			Properties appProp) throws ValidationException 
	{
		logger.debug("Strats AFPUpdateBO::removeInvalidAfpNums()..");
		String errorMsg = null;
		for( Iterator<Map.Entry<String, String>> iter = afpSrvMap.entrySet().iterator();
			    iter.hasNext();)
		{
			Map.Entry<String, String> entry = iter.next();
			logger.debug("AFP Number::"+entry.getValue());
			if(!entry.getValue().matches("[0-9]+"))
			{
				invalAfpBuff.append(entry.getValue());
				invalAfpBuff.append("(");
				invalAfpBuff.append(entry.getKey());
				invalAfpBuff.append(")");
				invalAfpBuff.append(";");
				iter.remove();
			}
		}
		if(invalAfpBuff.length()>0)
		{
			invalAfpBuff.deleteCharAt(invalAfpBuff.length()-1);
		}
		if(afpSrvMap.isEmpty())
		{
			//Throw ValidationException if all AFP numbers are invalid
			errorMsg = appProp.getProperty("ERROR_MSG_INVAL_AFP");
			throw new ValidationException(MessageFormat.format(errorMsg, invalAfpBuff.toString()));
		}
		logger.debug("Ends AFPUpdateBO::removeInvalidAfpNums()..");
		
	}

	
	/**
	 * This method validates if the input SRV values present in agile.
	 * Removes the entry in HashMap in case of invalid.
	 * Throws ValidationException in case of all SRV values are invalid.
	 * @param agileSession
	 * @param afpSrvMap
	 * @param strBuff
	 * @param invalAfpBuff 
	 * @param appProp 
	 * @throws APIException
	 * @throws ValidationException
	 */
	public void removeInvalidSrvValues(IAgileSession agileSession, 
				HashMap<String, String> afpSrvMap, StringBuffer invalSrvBuff, StringBuffer invalAfpBuff, Properties appProp)
				throws APIException, ValidationException
	{
		logger.debug("Starts AFPUpdateBO::removeInvalidSrvValues()..");
		IItem item = null;
		String srvVal = null; 
		String errorMsg = null;
		String partClass = null;
		for( Iterator<Map.Entry<String, String>> iter = afpSrvMap.entrySet().iterator();
			    iter.hasNext();)
		{
			boolean valFlag = true;
			Map.Entry<String, String> entry = iter.next();
			srvVal = entry.getKey();
			logger.debug("srvVal"+srvVal);
			if(StringUtils.isEmpty(srvVal) || srvVal.trim().equals("?"))
			{
				valFlag = false;
			}
			else
			{
				//Get input SRV part from Agile
				item = (IItem) agileSession.getObject(IItem.OBJECT_TYPE, srvVal);
				logger.debug("SRV Item::"+item);
				//Append SRV part in String Buffer and remove it from input Map if invalid
				if(item == null)
				{
					valFlag = false;
				}
				else
				{
					//Get Part class for item
					partClass = item.getValue(ItemConstants.ATT_TITLE_BLOCK_PART_TYPE).toString();
					logger.debug("partClass::" + partClass);
					//Check if item is SRV
					if(!SERVICE_INSTALL.equalsIgnoreCase(partClass))
					{
						valFlag = false;	
					}
				}
			}
			if(!valFlag)
			{
				invalSrvBuff.append(entry.getValue());
				invalSrvBuff.append("(");
				invalSrvBuff.append(entry.getKey());
				invalSrvBuff.append(")");
				invalSrvBuff.append(";");
				iter.remove();
			}
		}
			
		if(invalSrvBuff.length()>0)
		{
			invalSrvBuff.deleteCharAt(invalSrvBuff.length()-1);
		}
		if(afpSrvMap.isEmpty() && invalAfpBuff.length()==0)
		{
			//Throw ValidationException if all SRV parts are invalid
			errorMsg = appProp.getProperty("ERROR_MSG_INVAL_SRV");
			throw new ValidationException(MessageFormat.format(errorMsg, invalSrvBuff.toString()));
		}
		else if(afpSrvMap.isEmpty() && invalAfpBuff.length()>0)
		{
			//Throw ValidationException if all SRV parts and some AFP numbers are invalid
			errorMsg = appProp.getProperty("ERROR_MSG_INVAL_SRV_AFP");
			throw new ValidationException(MessageFormat.format(errorMsg,invalAfpBuff.toString(), invalSrvBuff.toString()));
		}
		logger.debug("Ends AFPUpdateBO::removeInvalidSrvValues()..");
		
	}

	/**
	 * This method updates AFP number for the input part. It replaces the
	 * existing AFP value with new if input SRV exists already in part.
	 * Otherwise it appends with ; as delimiter in recognized format. 
	 * @param agileSession
	 * @param partItem
	 * @param afpSrvMap
	 * @param appProp 
	 * @throws APIException
	 */
	public void updateAfpValues(IAgileSession agileSession, IItem partItem, 
			HashMap<String, String> afpSrvMap, Properties appProp) 
			throws APIException
	{
		logger.debug("Starts AFPUpdateBO::updateAfpValues()..");
		String afpSrvVal = null;
		String afpSrvFinalVal = null;
		//Get attribute ID of AFP number from property file
		String srvAttrId = appProp.getProperty("AFP_NUMBER");
		//Get AFP number values of input Part
		afpSrvVal = partItem.getValue(Integer.parseInt(srvAttrId)).toString().trim();
		 StringBuffer afpBuff = new StringBuffer("");
		if(StringUtils.isEmpty(afpSrvVal))
		{
			getFinalAfpSrvNumFormat(afpSrvMap,afpBuff);
		}
		else
		{
			logger.debug("AFP numbers already present in part::"+afpSrvVal);
			//Get old and new values combined
			getCombinedAfpSrvMap(afpSrvVal,afpSrvMap,afpBuff);
			//Get final AFP(SRV) format
			getFinalAfpSrvNumFormat(afpSrvMap,afpBuff);
		}
		afpSrvFinalVal = afpBuff.toString().trim();
		logger.debug("afpSrvFinalVal::"+afpSrvFinalVal);
		//Trim the final AFP values to 4000 chars of free text length in screen 
		if(afpSrvFinalVal.length()>4000)
		{
			logger.debug("afpSrvFinalVal length::"+afpSrvFinalVal.length());
			afpSrvFinalVal = afpSrvFinalVal.substring(0, 4000);
			logger.debug("afpSrvFinalVal substring::"+afpSrvFinalVal);
			logger.debug("afpSrvFinalVal last char::"+afpSrvFinalVal.charAt(afpSrvFinalVal.length()-1));
		}
		//Make sure all values are complete
		if(afpSrvFinalVal.charAt(afpSrvFinalVal.length()-1)!=')')
		{
			afpSrvFinalVal = afpSrvFinalVal.substring(0, afpSrvFinalVal.lastIndexOf(")")+1);
		}
		logger.debug("afpSrvFinalVal before set::"+afpSrvFinalVal);
		//Set the AFP values in attribute
		partItem.setValue(Integer.parseInt(srvAttrId), afpSrvFinalVal);
		logger.debug("Ends AFPUpdateBO::updateAfpValues()..");
		
	}

	/**
	 * This method updates history table with user ID, action taken and invalid 
	 * SRV values and AFP numbers if any for part item in case of successful 
	 * update of AFP number.
	 * @param item
	 * @param userId
	 * @param strBuff
	 * @param invalAfpBuff 
	 * @param appProp 
	 * @throws APIException
	 */
	public void updateHistoryTable(IItem item, String userId, StringBuffer invalSrvBuff, StringBuffer invalAfpBuff, Properties appProp) 
			throws APIException
	{
		logger.debug("Starts AFPUpdateBO::updateHistoryTable()..");
		String actionMsg = null;
		//If String Buffer length >0 then there will be at least one invalid item
		if(invalSrvBuff.length()>0 && invalAfpBuff.length()>0)
		{
			actionMsg = appProp.getProperty("ACTION_MSG_UPD_PARTIAL_BOTH");
			item.logAction(MessageFormat.format(actionMsg, userId,invalSrvBuff.toString(),invalAfpBuff.toString()));
					
		}
		else if(invalSrvBuff.length()>0 && invalAfpBuff.length()==0)
		{
			actionMsg = appProp.getProperty("ACTION_MSG_UPD_PARTIAL_SRV");
			item.logAction(MessageFormat.format(actionMsg, userId,invalSrvBuff.toString()));
		}
		else if(invalSrvBuff.length()==0 && invalAfpBuff.length()>0)
		{
			actionMsg = appProp.getProperty("ACTION_MSG_UPD_PARTIAL_AFP");
			item.logAction(MessageFormat.format(actionMsg, userId,invalAfpBuff.toString()));
		}
		else
		{
			actionMsg = appProp.getProperty("ACTION_MSG_UPD_COMPLETE");
			item.logAction(MessageFormat.format(actionMsg, userId));
		}
		logger.debug("Ends AFPUpdateBO::updateHistoryTable()..");
	}


	/**
	 * This method combines the existing and new AFP/SRV values. If any
	 * existing SRV presents in input map, it will be replaced by input value. 
	 * @param afpNumber
	 * @param afpSrvMap
	 * @param afpBuff
	 */
	private void getCombinedAfpSrvMap(String afpNumber, HashMap<String, String> afpSrvMap, 
				StringBuffer afpBuff)
	{
		logger.debug("Starts AFPUpdateBO::getCombinedAfpSrvMap()..");
		String srvVal = null;
		//Get ; delimited AFP(SRV) values in List
		List<String> afpSrvValList = Arrays.asList(afpNumber.split("\\s*;\\s*"));
		logger.debug("afpSrvValList"+afpSrvValList);
		for(String afpSrvVal:afpSrvValList)
		{
			logger.debug("srvVal"+afpSrvVal);
			logger.debug("(::"+StringUtils.countMatches(afpSrvVal, "("));
			logger.debug(")::"+StringUtils.countMatches(afpSrvVal, ")"));
			if(StringUtils.countMatches(afpSrvVal, "(") == 1 && StringUtils.countMatches(afpSrvVal, ")") == 1)
			{
				//Take SRV value surrounded by parenthesis
				srvVal = afpSrvVal.substring(afpSrvVal.indexOf("(")+1,afpSrvVal.indexOf(")"));
				logger.debug("srvValFinal"+srvVal);
				//Append the AFP(SRV) values in String Buffer if it is not in input
				if(!afpSrvMap.containsKey(srvVal))
				{
					afpBuff.append(afpSrvVal);
					afpBuff.append(";");	
				}
			}
			else
			{
				afpBuff.append(afpSrvVal);
				afpBuff.append(";");
			}
					
		}
		logger.debug("Ends AFPUpdateBO::getCombinedAfpSrvMap()..");
	}


	/**
	 * This method gives the final AFP(SRV) values in recognized format.
	 * @param afpSrvMap
	 * @param afpBuff
	 */
	private void getFinalAfpSrvNumFormat(HashMap<String, String> afpSrvMap, StringBuffer afpBuff)
	{
		logger.debug("Starts AFPUpdateBO::getFinalAfpSrvNumFormat()..");
		for(Map.Entry<String, String> entrySet : afpSrvMap.entrySet())
		{
			afpBuff.append(entrySet.getValue());
			afpBuff.append("(");
			afpBuff.append(entrySet.getKey());
			afpBuff.append(")");
			afpBuff.append(";");
		}
		if(afpBuff.length()>0)
		{
			afpBuff.deleteCharAt(afpBuff.length()-1);
		}
		logger.debug("Ends AFPUpdateBO::getFinalAfpSrvNumFormat()..");
	}

}
 