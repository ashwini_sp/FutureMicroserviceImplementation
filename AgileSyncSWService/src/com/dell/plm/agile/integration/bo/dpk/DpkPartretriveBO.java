package com.dell.plm.agile.integration.bo.dpk;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.AgileSessionFactory;
import com.agile.api.IAgileSession;
import com.agile.api.IItem;
import com.agile.api.ItemConstants;

import com.dell.plm.agile.aic.common.cryptography.StringEncryptor;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.dto.PartResponse;
import com.dell.plm.agile.integration.xml.DpkPartXMLGenerator;


public class DpkPartretriveBO {


	
	private static final Logger logger = Logger.getLogger(DpkPartretriveBO.class);
	
	/**
	 * @param args
	 */
	
	
	
	public PartResponse retrivePartAttributeResponse(final String partNumber,final PartResponse response)
	{
		logger.debug("The retrivePartAttributeResponse Method Starting...");
		
		
		synchronized(this)
		{
			Map<String,String> partDetails=new HashMap<String, String>();
			String strXML = null;
			boolean flag = false;
			Properties applicationProperties = null;
			IAgileSession session = null;
			Properties dbProperties = null; // Ajit--> Consolidation to properties file
						
			try{
				
				String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
				logger.debug("loading properties..");
				applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
				
				//Loading here std.system config properties file path  // Ajit--> Consolidation to properties file
				String stdSys_config_prop_filepath = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
				logger.debug("Std System Config Path*************************** :: " + stdSys_config_prop_filepath);
				dbProperties = FilePropertyUtility.loadProperties(stdSys_config_prop_filepath);
				
				/*String username = applicationProperties.getProperty("DPK_AGILE_USERNAME");*/ // Ajit-->Consolidation to properties file
				String username = dbProperties.getProperty("DELLPRINTWSX_USERNAME");
				logger.debug("username*************************** :: " + username);
				/*String password = StringEncryptor.decrypt(applicationProperties.getProperty("DPK_AGILE_PASSWORD"));*/ // Ajit-->Consolidation to properties file
				String password = StringEncryptor.decrypt(dbProperties.getProperty("DELLPRINTWSX_PASSWORD"));
				/*String url = applicationProperties.getProperty("DPK_AGILE_URL");*/  // Ajit-->Consolidation to properties file
				String url = dbProperties.getProperty("AGILE_BIGIP_URL");
                String desc= applicationProperties.getProperty("DESCRIPTION");
				session = createSession(url,username,password);
				IItem item= (IItem)session.getObject(ItemConstants.CLASS_ITEM_BASE_CLASS, partNumber);
								
				if(partNumber.trim().isEmpty() || partNumber.trim().equals("?") || item==null){	                	  
              	                	  
              	    partDetails.put("Part Number",partNumber);
                    partDetails.put("Part Type","");
                    partDetails.put("Part Class","");
                    partDetails.put("Description",desc);
                    partDetails.put("License Type","");
                    partDetails.put("License Binding Type","");
                    partDetails.put("License Duration","");
                    partDetails.put("License is an upgrade","");
                    partDetails.put("KeyGenPartNumber","");
                    partDetails.put("License is transferable",""); }
	                  else
	                  {                   	                                    
		                  String partClass= item.getValue(Integer.parseInt(applicationProperties.getProperty("PART_CLASS"))).toString();		                  
		                  String isDpk = applicationProperties.getProperty("DPK_SUBCLASS");
		                  if(partClass.equalsIgnoreCase(isDpk))
		                        {
		                	      partDetails.put("Part Number",item.getValue(Integer.parseInt(applicationProperties.getProperty("PART_NUMBER"))).toString());
		  	                      partDetails.put("Part Type",item.getValue(Integer.parseInt(applicationProperties.getProperty("PART_TYPE"))).toString());
		  	                      partDetails.put("Part Class",item.getValue(Integer.parseInt(applicationProperties.getProperty("PART_CLASS"))).toString());
		  	                      partDetails.put("Description",item.getValue(Integer.parseInt(applicationProperties.getProperty("PART_DESCRIPTION"))).toString());
		  	                      partDetails.put("License Type",item.getValue(Integer.parseInt(applicationProperties.getProperty("LICENSE_TYPE"))).toString());
		  	                      partDetails.put("License Binding Type",item.getValue(Integer.parseInt(applicationProperties.getProperty("LICENSE_BINDING_TYPE"))).toString());
		  	                      partDetails.put("License Duration",item.getValue(Integer.parseInt(applicationProperties.getProperty("LICENSE_DURATION"))).toString());
		  	                      partDetails.put("License is an upgrade",item.getValue(Integer.parseInt(applicationProperties.getProperty("LICENSE_IS_AN_UPGRADE"))).toString());	  	                              
		  	                      partDetails.put("License is transferable",item.getValue(Integer.parseInt(applicationProperties.getProperty("LICENSE_TRANSFERRABLE"))).toString());
		  	                      String kgp= item.getValue(ItemConstants.ATT_PAGE_THREE_LIST11).toString();
		  	                        if(kgp.equalsIgnoreCase(applicationProperties.getProperty("LICENSE_PART_TYPE_VAL")))
		  	                         {
		  	        	               partDetails.put("KeyGenPartNumber","Yes");  	        	
		  	        		
		  	                         }
		  	                         else
		  	                         {
		  	        	               partDetails.put("KeyGenPartNumber","No");  
		  	                         }	
		  	                    	 	          
		                         }
		       
		                  else
		                  {                  	  
		                	  partDetails.put("Part Number",item.getValue(Integer.parseInt(applicationProperties.getProperty("PART_NUMBER"))).toString());
	  	                      partDetails.put("Part Type",item.getValue(Integer.parseInt(applicationProperties.getProperty("PART_TYPE"))).toString());
	  	                      partDetails.put("Part Class",item.getValue(Integer.parseInt(applicationProperties.getProperty("PART_CLASS"))).toString());
	  	                      partDetails.put("Description",item.getValue(Integer.parseInt(applicationProperties.getProperty("PART_DESCRIPTION"))).toString());
		                      partDetails.put("License Type","");
		                      partDetails.put("License Binding Type","");
		                      partDetails.put("License Duration","");
		                      partDetails.put("License is an upgrade","");
		                      partDetails.put("KeyGenPartNumber","");
		                      partDetails.put("License is transferable","");   
		                          
		                    }
		                  	                  
		                  flag=true;		                 
	                  }	
				
				if(flag){				
					
					strXML = DpkPartXMLGenerator.createDocument(partDetails);
					response.setPartAttributeXML(strXML);
					response.setStatus("Part Details have been successfully retrieved");
					response.setObjectId(partNumber);
					response.setValidationError("");
				}else{
					strXML = DpkPartXMLGenerator.createDocument(partDetails);
					response.setPartAttributeXML(strXML);
					response.setStatus("Unable to Retrive the Part Details");
					response.setObjectId(partNumber);
					response.setValidationError("");
				}
				logger.debug(response.toString());
			}catch(Exception ex)
			{
				logger.error("Exception while working on Update Part Attribute Service:"+ex.getMessage());
				response.setStatus("Exception while working on Update Part Attribute Service:"+ex.getMessage());
				response.setObjectId(partNumber);
				response.setDbFailure(ex.getMessage());
			}
			finally
			{
				if(session!=null) {
				session.close();
				session = null;
			}
			}
		}
		logger.debug("The updateFIInstructionAttributeResponse Method Ending...");
		return response;
	}
	
	
	public IAgileSession createSession(String url, String username, String password) throws APIException {
		// TODO Auto-generated method stub
		IAgileSession session = null;
		
		
		AgileSessionFactory factory = AgileSessionFactory.getInstance(url);
		
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(AgileSessionFactory.USERNAME, username);
		map.put(AgileSessionFactory.PASSWORD, password);
		
		session = factory.createSession(map);
		return session;
	}




}
