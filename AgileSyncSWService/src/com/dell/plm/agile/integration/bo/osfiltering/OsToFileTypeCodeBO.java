package com.dell.plm.agile.integration.bo.osfiltering;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.log4j.Logger;

import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.dao.OsaLangDAO;
import com.dell.plm.agile.integration.dto.Response;
import com.dell.plm.agile.integration.xml.OsFilteringXMLGenerator;


/**
 * 
 * @author Vumesh_Gundapuneni
 *
 */
public class OsToFileTypeCodeBO {
	private static final Logger logger = Logger.getLogger(OsToFileTypeCodeBO.class);

	/**
	 * This method returns File Type Code to OS mapping XML contains Response Object   
	 * @param response
	 * @return Response
	 */
	public Response getOsToFileTypeCodeXMLResponse(final Response response)
	{
		logger.debug("The getOsToFileTypeCodeXMLResponse Method Starting.");
		synchronized(this){
			String strXML = null;
			Map<String,String> fileTypeOSCodeMap = null;
			try
			{
				fileTypeOSCodeMap = getOsToFileTypeCodeList();
				if(fileTypeOSCodeMap!=null && fileTypeOSCodeMap.size() > 0){
					strXML = OsFilteringXMLGenerator.createDocument(fileTypeOSCodeMap);
					response.setOsFileTypeCodeXML(strXML);
				}

				response.setStatus("Success");
				response.setObjectId("");
				response.setValidationError("");
				logger.debug(response.toString());
			}
			catch(Exception ex){
				logger.error("Exception while getting the OS to File Type Code Mapping: "+ex.getMessage());
				response.setStatus("Failure");
				response.setObjectId("");
				response.setDbFailure(ex.getMessage());
			}
			finally{
				if(fileTypeOSCodeMap!=null && fileTypeOSCodeMap.size() > 0){
					fileTypeOSCodeMap.clear();
					fileTypeOSCodeMap = null;
				}
			}
		}
		logger.debug("End of getOsToFileTypeCodeXMLResponse Method.");
		return response;
	}
	
	/**
	 * This Method Connect To Agile Services DB and Fetch the File Type Code and Corresponding Comma Separated OS 60 Codes in Map Object
	 * @return Map<String, String>
	 * @throws Exception
	 */
	private Map<String, String> getOsToFileTypeCodeList() throws Exception
	{
		logger.debug("The getOsToFileTypeCodeList Method Starting.");
		Map<String,String> fileTypeOSCodeMap = new HashMap<String,String>(); 
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rsOsFileType = null;
		Properties applicationProperties = null;
		String DB_PROPERTIES = "";
		MultiMap multiMap = null;
		try{
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			logger.debug("Loading aic_syncswservice_application.properties file...");
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
			DB_PROPERTIES = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
			/*DB_PROPERTIES = configBase+"\\"+DB_PROPERTIES;*/ // Ajit--> Consolidation to properties file
			logger.debug("DB Properties File Path: " + DB_PROPERTIES);
			OsaLangDAO osLangDAO = new OsaLangDAO();
			try{
				con =  osLangDAO.getConnection(DB_PROPERTIES);
				logger.debug("Got the DB Connection :" + con);
			}
			catch(FileNotFoundException ex)
			{
				logger.error("FileNotFoundException while getting the connection using properties file: "+ex.getMessage());
			}
			catch(IOException ex)
			{
				logger.error("IOException while getting the connection  using properties file: "+ex.getMessage());
			}
			catch(SQLException ex)
			{
				logger.error("SQLException while getting the connection  using properties file: "+ex.getMessage());
			}

			if(con != null)
			{
				String storedProcForOsFiltering = applicationProperties.getProperty("AGILESERVICES_DB_SP_FOR_OS_FILTERING").trim();
				logger.debug("Stored Procedure Name to fecth mapping records from AIC_AGILE_FILETYPECODES,OS_FTYPE_MAPPING and OSES Table is:"+storedProcForOsFiltering);
				cs=con.prepareCall("{call "+storedProcForOsFiltering+"(?)}");
				
				/** OS to File Type Code Mapping Cursor Out Parameter */
		        /*cs.registerOutParameter(1, oracle.jdbc.driver.OracleTypes.CURSOR);*/ // Ajit--> Changes to ojdbc6 jar
				cs.registerOutParameter(1, oracle.jdbc.OracleTypes.CURSOR);
		        cs.executeQuery();
				
		        /** Return OS File Type Cursor All Columns As Row Cursor */
		        rsOsFileType = (ResultSet)cs.getObject(1);
		        
		        /** The MultiValueMap class supports duplicate keys and unique values. This class belongs to Apache Common Collection jar file */
		        multiMap = new MultiValueMap();
		        
		        if(rsOsFileType!=null){
		        	while(rsOsFileType.next())
		        	{
		        		/** Complete File Type Code */
		        		String fileTypeCode = rsOsFileType.getString("FILE_TYPE_CODE");
		        		
		        		/** File Type Prefix Code */
		        		//String filePrefixCode = rsOsFileType.getString(2);
		        		
		        		/** OS 60 Code */
		        		String os60Code = rsOsFileType.getString("OSCODE");
		        		
		        		multiMap.put(fileTypeCode, os60Code);
		        	}
		        }
		        
		        Set fileTypeCodes = multiMap.keySet();
		        StringBuffer sb = null;
		        for(Object fileTypeCd: fileTypeCodes)
		        {
		        	List osCodes = (List)multiMap.get(fileTypeCd);
		        	sb = new StringBuffer("");
					for(Object osCode : osCodes){
						sb.append((String)osCode).append(",");
					}
					if(sb.length() > 0)
					{
						sb = sb.deleteCharAt(sb.length()-1);
						String os60Codes = sb.toString();
						fileTypeOSCodeMap.put((String)fileTypeCd, os60Codes);
					}
		        }
			}
		}
		catch(Exception e){
			logger.error("The Exception Thrown In getOsToFileTypeCodeList() Method: "+e.getMessage());
			throw e;
		}
		finally{
			if(rsOsFileType != null){
				rsOsFileType.close();
			}
			if(cs != null){
				cs.close();
			}
			if(con!=null){
				con.close();
			}
			if(multiMap !=null && multiMap.size()>0){
				multiMap.clear();
				multiMap = null;
			}
		}
		logger.debug("End of getOsToFileTypeCodeList Method.");
		return fileTypeOSCodeMap;
	}
}
