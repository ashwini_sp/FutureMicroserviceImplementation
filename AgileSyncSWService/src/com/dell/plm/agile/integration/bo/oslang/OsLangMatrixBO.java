/*
 * OsLangMetricsBO.java  
 * 
 * Version information
 * Date: 05-May-2011		
 * 
 * Copyright : DELL confidential
 * 
 */
package com.dell.plm.agile.integration.bo.oslang;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.dell.plm.agile.aic.common.dao.DBConnectionManagerSingleton;

//import com.dell.plm.agile.aic.common.dao.JDBCConnection;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.dao.OsaLangDAO;
import com.dell.plm.agile.integration.dto.Response;
import com.dell.plm.agile.integration.dto.oslang.PartOSLang;

import com.dell.plm.agile.integration.xml.OsLangXMLGenerator;



/**
 * This class reads the operating system and language details of a part number requested and sends it back to 
 * the user in response of the web service method invoked .
 * 
 * 
 * 		-------     ----------   			-----------------		-------------------------
 * 		Version 	  Author	 			Last modified date			Remarks(Modification)
 * 		------      ----------				------------------		------------------------
 * 		1.0			Yathiraja_Lingadahal	  05-May-2011				Created this class.	
 *
 */
public class OsLangMatrixBO
{
	private static final Logger logger = Logger.getLogger(OsLangMatrixBO.class);

	/**
	 * Synchronous method which returns the Response containing the XML.
	 * @param part_number part_number requested.
	 * @param response Response
	 * @param dbPropFilePath
	 * @return
	 */
	public Response getOsLangXMLResponse(final String part_number,final Response response)
	{
		synchronized(this)
		{
			List<PartOSLang> list = null;
			String strXML = null;
			try{
				//list = getPartOsLangList(part_number,"C:\\QueueProcess\\config\\rcddb.properties");
				list = getPartOsLangList(part_number);
				if(list.size()>0)
				{
					strXML = OsLangXMLGenerator.createDocument(part_number,list, null, null, null);
					response.setOsLangXML(strXML);

				}
				response.setStatus("Success");
				response.setObjectId(part_number);
				response.setValidationError("");
				logger.debug(response.toString());

			}catch(Exception ex)
			{
				logger.error("Exception while getting the part os lang list :"+ex.getMessage());
				response.setStatus("Failure");
				response.setObjectId(part_number);
				response.setDbFailure(ex.getMessage());
			}
		}

		return response;
	}

	/**
	 * Returns the list of PartOSLang which holds the operating system and language details of a part_number requested.
	 * @param part_number  part_number requested.
	 * @param dbPropFilePath db connection properties file path.
	 * @return List<PartOSLang>
	 * @throws Exception
	 */
	private List<PartOSLang> getPartOsLangList(final String part_number) throws Exception 
	{
		/*if(connection == null)
				connection = util.getDBConnection();*/
		List<PartOSLang> partOsLangList = new ArrayList<PartOSLang>();
		DBConnectionManagerSingleton dms = DBConnectionManagerSingleton.getInstance();
		
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Properties applicationProperties = null;
		String DB_PROPERTIES = "";
		try{
			
			/**Loading Application properties*/
			FilePropertyUtility fu = new FilePropertyUtility();
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			logger.debug("loading properties..");
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
			//applicationProperties = fu.loadWebAppProperties("/aic_syncswservice_application.properties");
			DB_PROPERTIES = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
			/*DB_PROPERTIES = configBase+"\\"+DB_PROPERTIES;*/ // Ajit--> Consolidation to properties file.
			OsaLangDAO osLangDAO = new OsaLangDAO();
			//Connection connection = osLangDAO.getConnection(DB_PROPERTIES, fu);
			logger.debug("File Path :" + DB_PROPERTIES);
			try{
			connection =  osLangDAO.getConnection(DB_PROPERTIES);
			}catch(FileNotFoundException ex)
			{
				logger.error("FileNotFoundException while getting the connection using properties file :"+ex.getMessage());
			}
			catch(IOException ex)
			{
				logger.error("IOException while getting the connection  using properties file:"+ex.getMessage());
			}
			catch(SQLException ex)
			{
				logger.error("SQLException while getting the connection  using properties file:"+ex.getMessage());
			}

			//connection = DBInit.getDBConnection(Constants.RCD, DB_PROPERTIES);
			if(connection != null)
			{
				logger.debug("Got the Connection :" + connection);
				String sql = "SELECT * FROM PART_OSLANG_MATRIX WHERE PART_NUMBER =?";
				
				pstmt =  connection.prepareStatement(sql);
				pstmt.setString(1, part_number);
				rs = pstmt.executeQuery();
				PartOSLang partOsLang = null;
				while(rs.next())
				{
					String strOS = rs.getString("OPERATINGSYSTEM");
					String strLang = rs.getString("LANGUAGES");
					partOsLang =  new PartOSLang(strOS,strLang);
					partOsLangList.add(partOsLang);
				}
			}
		}
		
		catch(Exception e)
		{
			throw e;
		}

		finally{
			if(connection != null)
			{
				connection.close();
			}
			if(pstmt != null)
			{
				pstmt.close();
			}
			if(rs != null)
			{
				rs.close();
			}
		}

		return partOsLangList;
	}
}
