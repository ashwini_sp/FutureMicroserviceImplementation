package com.dell.plm.agile.integration.bo.partAttribute;

import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.AgileSessionFactory;
import com.agile.api.IAgileSession;
import com.agile.api.IAttribute;
import com.agile.api.IItem;
import com.agile.api.ItemConstants;
import com.dell.plm.agile.aic.common.cryptography.StringEncryptor;
import com.dell.plm.agile.integration.dto.PartResponse;
import com.dell.plm.agile.integration.dto.Response;
import com.dell.plm.agile.integration.xml.UpdatePartAttributeXMLGenerator;

public class UpdatePartAttributeBO {

	
	private static final Logger logger = Logger.getLogger(UpdatePartAttributeBO.class);
	/**
	 * @param args
	 */
	public PartResponse updatePartAttributeResponse(final String swb_number,final String attributeName, final String attributeValue,final Properties applicationProperties, String allowedUser,final PartResponse response,final IAgileSession session, final IItem item)
	{
		logger.debug("The updateFIInstructionAttributeResponse Method Starting...");
		synchronized(this)
		{
			String strXML = null;
			boolean flag = false;
			try{
				StringTokenizer stringTokenizer = new StringTokenizer(allowedUser, "\\");
				String userAllowedAttributes = null;
				while(stringTokenizer.hasMoreTokens()){
					if(stringTokenizer.countTokens()==1)
						userAllowedAttributes = stringTokenizer.nextToken()+"_Attributes";
					else
						stringTokenizer.nextToken();
				}
				String allowedAttributes = applicationProperties.getProperty(userAllowedAttributes);
				logger.debug("Allowed Attributes for Modification--"+allowedAttributes);
				String allowedAttributesList[] = allowedAttributes.split(",");
				for(int i=0;i<allowedAttributesList.length;i++){
					String agileAttributeName = attributeName;
					if(allowedAttributesList[i].trim().equalsIgnoreCase(attributeName)){
						logger.debug("Item Revision:"+item.getRevision());
						item.setValue(agileAttributeName, attributeValue);
						logger.debug("Attribute is Updated");
						flag = true;
					}
				}
				
				if(flag){
					strXML = UpdatePartAttributeXMLGenerator.createDocument(swb_number);
					response.setPartAttributeXML(strXML);
					response.setStatus("Attribute has been Successfully Updated");
					response.setObjectId(swb_number);
					response.setValidationError("");
				}else{
					strXML = UpdatePartAttributeXMLGenerator.createDocument(swb_number);
					response.setPartAttributeXML(strXML);
					response.setStatus("Permission to Update the Attribute ("+attributeName+") is Denied to the User");
					response.setObjectId(swb_number);
					response.setValidationError("");
				}
				logger.debug(response.toString());
			}catch(Exception ex)
			{
				logger.error("Exception while working on Update Part Attribute Service:"+ex.getMessage());
				response.setStatus("Exception while working on Update Part Attribute Service:"+ex.getMessage());
				response.setObjectId(swb_number);
				response.setDbFailure(ex.getMessage());
			}
		}
		logger.debug("The updateFIInstructionAttributeResponse Method Ending...");
		return response;
	}
	
	
	public IAgileSession createSession(String url, String username, String password) throws APIException {
		// TODO Auto-generated method stub
		IAgileSession session = null;
		
		
		AgileSessionFactory factory = AgileSessionFactory.getInstance(url);
		
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(AgileSessionFactory.USERNAME, username);
		map.put(AgileSessionFactory.PASSWORD, password);
		
		session = factory.createSession(map);
		return session;
	}


}
