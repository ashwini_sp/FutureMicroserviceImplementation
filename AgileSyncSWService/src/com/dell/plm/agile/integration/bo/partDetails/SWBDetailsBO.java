package com.dell.plm.agile.integration.bo.partDetails;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.dao.OsaLangDAO;
import com.dell.plm.agile.integration.xml.SWBDetailsXMLGenerator;
import com.dell.plm.agile.integration.dto.SWBResponse;
import com.dell.plm.agile.integration.dto.oslang.PartOSLang;
import com.dell.plm.agile.integration.dto.predecessorsuccessor.SWBPredecessorSuccessor;
import com.dell.plm.agile.integration.dto.prereqcoreq.PrereqCoreqDtls;
import com.dell.plm.agile.integration.dto.prereqcoreq.ProjectPlatformAffected;


/**
 * This class reads the following and sends it back to the user 
 * in response of the web service method invoked:
 * 1) Operating system and language details of a part number requested.
 * 2) All the predecessors of a software bundle number requested.
 * 3) All the successors of a software bundle number requested.
 * 4) Prereq Number, Coreq Number, Platform Affected, Operating System and Language details of a Part Number requested.
 */

public class SWBDetailsBO
{
	private static final Logger logger = Logger.getLogger(SWBDetailsBO.class);

	/**
	 * This is synchronous method which returns the Response containing the XML.
	 * @param partNumber: Part number requested
	 * @param response Response
	 * @return SWBResponse
	 */
	public SWBResponse getPartDetailsXMLResponse(final String partNumber, final SWBResponse response)
	{
		logger.debug("The getPartDetailsXMLResponse Method Starting---");
		synchronized(this)
		{
			List<PartOSLang> osLangList = null;
			List<SWBPredecessorSuccessor> predList = null;
			List<SWBPredecessorSuccessor> succList = null;
			Map<String, List<PrereqCoreqDtls>> preCoreqMap = null;
			Map<Integer,Object> ppaMapList = null;
			String strPrereqOrderOfInstall = null;
			String strCoreqOrderOfInstall = null;
			Connection syncWSDbConnection = null;
			Connection dmRtDbConnection = null;

			String strXML = null;
			try
			{
				syncWSDbConnection = getSyncWSDbConnection();
				dmRtDbConnection = getDmRtDbConnection();

				osLangList = getPartOsLangList(partNumber, syncWSDbConnection );
				logger.debug("Got OSLang list: " +osLangList);
				predList = getSwbList(partNumber, 1, dmRtDbConnection);
				logger.debug("Got Predecessor list: " +predList);
				succList = getSwbList(partNumber, 2, dmRtDbConnection);
				logger.debug("Got Successor list: " +succList);
				ppaMapList = getPPADetails(dmRtDbConnection);
				preCoreqMap = getPartPrereqCoreqDtls(partNumber, ppaMapList, syncWSDbConnection);
				logger.debug("Got Prereq Coreq details: "+preCoreqMap);
				strPrereqOrderOfInstall = getOrderOfSwbInstallation(preCoreqMap,"P");
				strCoreqOrderOfInstall = getOrderOfSwbInstallation(preCoreqMap,"C");

				//put check to see that individual lists aren't empty, if empty, handle
				if((preCoreqMap!=null && preCoreqMap.size() > 0)  || osLangList.size()>0 || predList.size()>0 || succList.size()>0)
				{
					List<PrereqCoreqDtls> prereqDtlsList = (List<PrereqCoreqDtls>)preCoreqMap.get("P");
					List<PrereqCoreqDtls> coreqDtlsList = (List<PrereqCoreqDtls>)preCoreqMap.get("C");

					if(prereqDtlsList.size() >0 || coreqDtlsList.size()>0 || osLangList.size()>0 || predList.size()>0 || succList.size()>0)
					{
						strXML = SWBDetailsXMLGenerator.createDocument(partNumber,osLangList, null, null, predList, succList, preCoreqMap, strPrereqOrderOfInstall, strCoreqOrderOfInstall);
						response.setSWBDetailsXML(strXML);
					}
					//remove this if blank XML not needed if all lists are empty
					else
					{
						response.setSWBDetailsXML("");
					}
				}
				response.setStatus("Success");
				response.setObjectId(partNumber);
				response.setValidationError("");
				logger.debug(response.toString());
			}
			catch(Exception ex)
			{
				logger.error("Exception while getting the part os lang list :"+ex.getMessage());
				response.setStatus("Failure");
				response.setObjectId(partNumber);
				response.setDbFailure(ex.getMessage());
			}
			finally
			{
				if(dmRtDbConnection != null)
				{
					try 
					{
						dmRtDbConnection.close();
					} 
					catch (SQLException e) 
					{
						logger.error("Exception while getting coonection :"+e.getMessage());
					}
				}
				if(syncWSDbConnection != null)
				{
					try 
					{
						syncWSDbConnection.close();
					} 
					catch (SQLException e) 
					{
						logger.error("Exception while getting connection :"+e.getMessage());
					}
				}

				if(ppaMapList!=null && ppaMapList.size() > 0)
				{
					ppaMapList.clear();
					ppaMapList = null;
				}
				if(preCoreqMap!=null && preCoreqMap.size() > 0)
				{
					preCoreqMap.clear();
					preCoreqMap = null;
				}
			}
		}
		logger.debug("The getPartDetailsXMLResponse Method Ending...");
		return response;
	}

	/**
	 * This method returns connection to SyncWebServiceDb
	 * @return coonection
	 * @throws Exception
	 */
	private Connection getSyncWSDbConnection() throws Exception 
	{
		logger.info("Inside getSyncWSDbConnection method---");
		Connection connection = null;
		Properties applicationProperties = null;
		String DB_PROPERTIES = "";

		/**Loading Application properties*/
		String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
		logger.debug("loading properties..");
		applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
		DB_PROPERTIES = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
		/*DB_PROPERTIES = configBase+"\\"+DB_PROPERTIES;*/ // Ajit--> Consolidation to properties file
		OsaLangDAO osLangDAO = new OsaLangDAO();
		logger.debug("File Path :" + DB_PROPERTIES);

		try
		{
			connection =  osLangDAO.getConnection(DB_PROPERTIES);
			logger.info("Got connection: " +connection);
		}
		catch(FileNotFoundException ex)
		{
			logger.error("FileNotFoundException while getting the connection using properties file :"+ex.getMessage());
		}
		catch(IOException ex)
		{
			logger.error("IOException while getting the connection  using properties file:"+ex.getMessage());
		}
		catch(SQLException ex)
		{
			logger.error("SQLException while getting the connection  using properties file:"+ex.getMessage());
		}
		return connection;
	}

	/**
	 * This method returns connection to dm_rt_db
	 * @return connection
	 * @throws Exception
	 */
	private Connection getDmRtDbConnection() throws Exception 
	{
		logger.info("Inside getDmRtDbConnection method---");
		Connection con = null;
		Properties applicationProperties = null;
		String AGILEPLM_DM_RT_PROPERTIES = "";

		String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
		logger.debug("loading properties..");
		applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
		AGILEPLM_DM_RT_PROPERTIES = applicationProperties.getProperty("AGILEPLM_DM_RT_DBPROP_FILEPATH").trim();
		/*AGILEPLM_DM_RT_PROPERTIES = configBase+"\\"+AGILEPLM_DM_RT_PROPERTIES;*/ // Ajit--> Consolidation to properties file
		OsaLangDAO osLangDAO = new OsaLangDAO();
		logger.debug("File Path :" + AGILEPLM_DM_RT_PROPERTIES);

		try
		{
			/*con =  osLangDAO.getConnection(AGILEPLM_DM_RT_PROPERTIES);*/ // Ajit-->Consolidation to properties file
			con =  osLangDAO.getDBConnection(AGILEPLM_DM_RT_PROPERTIES);
			logger.info("Got connection: " +con);
		}
		catch(FileNotFoundException ex)
		{
			logger.error("FileNotFoundException while getting the connection using properties file: "+ex.getMessage());
		}
		catch(IOException ex)
		{
			logger.error("IOException while getting the connection  using properties file: "+ex.getMessage());
		}
		catch(SQLException ex)
		{
			logger.error("SQLException while getting the connection  using properties file: "+ex.getMessage());
		}
		return con;
	}

	/**
	 * Returns the list of PartOSLang which holds the operating system and language details of a part_number requested.
	 * @param part_number  part_number requested.
	 * @param dbPropFilePath db connection properties file path.
	 * @return List<PartOSLang>
	 * @throws Exception
	 */
	private List<PartOSLang> getPartOsLangList(final String partNumber, Connection connection) throws Exception 
	{
		List<PartOSLang> partOsLangList = new ArrayList<PartOSLang>();
		Properties applicationProperties = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			logger.debug("loading properties..");
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
			
			if(connection != null)
			{
				logger.debug("Got the Connection :" + connection);
				String sql = applicationProperties.getProperty("OSLANG_QUERY").trim();
				//String sql = "SELECT * FROM PART_OSLANG_MATRIX WHERE PART_NUMBER =?";

				pstmt =  connection.prepareStatement(sql);
				pstmt.setString(1, partNumber);
				rs = pstmt.executeQuery();
				PartOSLang partOsLang = null;
				while(rs.next())
				{
					String strOS = rs.getString("OPERATINGSYSTEM");
					String strLang = rs.getString("LANGUAGES");
					partOsLang =  new PartOSLang(strOS,strLang);
					partOsLangList.add(partOsLang);
				}
			}
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			if(pstmt != null)
			{
				pstmt.close();
			}
			if(rs != null)
			{
				rs.close();
			}
		}
		return partOsLangList;
	}

	/**
	 * Returns the list of SWBPredecessorSuccessor which holds the predecessor or successor software bundle numbers of a swb_number requested.
	 * @param swb_number  swb_number requested.
	 * @return List<SWBPredecessorSuccessor>
	 * @throws Exception
	 */
	private List<SWBPredecessorSuccessor> getSwbList(final String partNumber, int predorsucc, Connection con) throws Exception 
	{
		List<SWBPredecessorSuccessor> swbPredecessorSuccessorList = new ArrayList<SWBPredecessorSuccessor>();
		CallableStatement cs = null;
		ResultSet rs = null;
		Properties applicationProperties = null;
		String proc_Name = "";
		try
		{
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			logger.debug("loading properties..");
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");

			if(predorsucc==1)
			{proc_Name= applicationProperties.getProperty("AGILEPLM_DM_RT_SP_PREDCESSOR").trim();}
			else if(predorsucc==2)
			{proc_Name= applicationProperties.getProperty("AGILEPLM_DM_RT_SP_SUCCESSOR").trim();}
			logger.debug("proc_Name:" + proc_Name);	

			if(con != null)
			{
				logger.debug("Got the Connection :" + con);

				if(predorsucc==1)
				{
					cs=con.prepareCall("{call "+proc_Name+"(?,?)}");
				}

				else if(predorsucc==2)
				{	
					cs=con.prepareCall("{call "+proc_Name+"(?,?)}");
				}

				cs.setString(1,partNumber);
				/*cs.registerOutParameter(2, oracle.jdbc.driver.OracleTypes.CURSOR);*/ // Ajit--> Changing jar to ojdbc6
				cs.registerOutParameter(2, oracle.jdbc.OracleTypes.CURSOR);
				cs.executeQuery();
				logger.debug("Executed proc"+proc_Name);
				rs = (ResultSet)cs.getObject(2);

				SWBPredecessorSuccessor swbPredecessorSuccessor=null;

				if(rs!=null)
				{
					while(rs.next())
					{
						String swbNumber = rs.getString("ITEM_NUMBER");
						swbPredecessorSuccessor = new SWBPredecessorSuccessor(swbNumber);
						swbPredecessorSuccessorList.add(swbPredecessorSuccessor);
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Exception : "+ e.getMessage());
			throw e;
		}
		finally
		{
			if(rs != null)
			{
				rs.close();
			}
			if(cs != null)
			{
				cs.close();
			}

		}
		return swbPredecessorSuccessorList;
	}

	/**
	 * This method fetches all the records from PLATFORM_CODE_NAMES Table from AGILEPLM_DM_RT schema.
	 * @return Map<Integer,Object>
	 * @throws Exception
	 */
	private Map<Integer,Object> getPPADetails(Connection con) throws Exception
	{
		logger.debug("The getPPADetails Method Starting---");
		Map<Integer,Object> ppaMapList = new HashMap<Integer,Object>();
		CallableStatement cstmt = null;
		ResultSet rsPPA = null;
		Properties applicationProperties = null;
		String admDbPlatformCodeNmProc = "";
		try
		{
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			logger.debug("Loading properties for aic_syncswservice_application.properties...");
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");

			admDbPlatformCodeNmProc = applicationProperties.getProperty("ADM_DB_PLATFORM_CODE_NAME_PROC").trim();
			logger.debug("The Platform Code Name Stored Procedure Name: " + admDbPlatformCodeNmProc);

			if(con != null)
			{
				/**
				 *  The PKG_SUCCESSOR_POPULATE.SP_GET_PPA_DETAILS stored procedure pull all the records from 
				 *  AGILEPLM_DM.PLATFORM_CODE_NAME table based on max(id) of all the records. 
				 */
				cstmt = con.prepareCall("{call "+admDbPlatformCodeNmProc+"(?)}");
				/*cstmt.registerOutParameter(1, oracle.jdbc.driver.OracleTypes.CURSOR);*/ // Ajit-->Changing jar to ojdbc6
				cstmt.registerOutParameter(1, oracle.jdbc.OracleTypes.CURSOR);
				cstmt.executeQuery();
				logger.debug("Executed Stored Procedure: "+admDbPlatformCodeNmProc+" Successfully.");
				rsPPA = (ResultSet)cstmt.getObject(1);

				ProjectPlatformAffected projPlatformAff = null;
				if(rsPPA !=null)
				{
					while(rsPPA.next())
					{
						/** This will give the PH Number*/
						String strPhNbr = rsPPA.getString(1);

						/** This will give the PPA Name */
						String strPPA = rsPPA.getString(2);

						/** This will give Active flag Name */
						//String flag = rsPPA.getString(3);

						/** This will give ID number */
						Integer id = new Integer(rsPPA.getInt(4));

						//logger.debug(strPhNbr+"\t"+strPPA+"\t"+id);

						projPlatformAff = new ProjectPlatformAffected();
						projPlatformAff.setId(id);
						projPlatformAff.setPhNumber(strPhNbr);
						projPlatformAff.setPpa(strPPA);
						ppaMapList.put(id,projPlatformAff);
					}
				}
			}
			logger.debug("Total Records in Platform Code Name Table:"+ppaMapList.size());
		}
		catch(Exception e)
		{
			logger.error("Exception in getPPADetails() Method: "+ e.getMessage());
			throw e;
		}
		finally
		{
			if(rsPPA != null)
			{
				rsPPA.close();
			}
			if(cstmt != null)
			{
				cstmt.close();
			}
		}
		logger.debug("End of getPPADetails Method.");
		return ppaMapList;
	}


	/**
	 * This method returns the list of Prereq/Coreq Numbers in String based on map and flag requested.
	 * @param preCoreqMap
	 * @param flag (P-Prereq list & C- Coreq list)
	 * @return String (List of Prereq or Coreq SWB Numbers with comma separated) 
	 * @throws Exception
	 */
	private String getOrderOfSwbInstallation(Map<String, List<PrereqCoreqDtls>> preCoreqMap, String flag) throws Exception
	{
		logger.debug("The getOrderOfSwbInstallation Method Starting...");
		StringBuffer strOrderOfInstall =new StringBuffer("");
		Map<Integer,String> orderMap = new HashMap<Integer,String>();
		List<PrereqCoreqDtls> prereqCoreqDtlsList = (List<PrereqCoreqDtls>)preCoreqMap.get(flag);
		if(prereqCoreqDtlsList!=null && prereqCoreqDtlsList.size() > 0)
		{
			for(PrereqCoreqDtls prereqCoreqDtls:prereqCoreqDtlsList)
			{
				orderMap.put(prereqCoreqDtls.getOrderOfInstallation(),prereqCoreqDtls.getPrereqCoreqNbr().trim());
				logger.debug(prereqCoreqDtls.getOrderOfInstallation()+"\t"+prereqCoreqDtls.getPrereqCoreqNbr().trim());
			}
			for(int i=0;i<orderMap.size();i++)
			{    
				String swbNumber= orderMap.get(i+1);
				logger.debug(orderMap.get(i+1));
				strOrderOfInstall.append(swbNumber).append(",");
			}
			if(strOrderOfInstall.length() > 0)
			{
				strOrderOfInstall = strOrderOfInstall.deleteCharAt(strOrderOfInstall.length()-1);
			}
		}
		logger.debug("The Order of installation of SWB Numbers:"+strOrderOfInstall.toString()+" for Prereq/Coreq flag number: "+flag);
		logger.debug("The getOrderOfSwbInstallation Method Ending...");
		return strOrderOfInstall.toString();
	}

	/**
	 * This method returns the Map, which holds the Prereq or Coreq details as list based on a SWB Numbers requested.
	 * @param swb_number
	 * @param Map
	 * @return Map<String, List<PrereqCoreqDtls>>
	 * @throws Exception
	 */
	private Map<String, List<PrereqCoreqDtls>> getPartPrereqCoreqDtls(final String partNumber, final Map<Integer,Object> ppaMapList, Connection con) throws Exception
	{
		logger.debug("The getPartPrereqCoreqDtls Method Starting...");
		Map<String, List<PrereqCoreqDtls>> preCoreqMap = new HashMap<String, List<PrereqCoreqDtls>>(); 
		List<PrereqCoreqDtls> swbPrereqList = null;
		List<PrereqCoreqDtls> swbCoreqList = null;
		CallableStatement cs = null;
		ResultSet rsPreReq = null;
		ResultSet rsCoReq = null;
		Properties applicationProperties = null;
		Map<Integer,String> allMap = null;
		String allPlatformCode ="";
		String allOsCode ="";
		String allLangCode ="";
		try
		{
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			logger.debug("loading aic_syncswservice_application.properties file...");
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");

			allMap = new HashMap<Integer,String>();
			allPlatformCode = applicationProperties.getProperty("ALL_PLATFORM_VALUES").trim();
			allOsCode = applicationProperties.getProperty("ALL_OS_VALUES").trim();
			allLangCode = applicationProperties.getProperty("ALL_LANG_VALUES").trim();
			allMap.put(Integer.parseInt(allPlatformCode), applicationProperties.getProperty(allPlatformCode).trim());
			allMap.put(Integer.parseInt(allOsCode), applicationProperties.getProperty(allOsCode).trim());
			allMap.put(Integer.parseInt(allLangCode), applicationProperties.getProperty(allLangCode).trim());

			if(con != null)
			{
				String storedProcForPrereqCoreq = applicationProperties.getProperty("AGILESERVICES_DB_SP_FOR_PREREQ_COREQ").trim();
				logger.debug("Stored Procedure Name To Fecth Records from PREREQ_COREQ_DTLS Table is: " + storedProcForPrereqCoreq);

				cs=con.prepareCall("{call "+storedProcForPrereqCoreq+"(?,?,?,?,?,?,?)}");

				/** Set Candidate SWB Number */
				cs.setString(1,partNumber);

				/** Set All OS Code */
				cs.setString(2,allOsCode);

				/** Set All OS Code Value */
				cs.setString(3,allMap.get(Integer.parseInt(allOsCode)));

				/** Set All Language Code */
				cs.setString(4,allLangCode);

				/** Set All Language Code  Value*/
				cs.setString(5,allMap.get(Integer.parseInt(allLangCode)));

				/** Pre-req Cusrosr Out Parameter */
				/*cs.registerOutParameter(6, oracle.jdbc.driver.OracleTypes.CURSOR);*/ // Ajit--> changing jar to ojdbc6
				cs.registerOutParameter(6, oracle.jdbc.OracleTypes.CURSOR);

				/** Co-req Cusrosr Out Parameter */
				/*cs.registerOutParameter(7, oracle.jdbc.driver.OracleTypes.CURSOR);*/ // Ajit--> changing jar to ojdbc6 
				cs.registerOutParameter(7, oracle.jdbc.OracleTypes.CURSOR);
				cs.executeQuery();

				/** Return Pre-req Cursor Contains All Columns As Row Cursor  */
				rsPreReq = (ResultSet)cs.getObject(6);

				/** Return Co-req Cursor Contains All Columns As Row Cursor  */
				rsCoReq = (ResultSet)cs.getObject(7);

				swbPrereqList= getPrereqCoreqListFromResultSet(rsPreReq, ppaMapList, allMap);
				preCoreqMap.put("P", swbPrereqList);

				swbCoreqList = getPrereqCoreqListFromResultSet(rsCoReq, ppaMapList, allMap);
				preCoreqMap.put("C", swbCoreqList);
			}
		}
		catch(Exception e)
		{
			logger.error("The exception thrown in getPartPrereqCoreqDtls() Method: "+e.getMessage());
			throw e;
		}
		finally
		{
			if(rsPreReq != null)
			{
				rsPreReq.close();
			}

			if(rsCoReq != null)
			{
				rsCoReq.close();
			}
			if(cs != null)
			{
				cs.close();
			}
		}
		logger.debug("End of getPartPrereqCoreqDtls Method...");
		return preCoreqMap;
	}

	/**
	 * This method returns list of PrereqCoreqDtls based on ResultSet of  Prereq/Coreq. 
	 * @param rs
	 * @param ppaMapList
	 * @param allMap
	 * @return List<PrereqCoreqDtls>
	 * @throws Exception
	 */
	private List<PrereqCoreqDtls> getPrereqCoreqListFromResultSet(final ResultSet rs, final Map<Integer,Object> ppaMapList, final Map<Integer,String> allMap) throws Exception
	{
		logger.debug("The getPrereqCoreqListFromResultSet Method Starting...");
		List<PrereqCoreqDtls> swbPrereqCoreqList = new ArrayList<PrereqCoreqDtls>();
		PrereqCoreqDtls prereqCoreqDtls = null; 
		if(rs!=null)
		{
			while(rs.next())
			{
				String swbNumber = rs.getString("SWB_NUMBER");
				String prereqCoreqNbr = rs.getString("PREREQ_COREQ_NUMBER");
				String prereqCoreqFlag = rs.getString("PREREQ_COREQ_FLAG");
				Integer orderOfInstallation = new Integer(rs.getInt("ORDER_OF_INSTALLATION"));
				String specificOrLaterVer = rs.getString("SPECIFIC_OR_LATER_VERSION");
				String platformIds = rs.getString("PLATFORM");
				String languageIds = rs.getString("LANG_CODE");
				String lossOfFuncRsnTxt = rs.getString("LOSS_OF_FUNC_REASON_TXT");
				String osIds = rs.getString("OS_CODE");

				prereqCoreqDtls = new PrereqCoreqDtls();
				prereqCoreqDtls.setSwbNumber(swbNumber);
				prereqCoreqDtls.setPrereqCoreqNbr(prereqCoreqNbr);
				prereqCoreqDtls.setPrereqCoreqFlag(prereqCoreqFlag);
				prereqCoreqDtls.setOrderOfInstallation(orderOfInstallation);
				prereqCoreqDtls.setSpecificOrLaterVer(specificOrLaterVer);
				prereqCoreqDtls.setPlatformIds(platformIds);
				prereqCoreqDtls.setOsIds(osIds);
				prereqCoreqDtls.setLangIds(languageIds);
				prereqCoreqDtls.setLossOfFuncRsnTxt(lossOfFuncRsnTxt);


				/** Retrieve PH Numbers and PPA based on ID numbers. */  
				List <Integer> listPPAIds = prereqCoreqDtls.getPPAIdList();
				StringBuffer sbPhNumbers =new StringBuffer("");
				for(Integer ppaId:listPPAIds)
				{
					if(!allMap.containsKey(ppaId))
					{
						ProjectPlatformAffected ppaObj=(ProjectPlatformAffected)ppaMapList.get(ppaId);
						if(ppaObj!=null)
						{
							sbPhNumbers.append(ppaObj.getPhNumber()).append(",").append(ppaObj.getPpa()).append(";");
						}
						else
						{
							logger.error("The PPA id: "+ppaId+" doesn't have corresponding PH Number and PPA.");
						}
					}
					else
					{
						sbPhNumbers.append("All").append(",").append(allMap.get(ppaId)).append(";");
						break;
					}
				}
				if(sbPhNumbers!=null && sbPhNumbers.length() > 0)
				{
					sbPhNumbers = sbPhNumbers.deleteCharAt(sbPhNumbers.length()-1);
					prereqCoreqDtls.setPlatformPhNbr(sbPhNumbers.toString());
				}
				else
				{
					prereqCoreqDtls.setPlatformPhNbr(sbPhNumbers.toString());
				}
				logger.debug(swbNumber+"\t"+prereqCoreqNbr+"\t"+prereqCoreqFlag+"\t"+orderOfInstallation+"\t"+specificOrLaterVer+"\t"+prereqCoreqDtls.getPlatformPhNbr()+"\t"+languageIds+"\t"+osIds+"\t"+lossOfFuncRsnTxt);
				swbPrereqCoreqList.add(prereqCoreqDtls);
			}
		}
		logger.debug("The getPrereqCoreqListFromResultSet Method Ending...");
		return swbPrereqCoreqList;
	}
}		