package com.dell.plm.agile.integration.bo.partDetails;

import java.util.Iterator;
import java.util.Properties;
import org.apache.log4j.Logger;
import com.agile.api.APIException;
import com.agile.api.IAgileSession;
import com.agile.api.IChange;
import com.agile.api.IItem;
import com.agile.api.IRow;
import com.agile.api.ITable;
import com.agile.api.ItemConstants;
import com.dell.plm.agile.aic.common.cryptography.StringEncryptor;
import com.dell.plm.agile.aic.common.util.AgileCommonUtil;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.dto.AFPResponse;
import com.dell.plm.agile.integration.util.Utility;

/**
 *This class determines the status of a Software Bundle as:
 *1. Active
 *2. Invalid
 *3. Demoted
 */

public class SWBStatusBO
{
	private static final Logger logger = Logger.getLogger(SWBStatusBO.class);
	Properties applicationProperties = null;
	IAgileSession session = null;

	/**
	 * This is synchronous method which returns the Response containing the SWB Status.
	 * @param partNumber
	 * @param response 
	 * @return AFPResponse
	 */
	public AFPResponse setSwbStatus(final String partNumber, final AFPResponse response)
	{
		logger.debug("Inside setSwbStatus method---");
		synchronized(this)
		{
			String username = null;
			String password = null;
			String url = null;
			IItem item = null;
			IChange change = null;
			String changeType = "";
			String ackStatus = "";
			Properties dbProps = null;// Ajit--> Consolidation to properties file

			try
			{
				try
				{
					//Loading property file
					String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
					applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
					logger.debug("Loaded aic_syncswservice_application properties---");
				} 
				catch(Exception e) 
				{
					logger.error("Exception while loading property files: "+AgileCommonUtil.exception2String(e));
				}

				//Getting properties to create session
				/*username = applicationProperties.getProperty("ESUPPORT_ACK_UPDATE_USER");*/ //Ajit--> Consolidation to properties file
				
				String dbPropsfilePath = applicationProperties.getProperty("DBPROP_FILEPATH");
				dbProps = FilePropertyUtility.loadProperties(dbPropsfilePath);
				username = dbProps.getProperty("PLATFORMPROMOTION_USERNAME");
				logger.debug("username******************* :: " + username);
				/*password = StringEncryptor.decrypt(applicationProperties.getProperty("ESUPPORT_ACK_UPDATE_PASS"));*/ // Ajit--> Consolidation to properties file
				password = StringEncryptor.decrypt(dbProps.getProperty("PLATFORMPROMOTION_PASSWORD"));
				/*url = applicationProperties.getProperty("AGILE.URL");*/  // Ajit--> Consolidation to properties file
				url = dbProps.getProperty("AGILE_BIGIP_URL");
				logger.debug("url******************* :: " + url);
				//Creating session and getting object
				session = Utility.createSession(url,username,password);
				logger.debug("Session created!");
				item= (IItem) session.getObject(IItem.OBJECT_TYPE, partNumber);
				logger.debug("Got Item object!");

				//Initializing SWB Status as empty string
				response.setStatus("");

				if(item == null)
				{
					//Setting status as 'Invalid' if the Part Number given as input doesn't exist in Agile
					response.setStatus("Invalid");
					logger.info("SWB Status-->" +response.getStatus() +" ::: Reason: Part Number-->" +partNumber +" is not a valid part in Agile!");
				}
				else
				{
					if(!isSoftwareBundle(item) || !isARev(item) || !checkDestination(item))
					{
						/*Setting SWB status as 'Invalid' if the Part Number given as input is not a 'Software Bundle' or
						 * is not in 'A Revision' or doesn't contain 'Web (eSupport)' in 'Destination'
						 */
						response.setStatus("Invalid");
						logger.info("SWB Status-->" +response.getStatus());
					}
					else
					{
						//if 'E-Promoted Platforms' has values
						if(!getEpromotedPlatforms(item).isEmpty())
						{
							logger.info("'E-Promoted Platforms' is not empty. Iterating through Change History table for packaged WPCOs---");
							//Getting the Change History table and iterating through
							ITable AI= item.getTable(applicationProperties.getProperty("CHANGE_HISTORY"));
							Iterator<?> i = AI.iterator();
							while(i.hasNext()) 
							{
								IRow row=(IRow)i.next();

								//If row is a Packaged WPCO, getting the change object
								if((applicationProperties.getProperty("WEB_PROMOTION")).equalsIgnoreCase(row.getValue(applicationProperties.getProperty("CHANGE_HISTORY_CHANGE_TYPE")).toString()) && (applicationProperties.getProperty("PACKAGED")).equalsIgnoreCase(row.getValue(applicationProperties.getProperty("CHANGE_HISTORY_STATUS")).toString()))
								{
									change= (IChange) session.getObject(IChange.OBJECT_TYPE, row.getValue(applicationProperties.getProperty("CHANGE_HISTORY_NUMBER")));

									//Getting the 'Web Promotion' type of the WPCO
									changeType = change.getValue(applicationProperties.getProperty("MO_WEB_PROMOTION_TYPE")).toString();

									logger.info("Change Number-->"+change.getName() +" ::: Change Type-->"+changeType);

									//If the WPCO is of type 'Software Bundle Promote' or 'Auto Platform Promotion'
									if((applicationProperties.getProperty("WPCO_TYPE_SWB_PROMOTE")).equalsIgnoreCase(changeType) || (applicationProperties.getProperty("WPCO_TYPE_AUTO_PLAFORM_PROMOTE")).equalsIgnoreCase(changeType))
									{
										//Getting the value of Page Three attribute 'eSupport Ack Status' of WPCO
										ackStatus= change.getValue(applicationProperties.getProperty("PAGE_THREE_ACK_STATUS")).toString();
										logger.info("eSupport Ack Status-->" +ackStatus);

										if(!(applicationProperties.getProperty("STATUS_FAILED")).equalsIgnoreCase(ackStatus))
										{
											//Setting the SWB Status as 'Active' if the Page Three attribute 'eSupport Ack Status' of WPCO is not 'Failed'
											response.setStatus("Active");
											logger.info("SWB Status-->" +response.getStatus() +" ::: Reason: 'eSupport Ack Status' is not 'Failed'");
											break;
										}
									}
								}
							}

							/* Setting SWB Status as 'Invalid',
							 * when 'E-Promoted Platforms' attribute has values(s) and ---
							 * ---if 'Change History' table doesn't have any Packaged WPCO of type 'Software Bundle Promote' or 'Auto Platform Promotion',--- 
							 * --- whose 'eSupport Ack Status' is not 'Failed'
							 */
							if(!response.getStatus().equalsIgnoreCase("Active"))
							{
								response.setStatus("Invalid");
								logger.info("SWB Status-->" +response.getStatus() +" ::: Reason: 'E-Promoted Platforms' is not empty and 'Change History' table doesn't have any Packaged WPCO of type 'Software Bundle Promote' or 'Auto Platform Promotion', whose 'eSupport Ack Status' is not 'Failed'");
							}
						}
						else
						{
							logger.info("'E-Promoted Platforms' is empty. Iterating through Change History table for packaged WPCOs---");
							
							//When 'E-Promoted Platforms' is empty,, Iterating through Change History table
							ITable AI= item.getTable(applicationProperties.getProperty("CHANGE_HISTORY"));
							Iterator<?> i = AI.iterator();
							while(i.hasNext()) 
							{
								IRow row=(IRow)i.next();

								if((applicationProperties.getProperty("WEB_PROMOTION")).equalsIgnoreCase(row.getValue(applicationProperties.getProperty("CHANGE_HISTORY_CHANGE_TYPE")).toString()) && (applicationProperties.getProperty("PACKAGED")).equalsIgnoreCase(row.getValue(applicationProperties.getProperty("CHANGE_HISTORY_STATUS")).toString()))
								{
									logger.info("Change Number-->"+row.getValue(applicationProperties.getProperty("CHANGE_HISTORY_NUMBER"))); 
									//Setting SWB Status as 'Demoted' if row has a Packaged WPCO
									response.setStatus("Demoted");
									logger.info("SWB Status-->" +response.getStatus() +" ::: Reason: 'E-Promoted Platforms' attribute has no value and 'Change History' table has Packaged WPCO");
									break;
								}
							}

							/* Setting the SWB Status as 'Invalid',
							 * when 'E-Promoted Platforms' attribute has no value and if 'Change History' table doesn't have any Packaged WPCO
							 */
							if(!response.getStatus().equalsIgnoreCase("Demoted"))
							{
								response.setStatus("Invalid");
								logger.info("SWB Status-->" +response.getStatus() +" ::: Reason: 'E-Promoted Platforms' attribute has no value and 'Change History' table doesn't have any Packaged WPCO");
							}
						}
					}
				}
				response.setObjectId(partNumber);
				response.setValidationError("");
			}
			catch(Exception ex)
			{
				logger.error("Exception: "+ex.getMessage());
				response.setStatus("");
				response.setObjectId("");
				response.setValidationError(ex.getMessage());				
			}
			finally
			{
				username = null;
				password = null;
				url = null;
				item = null;
				changeType = null;
				ackStatus = null;
			}
		}
		logger.debug("End of setSwbStatus method.");
		return response;
	}

	/**
	 * This method determines if a Part is Software Bundle.
	 * @param IItem
	 * @return boolean
	 * @throws Exception 
	 */
	public boolean isSoftwareBundle(IItem item) throws Exception
	{
		logger.info("Inside isSoftwareBundle method with Part-->" +item.getName());
		String partClass = null;
		String partType = null;
		try
		{
			//Getting the 'Part Type' and 'Part Class' values
			partClass = item.getValue(ItemConstants.ATT_TITLE_BLOCK_PART_TYPE).toString();
			partType = item.getValue(ItemConstants.ATT_TITLE_BLOCK_PART_CATEGORY).toString();
			logger.info("Item Part Type-->" +partType +" ::: Part Class-->" +partClass);
			if(partClass !=null && !partClass.isEmpty() && partType !=null && !partType.isEmpty())
			{
				//Checking if both 'Part Type' and 'Part Class' are 'Software Bundle'
				if(applicationProperties.getProperty("SOFTWARE_BUNDLE").equalsIgnoreCase(partClass) && applicationProperties.getProperty("SOFTWARE_BUNDLE").equalsIgnoreCase(partType))
				{
					logger.info(" Both 'Part Type' and 'Part Class' are 'Software Bundle'. Returning isSoftwareBundle as true---");
					return true;
				}
				else
				{
					logger.info("Part-->"+item.getName() + " is not a Software Bundle part");
				}
			}
		}
		catch(Exception e)
		{
			logger.error(AgileCommonUtil.exception2String(e));
		}
		finally
		{
			partClass = null;
			partType = null;
		}
		return false;
	}

	/**
	 * This method checks the Page Three attribute 'Destination' of a Part.
	 * It returns true, if the Destination attribute contains the values web(eSupport).
	 * @param IItem
	 * @return boolean
	 * @throws APIException 
	 */
	boolean checkDestination(IItem item) throws Exception
	{
		logger.info("Inside method checkDestination with Part-->" +item.getName());
		try
		{
			String[] swbDest = getDestination(item).split(";");
			for(int d=0;d<swbDest.length;d++)
			{
				logger.info(swbDest[d]);
				if(swbDest[d].equalsIgnoreCase(applicationProperties.getProperty("WEB_ESUPPORT").trim()))
				{
					logger.info("Destination has web (eSupport), returning true---");
					return true;
				}
			}
			logger.info("Part-->" +item.getName() +" doesn't contain web (eSupport) as destination");
		}
		catch(Exception ex)
		{
			logger.info(AgileCommonUtil.exception2String(ex));
		}
		return false;
	}

	/**
	 * This method gets the Page Three attribute 'Destination' of a Part.
	 * @param IItem
	 * @return String
	 * @throws Exception 
	 */
	public String getDestination(IItem item) throws Exception
	{
		logger.info("Inside getDestination method with Part-->"+item.getName() );
		String dest = null;
		try
		{
			dest = item.getValue(applicationProperties.getProperty("DESTINATION")).toString();
			logger.info("Part-->"+item.getName() +" ::: Destination-->" +dest);
			if(dest != null && !dest.isEmpty())
			{
				return dest;
			}
		}
		catch(Exception e)
		{
			logger.error(AgileCommonUtil.exception2String(e));
		}
		return "";
	}

	/**
	 * This method check if the item is in the Lifecycle Phase 'A Revision'.
	 * @param IItem
	 * @return boolean
	 * @throws Exception 
	 */
	public boolean isARev(IItem item) throws Exception
	{
		logger.info("Inside isARev method with Part-->"+item.getName() );
		try
		{
			if(applicationProperties.getProperty("REVISION_A").equalsIgnoreCase(getRev(item)))
			{
				logger.info("Lifecycle Phase is in A Revision. Returning true--");
				return true;
			}
			else
			{
				logger.info("Part-->"+item.getName() + " is not in A Revision");
			}
		}
		catch(Exception e)
		{
			logger.error(AgileCommonUtil.exception2String(e));
		}
		return false;
	}

	/**
	 * This method gets the Lifecycle Phase of a Part.
	 * @param IItem
	 * @return String
	 * @throws Exception 
	 */
	public String getRev(IItem item) throws Exception
	{
		logger.info("Inside getRev method with Part-->"+item.getName() );
		String lifeCyclePhase = null;
		try
		{
			lifeCyclePhase = item.getValue(applicationProperties.getProperty("LIFECYCLE_PHASE")).toString();
			logger.info("Part-->"+item.getName() +" ::: Lifecyle Phase-->" +lifeCyclePhase);
			if(lifeCyclePhase !=null && !lifeCyclePhase.isEmpty())
			{
				return lifeCyclePhase;
			}
		}
		catch(Exception e)
		{
			logger.error(AgileCommonUtil.exception2String(e));
		}
		return "";
	}

	/**
	 * This method gets the value of ePromoted Platforms.
	 * @param Item
	 * @return String
	 * @throws Exception 
	 */
	public String getEpromotedPlatforms(IItem item) throws Exception
	{
		logger.info("Inside getEpromotedPlatforms method with Part-->"+item.getName() );
		String ePromotedPlatforms = null;
		try
		{
			ePromotedPlatforms = item.getValue(applicationProperties.getProperty("EPROMOTED_PLATFORMS")).toString();
			logger.info("Part-->"+item.getName() +" ::: ePromoted Platforms-->" +ePromotedPlatforms);
			if(ePromotedPlatforms !=null && !ePromotedPlatforms.isEmpty())
			{
				return ePromotedPlatforms;
			}
		}
		catch(Exception e)
		{
			logger.error(AgileCommonUtil.exception2String(e));
		}
		return "";
	}
}		