package com.dell.plm.agile.integration.bo.predecessorsuccessor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
//import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
//import static com.dell.plm.agile.integration.util.Constants.PROC_NAME_SWBPREDECESSOR;
//import com.dell.plm.agile.aic.common.dao.DBConnectionManagerSingleton;
//import com.dell.plm.agile.aic.common.exception.ValidationException;

import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
//import com.dell.plm.agile.integration.bo.oslang.OsLangMatrixBO;
import com.dell.plm.agile.integration.dao.OsaLangDAO;
import com.dell.plm.agile.integration.dto.Response;
import com.dell.plm.agile.integration.dto.SuccessorResponse;
//import com.dell.plm.agile.integration.dto.oslang.PartOSLang;
//D&D change 4
//import com.dell.plm.agile.integration.dto.predecessors.SWBPredecessor;
import com.dell.plm.agile.integration.dto.predecessorsuccessor.SWBPredecessorSuccessor;
//D&D change over
//import com.dell.plm.agile.integration.util.Constants;
import com.dell.plm.agile.integration.xml.SWBXMLGenerator;


/**
 * This class reads all the predecessors of a software bundle number requested and sends it back to 
 * the user in response of the web service method invoked .
 * 
 * @author Vumesh_Gundapuneni
 */
public class SWBPredecessorSuccessorBO {
	
	private static final Logger logger = Logger.getLogger(SWBPredecessorSuccessorBO.class);
	
	/**
	 * Synchronous method which returns the Response containing the XML.
	 * @param swb_number String requested.
	 * @param response Response
	 * @return
	 */
	public Response getPredecessorXMLResponse(final String swb_number,final Response response)
	{
		logger.debug("The getPredecessorXMLResponse Method Starting...");
		synchronized(this)
		{
			//D&D change 5
			//List<SWBPredecessor> list = null;
			List<SWBPredecessorSuccessor> list = null;
			//D&D change over
			String strXML = null;
			try{
				//D&D change 16
				//list = getSwbList(swb_number);
				list = getSwbList(swb_number,1);
				//D&D change over
				if(list.size()>0)
				{
					//D&D change 28
					//strXML = SWBXMLGenerator.createDocument(swb_number,list);
					strXML = SWBXMLGenerator.createDocument(swb_number,list,1);
					//D&D change over
					
					response.setSwbPredecessorXML(strXML);
				}
				response.setStatus("Success");
				response.setObjectId(swb_number);
				response.setValidationError("");
				logger.debug(response.toString());
			}
			catch(Exception ex)
			{
				logger.error("Exception while getting the predecessor swb number list :"+ex.getMessage());
				response.setStatus("Failure");
				response.setObjectId(swb_number);
				response.setDbFailure(ex.getMessage());
			}
		}
		logger.debug("The getPredecessorXMLResponse Method Ending...");
		return response;
	}
	
	//D&D change 6
		/**
		 * Synchronous method which returns the Response containing the XML.
		 * @param swb_number String requested.
		 * @param response Response
		 * @return
		 */
		public SuccessorResponse getSuccessorXMLResponse(final String swb_number,final SuccessorResponse succResponse)
		{
			logger.debug("The getSuccessorXMLResponse Method Starting...");
			synchronized(this)
			{
				
				List<SWBPredecessorSuccessor> list = null;
				
				String strXML = null;
				try{
					//D&D change 17
					//list = getSwbList(swb_number);
					list = getSwbList(swb_number,2);
					
					//D&D change over
					if(list.size()>0)
					{
						//D&D change 29
						strXML = SWBXMLGenerator.createDocument(swb_number,list,2);
						//D&D change over
						
						succResponse.setSwbSuccessorXML(strXML);
					}
					succResponse.setStatus("Success");
					succResponse.setObjectId(swb_number);
					succResponse.setValidationError("");
					logger.debug(succResponse.toString());
				}
				catch(Exception ex)
				{
					logger.error("Exception while getting the successor swb number list :"+ex.getMessage());
					succResponse.setStatus("Failure");
					succResponse.setObjectId(swb_number);
					succResponse.setDbFailure(ex.getMessage());
				}
				logger.debug(list);
			}
			
			logger.debug("The getSuccessorXMLResponse Method Ending...");
			return succResponse;
		}


	/**
	 * Returns the list of SWBPredecessorSuccessor which holds the predecessor or successor software bundle numbers of a swb_number requested.
	 * @param swb_number  swb_number requested.
	 * @return List<SWBPredecessorSuccessor>
	 * @throws Exception
	 */
	
	//D&D change 7: changed list type to <SWBPredecessorSuccessor> from <SWBPredecessor>
	//D&D change 18: added argument (int predorsucc)...1 is predecessor, 2 is successor
	private List<SWBPredecessorSuccessor> getSwbList(final String swb_number, int predorsucc) throws Exception 
	{
		//D&D change 8
		//List<SWBPredecessor> swbPredecessorList = new ArrayList<SWBPredecessor>();
		List<SWBPredecessorSuccessor> swbPredecessorSuccessorList = new ArrayList<SWBPredecessorSuccessor>();
		//D&D change over
		
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Properties applicationProperties = null;
		//String ADM_DB_PROPERTIES = "";
		String AGILEPLM_DM_RT_PROPERTIES = "";
		String proc_Name = "";
		try{
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			logger.debug("loading properties..");
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
			//ADM_DB_PROPERTIES = applicationProperties.getProperty("ADM_DBPROP_FILEPATH").trim();
			//ADM_DB_PROPERTIES = configBase+"\\"+ADM_DB_PROPERTIES;
			AGILEPLM_DM_RT_PROPERTIES = applicationProperties.getProperty("AGILEPLM_DM_RT_DBPROP_FILEPATH").trim();
			/*AGILEPLM_DM_RT_PROPERTIES = configBase+"\\"+AGILEPLM_DM_RT_PROPERTIES;*/ // Ajit--> Consolidation to propeties file
			OsaLangDAO osLangDAO = new OsaLangDAO();
			//logger.debug("File Path :" + ADM_DB_PROPERTIES);
			logger.debug("File Path :" + AGILEPLM_DM_RT_PROPERTIES);
			
			try
			{
				//con =  osLangDAO.getConnection(ADM_DB_PROPERTIES);
				/*con =  osLangDAO.getConnection(AGILEPLM_DM_RT_PROPERTIES);*/ // Ajit-->Consolidation to propeties file
				con =  osLangDAO.getDBConnection(AGILEPLM_DM_RT_PROPERTIES);
				
				//D&D change
				if(predorsucc==1)
				{proc_Name= applicationProperties.getProperty("AGILEPLM_DM_RT_SP_PREDCESSOR").trim();}
				else if(predorsucc==2)
				{proc_Name= applicationProperties.getProperty("AGILEPLM_DM_RT_SP_SUCCESSOR").trim();}
				//D&D change over
				
				logger.debug("proc_Name:" + proc_Name);	
			}
			catch(FileNotFoundException ex)
			{
				logger.error("FileNotFoundException while getting the connection using properties file: "+ex.getMessage());
			}
			catch(IOException ex)
			{
				logger.error("IOException while getting the connection  using properties file: "+ex.getMessage());
			}
			catch(SQLException ex)
			{
				logger.error("SQLException while getting the connection  using properties file: "+ex.getMessage());
			}

			if(con != null)
			{
				logger.debug("Got the Connection :" + con);
				
				//D&D change 19
				if(predorsucc==1)
				{
					cs=con.prepareCall("{call "+proc_Name+"(?,?)}");
					//cs=con.prepareCall("{call Sp_Predcessor_Populate(?,?)}");
				}
				
				else if(predorsucc==2)
				{	
					cs=con.prepareCall("{call "+proc_Name+"(?,?)}");
					//cs=con.prepareCall("{call Sp_Successor_Populate(?,?)}");	
				}
				//D&D change over
				
				cs.setString(1,swb_number);
		        /*cs.registerOutParameter(2, oracle.jdbc.driver.OracleTypes.CURSOR);*/ // Ajit--> Changes jar to ojdbc6
				cs.registerOutParameter(2, oracle.jdbc.OracleTypes.CURSOR);
				cs.executeQuery();
				logger.debug("Executed proc"+proc_Name);
				rs = (ResultSet)cs.getObject(2);
				
				//D&D change 9
				//SWBPredecessor swbPredecessor=null;
				SWBPredecessorSuccessor swbPredecessorSuccessor=null;
				//D&D change over
				
				if(rs!=null)
				{
					while(rs.next())
					{
						String swbNumber = rs.getString("ITEM_NUMBER");
						
						//D&D change 10
						//swbPredecessor = new SWBPredecessor(swbNumber);
						swbPredecessorSuccessor = new SWBPredecessorSuccessor(swbNumber);
						
						//swbPredecessorList.add(swbPredecessor);
						swbPredecessorSuccessorList.add(swbPredecessorSuccessor);
						//D&D change over
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Exception : "+ e.getMessage());
			throw e;
		}
		finally{
			if(rs != null){
				rs.close();
			}
			if(cs != null){
				cs.close();
			}
			if(con != null){
				con.close();
			}
		}
		
		//D&D change 20
		//return swbPredecessorList;
		return swbPredecessorSuccessorList;
		
	}
	//D&D change over
}
