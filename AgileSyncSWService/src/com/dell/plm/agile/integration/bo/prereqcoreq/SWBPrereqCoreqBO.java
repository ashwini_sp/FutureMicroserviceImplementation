/**
 * SWBPrereqCoreqBO.java
 * Version information: 1.0
 * Date:17th October 2013
 * Copyright : DELL confidential
 * @author Vumesh_Gundapuneni 
 */
package com.dell.plm.agile.integration.bo.prereqcoreq;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.dao.OsaLangDAO;
import com.dell.plm.agile.integration.dto.Response;
import com.dell.plm.agile.integration.dto.prereqcoreq.PrereqCoreqDtls;
import com.dell.plm.agile.integration.dto.prereqcoreq.ProjectPlatformAffected;
import com.dell.plm.agile.integration.xml.PrereqCoreqXMLGenerator;

/**
 * This class reads the Prereq Number,Coreq Number,Platform Affected,Operating System and Language details of a Part Number requested. Sends it back to 
 * the user in response of the web service method invoked.
 */

public class SWBPrereqCoreqBO {
	private static final Logger logger = Logger.getLogger(SWBPrereqCoreqBO.class);
	
	/**
	 * Synchronous method which returns the Response containing the XML.
	 * @param swb_number swb_number requested.
	 * @param response Response
	 * @return response Response	
	 */
	public Response getPrereqCoreqXMLResponse(final String swb_number,final Response response)
	{
		logger.debug("The getPrereqCoreqXMLResponse Method Starting...");
		synchronized(this)
		{
			Map<String, List<PrereqCoreqDtls>> preCoreqMap = null;
			Map<Integer,Object> ppaMapList = null;
			String strXML = null;
			String strPrereqOrderOfInstall = null;
			String strCoreqOrderOfInstall = null;
			try{
				ppaMapList = getPPADetails();
				preCoreqMap = getPartPrereqCoreqDtls(swb_number, ppaMapList);
				strPrereqOrderOfInstall = getOrderOfSwbInstallation(preCoreqMap,"P");
				strCoreqOrderOfInstall = getOrderOfSwbInstallation(preCoreqMap,"C");
				if(preCoreqMap!=null && preCoreqMap.size() > 0)
				{
					List<PrereqCoreqDtls> prereqDtlsList = (List<PrereqCoreqDtls>)preCoreqMap.get("P");
					List<PrereqCoreqDtls> coreqDtlsList = (List<PrereqCoreqDtls>)preCoreqMap.get("C");
					if((prereqDtlsList.size() >0) ||(coreqDtlsList.size()>0)){
						strXML = PrereqCoreqXMLGenerator.createDocument(swb_number, preCoreqMap, strPrereqOrderOfInstall, strCoreqOrderOfInstall);
						response.setPreReqCoreqXML(strXML);
					}else{
						response.setPreReqCoreqXML("");
					}
				}
						 
				response.setStatus("Success");
				response.setObjectId(swb_number);
				response.setValidationError("");
				logger.debug(response.toString());
			}
			catch(Exception ex)
			{
				logger.error("Exception while getting the part os lang list :"+ex.getMessage());
				response.setStatus("Failure");
				response.setObjectId(swb_number);
				response.setDbFailure(ex.getMessage());
			}
			finally{
				if(ppaMapList!=null && ppaMapList.size() > 0){
					ppaMapList.clear();
					ppaMapList = null;
				}
				
				if(preCoreqMap!=null && preCoreqMap.size() > 0){
					preCoreqMap.clear();
					preCoreqMap = null;
				}
			}
		}
		logger.debug("End of getPrereqCoreqXMLResponse Method...");
		return response;
	}
	
	/**
	 * The getPPADetails() Method fetch all the records from PLATFORM_CODE_NAMES Table from AGILEPLM_DM_RT schema 
	 * @return Map<Integer,Object>
	 * @throws Exception
	 */
	private Map<Integer,Object> getPPADetails() throws Exception
	{
		logger.debug("The getPPADetails Method Starting...");
		Map<Integer,Object> ppaMapList = new HashMap<Integer,Object>();
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rsPPA = null;
		Properties applicationProperties = null;
		String AGILEPLM_DM_RT_PROPERTIES = "";
		String admDbPlatformCodeNmProc = "";
		try{
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			logger.debug("Loading properties for aic_syncswservice_application.properties...");
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
			AGILEPLM_DM_RT_PROPERTIES = applicationProperties.getProperty("AGILEPLM_DM_RT_DBPROP_FILEPATH").trim();
			/*AGILEPLM_DM_RT_PROPERTIES = configBase+"\\"+AGILEPLM_DM_RT_PROPERTIES;*/ // Ajit--> Consolidation to properties file
			logger.debug("AGILEPLM_DM_RT DB Properties File Path: " + AGILEPLM_DM_RT_PROPERTIES);
			OsaLangDAO osLangDAO = new OsaLangDAO();
						
			try
			{
				/*con =  osLangDAO.getConnection(AGILEPLM_DM_RT_PROPERTIES);*/ // Ajit--> Consolidation to properties file
				con =  osLangDAO.getDBConnection(AGILEPLM_DM_RT_PROPERTIES);
				logger.debug("Got the DB Connection: " + con);
				admDbPlatformCodeNmProc = applicationProperties.getProperty("ADM_DB_PLATFORM_CODE_NAME_PROC").trim();
				logger.debug("The Platform Code Name Stored Procedure Name: " + admDbPlatformCodeNmProc);
			}
			catch(FileNotFoundException ex)
			{
				logger.error("FileNotFoundException while getting the connection using properties file: "+ex.getMessage());
			}
			catch(IOException ex)
			{
				logger.error("IOException while getting the connection  using properties file: "+ex.getMessage());
			}
			catch(SQLException ex)
			{
				logger.error("SQLException while getting the connection  using properties file: "+ex.getMessage());
			}

			if(con != null)
			{
				/**
				 *  The PKG_SUCCESSOR_POPULATE.SP_GET_PPA_DETAILS stored procedure pull all the records from 
				 *  AGILEPLM_DM.PLATFORM_CODE_NAME table based on max(id) of all the records. 
				 */
				cstmt = con.prepareCall("{call "+admDbPlatformCodeNmProc+"(?)}");
				/*cstmt.registerOutParameter(1, oracle.jdbc.driver.OracleTypes.CURSOR);*/ // Ajit--> Changes jar to ojdbc6
				cstmt.registerOutParameter(1, oracle.jdbc.OracleTypes.CURSOR);
				cstmt.executeQuery();
				logger.debug("Executed Stored Procedure: "+admDbPlatformCodeNmProc+" Successfully.");
				rsPPA = (ResultSet)cstmt.getObject(1);
				
				ProjectPlatformAffected projPlatformAff = null;
				if(rsPPA !=null){
					while(rsPPA.next()){
	 					
						/** This will give the PH Number*/
						String strPhNbr = rsPPA.getString(1);
						
						/** This will give the PPA Name */
	 					String strPPA = rsPPA.getString(2);
	 					
	 					/** This will give Active flag Name */
	 					//String flag = rsPPA.getString(3);
	 					
	 					/** This will give ID number */
	 					Integer id = new Integer(rsPPA.getInt(4));
						
	 					//logger.debug(strPhNbr+"\t"+strPPA+"\t"+id);
						
	 					projPlatformAff = new ProjectPlatformAffected();
						projPlatformAff.setId(id);
						projPlatformAff.setPhNumber(strPhNbr);
						projPlatformAff.setPpa(strPPA);
						ppaMapList.put(id,projPlatformAff);
					}
				}
			}
			logger.debug("Total Records in Platform Code Name Table:"+ppaMapList.size());
		}
		catch(Exception e)
		{
			logger.error("Exception in getPPADetails() Method: "+ e.getMessage());
			throw e;
		}
		finally{
			if(rsPPA != null){
				rsPPA.close();
			}
			if(cstmt != null){
				cstmt.close();
			}
			if(con != null){
				con.close();
			}
		}
		logger.debug("End of getPPADetails Method...");
		return ppaMapList;
	}
	

	/**
	 * Returns the list Prereq/Coreq Numbers in String based on map and flag requested.
	 * @param preCoreqMap
	 * @param flag (P-Prereq list & C- Coreq list)
	 * @return String (List of Prereq or Coreq SWB Numbers with comma separated) 
	 * @throws Exception
	 */
	private String getOrderOfSwbInstallation(Map<String, List<PrereqCoreqDtls>> preCoreqMap, String flag) throws Exception
	{
		logger.debug("The getOrderOfSwbInstallation Method Starting...");
		StringBuffer strOrderOfInstall =new StringBuffer("");
		Map<Integer,String> orderMap = new HashMap<Integer,String>();
		List<PrereqCoreqDtls> prereqCoreqDtlsList = (List<PrereqCoreqDtls>)preCoreqMap.get(flag);
		if(prereqCoreqDtlsList!=null && prereqCoreqDtlsList.size() > 0)
		{
			for(PrereqCoreqDtls prereqCoreqDtls:prereqCoreqDtlsList)
			{
				orderMap.put(prereqCoreqDtls.getOrderOfInstallation(),prereqCoreqDtls.getPrereqCoreqNbr().trim());
				logger.debug(prereqCoreqDtls.getOrderOfInstallation()+"\t"+prereqCoreqDtls.getPrereqCoreqNbr().trim());
			}
			
			for(int i=0;i<orderMap.size();i++)
			{    
				 String swbNumber= orderMap.get(i+1);
				 logger.debug(orderMap.get(i+1));
				 strOrderOfInstall.append(swbNumber).append(",");
			}
			if(strOrderOfInstall.length() > 0){
				strOrderOfInstall = strOrderOfInstall.deleteCharAt(strOrderOfInstall.length()-1);
			}
		}
		logger.debug("The Order of installation of SWB Numbers:"+strOrderOfInstall.toString()+" for Prereq/Coreq flag number: "+flag);
		logger.debug("The getOrderOfSwbInstallation Method Ending...");
		return strOrderOfInstall.toString();
	}
	
	
	/**
	 * Returns the Map, which holds the Prereq or Coreq details as list based on a swb_number requested.
	 * @param swb_number
	 * @param Map
	 * @return Map<String, List<PrereqCoreqDtls>>
	 * @throws Exception
	 */
	private Map<String, List<PrereqCoreqDtls>> getPartPrereqCoreqDtls(final String swb_number, final Map<Integer,Object> ppaMapList) throws Exception
	{
		logger.debug("The getPartPrereqCoreqDtls Method Starting...");
		Map<String, List<PrereqCoreqDtls>> preCoreqMap = new HashMap<String, List<PrereqCoreqDtls>>(); 
		List<PrereqCoreqDtls> swbPrereqList = null;
		List<PrereqCoreqDtls> swbCoreqList = null;
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rsPreReq = null;
		ResultSet rsCoReq = null;
		Properties applicationProperties = null;
		String DB_PROPERTIES = "";
		Map<Integer,String> allMap = null;
		String allPlatformCode ="";
		String allOsCode ="";
		String allLangCode ="";
		try{
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			logger.debug("loading aic_syncswservice_application.properties file...");
			applicationProperties = FilePropertyUtility.loadProperties(configBase+"\\config\\aic_syncswservice_application.properties");
			DB_PROPERTIES = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
			/*DB_PROPERTIES = configBase+"\\"+DB_PROPERTIES;*/ // Ajit--> Consolidation to properties file
			OsaLangDAO osLangDAO = new OsaLangDAO();
			logger.debug("File Path :" + DB_PROPERTIES);
			try{
				allMap = new HashMap<Integer,String>();
				allPlatformCode = applicationProperties.getProperty("ALL_PLATFORM_VALUES").trim();
				allOsCode = applicationProperties.getProperty("ALL_OS_VALUES").trim();
				allLangCode = applicationProperties.getProperty("ALL_LANG_VALUES").trim();
				allMap.put(Integer.parseInt(allPlatformCode), applicationProperties.getProperty(allPlatformCode).trim());
				allMap.put(Integer.parseInt(allOsCode), applicationProperties.getProperty(allOsCode).trim());
				allMap.put(Integer.parseInt(allLangCode), applicationProperties.getProperty(allLangCode).trim());
				
				con =  osLangDAO.getConnection(DB_PROPERTIES);
				logger.debug("Got the DB Connection :" + con);
			}
			catch(FileNotFoundException ex)
			{
				logger.error("FileNotFoundException while getting the connection using properties file: "+ex.getMessage());
			}
			catch(IOException ex)
			{
				logger.error("IOException while getting the connection  using properties file: "+ex.getMessage());
			}
			catch(SQLException ex)
			{
				logger.error("SQLException while getting the connection  using properties file: "+ex.getMessage());
			}

			if(con != null)
			{
				String storedProcForPrereqCoreq = applicationProperties.getProperty("AGILESERVICES_DB_SP_FOR_PREREQ_COREQ").trim();
				logger.debug("Stored Procedure Name To Fecth Records from PREREQ_COREQ_DTLS Table is: " + storedProcForPrereqCoreq);
				
				cs=con.prepareCall("{call "+storedProcForPrereqCoreq+"(?,?,?,?,?,?,?)}");
				
				/** Set Candidate SWB Number */
				cs.setString(1,swb_number);
				
				/** Set All OS Code */
				cs.setString(2,allOsCode);
				
				/** Set All OS Code Value */
				cs.setString(3,allMap.get(Integer.parseInt(allOsCode)));
				
				/** Set All Language Code */
				cs.setString(4,allLangCode);
				
				/** Set All Language Code  Value*/
				cs.setString(5,allMap.get(Integer.parseInt(allLangCode)));
				
				/** Pre-req Cusrosr Out Parameter */
		        /*cs.registerOutParameter(6, oracle.jdbc.driver.OracleTypes.CURSOR);*/ // Ajit--> Changes to ojdbc6 jar
				cs.registerOutParameter(6, oracle.jdbc.OracleTypes.CURSOR);
		        
		        /** Co-req Cusrosr Out Parameter */
		        /*cs.registerOutParameter(7, oracle.jdbc.driver.OracleTypes.CURSOR);*/ // Ajit--> Changes to ojdbc6 jar
				cs.registerOutParameter(7, oracle.jdbc.OracleTypes.CURSOR);
		        cs.executeQuery();
				
		        /** Return Pre-req Cursor Contains All Columns As Row Cursor  */
		        rsPreReq = (ResultSet)cs.getObject(6);
		        
		        /** Return Co-req Cursor Contains All Columns As Row Cursor  */
				rsCoReq = (ResultSet)cs.getObject(7);
				
				swbPrereqList= getPrereqCoreqListFromResultSet(rsPreReq, ppaMapList, allMap);
				preCoreqMap.put("P", swbPrereqList);
				
				swbCoreqList = getPrereqCoreqListFromResultSet(rsCoReq, ppaMapList, allMap);
				preCoreqMap.put("C", swbCoreqList);
			}
		}
		catch(Exception e){
			logger.error("The exception thrown in getPartPrereqCoreqDtls() Method: "+e.getMessage());
			throw e;
		}
		finally{
			if(rsPreReq != null){
				rsPreReq.close();
			}
			
			if(rsCoReq != null){
				rsCoReq.close();
			}
			if(cs != null){
				cs.close();
			}
			if(con!=null)
			{
				con.close();
			}
		}
		logger.debug("End of getPartPrereqCoreqDtls Method...");
		return preCoreqMap;
	}
	

	
	
	/**
	 * This method returns list of PrereqCoreqDtls based on ResultSet of  Prereq/Coreq. 
	 * @param rs
	 * @param ppaMapList
	 * @param allMap
	 * @return List<PrereqCoreqDtls>
	 * @throws Exception
	 */
	private List<PrereqCoreqDtls> getPrereqCoreqListFromResultSet(final ResultSet rs, final Map<Integer,Object> ppaMapList, final Map<Integer,String> allMap) throws Exception
	{
		logger.debug("The getPrereqCoreqListFromResultSet Method Starting...");
		List<PrereqCoreqDtls> swbPrereqCoreqList = new ArrayList<PrereqCoreqDtls>();
		PrereqCoreqDtls prereqCoreqDtls = null; 
		if(rs!=null)
		{
			while(rs.next())
			{
				String swbNumber = rs.getString("SWB_NUMBER");
				String prereqCoreqNbr = rs.getString("PREREQ_COREQ_NUMBER");
				String prereqCoreqFlag = rs.getString("PREREQ_COREQ_FLAG");
				Integer orderOfInstallation = new Integer(rs.getInt("ORDER_OF_INSTALLATION"));
				String specificOrLaterVer = rs.getString("SPECIFIC_OR_LATER_VERSION");
				String platformIds = rs.getString("PLATFORM");
				String languageIds = rs.getString("LANG_CODE");
				String lossOfFuncRsnTxt = rs.getString("LOSS_OF_FUNC_REASON_TXT");
				String osIds = rs.getString("OS_CODE");
				
				prereqCoreqDtls = new PrereqCoreqDtls();
				prereqCoreqDtls.setSwbNumber(swbNumber);
				prereqCoreqDtls.setPrereqCoreqNbr(prereqCoreqNbr);
				prereqCoreqDtls.setPrereqCoreqFlag(prereqCoreqFlag);
				prereqCoreqDtls.setOrderOfInstallation(orderOfInstallation);
				prereqCoreqDtls.setSpecificOrLaterVer(specificOrLaterVer);
				prereqCoreqDtls.setPlatformIds(platformIds);
				prereqCoreqDtls.setOsIds(osIds);
				prereqCoreqDtls.setLangIds(languageIds);
				prereqCoreqDtls.setLossOfFuncRsnTxt(lossOfFuncRsnTxt);
				
				
				/** Retrieve PH Numbers and PPA based on ID numbers. */  
				List <Integer> listPPAIds = prereqCoreqDtls.getPPAIdList();
				StringBuffer sbPhNumbers =new StringBuffer("");
				for(Integer ppaId:listPPAIds)
				{
					if(!allMap.containsKey(ppaId)){
						ProjectPlatformAffected ppaObj=(ProjectPlatformAffected)ppaMapList.get(ppaId);
						if(ppaObj!=null)
						{
							sbPhNumbers.append(ppaObj.getPhNumber()).append(",").append(ppaObj.getPpa()).append(";");
						}else{
							logger.error("The PPA id: "+ppaId+" doesn't have corresponding PH Number and PPA.");
						}
					}
					else{
						sbPhNumbers.append("All").append(",").append(allMap.get(ppaId)).append(";");
						break;
					}
				}
				if(sbPhNumbers!=null && sbPhNumbers.length() > 0){
					sbPhNumbers = sbPhNumbers.deleteCharAt(sbPhNumbers.length()-1);
					prereqCoreqDtls.setPlatformPhNbr(sbPhNumbers.toString());
				}else{
					prereqCoreqDtls.setPlatformPhNbr(sbPhNumbers.toString());
				}
				logger.debug(swbNumber+"\t"+prereqCoreqNbr+"\t"+prereqCoreqFlag+"\t"+orderOfInstallation+"\t"+specificOrLaterVer+"\t"+prereqCoreqDtls.getPlatformPhNbr()+"\t"+languageIds+"\t"+osIds+"\t"+lossOfFuncRsnTxt);
				swbPrereqCoreqList.add(prereqCoreqDtls);
			}
		}
		logger.debug("The getPrereqCoreqListFromResultSet Method Ending...");
		return swbPrereqCoreqList;
	}
}

