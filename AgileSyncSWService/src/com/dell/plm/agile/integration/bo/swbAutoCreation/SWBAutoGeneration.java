package com.dell.plm.agile.integration.bo.swbAutoCreation;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import com.agile.api.APIException;
import com.agile.api.ChangeConstants;
import com.agile.api.DataTypeConstants;
import com.agile.api.ExceptionConstants;
import com.agile.api.IAgileClass;
import com.agile.api.IAgileList;
import com.agile.api.IAgileSession;
import com.agile.api.IAutoNumber;
import com.agile.api.ICell;
import com.agile.api.IDataObject;
import com.agile.api.IItem;
import com.agile.api.IRow;
import com.agile.api.ITable;
import com.agile.api.ITwoWayIterator;
import com.agile.api.IUser;
import com.agile.api.ItemConstants;
import com.agile.api.UserConstants;
import com.dell.plm.agile.aic.common.exception.ValidationException;
import com.dell.plm.agile.integration.bean.AttributeDetails;
import com.dell.plm.agile.integration.bean.SWBCreateRequest;
import com.dell.plm.agile.integration.dao.OsaLangDAO;
import com.dell.plm.agile.integration.util.Constants;
import com.dell.plm.agile.integration.util.XMLReader;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class SWBAutoGeneration 
{
	private static final Logger logger = Logger.getLogger(SWBAutoGeneration.class);
	StringBuffer strBuffErr=new StringBuffer();
	String[] swbNumberDetails=new String[2];
	/**
	 * @param session
	 * @return
	 */

	public String[] createIndependentSWB(IAgileSession session, SWBCreateRequest swbCreateRequest,Properties applicationProperties)
	{
		String swb_number = "";
		IDataObject swb=null;
		 logger.info("inside createIndependentSWB");

		try{					
			String attributeList = applicationProperties.getProperty(Constants.INDEPENDENT_SWB_ATT_NAME_VAL);
			 logger.info("===attributeList==="+attributeList);
		    String ownerAttributeName = applicationProperties.getProperty(Constants.PAGE_THREE_OWNER);
			 logger.info("===ownerAttributeName==="+ownerAttributeName);
		    String relationshipClass = applicationProperties.getProperty(Constants.SWB_RELATIONSHIP_CLASS);
			 logger.info("===relationshipClass==="+relationshipClass);
		    String partClass = applicationProperties.getProperty(Constants.SOFTWARE_BUNDLE);
			 logger.info("===partClass==="+partClass);
		   String isSuccessorYes =applicationProperties.getProperty(Constants.IS_SUCCESSOR_YES);
			String description = applicationProperties.getProperty(Constants.TITLE_BLOCK_DESC);

		    String hardwarePartNumber=swbCreateRequest.getDevicePartNums();
			swb  = createObject(session,partClass);
		    updateDefaultAttributes(attributeList,swb);
		    if(swbCreateRequest.getAttributeDetails()!=null)
		    swb = setAttributeValues(session, swbCreateRequest.getAttributeDetails(), swb,description);
		    setOwnerAttribute(session, swbCreateRequest.getSwbOwner(), swb,ownerAttributeName);
			logger.info("hardwarePartNumber"+swbCreateRequest.getDevicePartNums()+"partnumber");
			if(!StringUtils.isEmpty(hardwarePartNumber))
				copyRelationship(swb, null, session,relationshipClass,swbCreateRequest,isSuccessorYes);
			logger.info("after addRelations");
			swb_number=swb.getValue(ItemConstants.ATT_TITLE_BLOCK_NUMBER).toString();
			logger.info("swb_number"+swb_number);
			swbNumberDetails[0]=swb_number;
			swbNumberDetails[1]=strBuffErr.toString();
		}catch(Exception exp){
			logger.error(exp.getMessage());
			exp.printStackTrace();
		}
		return swbNumberDetails;
	}

	private void setOwnerAttribute(IAgileSession session, String owner_name,IDataObject swb,String ownerAttributeName) throws APIException {
		try{
			IUser user = (IUser)session.getObject(UserConstants.CLASS_USER, owner_name);
			ICell cell=swb.getCell(ownerAttributeName);
			IAgileList valuesList = cell.getAvailableValues();
			valuesList.setSelection(new Object[]{user});
			cell.setValue(valuesList);
		}catch(Exception exp){
			logger.error(exp.getMessage());
		    strBuffErr.append("Error Occurred while setting "+ownerAttributeName+"value:"+owner_name);
		}
	}

	public String[] createSuccessorSWB(IAgileSession session, SWBCreateRequest swbCreateRequest,Properties applicationProperties,String xmlFilePath)
	{
		String swb_number = "";
		try{	
		    String relationshipClass = applicationProperties.getProperty(Constants.SWB_RELATIONSHIP_CLASS);
		    String isSuccessorYes =applicationProperties.getProperty(Constants.IS_SUCCESSOR_YES);
		    String isSuccessorNo =applicationProperties.getProperty(Constants.IS_SUCCESSOR_NO);
		    String referenceSWBNum=swbCreateRequest.getReferenceSWBNum();
		    String ownerAttributeName = applicationProperties.getProperty(Constants.PAGE_THREE_OWNER);
			String description = applicationProperties.getProperty(Constants.TITLE_BLOCK_DESC);

		    logger.info("==session==="+session);
			IDataObject sourceDataObject = (IDataObject)session.getObject(ItemConstants.CLASS_PART, referenceSWBNum);
			logger.info("==sourceDataObject=="+sourceDataObject);
			IDataObject newSWB=saveAsObject(sourceDataObject,session);
			logger.info("==successorSWB"+newSWB);
			updateAndClearAttributesOnSaveAs(newSWB, xmlFilePath);
			updateOSLangforNewSWB(newSWB.getName(),referenceSWBNum,applicationProperties,sourceDataObject);
			//clear --> get from xml
			// set defaulkt values
			//inputParamMap.put("Is Successor", "YES");
			if(swbCreateRequest.getAttributeDetails()!=null)
			newSWB = setAttributeValues(session, swbCreateRequest.getAttributeDetails(), newSWB,description);
		    setOwnerAttribute(session, swbCreateRequest.getSwbOwner(), newSWB,ownerAttributeName);
			//copying contents from source swb to target swb and HardwarePartnumbers if any
			copyRelationship(newSWB, sourceDataObject,session,relationshipClass,swbCreateRequest,isSuccessorYes);
			if(swbCreateRequest.getIsSuccessor()!=null && isSuccessorYes.equalsIgnoreCase(swbCreateRequest.getIsSuccessor())){
			   sourceDataObject.setValue("Successor", newSWB);
			   newSWB.setValue("Predecessor", sourceDataObject);
			   newSWB.setValue("Is Successor", "YES");
			}else if(swbCreateRequest.getIsSuccessor()==null && isSuccessorNo.equalsIgnoreCase(swbCreateRequest.getIsSuccessor())){
				newSWB.setValue("Is Successor", "NO");
			}
			swb_number=newSWB.getValue(ItemConstants.ATT_TITLE_BLOCK_NUMBER).toString();
			swbNumberDetails[0]=swb_number;
			swbNumberDetails[1]=strBuffErr.toString();
			logger.info("==swb_number=="+swb_number);
		}catch(Exception e){
			logger.error(e.getMessage());
			e.printStackTrace();
		    strBuffErr.append("Error Occurred while creating SWB");
			//logger.info(e.getMessage());
		}
		finally {
			//params = null;
		}
		return swbNumberDetails;
	}
	

	private void updateOSLangforNewSWB(String newSWBPartNumber,String referencePartNumber,Properties applicationProperties,IDataObject sourceDataObject) {
		// TODO Auto-generated method stub
	    //String hasOsLangValue =applicationProperties.getProperty(Constants.PAGE_THREE_OS_LANG);
		try {
		    String hasOsLangMatrixValue;
			hasOsLangMatrixValue = sourceDataObject.getValue(applicationProperties.getProperty(Constants.PAGE_THREE_OS_LANG)).toString();
		    logger.info("==hasOsLangMatrix"+hasOsLangMatrixValue);
			if(Constants.YES_VALUE.equalsIgnoreCase(hasOsLangMatrixValue)){
			    OsaLangDAO osLangDao=new OsaLangDAO();
				osLangDao.addOsLangForSWBPart(applicationProperties,newSWBPartNumber, referencePartNumber);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error Occurred in updating OS lang Attribute"+e.getMessage());
			e.printStackTrace();
			strBuffErr.append("Error Occurred in updating OS lang Attribute");
		}
	}

	private  void copyRelationship(IDataObject targetSWB, IDataObject sourceSWB, IAgileSession session,String relationshipClass,SWBCreateRequest swbCreateRequest,String isSuccessorYes) throws APIException, InterruptedException {
		// TODO Auto-generated method stub

		/**
		 * fetching the source swb relationship table contents
		 * where the relationship class name is equal to Product Structure|Device to Software Bundle
		 * copying the items that are in the source relationship table
		 * 
		 */

		// Target swb
		logger.info("target software bundle -> "+targetSWB);
		logger.info("source software bundle -> "+sourceSWB);
		ArrayList relRow= new ArrayList();
		HashMap sourceRelationshipMap= new HashMap();

		//loading the relationship tab of the target software bundle
		ITable targetRelTable = targetSWB.getTable(ItemConstants.TABLE_RELATIONSHIPS);
		logger.info("target relationship tab is fetched "+targetRelTable);
	//	 getRelationShipFrmSWB(targetSWB,relationshipClass,targetRelationshipMap);


		//HashMap relTabParams;
		boolean updateStatus;
		if(sourceSWB!=null && swbCreateRequest.getIsSuccessor()!=null && isSuccessorYes.equalsIgnoreCase(swbCreateRequest.getIsSuccessor()))
			relRow = getRelationShipFrmSWB(sourceSWB,relRow,relationshipClass,sourceRelationshipMap);

		/**
		 * Fetching the string which may or maynot consist of semicolon
		 * if semicolon separated each separated string is fetched
		 * if not semicolon separated, not checking for any conditions
		 * adding the string/item under the target swb relationship tab
		 * 
		 */
	    String hardwarePartNumbers=swbCreateRequest.getDevicePartNums();

	   getRelationShipDetailsFromDeviceParts(targetSWB, hardwarePartNumbers,
			session, relRow, sourceRelationshipMap);
		  logger.info("array list loaded with the items are " +relRow);

		// Refreshing the target software bundle item
		
		logger.info("refreshing the " +targetSWB+ " software bundle");
		try{
			if(targetRelTable!=null && relRow.size()>0)
			targetRelTable.createRows(relRow.toArray());
			logger.info("copied the contents from the source swb to the target swb and added the hardware part numbers to the target swb relationship tab");

		}
		catch(APIException e){
		    strBuffErr.append("Error Occurred while copying relationship to new SWB");
			logger.info("Exception caught while creating rows in the target swb relationship table "+e.getRootCause());

			//trying to refresh the object and create rows in the target swb relationship table
			targetSWB.refresh();
			targetRelTable.createRows(relRow.toArray());

			boolean isSuccess = false;
			int intLoop = 1;

			//checking if these are the exceptions caused during the updation
			if(((Integer)e.getErrorCode()).intValue()== ExceptionConstants.APDM_OBJVERSION_MISMATCH.intValue() 
					|| e.getMessage().indexOf("Someone is working on this object")!=-1 
					|| e.getMessage().indexOf("This object has been modified")!=-1
					|| e.getMessage().indexOf("Invalid Parameter")!=-1
					|| e.getMessage().contains("already related to")){
				while(intLoop < 10 && !isSuccess){
					try{
						//logger.info("data object is " + dataObj+ " -- Attribute name is " +attrName+ " -- Attribute value is " +attrVal );
						logger.info("Loop count is " +intLoop);

						//making the thread sleep for 1000 seconds
						Thread.sleep(1000);

						targetSWB.refresh();
						targetRelTable.createRows(relRow.toArray());
						targetSWB.refresh();

						updateStatus = true;
						logger.info("rows added in the relationship table of the target swb");
					}
					catch(APIException apie){
						logger.info("Exception apie: " +apie.getMessage());
						if((apie.getMessage().indexOf("Invalid Parameter") != -1)
								|| (e.getMessage().contains("already related to"))
								|| (apie.getMessage().indexOf("Someone else is working on this") != -1)){
							intLoop++;    
						}
						else
						{
							isSuccess = false;
							break;
						}
					}
				}
			}
			else{
				isSuccess = false;
				logger.info("isSuccess: "+isSuccess);
			}
			if(!isSuccess) 
			{
				logger.info("Throwing e: "+e.getMessage());
				throw e;
			}
		}
	}

	private void getRelationShipDetailsFromDeviceParts(IDataObject targetSWB,
			String hardwarePartNumbers, IAgileSession session,
			ArrayList relRow, HashMap sourceRelationshipMap) {
		HashMap relTabParams;
		StringBuffer invalidDeviceParts=new StringBuffer();
		IItem relItem=null;
		try{
			        if(StringUtils.isNotEmpty(hardwarePartNumbers)){
							String[] strAftSplit = hardwarePartNumbers.split(Constants.HARD_WARE_PART_SEPARATOR);
							for (String itemNum : strAftSplit) {
								   logger.info("item to be added to the relationship tab is " + itemNum);
									 relItem = (IItem) session.getObject(ItemConstants.CLASS_PART, itemNum);
									logger.info("relationship item " +relItem+ " to be added is loaded");
		
									if(relItem!=null){
										if(!sourceRelationshipMap.containsKey(relItem)){
											relTabParams = new HashMap();
											relTabParams.put(ItemConstants.ATT_RELATIONSHIPS_NAME, relItem); 
											relTabParams.put(ItemConstants.ATT_RELATIONSHIPS_LIST01, "Product Structure|Device to Software Bundle");
											relRow.add(relTabParams);
											sourceRelationshipMap.put(relItem, "");
										}
									}else{
										invalidDeviceParts.append(itemNum+",");
									}
						     }
							logger.error("invalidDeviceParts"+invalidDeviceParts.toString());
							if(invalidDeviceParts.length()>0){
								invalidDeviceParts.deleteCharAt(invalidDeviceParts.length()-1);
								strBuffErr.append("Invalid Device Parts "+invalidDeviceParts.toString());
							}
				   }
			        targetSWB.refresh();
			}catch(Exception e){
				logger.error("Exception caught while adding the hardware contents"+e);
			 //   strBuffErr.append("Error Occurred while copying relationship to new SWB");
			}finally{
				relItem=null;
			}
	}

	
	private HashMap getRelationShipFrmSWB(IDataObject sourceSWB,
			 String relationshipClass, HashMap targetRelationshipMap) throws APIException {
		// TODO Auto-generated method stub
		// Loading the relationship tab of the source software bundle where the relationship class name is device to software bundle
				IRow sourceRow ;
				ITable sourceRelTable =	sourceSWB.getTable(ItemConstants.TABLE_RELATIONSHIPS).where("[5851] == 'Product Structure|Device to Software Bundle'",null);
				logger.info("source relationship tab is fetched");

				// Iterating the relationship tab of source part
				ITwoWayIterator sourceItr = sourceRelTable.getTableIterator();
				
				HashMap relTabParams = null ;
				boolean updateStatus = false ;

				try{
					while(sourceItr.hasNext()){
						sourceRow = (IRow) sourceItr.next();
						relTabParams = new HashMap();
						logger.info("Item fetched from the source relationship table is "+ sourceRow.getReferent());
						//copying the contents of source swb relationship tab to the target swb
						targetRelationshipMap.put(sourceRow.getReferent(), "");
					}
				}
				catch(Exception e){
					logger.info("exception caught while copying the relationship contents from the source swb"+e);
				//	strBuffErr.append("Error Occurred while copying the relationship contents from device part numbers");
				}
				return targetRelationshipMap;
	}

	private ArrayList getRelationShipFrmSWB(IDataObject sourceSWB,ArrayList relRow,String relationshipClass,HashMap sourceRelationshipMap)
			throws APIException {
				// Loading the relationship tab of the source software bundle where the relationship class name is device to software bundle
				IRow sourceRow ;
				ITable sourceRelTable =	sourceSWB.getTable(ItemConstants.TABLE_RELATIONSHIPS).where("[5851] == '"+relationshipClass+"'",null);
				logger.info("source relationship tab is fetched");
			
				// Iterating the relationship tab of source part
				ITwoWayIterator sourceItr = sourceRelTable.getTableIterator();
				
				HashMap relTabParams = null ;
				boolean updateStatus = false ;
			
				try{
					while(sourceItr.hasNext()){
						sourceRow = (IRow) sourceItr.next();
						relTabParams = new HashMap();
                        logger.info("Item fetched from the source relationship table is "+ sourceRow.getReferent());
						//copying the contents of source swb relationship tab to the target swb
                            sourceRelationshipMap.put(sourceRow.getReferent(),"");
							relTabParams.put(ItemConstants.ATT_RELATIONSHIPS_NAME, sourceRow.getReferent());
							relTabParams.put(ItemConstants.ATT_RELATIONSHIPS_LIST01,sourceRow.getValue(ItemConstants.ATT_RELATIONSHIPS_LIST01));
							relRow.add(relTabParams);
					}
				}
				catch(Exception e){
					logger.info("exception caught while copying the relationship contents from the source swb"+e.getMessage());
					//strBuffErr.append("Error Occurred while copying the relationship contents from device part numbers");
				}
				return relRow;
	}
	
	public static void setMultiListValues(ICell cell, String itemString) throws Exception {
		/**
		 * fetching the multi list attribute
		 * setting the value for multilist
		 * 
		 */
		boolean updateStatus = false;
		try{
			// Fetching the available list values from the list
			IAgileList cellValues = (IAgileList) cell.getAvailableValues();
			String[] splittedValue = splitValue(itemString);
			logger.info("length of splitted value is "+ splittedValue.length);
			Object[] objAffList = new Object[splittedValue.length];
			for(int i =0 ; i < splittedValue.length ; i++){			
				objAffList[i] = splittedValue[i];
			}
			cellValues.setSelection(objAffList);
			cell.setValue(cellValues);

			//changing the boolean status after the dynamic attributes are updated successfully
			updateStatus = true;
		}catch(APIException ae){
			logger.info("Exception caught while updating the dynamic attributes"+ae.getMessage());
			throw new Exception();
		}
	}
	
	private IDataObject setAttributeValues(IAgileSession session,AttributeDetails[] attributeDetails, IDataObject swb,String description) throws Exception {
		String attributeName=null;
		String attributeValue=null;
		StringBuffer invalidAttributeNames = new StringBuffer();
		Map<String,Object> inputParamMap=new HashMap<String,Object>();
		ICell cell;		
			logger.info("inside setAttributeValues");
			logger.info("attributeDetails=="+attributeDetails);
			
			 for(AttributeDetails attributeDetail:attributeDetails){
					logger.info("inside for loop=="+attributeDetail);

				if(attributeDetail.getAttributeValue()!=null && attributeDetail.getAttributeValue().contains(";")){
					try {
						cell = swb.getCell(attributeDetail.getAttributeName());
						attributeValue = attributeDetail.getAttributeValue().toString();
						logger.info("Going to set attribute: "+attributeDetail.getAttributeName()+" with value:"+attributeValue);
					    
						setAttribute(cell,attributeValue);
					} catch(Exception ex) {
						logger.info("Multilist Attribute "+attributeDetail.getAttributeName()+" not set due to exception"+ex.getMessage());
						invalidAttributeNames.append(attributeDetail.getAttributeName()+";");
					} finally {
						cell = null;
						attributeValue = null;
					}

			    }else{
				  inputParamMap.put(attributeDetail.getAttributeName(), attributeDetail.getAttributeValue());
			    }
			 }
				logger.info("inputParamMap=="+inputParamMap);
				logger.info("before getting desc==");
			 String descriptionValue=(String)inputParamMap.get(description);
				logger.info("after getting desc");

			 if(descriptionValue!=null){
				logger.info("if desc not null");
				 inputParamMap.put(description,descriptionValue.trim().toUpperCase());
				logger.info(" after desc value is added in map");
			 }
		try{
	       if(inputParamMap.size()>0) {
				swb.setValues(inputParamMap);
	       }	
			logger.info(" after settting values");

			//add new items to the relationship class of the software bundle
		}catch(Exception e){
		     logger.info(e.getMessage());
					 for(Map.Entry<String,Object> entryObj:inputParamMap.entrySet()){
							attributeName=entryObj.getKey();
						try{
							cell = swb.getCell(attributeName);
							
						    
							if(entryObj.getValue()!=null){
						       attributeValue=entryObj.getValue().toString();
						       logger.info("Going to set attribute: "+attributeName+" with value:"+attributeValue);
							   setAttribute(cell,attributeValue);
							}
						} catch(Exception ecp) {
						     logger.info(ecp.getMessage());
							 logger.info("Error Occurred while setting the attribute name in exception blcok: "+attributeName+"value:"+attributeValue);
							 invalidAttributeNames.append(attributeName+";");
						} finally {
							cell = null;
							attributeValue = null;
						}
					}
				 
						
		  }
        logger.info("invalidAttributeNames:"+invalidAttributeNames);

			if(invalidAttributeNames.length()>0) {
				invalidAttributeNames.deleteCharAt(invalidAttributeNames.length()-1);
				strBuffErr.append("Invalid Attributes: "+invalidAttributeNames.toString()+"\r\n");
			}
		return swb;
	}
	
	private static void setAttribute(ICell attCell, String resetValue) throws Exception {
		try{
	        int cellDataType = attCell.getDataType();
	        logger.info("Data Type:"+cellDataType);
	         if(cellDataType==DataTypeConstants.TYPE_SINGLELIST){
	               //logger.info("Data Type : Single or Multi List");
	               IAgileList valuesList = attCell.getAvailableValues();
	               valuesList.setSelection(new Object[]{resetValue});
	               attCell.setValue(valuesList);
	        }else if(cellDataType==DataTypeConstants.TYPE_MULTILIST){
	        	setMultiListValues(attCell,resetValue);
	        }else if(cellDataType==DataTypeConstants.TYPE_STRING){
	        
	               //logger.info("Data Type : String");
	               attCell.setValue(resetValue);
	        }else if(cellDataType==DataTypeConstants.TYPE_DATE){
	               //logger.info("Data Type : Date");
	               attCell.setValue(resetValue);
	        }else if(cellDataType==DataTypeConstants.TYPE_MONEY){
	               //logger.info("Data Type : Money");
	               attCell.setValue(resetValue);
	        }else if(cellDataType==DataTypeConstants.TYPE_DOUBLE){
	               //logger.info("Data Type : Double");
	               attCell.setValue(resetValue);
	        }else if(cellDataType==DataTypeConstants.TYPE_INTEGER){
	               //logger.info("Data Type : Integer");
	               attCell.setValue(resetValue);
	        }
		}catch(Exception exp){
			logger.info("Exception occurred in set Attribute "+exp.getMessage());
			throw new Exception();
		}
  }

	
	


	/**
	 * @param session
	 * @param itemSubclass
	 * @return 
	 * @throws APIException 
	 * generalised methods for the all the classes created
	 * methods included in the class
	 * createSession -- creates the session
	 * createObject -- creates an object using the autonumber assigned to it
	 * updateDefaultAttributes --  updates the default values for the object
	 * addArrayListToAffItem -- adds a list of array items as the affected item of the change order
	 * addAffItemChangeOrder -- adds a single item to the affected item of the change order
	 * incRevX00nn -- fetches the current revision and increment it by 1
	 * incRevXnn00 -- fetches the current revision and increment it by 1
	 * setMultiListValues -- sets the multilist values
	 * splitValue -- splits the value with ";" as delimiter
	 * redlineAffItemBOM -- redlines the bom table of the affected item of change order
	 * addRelations -- adds new objects to the relationship tab of the object
	 * saveAsObject -- saves the current object into a new object
	 * getWhereUsedObject -- fetches the where used object number/s from the where used tab of the source object
	 * 
	 */
	//----------------------------------------------------------------creating the object--------------------------------------------------------------

	public  IDataObject createObject(IAgileSession session, String objectSubclassName){

		/**
		 *  creates an object
		 *  using the autonumber assigned to it
		 *  returns the created object
		 *  
		 */
		IDataObject object=null;
		//fetch the auto number source associated for the software bundle sub class
		try{
			IAgileClass objectSubclass = session.getAdminInstance().getAgileClass(objectSubclassName);
			logger.info("part subclass fetched is " +objectSubclass);
			IAutoNumber[] autoNumbers = objectSubclass.getAutoNumberSources();
			for (int i = 0; i < autoNumbers.length; i++) {
				logger.info("Autonumber assigned for the object is " +autoNumbers[i].getName());
			}
	
			//getting the next number that is going to be generated
			String objectAutoNumber = autoNumbers[0].getNextNumber(objectSubclass);
			logger.info("next number that is going to be generated is " +objectAutoNumber);
	
			//assigning the auto number and creating the object of the subclass fetched
			 object = (IDataObject)session.createObject(objectSubclass, objectAutoNumber);
			logger.info("new object created is " + object+" of " +objectSubclass+ " subclass");
		}catch(Exception exp){
			logger.error(exp.getMessage());
			exp.printStackTrace();
			strBuffErr.append(exp.getMessage());
		}
		return object;
	}

	//-----------------------------------------------------------updating the default values-----------------------------------------------------------

	public  void updateDefaultAttributes(String attrToSplit, IDataObject dataObj) throws APIException{
		/**
		 * updates the default attributes
		 * splits the string for ";"
		 * splits the key and value pair with "$" as delimiter
		 * sets the value with their respective attribute using the hashmap
		 * 
		 */
	   logger.info("attrToSplit=="+attrToSplit);
	   try{
			String[] attrAftSplit = attrToSplit.split(";");
			HashMap allAttributes = new HashMap();
			for (String resAftSplit : attrAftSplit) {
				logger.info("semi colon split item is " + resAftSplit);
				int dollarIndex = resAftSplit.indexOf("$");
				logger.info("dollar index is " +dollarIndex);
	
				String attrName = resAftSplit.substring(0, dollarIndex); 
				logger.info("attrName is " +attrName);
	
				String attrVal = resAftSplit.substring(dollarIndex+1, resAftSplit.length()); 
				logger.info("attrVal is " +attrVal);
	
				allAttributes.put(attrName,attrVal);
			}
			dataObj.setValues(allAttributes);
	   }catch(Exception exp){
			logger.info("Exception Occurred while updating Default Values"+exp.getMessage());
			strBuffErr.append("Error  Occurred while updating Default attribute Values to new SWB");
	   }
	}	

	//----------------------------------------- adding the arraylist to the affected item tab of change order -------------------------------------------

	public static void addArrayListToAffItem(IAgileSession session, ArrayList affItemArrayList, IDataObject changeOrder) throws APIException {
		// TODO Auto-generated method stub

		/**
		 * fetching the affected item table of the change order
		 * adding the array list items to the change order affected item
		 * setting the revision value by incrementing the current revision by 1
		 * 
		 */

		//fetching the affected item table of change
		ITable changeAffectedItemTab = changeOrder.getTable(ChangeConstants.TABLE_AFFECTEDITEMS); 
		logger.info("inside affected item table of change");

		//disabling the warning
		session.disableWarning(new Integer("568"));

		//fetching the array list items to be added to the affected item
		logger.info("size of the array list " +affItemArrayList.size());

		for(int i = 0; i < affItemArrayList.size() ; i ++){
			IItem item = (IItem) affItemArrayList.get(i); 
			logger.info("Affected item row " +i+ " getting added");
			logger.info("object fetched from arraylist " +item);

			Map aiParams = new HashMap();

			String revision = item.getRevision();
			logger.info("current revision is " +revision);

			String newRev = null;
			if(revision != null && !revision.equals("")){
				//newRev = incRevX00nn(revision);
				newRev = incRevXnn00(revision);
			}
			else{
				logger.info("current revision is null or not defined");
			}

			//adding the values to the hashmap variable and creating a row
			aiParams.put(ChangeConstants.ATT_AFFECTED_ITEMS_ITEM_NUMBER, item); 
			aiParams.put(ChangeConstants.ATT_AFFECTED_ITEMS_NEW_REV, newRev);
			IRow affectedItemRow = changeAffectedItemTab.createRow(aiParams);
		}

		logger.info("affected items successfully added to the change order " +changeOrder);

		//enabling the warning 
		session.enableWarning(new Integer("568"));
	}

	//----------------------------------------------adding the item to the affected item tab of change order-------------------------------------------

	public static void addAffItemChangeOrder(IAgileSession session, IDataObject changeOrder, IDataObject item, String lifecyclePhase, String revision) throws APIException {
		// TODO Auto-generated method stub

		/**
		 * fetching the affected item table of the change order
		 * adding the affected item to the change order affected item
		 * setting the lifecycle phase and revision
		 * 
		 */

		//fetching the affected item table of change
		ITable changeAffectedItemTab = changeOrder.getTable(ChangeConstants.TABLE_AFFECTEDITEMS); 
		logger.info("inside affected item table of change");

		//disabling the warning
		session.disableWarning(new Integer("568"));

		//adding affected item and setting revision to the item
		Map aiParams = new HashMap();
		aiParams.put(ChangeConstants.ATT_AFFECTED_ITEMS_ITEM_NUMBER, item); 
		aiParams.put(ChangeConstants.ATT_AFFECTED_ITEMS_NEW_REV, revision);
		IRow affectedItemRow = changeAffectedItemTab.createRow(aiParams); 

		//setting the lifecycle phase
		affectedItemRow.setValue(ChangeConstants.ATT_AFFECTED_ITEMS_LIFECYCLE_PHASE, lifecyclePhase);
		logger.info("added " +item+ " as the affected item to " +changeOrder+ " change order");
		logger.info("Revision for the affected item is set to " +revision);
		logger.info("Lifecycle phase for the affected item is set to " +lifecyclePhase);

		//enabling the warning 
		session.enableWarning(new Integer("568"));
	}

	//----------------------------------------------(X00-nn)incrementing the value of current revision by 1------------------------------------------------

	private static String incRevX00nn(String revision) {
		// TODO Auto-generated method stub

		/**
		 * fetching the current revision 
		 * splitting the current revision to hyphen index
		 * incrementing the suffix
		 * appending the suffix and prefix
		 * returning the new revision
		 * the syntax is X00-nn
		 * 
		 */

		//splitting the revision till hyphen
		int hyphenIndex = revision.indexOf("-");
		logger.info("hyphen index is " +hyphenIndex);

		//fetching the characters before hyphen
		String prefix = revision.substring(0, hyphenIndex); 
		logger.info("prefix is " +prefix);

		//fetching the characters after hyphen
		String suffix = revision.substring(hyphenIndex+1, revision.length());
		logger.info("suffix is " +suffix);

		//incrementing the suffix by 1 by parsing the string to int
		int suff = Integer.parseInt(suffix);
		suff++;
		logger.info("suffix after increment is " +suff);

		//parsing int back to string
		suffix = String.valueOf(suff);
		logger.info("new result is " +suffix);

		//checking the value of string and appending "0" if left out
		int suffixLength = suffix.length();
		logger.info("suffix length is " +suffixLength);
		if(suffixLength == 1){
			suffix = "0" +suffix;
			logger.info("suffix after appending zero is " +suffix);
		}

		//appending both the prefix and suffix
		String newRev = prefix + "-" + suffix ;
		logger.info("new revision is "+ newRev);

		return newRev;
	}

	//--------------------------------------------(Xnn-00)incrementing the value of current revision by 1-----------------------------------------------

	private static String incRevXnn00(String revision) {
		// TODO Auto-generated method stub

		/**
		 * fetching the current revision 
		 * splitting the current revision to hyphen index
		 * incrementing the suffix
		 * appending the suffix and prefix
		 * returning the new revision
		 * the syntax is Xnn-00
		 * 
		 */

		//splitting the revision till hyphen
		int hyphenIndex = revision.indexOf("-");
		logger.info("hyphen index is " +hyphenIndex);

		//fetching the characters before hyphen
		String prefix = revision.substring(0, hyphenIndex); 
		logger.info("prefix is " +prefix);

		//fetching the characters after hyphen
		String suffix = revision.substring(hyphenIndex+1, revision.length());
		logger.info("suffix is " +suffix);

		//splitting the prefix after an alphabet
		String prefAlphabet = prefix.substring(0, 1);
		logger.info("prefix aplhabet is " +prefAlphabet);
		//getting the characters after alphabet
		String prefSplit = prefix.substring(1, hyphenIndex); 
		logger.info("prefix after the alphabet "+prefSplit);

		// String newVersion = prefix + (Integer.parseInt(hyphenIndex.substring(1,hyphenIndex.length()))+1);
		int pref = Integer.parseInt(prefSplit);
		pref++;

		logger.info("prefix after increment " +pref);
		prefSplit = String.valueOf(pref);

		int prefLength = prefSplit.length();
		logger.info("prefix length is " +prefLength);
		String newRev = null;

		if(prefLength == 1){
			prefSplit = "0" +prefSplit;
			logger.info("prefix incremented to " +prefSplit);
			prefix = prefAlphabet + prefSplit ;
			logger.info("prefix after appending zero is " +prefix);
			logger.info("new result is " +prefix);
			newRev = prefix + "-" + suffix ;
			logger.info("new revision is "+ newRev);
		}
		else{
			prefix = prefAlphabet + prefSplit;
			logger.info("new result is " +prefix);
			newRev = prefix + "-" + suffix ;
			logger.info("new revision is "+ newRev);
		}
		return newRev;
	}

	
	//-------------------------------------------------------------splitting the attribute values------------------------------------------------------

	private static String[] splitValue(String attribitesToSplit) {
		// TODO Auto-generated method stub

		/**
		 * splitting the value till the ";"
		 * returning the value after splitting
		 * 
		 */

		String strToSplit = attribitesToSplit;
		String[] strAftSplit = strToSplit.split(";");
		for (String res : strAftSplit) {
			logger.info("split item is " + res);
		}
		return strAftSplit;
	}

	//-----------------------------------------------------redlining the bom tab of affected item row--------------------------------------------------

	public void redlineAffItemBOM(IDataObject dataObj, IDataObject changeOrder) throws APIException {
		// TODO Auto-generated method stub

		/**
		 * fetching the affected item tab from the change order
		 * fetching the redlined bom table of the change order affected item
		 * redlining the bom tab by adding an object
		 * 
		 */

		//loading the affected item tab of the change order
		ITable changeAffItemTable = changeOrder.getTable(ChangeConstants.TABLE_AFFECTEDITEMS);
		logger.info("change affected tab is fetched");	

		IRow affItemRow ;

		//iterating the relationship tab of source part
		ITwoWayIterator changeItr = changeAffItemTable.getTableIterator();

		//initialising the rows to array
		List relRow = new ArrayList();

		IDataObject affItemName = null;
		//fetching rows in source software bundle
		while(changeItr.hasNext()){
			affItemRow = (IRow) changeItr.next();
			affItemName = affItemRow.getReferent();
			logger.info("affected item name is " +affItemName);

			ITable redlineBOM = affItemName.getTable(ItemConstants.TABLE_REDLINEBOM);
			logger.info("fetched the redline bom table");

			logger.info("the item that is used for red lining is " +dataObj);
			IRow redlineRow = redlineBOM.createRow(dataObj);
			redlineRow.setValue(ItemConstants.ATT_BOM_QTY, new Integer(50));
			redlineRow.setValue(ItemConstants.ATT_BOM_FIND_NUM, new Integer(777));
			logger.info("Redlined sourceSWB succesfully");
			//break;
		}
		logger.info("redlining of bom tab is done for all the affected items present in the object");
	}

	//--------------------------------------------adding a new object to the relationship tab of the object row----------------------------------------

	public void addRelations(IAgileSession agileSession,IDataObject obj, String relClassValue, String objNumberToAdd) throws APIException {
		// TODO Auto-generated method stub

		/**
		 * objects to be added are stored in a string
		 * the objects are then split 
		 * added to the relationship table of the source object
		 * the relationship class name is set to the default value
		 * the default value is passed in String
		 * 
		 */
		IItem relObjToAdd=null;
		Map params =null;
        try{
		//fetching the relationship table of the source object
		ITable sourceObjRelTable = obj.getTable(ItemConstants.TABLE_RELATIONSHIPS);
		logger.info("source object relationship tab is fetched");

		//splitting the string with ";" as delimiter to fetch the string to be added as an relationship item number
		String[] strAftSplit = objNumberToAdd.split(Constants.INPUT_PARAM_SEPARATOR);
		for (String objNum : strAftSplit) {
			logger.info("string obtained after splitting " + objNum);			

			//loading the object
			 relObjToAdd = (IItem) agileSession.getObject(ItemConstants.CLASS_PART, objNum);
			logger.info("object " +relObjToAdd+ " is loaded to add to the relationship tab");
			//initialising the map and creating a new relationship row
			if(relObjToAdd!=null){
			 params = new HashMap();
			logger.info("relationship value to be added to the relationship class cell " +relClassValue);
			params.put(ItemConstants.ATT_RELATIONSHIPS_NAME, relObjToAdd); 
			params.put(ItemConstants.ATT_RELATIONSHIPS_LIST01, relClassValue);
			IRow sourceRelRow = sourceObjRelTable.createRow(params);
			logger.info("object " +relObjToAdd+ " is successfully added to the relationship tab of source object " +obj);
			}else{
	        	strBuffErr.append("Adding "+ objNum +" to the relationship tab failed ");

			}
			//refreshing the target software bundle item
			obj.refresh();
			logger.info("refreshing the " +obj+ " object");
		}
        }catch (Exception exp){
        	logger.error(exp.getMessage());
        	exp.printStackTrace();
        	strBuffErr.append(exp.getMessage());
        }
	}

	//--------------------------------------------saving a new object with the current object attribute values----------------------------------------

	public IDataObject saveAsObject(IDataObject sourceObject,IAgileSession agileSession) throws APIException, ValidationException {
		// TODO Auto-generated method stub
		/**
		 * an object is passed 
		 * using the atribute values of the passed object
		 * a new object is created
		 * 
		 */
		IDataObject newObject =null;
        try{
		String objectSubclassType = sourceObject.getValue(ItemConstants.ATT_TITLE_BLOCK_PART_TYPE).toString();
		logger.info("object subclass fetched is  " +objectSubclassType);

		//fetch the auto number source associated for the software bundle sub class
		IAgileClass objectSubclass = agileSession.getAdminInstance().getAgileClass(objectSubclassType);
		logger.info("object subclass for doing save as " +objectSubclass);

		IAutoNumber[] autoNumbers = objectSubclass.getAutoNumberSources();
		for (int i = 0; i < autoNumbers.length; i++) {
			logger.info("Autonumber assigned for the object is " +autoNumbers[i].getName());
		}

		//getting the next number that is going to be generated
		String objectAutoNumber = autoNumbers[0].getNextNumber(objectSubclass);
		logger.info("next number that is going to be generated is " +objectAutoNumber);
		 newObject = (IDataObject) sourceObject.saveAs(objectSubclass, objectAutoNumber);
		logger.info("new object " +newObject+ " is created from " +sourceObject+ " using save as method");
        }catch (APIException e) {
			// TODO Auto-generated catch block
			logger.info(e.getMessage());
			e.printStackTrace();
			throw new ValidationException(e.getMessage());
		}catch(Exception exp){
			logger.info(exp.getMessage());
			exp.printStackTrace();
			throw new ValidationException(exp.getMessage());
		}
		return newObject;
	}

	//---------------------------------fetching the parent object from the where used tab of the source object----------------------------------------

	public void getWhereUsedObject(IDataObject sourceObject) throws APIException {
		// TODO Auto-generated method stub

		/**
		 * the source object is loaded
		 * where used table of the source object is loaded
		 * all the parent items for the current source object is fetched
		 * 
		 */

		//IRow sourceWhereUsedRow ;
		//initialising the array list
		ArrayList pnrObj = new ArrayList();
		ArrayList ecoObj = new ArrayList();
		ArrayList hcoObj = new ArrayList();
		
		//fetching the where used tab of the object
		ITable itemWhereUsed = (ITable) sourceObject.getTable(ItemConstants.TABLE_WHEREUSED);
		logger.info("where used table is fetched from the item loaded");

		/*ITwoWayIterator whereUsedItr = itemWhereUsed.getTableIterator();
		logger.info("iterating the where used tab of the item to fetch the rows");
		 */	
		ITwoWayIterator whereUsedItr = itemWhereUsed.getReferentIterator();
		logger.info("iterating the where used tab of the item to fetch the rows");

		int count = 0;
		while(whereUsedItr.hasNext()){
			//sourceWhereUsedRow = (IRow) whereUsedItr.next();
			IDataObject dataObj = (IItem)whereUsedItr.next();
			//if(item != null){
			logger.info("the fetched object name from the row is " +dataObj.getName());				
			HashMap whereUsedParams = new HashMap();
			whereUsedParams.put(count,dataObj);	
			
			//fetching the item type of the object in the where used tab
			String subclassType = dataObj.getValue(ItemConstants.ATT_TITLE_BLOCK_ITEM_TYPE).toString();
			logger.info("subclass type of the fetched object is " +subclassType);

			//checking the subclass type of the fetched item and putting it to their respective arraylist
			if(subclassType.contains("Hierarchy")){
				logger.info("Belongs to the hierarchy subclass");
				//adding the object to the hco array list
				hcoObj.add(dataObj);
				logger.info("added " +dataObj+ " to the hco change order arraylist");
			}else{
				logger.info("Doesnt belong to the hierarchy subclass");
				//fetching the lifecycle phase of the object
				String lcp = dataObj.getValue(ItemConstants.ATT_TITLE_BLOCK_LIFECYCLE_PHASE).toString();
				
				//checking if the lifecycle phase is X Revision
				if(lcp.contains("X Rev")){
					logger.info("contains X Revision");
					//adding the object to the pnr array list
					pnrObj.add(dataObj);
					logger.info("added " +dataObj+ " to the pnr change order arraylist");
				}
				//checking if the lifecycle phase is A Revision
				else{
					logger.info("contains A Revision");
					//adding the object to the eco array list
					ecoObj.add(dataObj);
					logger.info("added " +dataObj+ " to the eco change order arraylist");
				}
			}				
			count++;
		}		
		logger.info("all the object name are fetched from the where used table of the source object");
		logger.info("***************PNR ArrayList***************");
		logger.info(pnrObj);
		logger.info("***************ECO ArrayList***************");
		logger.info(ecoObj);
		logger.info("***************HCO ArrayList***************");
		logger.info(hcoObj);
	}

	public Boolean isValidSWB(String predecessorSWBNum,IAgileSession agileSession,Properties applicationProperties,String isSuccessorInput) throws ValidationException {
		// TODO Auto-generated method stub
		IDataObject predecessorDataObj;
		String errorMessage=null;
		try {
			    logger.info("---inside isValidSWB--");
			    predecessorDataObj = (IItem) agileSession.getObject(ItemConstants.CLASS_PART, predecessorSWBNum);
				if(predecessorDataObj!=null){
					String partType = predecessorDataObj.getValue(ItemConstants.ATT_TITLE_BLOCK_PART_TYPE).toString();
					String partClass = predecessorDataObj.getValue(ItemConstants.ATT_TITLE_BLOCK_ITEM_TYPE).toString();
					String swbPart = applicationProperties.getProperty(Constants.SOFTWARE_BUNDLE);

				    logger.info("partType=="+partType);
				    logger.info("partClass=="+partClass);
				    logger.info("isSuccessorInput=="+isSuccessorInput);

					if(!(swbPart.equalsIgnoreCase(partType) && swbPart.equalsIgnoreCase(partClass))){
						errorMessage=applicationProperties.getProperty(Constants.MSG_NOT_VALID_SWB);
						throw new Exception();
						//283536
				    }else{
					    String isSuccessorYes =applicationProperties.getProperty(Constants.IS_SUCCESSOR_YES);
						String successor = applicationProperties.getProperty(Constants.PAGE_THREE_SUCCESSOR);
						String successorValue=predecessorDataObj.getValue(successor).toString();
					    logger.info("successor=="+predecessorDataObj.getValue(successor).toString().isEmpty());
				    	if(predecessorDataObj.getValue(successor)!=null && !(predecessorDataObj.getValue(successor).toString().isEmpty()) && isSuccessorYes.equalsIgnoreCase(isSuccessorInput)) {
						   errorMessage=applicationProperties.getProperty(Constants.MSG_SUCCUES_EXIST_SWB);
					       logger.info("errorMessage=="+errorMessage);
						   throw new Exception();
				        }
				    }
				   }else{
					errorMessage=applicationProperties.getProperty(Constants.MSG_NOT_VALID_SWB);
					throw new Exception();
				}
		}catch(Exception exp){
			logger.info("Exception occurred in Valid SWB");
			throw new ValidationException(errorMessage);
		}
		return true;
	}
	
	
	public Boolean isValidSWB(String predecessorSWBNum,IAgileSession agileSession,Properties applicationProperties) throws ValidationException {
		// TODO Auto-generated method stub
	//	String swbPart=Constants.SOFTWARE_BUNDLE;
		IDataObject predecessorDataObj;
		String errorMessage=null;
		try {
			logger.info("---inside isValidSWB--");
			predecessorDataObj = (IItem) agileSession.getObject(ItemConstants.CLASS_PART, predecessorSWBNum);
			if(predecessorDataObj!=null){
				String partType = predecessorDataObj.getValue(ItemConstants.ATT_TITLE_BLOCK_PART_TYPE).toString();
				String partClass = predecessorDataObj.getValue(ItemConstants.ATT_TITLE_BLOCK_ITEM_TYPE).toString();
				String swbPart = applicationProperties.getProperty(Constants.SOFTWARE_BUNDLE);
			    logger.info("partType=="+partType);
			    logger.info("partClass=="+partClass);
				//String partClass=SOFTWARE_BUNDLE;
				if(!(swbPart.equalsIgnoreCase(partType) && swbPart.equalsIgnoreCase(partClass))){
					errorMessage=applicationProperties.getProperty(Constants.MSG_WHR_USED_NOT_VALD_SWB);
					throw new Exception();
					//283536
			    }
			}else{
				errorMessage=applicationProperties.getProperty(Constants.MSG_WHR_USED_NOT_VALD_SWB);
				throw new Exception();
			}
		}catch(Exception exp){
			logger.info("Exception occurred in is Valid SWB"+exp.getMessage());
			throw new ValidationException(errorMessage);
		}
		return true;
	}


	public void updateAndClearAttributesOnSaveAs(IDataObject newObject,String xmlFilePath){
			IAgileSession session = null;
			try {
				logger.info("Started check");
				
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				Document doc = db.parse(new File(xmlFilePath));
				XMLReader xmlReader =new XMLReader();
				HashMap xmlMap=xmlReader.readXML(doc,"SoftwareBundle","PartsClass","ItemsBaseClass");
				logger.info("xmlMap=="+xmlMap);
				ArrayList attributeList = (ArrayList) xmlMap.get("Page_Set");	
				clearAttrForObject(newObject, attributeList);
			} catch(Exception ex) {
				logger.info(ex);
			} finally {
				if(session!=null) {
					session.close();
					session= null;
				}
			}
		}
	/*private static void clearAttrForObject(IDataObject dataObject,ArrayList<String> attributeList) throws APIException {
			logger.info("Attribute List :"+attributeList);
			HashMap<String,Object> allAttr = new HashMap<String,Object>();
			try {
				for(int j=0;j<attributeList.size();j++){
					String attributeStr=attributeList.get(j).trim();
					logger.info("attributeStr"+attributeStr);
					String[] attributeVal = attributeStr.split(":");
					String attributeName=attributeVal[0];
					String valueStr = attributeVal[1];
					if(valueStr.equals("null")) {
						allAttr.put(attributeName, null);
					} else {
						allAttr.put(attributeName, valueStr);
					}
				}	//End of Attribute Iteration 
				logger.info("allAttr"+allAttr);
				//Clear all attribute values in one go
				dataObject.setValues(allAttr);
				logger.info("cleared all allAttr");
			} catch(Exception ex) {
				logger.info("Exception while setting all attributes=="+ex.getMessage());
				ex.printStackTrace();
				for(int j=0;j<attributeList.size();j++){
					String attributeStr=attributeList.get(j);
					attributeStr=attributeList.get(j).trim();
					logger.info("attributeStr"+attributeStr);
					String[] attributeVal = attributeStr.split(":");
					String attributeName=attributeVal[0];
					String valueStr = attributeVal[1];
					logger.info("Attribute Name :"+attributeName);
					
					try{
						//ICell cell =  dataObject.getCell(new Integer(baseIdStr));
						ICell cell =  dataObject.getCell(attributeName);
						cell.setValue(null);
						logger.info("Cleared Attribute :"+attributeName);
					}catch(Exception apiEx){
						apiEx.printStackTrace();
						//logger.info(CommonUtil.exception2String(apiEx));
					}
				}
			}
	}*/
	private static void clearAttrForObject(IDataObject dataObject,ArrayList<String> attributeList) throws APIException {
        logger.info("Attribute List :"+attributeList);
        HashMap<String,Object> allAttr = new HashMap<String,Object>();
        try {
              for(int j=0;j<attributeList.size();j++){
                     String attributeStr=attributeList.get(j).trim();
                     logger.info("attributeStr"+attributeStr);
                     String[] attributeVal = attributeStr.split(":");
                     String attributeName=attributeVal[0];
                     String valueStr = attributeVal[1];
                     if(valueStr.equals("null")) {
                            allAttr.put(attributeName, null);
                     } else {
                            allAttr.put(attributeName, valueStr);
                     }
              }      //End of Attribute Iteration 
              logger.info("allAttr"+allAttr);
              //Clear all attribute values in one go
              dataObject.setValues(allAttr);
              logger.info("cleared all allAttr");
        } catch(Exception ex) {
              logger.info("Exception while setting all attributes=="+ex.getMessage());
              ex.printStackTrace();
              for(int j=0;j<attributeList.size();j++){
                     String attributeStr=attributeList.get(j);
                     attributeStr=attributeList.get(j).trim();
                     logger.info("attributeStr"+attributeStr);
                     String[] attributeVal = attributeStr.split(":");
                     String attributeName=attributeVal[0];
                     String valueStr = attributeVal[1];
                     logger.info("Attribute Name :"+attributeName);
                     
                     try{
                           //ICell cell =  dataObject.getCell(new Integer(baseIdStr));
                           ICell cell =  dataObject.getCell(attributeName);
                            if(valueStr.equals("null")) {
                                  cell.setValue(null);
                           } else {
                                  cell.setValue(valueStr);
                           }
                           
                           
                           logger.info("Cleared Attribute :"+attributeName);
                     }catch(Exception apiEx){
                           apiEx.printStackTrace();
                            //logger.info(CommonUtil.exception2String(apiEx));
                     }
              }
        }
}

}
