package com.dell.plm.agile.integration.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.dell.plm.agile.aic.common.cryptography.FileEncryptorUtil;
import com.dell.plm.agile.aic.common.dao.DBConnectionManagerSingleton;
import com.dell.plm.agile.aic.common.exception.ValidationException;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;
import com.dell.plm.agile.integration.util.Utility;

public class OsaLangDAO {
	private static Logger logger = Logger.getLogger(OsaLangDAO.class);
	
	
	public Connection getConnection(String dbPropFilePath)throws ValidationException, SQLException, FileNotFoundException, IOException
	{
		//FileEncryptorUtil.encryptFile(dbPropFilePath);//Ajit--> Consolidation to properties files
		DBConnectionManagerSingleton dms = DBConnectionManagerSingleton.getInstance();
		logger.debug("Encrypt the File done");
		Connection connection = null;
        Properties asyncDBProperties = FilePropertyUtility.loadProperties(dbPropFilePath);
        /*String dbUrl = asyncDBProperties.getProperty("DB_URL").trim();*/ // Ajit--> Consolidation to properties files
        String dbUrl = asyncDBProperties.getProperty("ESB_DB_URL").trim();
        /*String dbUserName = asyncDBProperties.getProperty("DB_USERNAME").trim();*/ // Ajit--> Consolidation to properties files
        String dbUserName = asyncDBProperties.getProperty("ESB_DB_SCHEMA_AGILE_SERVICES_USERNAME").trim();
        /*String dbPassword = asyncDBProperties.getProperty("DB_PASSWORD").trim();*/ // Ajit--> Consolidation to properties files
        String dbPassword = asyncDBProperties.getProperty("ESB_DB_SCHEMA_AGILE_SERVICES_PASSWORD").trim();
        Utility util = new Utility();
        dbPassword = util.getDecryptPwd(dbPassword);
        logger.debug("url:"+dbUrl+"userName:"+dbUserName);
        if(dbUrl == null || dbUrl.equalsIgnoreCase("") || dbUserName == null || dbUserName.equalsIgnoreCase("") || dbPassword == null || dbPassword.equalsIgnoreCase(""))
        {
              logger.error("Properties file Not Loaded .."+"url:"+dbUrl+"userName:"+dbUserName);
              throw new ValidationException("Can't load properties file");
        }
        
        //logger.debug("after decrypt:"+dbPassword);
       
        connection = dms.getConnection(dbUrl, dbUserName, dbPassword);
        logger.debug("getDBConnection():" + connection);
        return connection;

	}
	
	
	/**
	 * @param dmRtPropFilepath
	 * @return
	 * @throws ValidationException
	 * @throws SQLException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public Connection getDBConnection(String dmRtPropFilepath)throws ValidationException, SQLException, FileNotFoundException, IOException 
	{
		DBConnectionManagerSingleton dms = DBConnectionManagerSingleton.getInstance();
		Connection connection = null;
        Properties syncDBProperties = FilePropertyUtility.loadProperties(dmRtPropFilepath);
        String dbUrl = syncDBProperties.getProperty("BI_DB_URL").trim();
        String dbUserName = syncDBProperties.getProperty("BI_DB_SCHEMA_AGILEPLM_DM_RT_USERNAME").trim();
        String dbPassword = syncDBProperties.getProperty("BI_DB_SCHEMA_AGILEPLM_DM_RT_PASSWORD").trim();
        Utility util = new Utility();
        dbPassword = util.getDecryptPwd(dbPassword);
        logger.debug("url:"+dbUrl+"userName:"+dbUserName);
        if(dbUrl == null || dbUrl.equalsIgnoreCase("") || dbUserName == null || dbUserName.equalsIgnoreCase("") || dbPassword == null || dbPassword.equalsIgnoreCase(""))
        {
              logger.error("Properties file Not Loaded .."+"url:"+dbUrl+"userName:"+dbUserName);
              throw new ValidationException("Can't load properties file");
        }
        
        //logger.debug("after decrypt:"+dbPassword);
       
        connection = dms.getConnection(dbUrl, dbUserName, dbPassword);
        logger.debug("getDBConnection():" + connection);
        return connection;

	}
	
	public void addOsLangForSWBPart(Properties applicationProperties,String newSWBPartNum,String refSWBPartNum) throws Exception
	{
		logger.debug("Enter into addOsLangForSWBPart ");
		Connection connection=null;
		try{
		String DB_PROPERTIES = applicationProperties.getProperty("DBPROP_FILEPATH").trim();
	     connection=getConnection(DB_PROPERTIES);
		logger.debug("newSWBPartNum Start"+newSWBPartNum);
		logger.debug("refSWBPartNum Start"+refSWBPartNum);
		CallableStatement insertReqStmt = null;
		insertReqStmt = connection.prepareCall("{call AIC_OSLANG_PKG.prc_addoslang_for_newpart(?,?)}");
		// insertReqStmt.setString(1,action);
		/**1 is Action Parameter (PCID Action, FileDownLoad Action=1, ZIPACK Action= 4 )*/
		insertReqStmt.setString(1, newSWBPartNum);
		/**2 is dependentqueueid parameter*/
		insertReqStmt.setString(2, refSWBPartNum);
		insertReqStmt.execute();
		logger.debug("add ServiceQueue Method End"+insertReqStmt);
		logger.debug("End addServiceQueue...");
		}catch(Exception exp){
			logger.debug("Error occurred while updating hasOsLangMatrix");
		}finally{
			if(connection!=null)
				connection.close();
		}
		
	}
	
}
