package com.dell.plm.agile.integration.dto;

/**
 * This DTO class holds the attributes and getter setter methods
 * used for AFP numbers update.
 * @Project AFP Update
 * @author Siva_Ramaraj
 * @Date October 6,2016
 */
public class AFPResponse 
{
	private String status;
	private String objectId;
	private String validationError =null;
	
	public String getStatus() 
	{
		return status;
	}
	
	public void setStatus(String status) 
	{
		this.status = status;
	}
	
	public String getObjectId() 
	{
		return objectId;
	}
	
	public void setObjectId(String objectId) 
	{
		this.objectId = objectId;
	}
	
	public String getValidationError() 
	{
		return validationError;
	}
	
	public void setValidationError(String validationError) 
	{
		this.validationError = validationError;
	}
	
	public String toString()
	{
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("status:").append(status).append(" ");
		strBuff.append("partNumber:").append(objectId).append(" ");
		if(validationError != null)
		{
			strBuff.append("validationError:").append(validationError).append(" ");
		}
		return strBuff.toString();
	}
}
