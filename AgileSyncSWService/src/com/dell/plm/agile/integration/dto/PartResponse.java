package com.dell.plm.agile.integration.dto;

public class PartResponse {
	private String partAttributeXML = null;
	private String status;
	private String objectId;
	private String validationError =null;
	private String dbFailure =null;
	
	public String getPartAttributeXML() {
		return partAttributeXML;
	}

	public void setPartAttributeXML(String partAttributeXML) {
		this.partAttributeXML = partAttributeXML;
	}
	
	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getValidationError() {
		return validationError;
	}

	public void setValidationError(String validationError) {
		this.validationError = validationError;
	}

	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("status:").append(status).append(" ");
		sb.append("part_number:").append(objectId).append(" ");
		if(validationError != null)
		{
			sb.append("validationError:").append(validationError).append(" ");
		}
		if(dbFailure != null)
		{
			sb.append("Database error:").append(dbFailure);
		}
		return sb.toString();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDbFailure() {
		return dbFailure;
	}

	public void setDbFailure(String dbFailure) {
		this.dbFailure = dbFailure;
	}

}
