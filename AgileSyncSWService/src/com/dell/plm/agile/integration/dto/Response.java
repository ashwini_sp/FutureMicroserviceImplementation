package com.dell.plm.agile.integration.dto;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class Response 
{
	private String status;
	private String objectId;
	private String validationError =null;
	private String dbFailure =null;
	private String osLangXML =null;
	
	//private Status status;

	private String swbPredecessorXML =null;
	
	/** Added as part of Prereq and Coreq Project */
	private String preReqCoreqXML =null;
	
	/** Added as part of OS Filtering Project */
	private String osFileTypeCodeXML = null;	
	
	/**
	 * @return
	 */
	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getValidationError() {
		return validationError;
	}

	public void setValidationError(String validationError) {
		this.validationError = validationError;
	}

	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("status:").append(status).append(" ");
		sb.append("part_number:").append(objectId).append(" ");
		if(validationError != null)
		{
			sb.append("validationError:").append(validationError).append(" ");
		}
		if(dbFailure != null)
		{
			sb.append("Database error:").append(dbFailure);
		}
		return sb.toString();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDbFailure() {
		return dbFailure;
	}

	public void setDbFailure(String dbFailure) {
		this.dbFailure = dbFailure;
	}

	public String getOsLangXML() {
		return osLangXML;
	}

	public void setOsLangXML(String osLangXML) {
		this.osLangXML = osLangXML;
	}

	public String getSwbPredecessorXML() {
		return swbPredecessorXML;
	}

	public void setSwbPredecessorXML(String swbPredecessorXML) {
		this.swbPredecessorXML = swbPredecessorXML;
	}

	/**
	 * @return the preReqCoreqXML
	 */
	public String getPreReqCoreqXML() {
		return preReqCoreqXML;
	}

	/**
	 * @param preReqCoreqXML the preReqCoreqXML to set
	 */
	public void setPreReqCoreqXML(String preReqCoreqXML) {
		this.preReqCoreqXML = preReqCoreqXML;
	}

	/**
	 * @return the osFileTypeCodeXML
	 */
	public String getOsFileTypeCodeXML() {
		return osFileTypeCodeXML;
	}

	/**
	 * @param osFileTypeCodeXML the osFileTypeCodeXML to set
	 */
	public void setOsFileTypeCodeXML(String osFileTypeCodeXML) {
		this.osFileTypeCodeXML = osFileTypeCodeXML;
	}


}
