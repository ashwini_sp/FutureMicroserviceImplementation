package com.dell.plm.agile.integration.dto;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class SWBAutoCreationResponse 
{
	private String status;
	private String newSWBNumber;
	private String referneceSWBNumber;
	private String errorCode;
	private String errorMessage;
	private String messageId;
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public String getNewSWBNumber() {
		return newSWBNumber;
	}
	public void setNewSWBNumber(String newSWBNumber) {
		this.newSWBNumber = newSWBNumber;
	}
	public String getReferneceSWBNumber() {
		return referneceSWBNumber;
	}
	public void setReferneceSWBNumber(String referneceSWBNumber) {
		this.referneceSWBNumber = referneceSWBNumber;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
