package com.dell.plm.agile.integration.dto;

public class SWBFirstLevelWUResponse {
	String messageId;
	String swbNumber;
	String parentPartNumbers;
	String errorMessage;
	String errorCode;
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getParentPartNumbers() {
		return parentPartNumbers;
	}
	public void setParentPartNumbers(String parentPartNumbers) {
		this.parentPartNumbers = parentPartNumbers;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	String status;
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getSwbNumber() {
		return swbNumber;
	}
	public void setSwbNumber(String swbNumber) {
		this.swbNumber = swbNumber;
	}
}
