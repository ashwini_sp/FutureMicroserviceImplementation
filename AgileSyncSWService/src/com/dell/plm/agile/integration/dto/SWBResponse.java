package com.dell.plm.agile.integration.dto;

public class SWBResponse 
{
	private String status;
	private String objectId;
	private String validationError =null;
	private String dbFailure =null;
	private String SWBDetailsXML = null;

	public String getStatus() 
	{
		return status;
	}

	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getObjectId() 
	{
		return objectId;
	}

	public void setObjectId(String objectId) 
	{
		this.objectId = objectId;
	}

	public String getValidationError() 
	{
		return validationError;
	}

	public void setValidationError(String validationError) 
	{
		this.validationError = validationError;
	}

	public String getDbFailure() 
	{
		return dbFailure;
	}

	public void setDbFailure(String dbFailure) 
	{
		this.dbFailure = dbFailure;
	}
	
	public String getSWBDetailsXML() 
	{
		return SWBDetailsXML;
	}

	public void setSWBDetailsXML(String SWBDetailsXML) 
	{
		this.SWBDetailsXML = SWBDetailsXML;
	}
}
