package com.dell.plm.agile.integration.dto;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class SuccessorResponse 
{
	private String status;
	private String objectId;
	private String validationError =null;
	private String dbFailure =null;
	
	
	
	//D&D change 11
	private String swbSuccessorXML =null;
	//D&D change over
	
	/**
	 * @return
	 */
	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getValidationError() {
		return validationError;
	}

	public void setValidationError(String validationError) {
		this.validationError = validationError;
	}

	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("status:").append(status).append(" ");
		sb.append("part_number:").append(objectId).append(" ");
		if(validationError != null)
		{
			sb.append("validationError:").append(validationError).append(" ");
		}
		if(dbFailure != null)
		{
			sb.append("Database error:").append(dbFailure);
		}
		return sb.toString();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDbFailure() {
		return dbFailure;
	}

	public void setDbFailure(String dbFailure) {
		this.dbFailure = dbFailure;
	}

	

	//D&D change 12
	public String getSwbSuccessorXML() {
		return swbSuccessorXML;
	}

	public void setSwbSuccessorXML(String swbSuccessorXML) {
		this.swbSuccessorXML = swbSuccessorXML;
	}
	//D&D change over

}
