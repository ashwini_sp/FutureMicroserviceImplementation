/*
 * PartOSLang.java  
 * 
 * Version information
 * Date: 05-May-2011		
 * 
 * Copyright : DELL confidential
 * 
 */
package com.dell.plm.agile.integration.dto.oslang;

import java.util.ArrayList;
import java.util.List;


/**
 * This class holds the operating system and language details of a part number requested.
 * 
 * 
 * 		-------     ----------   			-----------------		-------------------------
 * 		Version 	  Author	 			Last modified date			Remarks(Modification)
 * 		------      ----------				------------------		------------------------
 * 		1.0			Yathiraja_Lingadahal	  05-May-2011				Created this class.	
 *
 */
public class PartOSLang{
	
	//Operating system of the part
	private String os =null;
	//Languages 
	private String lang =null;
	
	/**
	 * Creates instance of this class and sets the variables.
	 * @param os
	 * @param lang
	 */
	public PartOSLang(String os,String lang)
	{
		this.os = os;
		this.lang= lang;
	}
	
	/**
	 * Returns the OS
	 * @return
	 */
	public String getOs()
	{
		return os;
	}
	/**
	 * Returns the lang
	 * @return
	 */
	public String getLang()
	{
		return lang;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return "OS="+os+ " Langs="+lang;
	}

	/**
	 * Returns the list of langs
	 * @return
	 */
	public List<String> getLanguageList()
	{
		List <String> listLang = null;
		String [] arrLang = this.lang.split(";");

		if(arrLang.length>0)
		{
			listLang = new ArrayList<String>();
			for(String strLan :arrLang)
			{
				listLang.add(strLan);
			}
		}

		return listLang;
	}

	/**
	 * Returns the array of langs.
	 * @return
	 */
	public String [] getLanguagArray()
	{
		String [] arrLang = this.lang.split(";");

		return arrLang;
	}
}

