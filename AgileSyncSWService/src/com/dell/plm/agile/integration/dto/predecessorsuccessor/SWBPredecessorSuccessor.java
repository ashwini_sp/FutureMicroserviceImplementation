/**
 * 
 */
package com.dell.plm.agile.integration.dto.predecessorsuccessor;

/**
 * @author Vumesh_Gundapuneni
 *
 */
public class SWBPredecessorSuccessor {
	
	private String swbNumber =null;
	
	public SWBPredecessorSuccessor(String swbNumber) {
		this.swbNumber = swbNumber;
	}

	/**
	 * @param swbNumber the swbNumber to set
	 */
	public void setSwbNumber(String swbNumber) {
		this.swbNumber = swbNumber;
	}
	
	
	/**
	 * @return the swbNumber
	 */
	public String getSwbNumber() {
		return swbNumber;
	}
	
	
	@Override
	public String toString() {
		return "SWBPredecessor [swbNumber=" + swbNumber + "]";
	}
	
}
