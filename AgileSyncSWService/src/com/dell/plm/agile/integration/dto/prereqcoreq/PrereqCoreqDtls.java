package com.dell.plm.agile.integration.dto.prereqcoreq;

import java.util.ArrayList;
import java.util.List;

public class PrereqCoreqDtls
{
	    private String swbNumber = null;
	    private String prereqCoreqNbr = null;
	    private String prereqCoreqFlag = null;
	    private Integer orderOfInstallation = null;
	    private String specificOrLaterVer = null;
	    private String platformIds = null;
	    private String platformNames = null;
	    private String platformPhNbr = null;
	    private String osIds =null;
	    private String osNames =null;
	    private String langIds =null;
	    private String langNames =null;
	    private String lossOfFuncRsnTxt = null;
	    
	    	    
		/**
		 * @return the swbNumber
		 */
		public String getSwbNumber() {
			return swbNumber;
		}


		/**
		 * @param swbNumber the swbNumber to set
		 */
		public void setSwbNumber(String swbNumber) {
			this.swbNumber = swbNumber;
		}


		/**
		 * @return the prereqCoreqNbr
		 */
		public String getPrereqCoreqNbr() {
			return prereqCoreqNbr;
		}


		/**
		 * @param prereqCoreqNbr the prereqCoreqNbr to set
		 */
		public void setPrereqCoreqNbr(String prereqCoreqNbr) {
			this.prereqCoreqNbr = prereqCoreqNbr;
		}


		/**
		 * @return the prereqCoreqFlag
		 */
		public String getPrereqCoreqFlag() {
			return prereqCoreqFlag;
		}


		/**
		 * @param prereqCoreqFlag the prereqCoreqFlag to set
		 */
		public void setPrereqCoreqFlag(String prereqCoreqFlag) {
			this.prereqCoreqFlag = prereqCoreqFlag;
		}


		/**
		 * @return the orderOfInstallation
		 */
		public Integer getOrderOfInstallation() {
			return orderOfInstallation;
		}


		/**
		 * @param orderOfInstallation the orderOfInstallation to set
		 */
		public void setOrderOfInstallation(Integer orderOfInstallation) {
			this.orderOfInstallation = orderOfInstallation;
		}


		/**
		 * @return the specificOrLaterVer
		 */
		public String getSpecificOrLaterVer() {
			return specificOrLaterVer;
		}


		/**
		 * @param specificOrLaterVer the specificOrLaterVer to set
		 */
		public void setSpecificOrLaterVer(String specificOrLaterVer) {
			this.specificOrLaterVer = specificOrLaterVer;
		}


		/**
		 * @return the platformIds
		 */
		public String getPlatformIds() {
			return platformIds;
		}


		/**
		 * @param platformIds the platformIds to set
		 */
		public void setPlatformIds(String platformIds) {
			this.platformIds = platformIds;
		}


		/**
		 * @return the platformNames
		 */
		public String getPlatformNames() {
			return platformNames;
		}


		/**
		 * @param platformNames the platformNames to set
		 */
		public void setPlatformNames(String platformNames) {
			this.platformNames = platformNames;
		}


		/**
		 * @return the platformPhNbr
		 */
		public String getPlatformPhNbr() {
			return platformPhNbr;
		}


		/**
		 * @param platformPhNbr the platformPhNbr to set
		 */
		public void setPlatformPhNbr(String platformPhNbr) {
			this.platformPhNbr = platformPhNbr;
		}


		/**
		 * @return the osIds
		 */
		public String getOsIds() {
			return osIds;
		}


		/**
		 * @param osIds the osIds to set
		 */
		public void setOsIds(String osIds) {
			this.osIds = osIds;
		}


		/**
		 * @return the osNames
		 */
		public String getOsNames() {
			return osNames;
		}


		/**
		 * @param osNames the osNames to set
		 */
		public void setOsNames(String osNames) {
			this.osNames = osNames;
		}


		/**
		 * @return the langIds
		 */
		public String getLangIds() {
			return langIds;
		}


		/**
		 * @param langIds the langIds to set
		 */
		public void setLangIds(String langIds) {
			this.langIds = langIds;
		}


		/**
		 * @return the langNames
		 */
		public String getLangNames() {
			return langNames;
		}


		/**
		 * @param langNames the langNames to set
		 */
		public void setLangNames(String langNames) {
			this.langNames = langNames;
		}


		/**
		 * @return the lossOfFuncRsnTxt
		 */
		public String getLossOfFuncRsnTxt() {
			return lossOfFuncRsnTxt;
		}


		/**
		 * @param lossOfFuncRsnTxt the lossOfFuncRsnTxt to set
		 */
		public void setLossOfFuncRsnTxt(String lossOfFuncRsnTxt) {
			this.lossOfFuncRsnTxt = lossOfFuncRsnTxt;
		}

		/**
		* Returns the list of PPA Ids 
		* @return 
		*/
		public List<Integer> getPPAIdList() throws Exception
		{
			List <Integer> listPPAIds = null;
			String [] arrPPAIds = this.platformIds.split(",");
			
			if(arrPPAIds.length > 0)
			{
				listPPAIds = new ArrayList<Integer>();
				for(String strPpaId :arrPPAIds)
				{
					try{
						if(strPpaId.trim().length() > 0){
							listPPAIds.add(Integer.parseInt(strPpaId.trim()));
						}
					}
					catch(Exception e)
					{
						throw e;
					}
				}
			}
			return listPPAIds;
		}
		
		
		/**
		 * Returns the list of oses ids
		 * @return
		 */
		public List<Integer> getOsIdList() throws Exception
		{
			List <Integer> listOsIds = null;
			String [] arrOs = this.osIds.split(",");

			if(arrOs.length > 0)
			{
				listOsIds = new ArrayList<Integer>();
				for(String strOs : arrOs)
				{
					try{
						if(strOs.trim().length() > 0){
							listOsIds.add(Integer.parseInt(strOs.trim()));
						}
					}
					catch(Exception e)
					{
						throw e;
					}
				}
			}
			return listOsIds;
		}
		
		
		/**
		 * Returns the list of lang ids
		 * @return
		 */
		public List<Integer> getLangIdList() throws Exception
		{
			List <Integer> listLangIds = null;
			String [] arrLang = this.langIds.split(",");

			if(arrLang.length > 0)
			{
				listLangIds = new ArrayList<Integer>();
				for(String strLang :arrLang)
				{
					try{
						if(strLang.trim().length() > 0){
							listLangIds.add(Integer.parseInt(strLang.trim()));
						}
					}
					catch(Exception e)
					{
						throw e;
					}
				}
			}
			return listLangIds;
		}
}
