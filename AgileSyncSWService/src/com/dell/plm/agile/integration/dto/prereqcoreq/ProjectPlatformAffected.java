package com.dell.plm.agile.integration.dto.prereqcoreq;

public class ProjectPlatformAffected
{
	private Integer id=null;
	private String phNumber=null;
	private String ppa=null;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the phNumber
	 */
	public String getPhNumber() {
		return phNumber;
	}
	/**
	 * @param phNumber the phNumber to set
	 */
	public void setPhNumber(String phNumber) {
		this.phNumber = phNumber;
	}
	/**
	 * @return the ppa
	 */
	public String getPpa() {
		return ppa;
	}
	/**
	 * @param ppa the ppa to set
	 */
	public void setPpa(String ppa) {
		this.ppa = ppa;
	}
	
	@Override
	public String toString() {
		return "ProjectPlatformAffected [id=" + id + ", phNumber=" + phNumber
				+ ", ppa=" + ppa + "]";
	}
}
