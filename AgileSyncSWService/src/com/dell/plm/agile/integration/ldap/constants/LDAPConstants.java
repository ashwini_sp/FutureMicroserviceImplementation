/*
 * LDAPConstants.java  
 * 
 * Version information
 * Date: 16-Sept-2011
 * 
 * Copyright : DELL confidential
 * 
 */
package com.dell.plm.agile.integration.ldap.constants;

/**
 * Contains the constants which are mapped with ladp.properties keys.
 * 
 * 		-------     ----------   		 -----------------		-------------------------
 * 		Version 	  Author	 		 Last modified date			Remarks(Modification)
 * 		------      ----------			 ------------------		------------------------
 * 		1.0			Yathiraja_Lingadahal	16-Sept-2011  			Created this class.	
 */
public class LDAPConstants {
	public static final String ENABLED_LDAP_AUTHENTICATION = "ENABLED_LDAP_AUTHENTICATION";
	public static final String CONTEXT_FACTORY_LDAP = "CONTEXT_FACTORY_LDAP";
	public static final String PROTOCOL_LDAP = "PROTOCOL_LDAP";
	public static final String PORT_LDAP = "PORT_LDAP";
	public static final String DOMAIN_AMERICAS = "DOMAIN_AMERICAS";
	public static final String DOMAIN_ASIA_PACIFIC = "DOMAIN_ASIA_PACIFIC";
	public static final String HOST_AMERICAS = "HOST_AMERICAS";
	public static final String HOST_ASIA_PACIFIC = "HOST_ASIA_PACIFIC";
	public static final String BASEDN_AMERICAS = "BASEDN_AMERICAS";
	public static final String BASEDN_ASIA_PACIFIC = "BASEDN_ASIA_PACIFIC";
	public static final String PRINCIPAL_AMERICAS = "PRINCIPAL_AMERICAS";
	public static final String PRINCIPAL_ASIA_PACIFIC = "PRINCIPAL_ASIA_PACIFIC";
	public static final String KEYSTORE = "KEYSTORE";
	public static final String ENABLED_SSL = "ENABLED_SSL";
	public static final String PART_NAME="PART_NAME";
}
