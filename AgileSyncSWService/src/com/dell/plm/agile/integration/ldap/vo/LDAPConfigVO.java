/*
 * LDAPConfigVO.java  
 * 
 * Version information
 * Date: 16-Sept-2011
 * 
 * Copyright : DELL confidential
 * 
 */
package com.dell.plm.agile.integration.ldap.vo;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


/**
 * Contains the parameters required for LDAP connection
 * 
 * 		-------     ----------   		 -----------------		-------------------------
 * 		Version 	  Author	 		 Last modified date			Remarks(Modification)
 * 		------      ----------			 ------------------		------------------------
 * 		1.0			Yathiraja_Lingadahal	16-Sept-2011  			Created this class.	
 * 		1.1			Yathiraja_Lingadahal	19-Sept-2011  			Added property for user accounts map.	
 */
public class LDAPConfigVO implements Serializable{
   
    /**
	 * 
	 */
	private static final long serialVersionUID = -7010424690382394063L;

	public LDAPConfigVO() {
    }

    private String host;
    private String port;
    private boolean enableSSL;
    private String principal;
    private String ldapPassword;
    private String ldapUser;
    private String keystore;
    private String baseDN;
    private String ladapContextFactory;
    private String protocol;
    private String provider;
    private String domain;
    
    private Map<String,String> userAccountsMap = null;

    public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public boolean isEnableSSL() {
        return enableSSL;
    }

    public void setEnableSSL(boolean enableSSL) {
        this.enableSSL = enableSSL;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getLdapPassword() {
        return ldapPassword;
    }

    public void setLdapPassword(String ldapPassword) {
        this.ldapPassword = ldapPassword;
    }

    public String getKeystore() {
        return keystore;
    }

    public void setKeystore(String keystore) {
        this.keystore = keystore;
    }

    public String getBaseDN() {
        return baseDN;
    }

    public void setBaseDN(String baseDN) {
        this.baseDN = baseDN;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

	public String getLdapUser() {
		return ldapUser;
	}

	public void setLdapUser(String ldapUser) {
		this.ldapUser = ldapUser;
	}

	public String getLadapContextFactory() {
		return ladapContextFactory;
	}

	public void setLadapContextFactory(String ladapContextFactory) {
		this.ladapContextFactory = ladapContextFactory;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	public Map<String, String> getUserAccountsMap() {
		return userAccountsMap;
	}

	public void setUserAccountsMap(Map<String, String> userAccountsMap) {
		this.userAccountsMap = userAccountsMap;
	}

}
