package com.dell.plm.agile.integration.util;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class Constants
{
	   public static final String RCD = "rcd";
	   public static final String RCD_REQUESTOR = "RCD";
	   public static final String FILEDOWNLOAD_ACTION = "1";
	   public static final String RCD_ACTION = "2";
	   public static final String RCD_DOWNLOAD_ACK = "3";
	   public static final String DSFORM_ACTION = "5";
	   public static final String PCI_ID_ACTION = "8";
	   public static final String CFGREADER_ACTION = "14";
	   
	   // SWB AUTO Creation
	   public static final String SWB_AUTO_GENERATION_PROP = "swb_auto_generation.properties";
	   public static final String INDEPENDENT_SWB_ATT_NAME_VAL = "INDEPENDENT_SWB_ATT_NAME_VAL";
	   public static final String INPUT_PARAM_SEPARATOR=",";
	   public static final String SWB_RELATIONSHIP_CLASS="SWB_RELATIONSHIP_CLASS";
	   public static final String SOFTWARE_BUNDLE = "SOFTWARE_BUNDLE";
	   public static final String PAGE_THREE_OWNER = "PAGE_THREE_OWNER";
	   public static final String  CONFIG_PNR_AUTOMATION="config_pnrautomation.xml";
	   public static final String PAGE_THREE_SUCCESSOR="PAGE_THREE_SUCCESSOR";
	   public static final String IS_SUCCESSOR_YES="IS_SUCCESSOR_YES";
	   public static final String IS_SUCCESSOR_NO="IS_SUCCESSOR_NO";
	   public static final String PAGE_THREE_OS_LANG="PAGE_THREE_OS_LANG";
	   public static final String YES_VALUE="Yes";
	   public static final String HARD_WARE_PART_SEPARATOR = ",";
	   public static final String MSG_NOT_VALID_AGILE_USER = "MSG_NOT_VALID_AGILE_USER";
	   public static final String MSG_MANDATORY = "MSG_MANDATORY";
	   public static final String MSG_NOT_VALID_SWB = "MSG_NOT_VALID_SWB";
	   public static final String MSG_SUCCUES_EXIST_SWB = "MSG_SUCCUES_EXIST_SWB";
	   public static final String MSG_WHR_USED_NOT_VALD_SWB = "MSG_WHR_USED_NOT_VALD_SWB";
	   public static final String TITLE_BLOCK_DESC = "TITLE_BLOCK_DESC";

	   
	   
}

