package com.dell.plm.agile.integration.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.agile.api.APIException;
import com.agile.api.AgileSessionFactory;
import com.agile.api.IAgileSession;
import com.agile.api.IUser;
import com.agile.api.UserConstants;
import com.dell.plm.agile.aic.common.cryptography.StringEncryptor;
import com.dell.plm.agile.aic.common.exception.ValidationException;
import com.dell.plm.agile.aic.common.util.FilePropertyUtility;

/**
 * @author ram_mohan_rao_kotni
 *
 */
public class Utility 
{
	private static final String CONST_RCD = "rcd";
	public static String DB_PROPERTIES = "";
	private static Logger logger = Logger.getLogger(Utility.class);


	/**
	 * 
	 */
	public void initailizeLogs()
	{
		/**Initialize the logs */
		try
		{
			String configBase = FilePropertyUtility.getConfigBasePath("synchwebservice");
			StringBuffer log4jPath = new StringBuffer(configBase);
			log4jPath.append("\\config").append("\\syncswservice_log4j.xml");
			DOMConfigurator.configure(log4jPath.toString());
		}catch(Exception ex){
			System.out.println("Unable to Configure Log4j"+ex.getMessage());
		}
	}

	/**
	 * @param dscrDownStreamNames
	 * @param downStreamName
	 * @return boolean
	 */
	public boolean isValidDownStream(String dscrDownStreamNames, String downStreamName) 
	{
		boolean isFound = false;
		String token = "";
		StringTokenizer tokenizer = new StringTokenizer(dscrDownStreamNames, ",");
		while (tokenizer.hasMoreElements()) 
		{
			token = tokenizer.nextToken(); 
			if (token.equalsIgnoreCase(downStreamName)) 
			{
				isFound = true;
			} 
		}
		return isFound;
	}

	/**
	 * @param ecryptPwd
	 * @return
	 */
	public static String getDecryptPwd(String ecryptPwd)
	{
		String decryptPwd = ""; 
		decryptPwd = StringEncryptor.decrypt(ecryptPwd);
		return decryptPwd;
	}
	
	
	public static IAgileSession createSession(String url, String username, String password) throws APIException {
		// TODO Auto-generated method stub
		IAgileSession session = null;
		
		
		AgileSessionFactory factory = AgileSessionFactory.getInstance(url);
		
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(AgileSessionFactory.USERNAME, username);
		map.put(AgileSessionFactory.PASSWORD, password);
		
		session = factory.createSession(map);
		return session;
	}
	
	public static boolean isCurrentUserValid(IAgileSession session,String owner,Properties applicationProperties)throws Exception{
		String errormessage=null;
		String userStatus = null;
		try{
			if(StringUtils.isEmpty(owner)){
				errormessage="Owner "+applicationProperties.getProperty(Constants.MSG_MANDATORY);
		     throw new Exception();
			}
			IUser user = (IUser)session.getObject(UserConstants.CLASS_USER, owner);
			if(user==null){
				errormessage=applicationProperties.getProperty(Constants.MSG_NOT_VALID_AGILE_USER);
				throw new Exception();
			} else {
				userStatus = user.getValue(UserConstants.ATT_GENERAL_INFO_STATUS).toString();
				if(!userStatus.equals(applicationProperties.getProperty("USER_ACTIVE_STATUS"))) {
					errormessage=applicationProperties.getProperty(Constants.MSG_NOT_VALID_AGILE_USER);
					throw new Exception();
				}
			}
				
		}catch(Exception exp)
		{
			logger.info("Exception : "+exception2String(exp));
			throw new ValidationException(errormessage);
		}
		return true;
	}

	
	public static String exception2String(Throwable throwable) 
	{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		throwable.printStackTrace(pw);
		pw.flush();
		pw.close();
		return sw.toString();
	}

	public static void validateMandatoryAttributes(String attributeName, String attributeValue,Properties applicationProperties) throws ValidationException {
		// TODO Auto-generated method stub
		if(attributeValue==null || attributeValue.trim().length()==0){
			String errorMessage=applicationProperties.getProperty(Constants.MSG_MANDATORY);
			logger.info("attributeName : "+ attributeName + "attributeValue: "+attributeValue);
			throw new ValidationException(attributeName+" "+errorMessage);
		}
	}
}
