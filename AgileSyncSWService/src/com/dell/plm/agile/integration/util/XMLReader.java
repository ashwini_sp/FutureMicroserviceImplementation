package com.dell.plm.agile.integration.util;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.agile.api.IAgileSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * XMLReader reads config XML file and pases into required format
 * 
 * @author Gunapati_Reddy
 * 
 */

public class XMLReader {
	// Global variables
	static org.apache.log4j.Logger logger = null;
	static IAgileSession session = null;
	HashMap hmInfo = null;

	/**
	 * Get elements based on base class, class and subclass
	 * 
	 * @param doc
	 * @param newSubClassName
	 * @param superClassName
	 * @param baseclassName
	 * @return HashMap, contains key-value pairs to do process in main
	 * @throws Exception
	 */

	public HashMap readXML(Document doc, String newSubClassName,
			String superClassName, String baseclassName) throws Exception {
		try {
			logger = Logger.getLogger(XMLReader.class);
			logger.info("newSubClassName -->>" + newSubClassName
					+ "---superClassName -->>" + superClassName
					+ "==baseclassName---" + baseclassName);
			//DOMConfigurator.configure(CreateAndSaveAsEventsConstants.LOG_4J);
			NodeList nodeLst = doc.getElementsByTagName(baseclassName);
			//Updates for SSWB project Starts. 
			
			newSubClassName=newSubClassName.replaceAll("[^a-zA-Z0-9]", "");
			
			//Updates for SSWB project Ends. 
			getAllElements(nodeLst, superClassName, newSubClassName);
			logger.info("DONE");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hmInfo;
	}

	/**
	 * get all elements by iterating DOM object
	 * 
	 * @param nodeLst
	 * @param className
	 * @param newSubClassName
	 * @throws Exception
	 */

	private void getAllElements(NodeList nodeLst, String className,
			String newSubClassName) throws Exception {
		if (hmInfo == null) {
			hmInfo = new HashMap();
		}
		
		logger.info("newSubClassName="+newSubClassName);
		
		logger.info("total length in get all elements --" + nodeLst.getLength());
		for (int s = 0; s < nodeLst.getLength(); s++) {

			Node fstNode = nodeLst.item(s);

			if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) fstNode;

				/*
				 * Iterate nodelist and process based on tag
				 */

				NodeList secNodeList = element.getChildNodes();
				for (int i = 0; i < secNodeList.getLength(); i++) {
					Node secNode = secNodeList.item(i);
					// getting info from attributes and Subclass tab
					if (secNode.getNodeType() == Node.ELEMENT_NODE) {
						Element secelement = (Element) secNode;
						logger.info("element out name --->"
								+ secelement.getNodeName());
						if (secelement.getNodeName().equals("Attributes")) {
							getAllAttributes(secelement.getChildNodes());
						} else if (secelement.getNodeName().equals("Tabs")) {
							getTabs(secelement.getChildNodes());
						} else if (secelement.getNodeName().equals("Subclass")) {
							NodeList subclassNodeList = secelement
									.getElementsByTagName(newSubClassName);
							for (int j = 0; j < subclassNodeList.getLength(); j++) {
								logger.info("getting subclass info");
								Node subClassNode = subclassNodeList.item(j);
								if (subClassNode.getNodeType() == Node.ELEMENT_NODE) {
									getSubClassInfo(subClassNode
											.getChildNodes());
								}
							}

						} else if (secelement.getNodeName().equals("Class")) {
							logger.info("getAllElements: element name --->Class");
							getAllElements(
									secelement.getElementsByTagName(className),
									className, newSubClassName);
						}
					}
				}// for loop ended

			}
		}

	}

	/**
	 * Gets page2/page3/exception attributes from given node
	 * 
	 * @param nodeLst
	 * @throws Exception
	 */

	private void getAllAttributes(NodeList nodeLst) throws Exception {
		for (int s = 0; s < nodeLst.getLength(); s++) {

			Node fstNode = nodeLst.item(s);
			if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) fstNode;
				logger.info("element.getNodeName() -->>"
						+ element.getNodeName());
				
				if (element.getNodeName().equals("Page")) {
					
					getAttributes(element.getChildNodes(), "Page");
				} 
			}
		}
	}



	

	/**
	 * Gets all attributes to be cleared or retained or defaulted for page2 or
	 * page3
	 * 
	 * @param nodeLst
	 * @param attributeNode
	 * @throws Exception
	 */

	private void getAttributes(NodeList nodeLst, String attributeNode)
			throws Exception {
		ArrayList tempList = null;
		for (int s = 0; s < nodeLst.getLength(); s++) {

			Node fstNode = nodeLst.item(s);
			tempList = null;

			if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) fstNode;
				if (element.getNodeName().equals("Clear")) {
					if (element.getChildNodes().item(0) != null) {
						if (hmInfo.containsKey(attributeNode + "_Clear")) {
							tempList = (ArrayList) hmInfo.get(attributeNode
									+ "_Clear");
							if (element.getChildNodes().item(0) != null) {
								tempList.addAll(getListFromString(
										element.getChildNodes().item(0)
										.getNodeValue(), ","));
								hmInfo.put(attributeNode + "_Clear", tempList);
							}
						} else {
							if (element.getChildNodes().item(0) != null) {
								hmInfo.put(
										attributeNode + "_Clear",
										getListFromString(element
												.getChildNodes().item(0)
												.getNodeValue(), ","));
							}
						}
					}
				} else if (element.getNodeName().equals("Default")) {
					logger.info("Default Attributes -" + element.getNodeValue());
					if (element.getChildNodes().item(0) != null) {
						if (hmInfo.containsKey(attributeNode + "_Default")) {
							tempList = (ArrayList) hmInfo.get(attributeNode
									+ "_Default");
							if (element.getChildNodes().item(0) != null) {
								tempList.addAll(getListFromString(
										element.getChildNodes().item(0)
										.getNodeValue(), ","));
								hmInfo.put(attributeNode + "_Default", tempList);
							}
						} else {
							if (element.getChildNodes().item(0) != null) {
								hmInfo.put(
										attributeNode + "_Default",
										getListFromString(element
												.getChildNodes().item(0)
												.getNodeValue(), ","));
							}
						}
					}

					logger.info("default attributes");

				} else if (element.getNodeName().equals("Set")) {
					logger.info("Set Attributes -" + element.getNodeValue());
					if (element.getChildNodes().item(0) != null) {
						if (hmInfo.containsKey(attributeNode + "_Set")) {
							tempList = (ArrayList) hmInfo.get(attributeNode
									+ "_Set");
							if (element.getChildNodes().item(0) != null) {
								tempList.addAll(getListFromString(
										element.getChildNodes().item(0)
										.getNodeValue(), ","));
								hmInfo.put(attributeNode + "_Set", tempList);
							}
						} else {
							if (element.getChildNodes().item(0) != null) {
								hmInfo.put(
										attributeNode + "_Set",
										getListFromString(element
												.getChildNodes().item(0)
												.getNodeValue(), ","));
							}
						}
					}

					logger.info("default attributes");

				} else if (element.getNodeName().equals("Retain")) {
					logger.info("Retain Attributes-" + element.getNodeValue());
					if (element.getChildNodes().item(0) != null) {
						if (hmInfo.containsKey(attributeNode + "_Retain")) {
							tempList = (ArrayList) hmInfo.get(attributeNode
									+ "_Retain");
							if (element.getChildNodes().item(0) != null) {
								tempList.addAll(getListFromString(
										element.getChildNodes().item(0)
										.getNodeValue(), ","));
								hmInfo.put(attributeNode + "_Retain", tempList);
							}
						} else {
							if (element.getChildNodes().item(0) != null) {
								hmInfo.put(
										attributeNode + "_Retain",
										getListFromString(element
												.getChildNodes().item(0)
												.getNodeValue(), ","));
							}
						}
					}
				}
				
			}
		}
	}

	/**
	 * Gets tags for subclass
	 * 
	 * @param nodeLst
	 * @throws Exception
	 */

	private void getSubClassInfo(NodeList nodeLst) throws Exception {
		logger.info("total length in getSubClassInfo info--"
				+ nodeLst.getLength());
		for (int s = 0; s < nodeLst.getLength(); s++) {

			Node fstNode = nodeLst.item(s);
			if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) fstNode;
				if (element.getNodeName().equals("Attributes")) {
					getAllAttributes(element.getChildNodes());
				} else if (element.getNodeName().equals("Tabs")) {
					getTabs(element.getChildNodes());
				}
			}

		}
	}

	/**
	 * Gets tab to be processed
	 * 
	 * @param nodeLst
	 */
	private void getTabs(NodeList nodeLst) {
		if (nodeLst != null)
			for (int s = 0; s < nodeLst.getLength(); s++) {

				Node fstNode = nodeLst.item(s);

				if (fstNode.getNodeType() == Node.ELEMENT_NODE) {

					Element element = (Element) fstNode;
					logger.info("element.getNodeName() in getTabs-->>"
							+ element.getNodeName());
					if (element.getNodeName().startsWith("Tab_")) {
						if (element.getChildNodes().item(0) != null)
							hmInfo.put(element.getNodeName(), (String) (element
									.getChildNodes().item(0).getNodeValue()));

					} else if (element.getNodeName().equals("Exception")) {
						getTabs(element.getChildNodes());
					}
				}

			}
	}

	/**
	 * Gets tokenized stings from a string
	 * 
	 * @param propertyValue
	 * @param separator
	 * @return
	 * @throws Exception
	 */
	private static ArrayList getListFromString(String propertyValue,
			String separator) throws Exception {
		ArrayList<String> list = new ArrayList();
		if (propertyValue != null) {
			StringTokenizer st = new StringTokenizer(propertyValue, separator);
			while (st.hasMoreTokens()) {
				list.add(st.nextToken());
			}

		}
		return list;
	}

}