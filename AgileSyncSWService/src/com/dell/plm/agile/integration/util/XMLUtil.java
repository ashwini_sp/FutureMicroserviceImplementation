/*
 * XMLUtil.java  
 * 
 * Version information
 * Date: 05-May-2011		
 * 
 * Copyright : DELL confidential
 * 
 */
package com.dell.plm.agile.integration.util;

import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
/**
 * Utility class for the XML .
 * 
 * 
 * 		-------     ----------   			-----------------		-------------------------
 * 		Version 	  Author	 			Last modified date			Remarks(Modification)
 * 		------      ----------				------------------		------------------------
 * 		1.0			Yathiraja_Lingadahal	  05-May-2011				Created this class.	
 *
 */
public class XMLUtil{
	
	//Logger instance of the current class.
	 private static final Logger logger = Logger.getLogger(XMLUtil.class);
	 /**
		 * Gets XML Data as string from document.
		 * @param doc
		 * @return
		 */
		public static final String  getXMLString(Document doc)
		{
			logger.info("Inside getXMLString");
			logger.info("Document created is :::"+doc);
			doc.setXmlStandalone(true);
			DOMSource domSource = new DOMSource(doc);
			Transformer transformer=null;
			String xmlDataString =  null;
			try {
				transformer = TransformerFactory.newInstance().newTransformer();
				transformer.setOutputProperty(OutputKeys.METHOD, "xml");
				transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION ,"no");
				StringWriter sw = new StringWriter();
				StreamResult sr = new StreamResult(sw);
				transformer.transform(domSource, sr);
				xmlDataString = sw.toString();
				logger.info(xmlDataString);
			} catch (TransformerConfigurationException e1) {
				logger.error("TransformerConfigurationException inside getXMLString()");
				// TODO Auto-generated catch block

			} catch (TransformerFactoryConfigurationError e1) {
				// TODO Auto-generated catch block
				logger.error("TransformerFactoryConfigurationError insidegetXMLString()");
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				logger.error("TransformerException inside getXMLString()");
			} 
			return xmlDataString;

		}

 }
