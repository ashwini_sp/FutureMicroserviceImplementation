package com.dell.plm.agile.integration.xml;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.dell.plm.agile.integration.util.XMLUtil;

public class DpkPartXMLGenerator {

	private static final Logger logger = Logger.getLogger(DpkPartXMLGenerator.class);
	
	/**
	 * This Method Creates The XML Document Based on Input File Type Code to OS Map
	 * @param partDetails
	 * @return String
	 * @throws Exception
	 */
	public static String createDocument(Map<String,String> partDetails) throws Exception 
	{
		logger.info("Starts: creating XML for dpk:DPK");
		String strXML = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document  dom = db.newDocument();
			createDOMTree(dom, partDetails);
			strXML = XMLUtil.getXMLString(dom);
			logger.debug("generated XML is: "+strXML);
		}catch(ParserConfigurationException pce) {
			logger.error("Error While Trying To Instantiate DocumentBuilder", pce);
			throw pce;
		}
		logger.info("Ends: creating XML");
		return strXML;
	}
	
	/**
	 * Creates The DOM Tree Structure Based On Input Document and Map Object 
	 * @param dom
	 * @param partDetails
	 * @throws Exception
	 */
	private static void createDOMTree(org.w3c.dom.Document dom, Map<String, String> partDetails) throws Exception
	{
		logger.info("Start: creating DOM Tree");
		try{
			/** Create The Root Element */
			org.w3c.dom.Element rootEle = dom.createElement("PartList");
			//rootEle.setAttribute("xsi:schemaLocation", "http://dell.com/OsFiltering.xsd OsFiltering.xsd");
			//rootEle.setAttribute("xmlns:osf", "http://dell.com/OsFiltering.xsd");
			rootEle.setAttribute("xmlns", "http://agile.us.dell.com/db/partinfo");
			dom.appendChild(rootEle);
			appendPartDetailsToRoot(dom,rootEle,partDetails);
		}catch(Exception e){
			logger.error("Exception while creating the DOM Tree: "+e);
			throw e;
		}
		logger.info("End: creating DOM Tree");
	}
	
	
	/**
	 * Create the part attribute tags under root element 
	 * @param dom
	 * @param rootEle
	 * @param partDetails
	 * @throws Exception
	 */
	private static void appendPartDetailsToRoot(org.w3c.dom.Document dom, org.w3c.dom.Element rootEle, Map<String, String> partDetails) throws Exception
	{
		logger.info("Start: The appendOsFilteringChildrenToRoot method");
		try{
			if(partDetails!=null && partDetails.size() > 0){
				Set<String> partAttributes = partDetails.keySet();
				Iterator<String> it = partAttributes.iterator();
				Element key = dom.createElement("Row");
				while(it.hasNext()){
					String partAttribute = it.next();
					//key.setAttribute(partAttribute, partAttribute);
					String attribute = partDetails.get(partAttribute);
					//org.w3c.dom.Text textoS60Codes = dom.createTextNode(attribute);
					//key.appendChild(textoS60Codes);
					//createOsFileTypeTags(dom,rootEle,partAttribute,attribute);
					Element att=dom.createElement("Column");
					att.setAttribute("name", partAttribute);									
			        org.w3c.dom.Text textcode = dom.createTextNode(attribute);	
			        att.appendChild(textcode);										
					key.appendChild(att);
										
				}
				rootEle.appendChild(key);
			}
		}
		catch(Exception e){
			logger.error("Exception while fetching and appending part details",e);
			throw e;
		}
		logger.info("End: The appendPartDetailsToRoot method");
	}
	
	
	


}
