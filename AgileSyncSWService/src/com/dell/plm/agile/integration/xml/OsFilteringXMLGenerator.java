package com.dell.plm.agile.integration.xml;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import com.dell.plm.agile.integration.util.XMLUtil;


/**
 * 
 * @author Vumesh_Gundapuneni
 *
 */
public class OsFilteringXMLGenerator{
	private static final Logger logger = Logger.getLogger(OsFilteringXMLGenerator.class);
	
	/**
	 * This Method Creates The XML Document Based on Input File Type Code to OS Map
	 * @param fileTypeOSCodeMap
	 * @return String
	 * @throws Exception
	 */
	public static String createDocument(Map<String,String> fileTypeOSCodeMap) throws Exception 
	{
		logger.info("Starts: creating XML for osf:osFileTypeMapping");
		String strXML = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document  dom = db.newDocument();
			createDOMTree(dom, fileTypeOSCodeMap);
			strXML = XMLUtil.getXMLString(dom);
			logger.debug("osf:osFileTypeMapping generated XML is: "+strXML);
		}catch(ParserConfigurationException pce) {
			logger.error("Error While Trying To Instantiate DocumentBuilder", pce);
			throw pce;
		}
		logger.info("Ends: creating XML osf:osFileTypeMapping");
		return strXML;
	}
	
	/**
	 * Creates The DOM Tree Structure Based On Input Document and Map Object 
	 * @param dom
	 * @param fileTypeOSCodeMap
	 * @throws Exception
	 */
	private static void createDOMTree(org.w3c.dom.Document dom, Map<String, String> fileTypeOSCodeMap) throws Exception
	{
		logger.info("Start: creating DOM Tree");
		try{
			/** Create The Root Element */
			org.w3c.dom.Element rootEle = dom.createElement("osf:osFileTypeMapping");
			rootEle.setAttribute("xsi:schemaLocation", "http://dell.com/OsFiltering.xsd OsFiltering.xsd");
			rootEle.setAttribute("xmlns:osf", "http://dell.com/OsFiltering.xsd");
			rootEle.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			dom.appendChild(rootEle);
			appendOsFilteringChildrenToRoot(dom,rootEle,fileTypeOSCodeMap);
		}catch(Exception e){
			logger.error("Exception while creating the DOM Tree: "+e);
			throw e;
		}
		logger.info("End: creating DOM Tree");
	}
	
	
	/**
	 * Create the file type mapping tags under root element 
	 * @param dom
	 * @param rootEle
	 * @param fileTypeOSCodeMap
	 * @throws Exception
	 */
	private static void appendOsFilteringChildrenToRoot(org.w3c.dom.Document dom, org.w3c.dom.Element rootEle, Map<String, String> fileTypeOSCodeMap) throws Exception
	{
		logger.info("Start: The appendOsFilteringChildrenToRoot method");
		try{
			if(fileTypeOSCodeMap!=null && fileTypeOSCodeMap.size() > 0){
				Set<String> fileTypeCodes = fileTypeOSCodeMap.keySet();
				Iterator<String> it = fileTypeCodes.iterator();
				while(it.hasNext()){
					String fileTypeCode = it.next();
					String os60MapCodes = fileTypeOSCodeMap.get(fileTypeCode);
					createOsFileTypeTags(dom,rootEle,fileTypeCode,os60MapCodes);
				}
			}
		}
		catch(Exception e){
			logger.error("Exception while creating and appending File Type Code and OS Code element:",e);
			throw e;
		}
		logger.info("End: The appendPrereqCoreqChildrenToRoot method");
	}
	
	
	/**
	 * This Method Create the osf:fileType Tags Under Root Element 
	 * @param dom
	 * @param elRoot
	 * @param fileTypeCode
	 * @param os60MapCodes
	 * @throws Exception
	 */
	private static org.w3c.dom.Element createOsFileTypeTags(org.w3c.dom.Document dom, org.w3c.dom.Element elRoot, String fileTypeCode, String os60MapCodes) throws Exception
	{
		logger.info("Start: Creating osf:fileType Tag In createOsFileTypeTags For Each File Type Code to OS 60 Codes Mapping");
		
		try{
			Element fType = dom.createElement("osf:fileType");
			fType.setAttribute("code", fileTypeCode);
			String s[] = fileTypeCode.split(":");
			fType.setAttribute("prefixCode", s[0]);
			
			
			Element oS60Codes = dom.createElement("osf:oS60Code");
			org.w3c.dom.Text textoS60Codes = dom.createTextNode(os60MapCodes);
			oS60Codes.appendChild(textoS60Codes);
			fType.appendChild(oS60Codes);
			
			elRoot.appendChild(fType);
			logger.info("File Type Code #"+ fileTypeCode+"\t"+"OS 60 Codes #"+os60MapCodes);	
		}catch(Exception e){
			logger.error("Exception in createOsFileTypeTags Method.", e);
			throw e;
		}
		logger.info("End: Creating osf:fileType Tag In createOsFileTypeTags For Each File Type Code to OS 60 Codes Mapping");
		return elRoot;
	}
}
