/*
 * OsLangXMLGenerator.java  
 * 
 * Version information
 * Date: 05-May-2011		
 * 
 * Copyright : DELL confidential
 * 
 */
package com.dell.plm.agile.integration.xml;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.dell.plm.agile.integration.dto.oslang.PartOSLang;
import com.dell.plm.agile.integration.util.XMLUtil;

/**
 * Generates the xml of the  osl:OS-LANG.
 * 
 * 
 * 		-------     ----------   			-----------------		-------------------------
 * 		Version 	  Author	 			Last modified date			Remarks(Modification)
 * 		------      ----------				------------------		------------------------
 * 		1.0			Yathiraja_Lingadahal	  05-May-2011				Created this class.	
 * 		1.1			Yathiraja_Lingadahal	  10-Aug-2011				Changed for XML structure with different languages for each OS.	
 *
 */
public class OsLangXMLGenerator{
	private static final Logger logger = Logger.getLogger(OsLangXMLGenerator.class);
	/**
	 * Using JAXP in implementation independent manner create a document object
	 * using which we create a xml tree in memory
	 */
	public static String createDocument(String part_number,List<PartOSLang> oslist,String fileName,String IsOSLangApplied,String IsOSLangUniform)throws Exception {

		//get an instance of factory
		logger.info("Starts: creating XML osl:OS-LANG");

		String strXML = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			//get an instance of builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//create an instance of DOM
			org.w3c.dom.Document  dom = db.newDocument();
			Map osLangsMap = getOSLangsMap(oslist);
			createDOMTree(part_number,dom,osLangsMap,IsOSLangApplied,IsOSLangUniform);
			//createDOMTree(part_number,dom,oslist,IsOSLangApplied,IsOSLangUniform);
			strXML = XMLUtil.getXMLString(dom);
			// osFilePath = printToFile(dom,fileName);
			logger.debug("osl:OS-LANG XML generated is ----"+strXML);
		}catch(ParserConfigurationException pce) {
			//dump it
			logger.error("Error while trying to instantiate DocumentBuilder",pce);
			throw pce;
		}
		logger.info("Ends: creating XML osl:OS-LANG");
		return strXML;
	}
	
	private static Map<String,Set> getOSLangsMap(final List<PartOSLang> oslist)
	{
		Map<String, Set> osLangsMap = null;
		if(null != oslist)
		{
			osLangsMap = new HashMap<String,Set>();
			Iterator <PartOSLang>iterList = oslist.iterator();
			while(iterList.hasNext())
			{
				PartOSLang partOsLang = iterList.next();
				
				String os = partOsLang.getOs();
				String array[] = partOsLang.getLanguagArray();
				Set<String> setLangs = (Set<String>)osLangsMap.get(os);
				if(null != setLangs)
				{
					if(array.length>0)
					{
						for(String lang:array)
						{
							setLangs.add(lang);
						}
					}
				}
				else
				{
					setLangs = new TreeSet<String>();
					if(array.length>0)
					{
						for(String lang:array)
						{
							setLangs.add(lang);
						}
					}
					
				}
				
				osLangsMap.put(os, setLangs);
				
			}
		}

		return osLangsMap;
	}
	
	/**
	 *  Creates the DOM tree structure 
	 * @param part_number
	 * @param dom
	 * @param myData
	 * @param IsOSLangApplied
	 * @param IsOSLangUniform
	 * @throws Exception
	 */
	private static void createDOMTree(String part_number,org.w3c.dom.Document dom,Map<String,Set> myDataMap,String IsOSLangApplied,String IsOSLangUniform)throws Exception{

		//create the root element 
		logger.info("Starts: creating DOM Tree");
		try{
			org.w3c.dom.Element rootEle = dom.createElement("osl:OS-LANG");
			rootEle.setAttribute("xsi:schemaLocation", "http://dell.com/OSLANG.xsd OSLang.xsd");
			rootEle.setAttribute("xmlns:osl", "http://dell.com/OSLANG.xsd");
			rootEle.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootEle.setAttribute("part_number", part_number);
			logger.debug("IsOSLangUniform----"+IsOSLangUniform);
			logger.debug("IsOSLangApplied----"+IsOSLangApplied);
			if(IsOSLangApplied!=null && IsOSLangApplied.equalsIgnoreCase("on")){
				rootEle.setAttribute("has-os-lang-matrix", "true");
			}else if(IsOSLangApplied==null){
				//Do not set this attribute
			}
			else{
				rootEle.setAttribute("has-os-lang-matrix", "false");
			}
			if(IsOSLangUniform!=null && IsOSLangUniform.equalsIgnoreCase("on")){
				rootEle.setAttribute("os-lang-uniformly-applied", "true");
			}
			else if(IsOSLangUniform==null){
				//Do not set this attribute
			}
			else{
				rootEle.setAttribute("os-lang-uniformly-applied", "false");
			}
			dom.appendChild(rootEle);
			appendOSLangChildrenToRoot(dom,myDataMap,rootEle);
			//appendOSLangChildrenToRoot(dom,myData,rootEle);
		}catch(Exception e){
			logger.error("Exception while creating the DOM Tree----"+e);
			throw e;
		}

		logger.info("Ends: creating DOM Tree");

	}
	
	/**
	 * Appends the OS and lang nodes to the root element.
	 * @param dom
	 * @param myData
	 * @param rootEle
	 * @throws Exception
	 */
	private static void appendOSLangChildrenToRoot(org.w3c.dom.Document dom,Map<String,Set> myDataMap,org.w3c.dom.Element rootEle)throws Exception{

		//create the root element 
		try{
			//No enhanced for
			logger.info("Starts:Iterating list of PartOSLang");
			if(myDataMap!=null){
				Set<String> osItr = myDataMap.keySet();
				Iterator<String> oslangItr = osItr.iterator();
				while (oslangItr.hasNext()) {
					String osName = oslangItr.next();
					Set langsSet = myDataMap.get(osName);
					//String [] osLangArr = partOs.getLanguagArray();
					//String osName =  partOs.getOs();
					createOSTags(dom,rootEle,osName,langsSet);
				}
			}
		}catch(Exception e){
			logger.error("Exception while creating and appending OS and lang elements----",e);
			throw e;
		}
		
		logger.info("Ends:Iterating list of PartOSLang");

	}
	/**
	 * Helper method which creates OS and lanuages elements. 

	 * @param dom
	 * @param elRoot
	 * @param OSname
	 * @param lang
	 * @return
	 * @throws Exception
	 */
	private static org.w3c.dom.Element createOSTags(org.w3c.dom.Document dom,org.w3c.dom.Element elRoot,String OSname,String[] lang)throws Exception{
		logger.info("Starts:creating OSTags for each list item");
		try{
			//create author element and author text node and attach it to bookElement
			Element osEle = dom.createElement("osl:os");
			osEle.setAttribute("os-name", OSname);
			Element langsEle = dom.createElement("osl:languages");
			List<String> list = java.util.Arrays.asList(lang);
			Iterator<String> iterator = list.iterator();
			while (iterator.hasNext()) {
				org.w3c.dom.Element elLang = dom.createElement("osl:language");
				org.w3c.dom.Text textLang = dom.createTextNode((String) iterator.next()); 
				elLang.appendChild(textLang);
				langsEle.appendChild(elLang);
			}
			osEle.appendChild(langsEle);
			elRoot.appendChild(osEle);

			//create title element and title text node and attach it to bookElement
		}catch(Exception e){
			logger.error("Exception while creating and exception.,...----",e);
			//throw e;
		}
		logger.info("Ends:creating OSTags for each list item");
		return elRoot;

	}

	/**
	 * Helper method which creates OS and lanuages elements. 

	 * @param dom
	 * @param elRoot
	 * @param OSname
	 * @param lang
	 * @return
	 * @throws Exception
	 */
	private static org.w3c.dom.Element createOSTags(org.w3c.dom.Document dom,org.w3c.dom.Element elRoot,final String OSname,final Set<String> langs)throws Exception{
		logger.info("Starts:creating OSTags for each list item");
		try{
			//create author element and author text node and attach it to bookElement
			Element osEle = dom.createElement("osl:os");
			osEle.setAttribute("os-name", OSname);
			Element langsEle = dom.createElement("osl:languages");
			//List<String> list = java.util.Arrays.asList(lang);
			Iterator<String> iterator = langs.iterator();
			while (iterator.hasNext()) {
				org.w3c.dom.Element elLang = dom.createElement("osl:language");
				org.w3c.dom.Text textLang = dom.createTextNode((String) iterator.next()); 
				elLang.appendChild(textLang);
				langsEle.appendChild(elLang);
			}
			osEle.appendChild(langsEle);
			elRoot.appendChild(osEle);

			//create title element and title text node and attach it to bookElement
		}catch(Exception e){
			logger.error("Exception while create os tags 099807987908---",e);
			//throw e;
		}
		logger.info("Ends:creating OSTags for each list item");
		return elRoot;

	}


}