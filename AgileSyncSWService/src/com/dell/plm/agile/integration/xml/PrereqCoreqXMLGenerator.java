/*
 * PrereqCoreqXMLGenerator.java  
 * 
 * Version information
 * Date: 31st October 2013		
 * 
 * Copyright : DELL confidential
 * 
 */
package com.dell.plm.agile.integration.xml;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import com.dell.plm.agile.integration.dto.prereqcoreq.PrereqCoreqDtls;
import com.dell.plm.agile.integration.util.XMLUtil;


public class PrereqCoreqXMLGenerator{
	private static final Logger logger = Logger.getLogger(PrereqCoreqXMLGenerator.class);
	
	public static String createDocument(String swb_number, Map<String, List<PrereqCoreqDtls>> preCoreqMap, String strPrereqOrderOfInstall, String strCoreqOrderOfInstall) throws Exception 
	{
		logger.info("Starts: creating XML for pcd:swbList.");
		String strXML = null;
		
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document  dom = db.newDocument();
			createDOMTree(swb_number, dom, preCoreqMap, strPrereqOrderOfInstall, strCoreqOrderOfInstall);
			strXML = XMLUtil.getXMLString(dom);
			logger.debug("pcd:swbList generated XML is: "+strXML);
		}catch(ParserConfigurationException pce) {
			logger.error("Error While Trying To Instantiate DocumentBuilder", pce);
			throw pce;
		}
		logger.info("Ends: creating XML pcd:swbList");
		return strXML;
	}
	
	/**
	 * Creates the DOM tree structure 
	 * @param swb_number
	 * @param dom
	 * @param preCoreqMap
	 * @param strPrereqOrderOfInstall
	 * @param strCoreqOrderOfInstall
	 * @throws Exception
	 */
	private static void createDOMTree(String swb_number, org.w3c.dom.Document dom, Map<String, List<PrereqCoreqDtls>> preCoreqMap, String strPrereqOrderOfInstall, String strCoreqOrderOfInstall) throws Exception
	{
		logger.info("Starts: creating DOM Tree");
		try{
			/* create the root element */
			org.w3c.dom.Element rootEle = dom.createElement("pcd:swbList");
			rootEle.setAttribute("xsi:schemaLocation", "http://dell.com/PrereqCoreq.xsd PrereqCoreq.xsd");
			rootEle.setAttribute("xmlns:pcd", "http://dell.com/PrereqCoreq.xsd");
			rootEle.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootEle.setAttribute("part_number", swb_number);
			dom.appendChild(rootEle);
			createCommonTags(dom, rootEle, strPrereqOrderOfInstall, strCoreqOrderOfInstall);
			appendPrereqCoreqChildrenToRoot(dom, rootEle, preCoreqMap);
		}catch(Exception e){
			logger.error("Exception while creating the DOM Tree: "+e);
			throw e;
		}
		logger.info("Ends: creating DOM Tree");
	}
	
	/**
	 *  Creates the common tags under root element 
	 * @param dom
	 * @param elRoot
	 * @param strPrereqOrderOfInstall
	 * @param strCoreqOrderOfInstall
	 * @throws Exception
	 */
	private static org.w3c.dom.Element createCommonTags(org.w3c.dom.Document dom, org.w3c.dom.Element elRoot, String strPrereqOrderOfInstall, String strCoreqOrderOfInstall) throws Exception
	{
		logger.info("The createCommonTags Method is Starting.");
		try
		{
			Element prereqEle = dom.createElement("pcd:preReqSwbNbrs");
			if(strPrereqOrderOfInstall!=null && strPrereqOrderOfInstall.length()>0)
			{
				org.w3c.dom.Text textPrereqEle = dom.createTextNode(strPrereqOrderOfInstall);
				prereqEle.appendChild(textPrereqEle);
			}else{
				org.w3c.dom.Text textPrereqEle = dom.createTextNode("");
				prereqEle.appendChild(textPrereqEle);
			}
			elRoot.appendChild(prereqEle);
			logger.info("Added pcd:preReqSwbNbrs to the root element.");
			
			Element coreqEle = dom.createElement("pcd:coReqSwbNbrs");
			if(strCoreqOrderOfInstall!=null && strCoreqOrderOfInstall.length() >0)
			{
				org.w3c.dom.Text textCoreqEle = dom.createTextNode(strCoreqOrderOfInstall);
				coreqEle.appendChild(textCoreqEle);
			}else{
				org.w3c.dom.Text textCoreqEle = dom.createTextNode("");
				coreqEle.appendChild(textCoreqEle);
			}
			elRoot.appendChild(coreqEle);
			logger.info("Added pcd:coReqSwbNbrs to the root element.");
		}
		catch(Exception e)
		{
			logger.error("Exception while createCommonTags Method is: "+e.getMessage());
		}
		logger.info("The createCommonTags Method is Ending.");
		return elRoot;
	}
	
	
	/**
	 * Creates the swb tags under root element 
	 * @param dom
	 * @param rootEle
	 * @param preCoreqMap
	 * @throws Exception
	 */
	private static void appendPrereqCoreqChildrenToRoot(org.w3c.dom.Document dom, org.w3c.dom.Element rootEle, Map<String, List<PrereqCoreqDtls>> preCoreqMap) throws Exception
	{
		logger.info("Starts: The appendPrereqCoreqChildrenToRoot method");
		try{
			if(preCoreqMap!=null) {
				Set<String> pcItr = preCoreqMap.keySet();
				if(pcItr.contains("P"))
				{
					List<PrereqCoreqDtls> prereqList = (List<PrereqCoreqDtls>)preCoreqMap.get("P");
					if(prereqList.size() > 0){
						Iterator<PrereqCoreqDtls> prereqItr = prereqList.iterator();
						while (prereqItr.hasNext()){
							PrereqCoreqDtls prereqDtls = (PrereqCoreqDtls) prereqItr.next();
							createSWBTags(dom, rootEle, prereqDtls);
						}
					}
				}
				
				if(pcItr.contains("C"))
				{
					List<PrereqCoreqDtls> coreqList = (List<PrereqCoreqDtls>)preCoreqMap.get("C");
					if(coreqList.size() > 0) {
						Iterator<PrereqCoreqDtls> coreqItr = coreqList.iterator();
						while (coreqItr.hasNext()){
							PrereqCoreqDtls coreqDtls = (PrereqCoreqDtls)coreqItr.next();
							createSWBTags(dom, rootEle, coreqDtls);
						}
					}
				}
			}
		}
		catch(Exception e){
			logger.error("Exception while creating and appending OS and lang elements----",e);
			throw e;
		}
		logger.info("Ends: The appendPrereqCoreqChildrenToRoot method");
	}
	
	
	/**
	 * Creates the swb tags under root element 
	 * @param dom
	 * @param elRoot
	 * @param prereqDtls
	 * @throws Exception
	 */
	private static org.w3c.dom.Element createSWBTags(org.w3c.dom.Document dom, org.w3c.dom.Element elRoot, PrereqCoreqDtls prereqDtls) throws Exception
	{
		logger.info("Starts:creating pcd:swb tag in createSWBTags for each prereq or coreq records");
		try{
			Element swb = dom.createElement("pcd:swb");
			swb.setAttribute("part_number", prereqDtls.getPrereqCoreqNbr());
			
			Element prereqCoreqFlag = dom.createElement("pcd:isSWBPrereq");
			org.w3c.dom.Text textPrereqCoreqFlag = null;
			if(prereqDtls.getPrereqCoreqFlag().equalsIgnoreCase("P")){
				textPrereqCoreqFlag = dom.createTextNode("Yes");
			}else{
				textPrereqCoreqFlag = dom.createTextNode("No");
			}
			prereqCoreqFlag.appendChild(textPrereqCoreqFlag);
			swb.appendChild(prereqCoreqFlag);
			
			Element specificVerFlag = dom.createElement("pcd:specificVer");
			Element specificOrLaterVerFlag = dom.createElement("pcd:specificOrLaterVer");
			org.w3c.dom.Text textSpecificVer = null;
			org.w3c.dom.Text textSpecificOrLater = null;
			if(prereqDtls.getSpecificOrLaterVer().equalsIgnoreCase("S")){
				textSpecificVer = dom.createTextNode("Yes");
				textSpecificOrLater = dom.createTextNode("No");
			}else if(prereqDtls.getSpecificOrLaterVer().equalsIgnoreCase("L")){
				textSpecificVer = dom.createTextNode("No");
				textSpecificOrLater = dom.createTextNode("Yes");
			}
			specificVerFlag.appendChild(textSpecificVer);
			swb.appendChild(specificVerFlag);
			
			specificOrLaterVerFlag.appendChild(textSpecificOrLater);
			swb.appendChild(specificOrLaterVerFlag);
			
						
			Element orderOfInstall = dom.createElement("pcd:OrderOfInstall");
			org.w3c.dom.Text textOrderOfInstall = dom.createTextNode(prereqDtls.getOrderOfInstallation().toString());
			orderOfInstall.appendChild(textOrderOfInstall);
			swb.appendChild(orderOfInstall);
			
						
			Element projPlatformsAffPHNbr = dom.createElement("pcd:projPlatformsAffPHNbr");
			org.w3c.dom.Text textProjPlatformsAffPHNbr = dom.createTextNode(prereqDtls.getPlatformPhNbr());
			projPlatformsAffPHNbr.appendChild(textProjPlatformsAffPHNbr);
			swb.appendChild(projPlatformsAffPHNbr);
			
			Element oses = dom.createElement("pcd:OSES");
			if(prereqDtls.getOsIds()!=null)
			{
				org.w3c.dom.Text textOses = dom.createTextNode(prereqDtls.getOsIds());
				oses.appendChild(textOses);
			}else{
				org.w3c.dom.Text textOses = dom.createTextNode("");
				oses.appendChild(textOses);
			}
			swb.appendChild(oses);
			
			
			Element Languages = dom.createElement("pcd:Languages");
			if(prereqDtls.getLangIds()!=null)
			{
				org.w3c.dom.Text textLanguages = dom.createTextNode(prereqDtls.getLangIds());
				Languages.appendChild(textLanguages);
			}else{
				org.w3c.dom.Text textLanguages = dom.createTextNode("");
				Languages.appendChild(textLanguages);
			}
			swb.appendChild(Languages);
			
			Element lossOfFuncRsnTxt = dom.createElement("pcd:lossOfFuncRsnTxt");
			org.w3c.dom.Text textLossOfFuncRsnTxt = dom.createTextNode(prereqDtls.getLossOfFuncRsnTxt());
			lossOfFuncRsnTxt.appendChild(textLossOfFuncRsnTxt);
			swb.appendChild(lossOfFuncRsnTxt);

			elRoot.appendChild(swb);
		}catch(Exception e){
			logger.error("Exception in createSWBTags Method.", e);
		}
		logger.info("Ends:creating pcd:swb tag in createSWBTags for each prereq or coreq records");
		return elRoot;
	}
}