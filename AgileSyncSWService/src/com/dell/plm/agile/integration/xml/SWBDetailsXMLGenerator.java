package com.dell.plm.agile.integration.xml;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import com.dell.plm.agile.integration.util.XMLUtil;
import com.dell.plm.agile.integration.dto.oslang.PartOSLang;
import com.dell.plm.agile.integration.dto.predecessorsuccessor.SWBPredecessorSuccessor;
import com.dell.plm.agile.integration.dto.prereqcoreq.PrereqCoreqDtls;

/**
 * This class generates the XML for SWB Part details.   
 *
 */
public class SWBDetailsXMLGenerator
{
	private static final Logger logger = Logger.getLogger(SWBDetailsXMLGenerator.class);

	/**
	 * This method uses JAXP in implementation independent manner to create a document object
	 * using which we create an XML tree in memory
	 * @param partNumber
	 * @param oslist
	 * @param IsOSLangApplied
	 * @param IsOSLangUniform
	 * @param predlist
	 * @param succlist
	 * @return String
	 * @throws Exception
	 */

	public static String createDocument(String partNumber, List<PartOSLang> oslist, String IsOSLangApplied, String IsOSLangUniform, List<SWBPredecessorSuccessor> predlist, List<SWBPredecessorSuccessor> succlist, Map<String, List<PrereqCoreqDtls>> preCoreqMap, String strPrereqOrderOfInstall, String strCoreqOrderOfInstall) throws Exception 
	{
		logger.info("Starts: creating XML Part details---");
		String strXML = null;
		try 
		{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document  dom = db.newDocument();

			Map<String, Set<String>> osLangsMap = getOSLangsMap(oslist);
			String swbPredecessorList = getSWBMap(predlist);
			String swbSuccessorList = getSWBMap(succlist);

			createDOMTree(partNumber, dom, osLangsMap, IsOSLangApplied, IsOSLangUniform, swbPredecessorList, swbSuccessorList, preCoreqMap, strPrereqOrderOfInstall, strCoreqOrderOfInstall);
			strXML = XMLUtil.getXMLString(dom);
			logger.debug("Part Details XML generated is--- "+strXML);
		}
		catch(ParserConfigurationException pce) 
		{
			logger.error("Error while trying to instantiate DocumentBuilder",pce);
			throw pce;
		}
		logger.info("Ends: creating XML Part Details---");
		return strXML;
	}

	/**
	 * This method returns the OS Language map.
	 * @param oslist
	 * @return
	 */
	private static Map<String, Set<String>> getOSLangsMap(final List<PartOSLang> oslist)
	{
		Map<String, Set<String>> osLangsMap = null;
		if(null != oslist)
		{
			osLangsMap = new HashMap<String,Set<String>>();
			Iterator <PartOSLang>iterList = oslist.iterator();
			while(iterList.hasNext())
			{
				PartOSLang partOsLang = iterList.next();

				String os = partOsLang.getOs();
				String array[] = partOsLang.getLanguagArray();
				Set<String> setLangs = (Set<String>)osLangsMap.get(os);
				if(null != setLangs)
				{
					if(array.length>0)
					{
						for(String lang:array)
						{
							setLangs.add(lang);
						}
					}
				}
				else
				{
					setLangs = new TreeSet<String>();
					if(array.length>0)
					{
						for(String lang:array)
						{
							setLangs.add(lang);
						}
					}
				}
				osLangsMap.put(os, setLangs);
			}
		}
		return osLangsMap;
	}

	/**
	 * This method returns the Software Bundle Predecessor/ Successor list.
	 * @param swblist
	 * @return
	 */
	private static String getSWBMap(final List<SWBPredecessorSuccessor> swblist)
	{
		StringBuffer swbList = new StringBuffer(""); 
		if(null != swblist)
		{
			Iterator<SWBPredecessorSuccessor> iterList = swblist.iterator();
			while(iterList.hasNext())
			{
				SWBPredecessorSuccessor swbPredecessorSuccessor = iterList.next();
				String swbNumber = swbPredecessorSuccessor.getSwbNumber();

				swbList.append(swbNumber).append(",");
			}	
		}
		if(swbList.length() > 0)
		{
			swbList = swbList.deleteCharAt(swbList.length()-1);
		}
		String swbPredecessorSuccessorList = swbList.toString();
		return swbPredecessorSuccessorList;
	}

	/**
	 * This method creates the DOM tree structure. 
	 * @param partNumber
	 * @param dom
	 * @param myDataMap
	 * @param IsOSLangApplied
	 * @param IsOSLangUniform
	 * @param swbPredecessorList
	 * @param swbSuccessorList
	 * @throws Exception
	 */
	private static void createDOMTree(String partNumber, org.w3c.dom.Document dom, Map<String,Set<String>> myDataMap, String IsOSLangApplied, String IsOSLangUniform, String swbPredecessorList, String swbSuccessorList, Map<String, List<PrereqCoreqDtls>> preCoreqMap, String strPrereqOrderOfInstall, String strCoreqOrderOfInstall) throws Exception
	{
		logger.info("Starts: creating DOM Tree");
		try
		{
			org.w3c.dom.Element rootEle = dom.createElement("Part:SWB-Details");

			rootEle.setAttribute("xsi:schemaLocation", "http://dell.com/SWBDETAILS.xsd SWBDETAILS.xsd");
			rootEle.setAttribute("xmlns:swbbdetails", "http://dell.com/SWBDETAILS.xsd");
			rootEle.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootEle.setAttribute("part_number", partNumber);

			logger.debug("IsOSLangUniform----"+IsOSLangUniform);
			logger.debug("IsOSLangApplied----"+IsOSLangApplied);

			if(IsOSLangApplied != null && IsOSLangApplied.equalsIgnoreCase("on"))
			{
				rootEle.setAttribute("has-os-lang-matrix", "true");
			}
			else if(IsOSLangApplied==null)
			{
				//Do not set this attribute
			}
			else
			{
				rootEle.setAttribute("has-os-lang-matrix", "false");
			}

			if(IsOSLangUniform != null && IsOSLangUniform.equalsIgnoreCase("on"))
			{
				rootEle.setAttribute("os-lang-uniformly-applied", "true");
			}
			else if(IsOSLangUniform==null)
			{
				//Do not set this attribute
			}
			else
			{
				rootEle.setAttribute("os-lang-uniformly-applied", "false");
			}

			dom.appendChild(rootEle);

			//For Predecessor Successor
			createSWBTags(dom,rootEle,swbPredecessorList, swbSuccessorList);

			//For Prereq Coreq
			createCommonTags(dom, rootEle, strPrereqOrderOfInstall, strCoreqOrderOfInstall);
			appendPrereqCoreqChildrenToRoot(dom, rootEle, preCoreqMap);

			//For OS Lang Matrix
			appendOSLangChildrenToRoot(dom,myDataMap,rootEle);

		}
		catch(Exception e)
		{
			logger.error("Exception while creating the DOM Tree---"+e);
			throw e;
		}
		logger.info("Ends: creating DOM Tree");
	}

	/**
	 * Appends the OS and lang nodes to the root element.
	 * @param dom
	 * @param myData
	 * @param rootEle
	 * @throws Exception
	 */
	private static void appendOSLangChildrenToRoot(org.w3c.dom.Document dom,Map<String,Set<String>> myDataMap,org.w3c.dom.Element rootEle)throws Exception
	{
		try
		{
			logger.info("Starts:Iterating list of PartOSLang---");
			if(myDataMap!=null)
			{
				Set<String> osItr = myDataMap.keySet();
				Iterator<String> oslangItr = osItr.iterator();
				while (oslangItr.hasNext()) 
				{
					String osName = oslangItr.next();
					Set<String> langsSet = myDataMap.get(osName);
					createOSTags(dom,rootEle,osName,langsSet);
				}
			}
		}
		catch(Exception e)
		{
			logger.error("Exception while creating and appending OS and lang elements----",e);
			throw e;
		}
		logger.info("Ends:Iterating list of PartOSLang");
	}

	/**
	 * This method creates tags for Part.
	 * @param dom
	 * @param elRoot
	 * @param OSname
	 * @param langs
	 * @param swbPredecessorList
	 * @param swbSuccessorList
	 * @return
	 * @throws Exception
	 */
	/*private static org.w3c.dom.Element createPartTags(org.w3c.dom.Document dom,org.w3c.dom.Element elRoot,final String OSname,final Set<String> langs, final String swbPredecessorList, final String swbSuccessorList) throws Exception
	{
		logger.info("Starts:creating Tags for Part Details---");
		try
		{
			//OS Lang
			Element osEle = dom.createElement("osl:os");
			osEle.setAttribute("os-name", OSname);
			Element langsEle = dom.createElement("osl:languages");
			Iterator<String> iterator = langs.iterator();
			while (iterator.hasNext())
			{
				org.w3c.dom.Element elLang = dom.createElement("osl:language");
				org.w3c.dom.Text textLang = dom.createTextNode((String) iterator.next()); 
				elLang.appendChild(textLang);
				langsEle.appendChild(elLang);
			}
			osEle.appendChild(langsEle);
			elRoot.appendChild(osEle);

			//SWB Predecessor
			Element predEles = dom.createElement("swb:predecessors");
			Element predEle = dom.createElement("swb:predecessor");
			org.w3c.dom.Text textSwbPred = dom.createTextNode(swbPredecessorList); 
			predEle.appendChild(textSwbPred);
			predEles.appendChild(predEle);
			elRoot.appendChild(predEles);

			//SWB Successor
			Element succEles = dom.createElement("swb:successors");
			Element succEle = dom.createElement("swb:successor");
			org.w3c.dom.Text textSwbSucc = dom.createTextNode(swbSuccessorList); 
			succEle.appendChild(textSwbSucc);
			succEles.appendChild(succEle);
			elRoot.appendChild(succEles);
		}
		catch(Exception e)
		{
			logger.error("Exception while create part details tags---" +e.getMessage());
		}
		logger.info("Ends:creating Tags for Part Details.");
		return elRoot;
	}*/

	/**
	 * This is helper method which creates OS and lanuages elements. 
	 * @param dom
	 * @param elRoot
	 * @param OSname
	 * @param lang
	 * @return org.w3c.dom.Element
	 * @throws Exception
	 */
	private static org.w3c.dom.Element createOSTags(org.w3c.dom.Document dom,org.w3c.dom.Element elRoot,final String OSname,final Set<String> langs)throws Exception{
		logger.info("Starts:creating OSTags for each list item---");
		try
		{
			Element osEle = dom.createElement("osl:os");
			osEle.setAttribute("os-name", OSname);
			Element langsEle = dom.createElement("osl:languages");
			Iterator<String> iterator = langs.iterator();
			while (iterator.hasNext()) 
			{
				org.w3c.dom.Element elLang = dom.createElement("osl:language");
				org.w3c.dom.Text textLang = dom.createTextNode((String) iterator.next()); 
				elLang.appendChild(textLang);
				langsEle.appendChild(elLang);
			}
			osEle.appendChild(langsEle);
			elRoot.appendChild(osEle);

		}
		catch(Exception e)
		{
			logger.error("Exception while create OS tags 099807987908---",e);
		}
		logger.info("Ends:creating OSTags for each list item.");
		return elRoot;
	}

	/**
	 * This is helper method to create SWB tags.
	 * @param dom
	 * @param elRoot
	 * @param swbPredecessorList
	 * @param swbSuccessorList
	 * @return org.w3c.dom.Element
	 * @throws Exception
	 */
	private static org.w3c.dom.Element createSWBTags(org.w3c.dom.Document dom,org.w3c.dom.Element elRoot,  final String swbPredecessorList, final String swbSuccessorList) throws Exception
	{
		logger.info("Starts:creating SWB Tags for comma separated list for swb numbers---");
		try
		{
			//SWB Predecessor
			Element predEles = dom.createElement("swb:predecessors");
			Element predEle = dom.createElement("swb:predecessor");
			org.w3c.dom.Text textSwbPred = dom.createTextNode(swbPredecessorList); 
			predEle.appendChild(textSwbPred);
			predEles.appendChild(predEle);
			elRoot.appendChild(predEles);

			//SWB Successor
			Element succEles = dom.createElement("swb:successors");
			Element succEle = dom.createElement("swb:successor");
			org.w3c.dom.Text textSwbSucc = dom.createTextNode(swbSuccessorList); 
			succEle.appendChild(textSwbSucc);
			succEles.appendChild(succEle);
			elRoot.appendChild(succEles);
		}
		catch(Exception e)
		{
			logger.error("Exception while create SWB details tags---" +e.getMessage());
		}
		logger.info("Ends:creating Tags for SWB Details.");
		return elRoot;
	}

	/**
	 * This method creates the common tags under root element.
	 * @param dom
	 * @param elRoot
	 * @param strPrereqOrderOfInstall
	 * @param strCoreqOrderOfInstall
	 * @throws Exception
	 */
	private static org.w3c.dom.Element createCommonTags(org.w3c.dom.Document dom, org.w3c.dom.Element elRoot, String strPrereqOrderOfInstall, String strCoreqOrderOfInstall) throws Exception
	{
		logger.info("The createCommonTags Method is Starting.");
		try
		{
			Element prereqEle = dom.createElement("pcd:preReqSwbNbrs");
			if(strPrereqOrderOfInstall!=null && strPrereqOrderOfInstall.length()>0)
			{
				org.w3c.dom.Text textPrereqEle = dom.createTextNode(strPrereqOrderOfInstall);
				prereqEle.appendChild(textPrereqEle);
			}
			else
			{
				org.w3c.dom.Text textPrereqEle = dom.createTextNode("");
				prereqEle.appendChild(textPrereqEle);
			}
			elRoot.appendChild(prereqEle);
			logger.info("Added pcd:preReqSwbNbrs to the root element.");

			Element coreqEle = dom.createElement("pcd:coReqSwbNbrs");
			if(strCoreqOrderOfInstall!=null && strCoreqOrderOfInstall.length() >0)
			{
				org.w3c.dom.Text textCoreqEle = dom.createTextNode(strCoreqOrderOfInstall);
				coreqEle.appendChild(textCoreqEle);
			}
			else
			{
				org.w3c.dom.Text textCoreqEle = dom.createTextNode("");
				coreqEle.appendChild(textCoreqEle);
			}
			elRoot.appendChild(coreqEle);
			logger.info("Added pcd:coReqSwbNbrs to the root element.");
		}
		catch(Exception e)
		{
			logger.error("Exception while createCommonTags Method is: "+e.getMessage());
		}
		logger.info("The createCommonTags Method is Ending.");
		return elRoot;
	}


	/**
	 * This method creates the Prereq Coreq tags under root element. 
	 * @param dom
	 * @param rootEle
	 * @param preCoreqMap
	 * @throws Exception
	 */
	private static void appendPrereqCoreqChildrenToRoot(org.w3c.dom.Document dom, org.w3c.dom.Element rootEle, Map<String, List<PrereqCoreqDtls>> preCoreqMap) throws Exception
	{
		logger.info("Starts: The appendPrereqCoreqChildrenToRoot method");
		try
		{
			if(preCoreqMap!=null) 
			{
				Set<String> pcItr = preCoreqMap.keySet();
				if(pcItr.contains("P"))
				{
					List<PrereqCoreqDtls> prereqList = (List<PrereqCoreqDtls>)preCoreqMap.get("P");
					if(prereqList.size() > 0)
					{
						Iterator<PrereqCoreqDtls> prereqItr = prereqList.iterator();
						while (prereqItr.hasNext())
						{
							PrereqCoreqDtls prereqDtls = (PrereqCoreqDtls) prereqItr.next();
							createSWBPrereqCoreqTags(dom, rootEle, prereqDtls);
						}
					}
				}

				if(pcItr.contains("C"))
				{
					List<PrereqCoreqDtls> coreqList = (List<PrereqCoreqDtls>)preCoreqMap.get("C");
					if(coreqList.size() > 0) 
					{
						Iterator<PrereqCoreqDtls> coreqItr = coreqList.iterator();
						while (coreqItr.hasNext())
						{
							PrereqCoreqDtls coreqDtls = (PrereqCoreqDtls)coreqItr.next();
							createSWBPrereqCoreqTags(dom, rootEle, coreqDtls);
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			logger.error("Exception while creating and appending OS and lang elements----",e);
			throw e;
		}
		logger.info("Ends: The appendPrereqCoreqChildrenToRoot method");
	}


	/**
	 * This method creates the SWB Prereq Coreq tags under root element .
	 * @param dom
	 * @param elRoot
	 * @param prereqDtls
	 * @throws Exception
	 */
	private static org.w3c.dom.Element createSWBPrereqCoreqTags(org.w3c.dom.Document dom, org.w3c.dom.Element elRoot, PrereqCoreqDtls prereqDtls) throws Exception
	{
		logger.info("Starts:creating pcd:swb tag in createSWBTags for each prereq or coreq records");
		try
		{
			Element swb = dom.createElement("pcd:swb");
			swb.setAttribute("part_number", prereqDtls.getPrereqCoreqNbr());

			Element prereqCoreqFlag = dom.createElement("pcd:isSWBPrereq");
			org.w3c.dom.Text textPrereqCoreqFlag = null;
			if(prereqDtls.getPrereqCoreqFlag().equalsIgnoreCase("P"))
			{
				textPrereqCoreqFlag = dom.createTextNode("Yes");
			}
			else
			{
				textPrereqCoreqFlag = dom.createTextNode("No");
			}
			prereqCoreqFlag.appendChild(textPrereqCoreqFlag);
			swb.appendChild(prereqCoreqFlag);

			Element specificVerFlag = dom.createElement("pcd:specificVer");
			Element specificOrLaterVerFlag = dom.createElement("pcd:specificOrLaterVer");
			org.w3c.dom.Text textSpecificVer = null;
			org.w3c.dom.Text textSpecificOrLater = null;
			if(prereqDtls.getSpecificOrLaterVer().equalsIgnoreCase("S"))
			{
				textSpecificVer = dom.createTextNode("Yes");
				textSpecificOrLater = dom.createTextNode("No");
			}
			else if(prereqDtls.getSpecificOrLaterVer().equalsIgnoreCase("L"))
			{
				textSpecificVer = dom.createTextNode("No");
				textSpecificOrLater = dom.createTextNode("Yes");
			}
			specificVerFlag.appendChild(textSpecificVer);
			swb.appendChild(specificVerFlag);

			specificOrLaterVerFlag.appendChild(textSpecificOrLater);
			swb.appendChild(specificOrLaterVerFlag);

			Element orderOfInstall = dom.createElement("pcd:OrderOfInstall");
			org.w3c.dom.Text textOrderOfInstall = dom.createTextNode(prereqDtls.getOrderOfInstallation().toString());
			orderOfInstall.appendChild(textOrderOfInstall);
			swb.appendChild(orderOfInstall);

			Element projPlatformsAffPHNbr = dom.createElement("pcd:projPlatformsAffPHNbr");
			org.w3c.dom.Text textProjPlatformsAffPHNbr = dom.createTextNode(prereqDtls.getPlatformPhNbr());
			projPlatformsAffPHNbr.appendChild(textProjPlatformsAffPHNbr);
			swb.appendChild(projPlatformsAffPHNbr);

			Element oses = dom.createElement("pcd:OSES");
			if(prereqDtls.getOsIds()!=null)
			{
				org.w3c.dom.Text textOses = dom.createTextNode(prereqDtls.getOsIds());
				oses.appendChild(textOses);
			}
			else
			{
				org.w3c.dom.Text textOses = dom.createTextNode("");
				oses.appendChild(textOses);
			}
			swb.appendChild(oses);

			Element Languages = dom.createElement("pcd:Languages");
			if(prereqDtls.getLangIds()!=null)
			{
				org.w3c.dom.Text textLanguages = dom.createTextNode(prereqDtls.getLangIds());
				Languages.appendChild(textLanguages);
			}
			else
			{
				org.w3c.dom.Text textLanguages = dom.createTextNode("");
				Languages.appendChild(textLanguages);
			}
			swb.appendChild(Languages);

			Element lossOfFuncRsnTxt = dom.createElement("pcd:lossOfFuncRsnTxt");
			org.w3c.dom.Text textLossOfFuncRsnTxt = dom.createTextNode(prereqDtls.getLossOfFuncRsnTxt());
			lossOfFuncRsnTxt.appendChild(textLossOfFuncRsnTxt);
			swb.appendChild(lossOfFuncRsnTxt);

			elRoot.appendChild(swb);
		}
		catch(Exception e)
		{
			logger.error("Exception in createSWBTags Method.", e);
		}
		logger.info("Ends:creating pcd:swb tag in createSWBTags for each prereq or coreq records");
		return elRoot;
	}
}
