package com.dell.plm.agile.integration.xml;

import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import com.dell.plm.agile.integration.util.XMLUtil;

//D&D change 13
//import com.dell.plm.agile.integration.dto.predecessors.SWBPredecessor;
import com.dell.plm.agile.integration.dto.predecessorsuccessor.SWBPredecessorSuccessor;
//D&D change over

public class SWBXMLGenerator {

	private static final Logger logger = Logger.getLogger(SWBXMLGenerator.class);
	/**
	 * Using JAXP in implementation independent manner create a document object
	 * using which we create a XML tree in memory
	 */
	
	//D&D change 14: changed list type from <SWBPredecessor> to <SWBPredecessorSuccessor>
	//D&D change 30: added the argument (int predorsucc)
	public static String createDocument(String swb_number,List<SWBPredecessorSuccessor> swblist, int predorsucc) throws Exception
	{
		logger.info("Starts: creating XML...");
		String strXML = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document  dom = db.newDocument();
			
			//D&D change 21
			//String swbPredecessorList = getSWBMap(swblist);
			//createDOMTree(swb_number,dom,swbPredecessorList);
			String swbPredecessorSuccessorList = getSWBMap(swblist);
			createDOMTree(swb_number,dom,swbPredecessorSuccessorList,predorsucc);
			//D&D change over
			
			strXML = XMLUtil.getXMLString(dom);
			logger.debug("SWB XML generated is ----"+strXML);
		}catch(ParserConfigurationException pce) {
			logger.error("Error while trying to instantiate DocumentBuilder",pce);
			throw pce;
		}
		logger.info("Ends: creating XML swb:SWB");
		return strXML;
	}
	
	//D&D change 15: changed list type from <SWBPredecessor> to <SWBPredecessorSuccessor> at 3 places in this method
	private static String getSWBMap(final List<SWBPredecessorSuccessor> swblist)
	{
		StringBuffer swbList = new StringBuffer(""); 
		if(null != swblist)
		{
			Iterator<SWBPredecessorSuccessor> iterList = swblist.iterator();
			while(iterList.hasNext())
			{
				//D&D change 22
				SWBPredecessorSuccessor swbPredecessorSuccessor = iterList.next();
				String swbNumber = swbPredecessorSuccessor.getSwbNumber();
				//D&D change over
				
				swbList.append(swbNumber).append(",");
			}	
			
		}
		if(swbList.length() > 0)
		{
			swbList = swbList.deleteCharAt(swbList.length()-1);
		}
		
		//D&D change 23
		//String swbPredecessorList = swbList.toString();
		//return swbPredecessorList;
		
		String swbPredecessorSuccessorList = swbList.toString();
		return swbPredecessorSuccessorList;
		
		//D&D change over
	}
	
	//D&D change 24: changed name of argument from String swbPredecessorList to String swbPredecessorSuccessorList
	//D&D change 31: added the argument (int predorsucc)
	private static void createDOMTree(String swb_number,org.w3c.dom.Document dom,String swbPredecessorSuccessorList, int predorsucc) throws Exception
	{
		logger.info("Starts: creating DOM Tree");
		try{
			/** create the root element */
			org.w3c.dom.Element rootEle = dom.createElement("swb:swb-number");
			rootEle.setAttribute("xsi:schemaLocation", "http://dell.com/SWB.xsd SWB.xsd");
			rootEle.setAttribute("xmlns:swb", "http://dell.com/SWB.xsd");
			rootEle.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootEle.setAttribute("swb_number", swb_number);
			dom.appendChild(rootEle);
			
			//D&D change 25
			//createSWBTags(dom,rootEle,swbPredecessorList);
			
			createSWBTags(dom,rootEle,swbPredecessorSuccessorList, predorsucc);
			
			//D&D change over
			
		}catch(Exception e){
			logger.error("Exception while creating the DOM Tree----"+e);
			throw e;
		}
		logger.info("Ends: creating DOM Tree");
	}
	
	
	//D&D change 26: changed name of argument from String swbPredecessorList to String swbPredecessorSuccessorList
	//D&D change 32: added the argument (int predorsucc)
	private static org.w3c.dom.Element createSWBTags(org.w3c.dom.Document dom,org.w3c.dom.Element elRoot,final String swbPredecessorSuccessorList,int predorsucc) throws Exception
	{
		logger.info("Starts:creating SWB Tags for comma separated list for swb numbers");
		try{
			
			
			//D&D change 33
			/*
			 Element swbEles = dom.createElement("swb:predecessors");
			 
			 Element swbEle = dom.createElement("swb:predecessor");
			 */
			
			Element swbEles=null;;
			Element swbEle=null;;
			
			if(predorsucc==1)
			{
			swbEles = dom.createElement("swb:predecessors");
			
			swbEle = dom.createElement("swb:predecessor");
			}
			
			else if(predorsucc==2)
			{
			swbEles = dom.createElement("swb:successors");
			
			swbEle = dom.createElement("swb:successor");
			}
			//D&D change over
			
			//D&D change 27
			//org.w3c.dom.Text textSwbNbr = dom.createTextNode(swbPredecessorList); 
			org.w3c.dom.Text textSwbNbr = dom.createTextNode(swbPredecessorSuccessorList); 
			//D&D change over
			
			swbEle.appendChild(textSwbNbr);
			swbEles.appendChild(swbEle);
			elRoot.appendChild(swbEles);

		}catch(Exception e){
			
			//D&D change 34
			//logger.error("Exception while create swb predecessors: " +e.getMessage());
			if(predorsucc==1){logger.error("Exception while create swb predecessors: " +e.getMessage());}
			else if (predorsucc==2){logger.error("Exception while create swb successors: " +e.getMessage());}
			//D&D change over
		}
		logger.info("Ends:creating SWB Tags for comma separated list for swb numbers");
		return elRoot;
	}
}
