package com.dell.plm.agile.integration.xml;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import com.dell.plm.agile.integration.util.XMLUtil;

public class UpdatePartAttributeXMLGenerator {
	
	private static final Logger logger = Logger.getLogger(UpdatePartAttributeXMLGenerator.class);
	
	public static String createDocument(String swb_number) throws Exception
	{
		logger.info("Starts: creating XML...");
		String strXML = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document  dom = db.newDocument();
			createDOMTree(swb_number,dom);
			strXML = XMLUtil.getXMLString(dom);
			logger.debug("SWB XML generated is ----"+strXML);
		}catch(ParserConfigurationException pce) {
			logger.error("Error while trying to instantiate DocumentBuilder",pce);
			throw pce;
		}
		logger.info("Ends: creating XML swb:SWB");
		return strXML;
	}
	
	
	
	
		
	private static void createDOMTree(String swb_number,org.w3c.dom.Document dom) throws Exception
	{
		logger.info("Starts: creating DOM Tree");
		try{
			/** create the root element */
			org.w3c.dom.Element rootEle = dom.createElement("swb:swb-number");
			rootEle.setAttribute("xsi:schemaLocation", "http://dell.com/SWB.xsd SWB.xsd");
			rootEle.setAttribute("xmlns:swb", "http://dell.com/SWB.xsd");
			rootEle.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootEle.setAttribute("swb_number", swb_number);
			dom.appendChild(rootEle);
		}catch(Exception e){
			logger.error("Exception while creating the DOM Tree----"+e);
			throw e;
		}
		logger.info("Ends: creating DOM Tree");
	}
	
}
