package com.dell.plm.agile.common.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.ExceptionConstants;
import com.agile.api.IAgileList;
import com.agile.api.IAgileSession;
import com.agile.api.ICell;
import com.agile.api.IDataObject;
import com.agile.api.IRow;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;

/**
 * 
 * @author nitin_k_kumar
 *
 */
public class CommonAglUtil {

	private static Logger log;
	
	static{
		log = GDevLogger.getLogger();
	}
	
	public static Properties loadProperties(String path) {
		// properties in the classpath
		
		Properties props = new Properties();

		FileInputStream oFIS = null;
		try {
			oFIS = new FileInputStream(path);
			props.load(oFIS);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		finally{
			try {
				if( oFIS!= null)
					oFIS.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return props;
	}
	
	/**
	 *  Get list value from IROW
	 * @param rowObject
	 * @param attribute
	 * @return attributeValue
	 */
	public static String getListValue(IRow rowObject, Integer attribute) {
		ICell cell;
		try {
			cell = rowObject.getCell(attribute);
			IAgileList cl = (IAgileList) cell.getValue();
			String attributeValue = null;
			IAgileList[] selected = cl.getSelection();
			if (selected != null && selected.length > 0) {
				attributeValue = (selected[0].getValue()).toString();
			}
			return attributeValue;
		} catch (APIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	
	/**
	 * Get list value from IItem
	 * @param partObject
	 * @param attribute
	 * @return attributeValue
	 */
	public static String getListValueFromDataObject (IDataObject partObject , Integer attribute)
    {
		ICell cell;
		try {
			cell = partObject.getCell(attribute);
			IAgileList cl = (IAgileList) cell.getValue();
			String attributeValue = null;
			IAgileList[] selected = cl.getSelection();
			if (selected != null && selected.length > 0) {
				attributeValue = (selected[0].getValue()).toString();
			}
			return attributeValue;
		} catch (APIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	
	

	/**
	 * Get Multilist value from IROW
	 * @param row
	 * @param attribute
	 * @return attributeValue
	 */
	public static String getMultiListValue(IRow row,
			Integer attribute) {
		try {
		ICell cell = row.getCell(attribute);
		// Get the current IAgileList object for Product Lines
		IAgileList list = (IAgileList)cell.getValue();
		// Convert the current value from the list to a string 
		String attributeValue = list.toString();
		
		return attributeValue;
		} catch (APIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * Get Multilist Value from IItem
	 * @param partObject
	 * @param attribute
	 * @return
	 */
	public static String getMultiListValueFromDataObject (IDataObject partObject , Integer attribute)
    {
		ICell cell;
		try {
			cell = partObject.getCell(attribute);
			IAgileList list = (IAgileList) cell.getValue();
			String attributeValue = list.toString();
			return attributeValue;
		} catch (APIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	

	/**
	 * Set Multilist Value for IItem,IChange
	 * @param dataObject
	 * @param attribute
	 * @return
	 * @throws Exception 
	 */
	
	public static void setMultiListValueFromDataObject(IAgileSession session, IDataObject dataObject, Integer attributeName, List<String> ffaValueList) throws Exception
	{
		try {
			boolean isSuccess = false;
			int intLoop = 1;
			boolean isRefreshError = true;	
			String cellValues=null;
			IAgileList cellFFA=null;
			session.disableAllWarnings();
			
			
			ICell cell= dataObject.getCell(attributeName);
//			String attfulfillmentFacility= cell.getName();
			
			if(cell!=null && ! cell.isReadOnly())
			{
				cellValues =  cell.getValue().toString();
				log.info("Cell Name: "+cell.getName()+" Cell Existing Values: "+cellValues.toString());
				
				if(cellValues!=null)
					{
						ffaValueList.addAll(Arrays.asList(cellValues.split(";")));
						/*String cellValueList[] = cellValues.split(";");
						for(String exisitngValues: cellValueList)
							{
								if(!ffaValueList.contains(exisitngValues))
									ffaValueList.add(exisitngValues);
							}*/
						Set ffaValueSet= new LinkedHashSet<>(ffaValueList);
						
						cellFFA= cell.getAttribute().getAvailableValues();
						cellFFA.setSelection(ffaValueSet.toArray());
						ffaValueList=null;
					}
				try {
					log.info("Setting the "+cell.getName() +" attribute with the Values: "+cellFFA);
					dataObject.refresh();
					cell.setValue(cellFFA);
					dataObject.refresh();
				} catch(APIException ex) {
					if(((Integer)ex.getErrorCode()).intValue()== ExceptionConstants.APDM_OBJVERSION_MISMATCH.intValue() ||
							ex.getMessage().indexOf("Someone is working on this object")!=-1){
						while(intLoop < 5 && !isSuccess && isRefreshError) {
							try {
								log.info("Trying to Refresh and setting Values, Tentative "+ intLoop);
								Thread.sleep(10000);
								dataObject.refresh();
								cell.setValue(cellFFA);
								dataObject.refresh();
								log.info("****************** UPDATED Values Successfully ****************");
								
								isSuccess = true;
							} catch (APIException apie) {
								if(((Integer)apie.getErrorCode()).intValue()== ExceptionConstants.APDM_OBJVERSION_MISMATCH.intValue() ||
										apie.getMessage().indexOf("Someone is working on this object")!=-1){
									intLoop++;
								} else {
									isRefreshError = false;
									break;
								}
							} catch (Exception e) {
								isRefreshError = false;
								break;
							}
						}
					} else {
						isRefreshError = false;
					}
					if(!isRefreshError || !isSuccess) {
						throw ex;
					}
				}
			}
			session.enableAllWarnings();
			
		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.info("APIException occurs while setting the multilist attribute : "+e);
			e.printStackTrace();
			throw new Exception("Could not set the Fulfillment Facility Values");
		}
		catch(Exception ex)
		{
			log.info("APIException occurs while setting the multilist attribute : "+ex);
			ex.printStackTrace();
			throw new Exception("Could not set the Fulfillment Facility Affected attribute values");
		}
		
	}
	
	
	
	 	
	/**
	 * Get list from String with a delimiter
	 * @param propertyValue
	 * @param separator
	 * @return list
	 */
	  public static ArrayList<String> getListFromString(String propertyValue,String separator){
			ArrayList<String> list = new ArrayList<String>();
			if(propertyValue!=null){
				StringTokenizer st = new StringTokenizer(propertyValue,separator);
				while(st.hasMoreTokens()){
					list.add(st.nextToken());
				}

			}
			return list;
		}
	
	  
	  /**
	     * Returns the Stack Trace of an Exception as a String.
	     * @param throwable Object whose Stack Trace is Required
	     * @return Stack Trace of the specified Throwable Object as String
	     */
	    public  static String exception2String(Throwable throwable) {
	        StringWriter sw = new StringWriter();
	        PrintWriter pw = new PrintWriter(sw, true);
	        throwable.printStackTrace(pw);
	        pw.flush();
	        pw.close();
	        return sw.toString();
	    }
}
