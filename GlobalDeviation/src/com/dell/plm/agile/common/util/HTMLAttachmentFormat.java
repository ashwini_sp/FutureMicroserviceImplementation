package com.dell.plm.agile.common.util;

public class HTMLAttachmentFormat {

	
	private static StringBuffer attachment;
	
	public static void start () {
		
		attachment = new StringBuffer("<HTML><TITLE>GLOBAL DEVIATION ERROR/WARNING ATTACHMENT</TITLE><BODY>");
		
	}
	public static void end () {
		attachment.append("</BODY></HTML>");
	}
	
	/**
	 * 
	 * @return
	 */
	public static String generateHTMLTitle(String title) {
		attachment.append("<B>"+ title + "</B>").append("<BR>");
		return attachment.toString();
	}

	/**
	 * 
	 * @return
	 */
	public static String generateHTMLMessage(String message) {
		attachment.append(red("<B>"+ message + "</B>")).append("<BR><BR><BR>");
		return attachment.toString();
	}

	
	
	public static String generateHTMLValidationErrorTAG() {
		
		attachment.append(red("<B>GLOBAL DEVIATIONS VALIDATION ERRORS</B>")).append("<BR><BR>");
		return attachment.toString();
	}
	
	/**
	 * 
	 * @return
	 */
	public static String generateHTMLValidationError(String error) {
		attachment.append(maroon(error) ).append("<BR><BR>");
		return attachment.toString();
	}

	
	
	
	public static String generateHTMLValidationWarningTAG() {
		
		attachment.append(red("<B>GLOBAL DEVIATIONS VALIDATION WARNINGS:</B>")).append("<BR><BR>");
		return attachment.toString();
	}
	/**
	 * 
	 * @return
	 */
	public static String generateHTMLValidationWarning(String warning) {
		attachment.append(green(warning) ).append("<BR><BR>");
		return attachment.toString();
	}

	
	/**
	 * Add the HTML Tag for Font Bold
	 * @param String
	 * @return String with bold tags added
	 */
	public static String bold(String str) {
		return("<B>" + str + "</B>");
	}

	/**
	 * Add the HTML Tag for Font color red
	 * @param String
	 * @return String with Font tags added
	 */
	public static String red(String str) {
		return("<Font color='red'>" + str + "</Font>");
	}

	/**
	 * Add the HTML Tag for Font color maroon
	 * @param String
	 * @return String with Font tags added
	 */
	public static String maroon(String str) {
		return("<Font color='maroon'>" + str + "</Font>");
	}

	/**
	 * Add the HTML Tag for Font color blue
	 * @param String
	 * @return String with Font tags added
	 */
	public static String blue(String str) {
		return("<Font color='blue'>" + str + "</Font>");
	}

	/**
	 * Add the HTML Tag for Font color green
	 * @param String
	 * @return String with Font tags added
	 */
	public static String green(String str) {
		return("<Font color='green'>" + str + "</Font>");
	}
	
	public static StringBuffer getAttachmentObject ()
	{
		if(attachment == null)
			start();
		return attachment;
	}
}
