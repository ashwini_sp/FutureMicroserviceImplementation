package com.dell.plm.agile.common.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class LoadProps {

	public static Properties loadProperties(String path) {
		// properties in the classpath

		Properties props = new Properties();

		FileInputStream oFIS = null;
		try {
			oFIS = new FileInputStream(path);
			props.load(oFIS);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		finally{
			try {
				if( oFIS!= null)
					oFIS.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return props;
	}

}
