package com.dell.plm.agile.common.util;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.IAgileSession;
import com.agile.api.IUser;
import com.agile.api.UserConstants;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;

/**
 * This class sends email to the agile user */

public class MailUser {

	private String originator = "";
	private String subject = "";
	private String body = "";
	private String attachmentURL = "";
	private String isAttach = "";
	private String smtpHost = "";
	private String from = "";

	private String email;
	private Logger log;
	private IAgileSession session;

	/**
	 * Constructor to initialize the parameter
	 * @param session 
	 */

	public MailUser(IAgileSession session, String originator, String subject,
			String body, String attachmentURL, String isAttach,
			String smtpHost, String from) {

		log = GDevLogger.getLogger();
		this.session = session;
		this.originator = originator;
		this.subject = subject;
		this.body = body;
		this.attachmentURL = attachmentURL;
		this.isAttach = isAttach;
		this.smtpHost = smtpHost;
		this.from = from;

	}

	/**
	 * Check email address , send mail
	 * @throws APIException
	 * @throws SQLException
	 */
	public void doSend() throws APIException, SQLException {
		if (getEmailAddress()) {
			sendEmail();
		}
	}
	
	/**
	 * send mail with the mail id specified as parameter
	 * @throws APIException
	 * @throws SQLException
	 */
	public void doSend(String emailId) throws APIException, SQLException {
		this.email = emailId;
		sendEmail();
		
	}

	/**
	 * Get the user email address
	 * @return boolean
	 * @throws APIException
	 * @throws SQLException
	 */
	private boolean getEmailAddress() throws APIException, SQLException {
		String txtUserID = "";

		txtUserID = originator;

		if (txtUserID.indexOf("(") >= 0) {
			txtUserID = txtUserID.substring(1, txtUserID.length() - 1);
		}

		// now the txtUserID is first name last name

		String xtxtUserID = txtUserID.trim();
		xtxtUserID = xtxtUserID.replace(' ', '_');
		xtxtUserID = xtxtUserID.toLowerCase();

		IUser user = (IUser) session.getObject(IUser.OBJECT_TYPE, xtxtUserID);
		if (user != null) {
			email = (String) user
					.getValue(UserConstants.ATT_GENERAL_INFO_EMAIL);
			log.info("Email Id : " + email);
			if (email == null) {
				log.info("Cannot locate e-mail address for user" + xtxtUserID);
				return false;
			}
		} else {
			log.info("Cannot locate user " + xtxtUserID);
			return false;
		}
		return true;
	}

	/**
	 * Send mail to user
	 * @throws SQLException
	 */
	private void sendEmail() throws SQLException {

		String[] to = { email };

		if (isAttach.equalsIgnoreCase("True")) {
			new SendMail(smtpHost, from, to, subject, body, attachmentURL);
		} else {
			new SendMail(smtpHost, from, to, subject, body);
		}
	}

}