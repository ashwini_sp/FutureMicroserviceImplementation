package com.dell.plm.agile.common.util;
/*
 * SendMail
 *
 * version 1.0
 *
 * Mar, 01, 2001
 *
 * DELL Computer Corporation
 */
 /* Mail Package */
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;



/**
 *  Email component which provides methods to set the email server Host name,
 *  senders address, recipients address, subject and body of message etc and
 *  send the email message with or without attachment.
 */

public class SendMail {

    /**
     * Constructor for text/html email
     */
	public SendMail(String smtpHost, String from, String[] to, String subject,
					 String body) throws SQLException{
		send(smtpHost, from, to, subject, body);
	}

    /**
     * Constructor for text email with attachment
     */
	public SendMail(String smtpHost, String from, String[] to, String subject,
					 String body, String attachFile) throws SQLException{
		sendFile(smtpHost, from, to, subject, body, attachFile);
	}


	/**
	 * Send the email message.
	 */
	private void send(String smtpHost, String from, String[] to, String subject,
					 String body) throws SQLException {

		/* Get the System properties */
		Properties properties=System.getProperties();
		Session session=Session.getInstance(properties, null);
		MimeMessage message;

		message=new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress(from));
			for (int i=0; i<to.length; i++) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));
			}
			message.setSubject(subject);
			message.setText(body);
			//message.setContent(FormatHTML.wrapHTML(body, subject), "text/html");

			Transport transport = session.getTransport("smtp");
			transport.connect(smtpHost, "", "");

			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		}
		catch(SendFailedException e) {
			e.printStackTrace();
		}
		catch(MessagingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Send email with file attachment
	 */

    private void sendFile(String smtpHost, String from, String[] to, String subject,
					 String body, String attachFile) throws SQLException {

	// create some properties and get the default Session
	Properties props = System.getProperties();
	props.put("mail.smtp.host", smtpHost);

	Session session = Session.getDefaultInstance(props, null);

	try {
	    // create a message
	    MimeMessage message = new MimeMessage(session);
	    message.setFrom(new InternetAddress(from));
		for (int i=0; i<to.length; i++) {
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));
		}
	    message.setSubject(subject);

	    // create and fill the first message part
	    MimeBodyPart mbp1 = new MimeBodyPart();
	    mbp1.setText(body);

	    // create the second message part
	    MimeBodyPart mbp2 = new MimeBodyPart();


            // attach the file to the message
   	    FileDataSource fds = new FileDataSource(attachFile);
	    mbp2.setDataHandler(new DataHandler(fds));
	    mbp2.setFileName(fds.getName());

	    // create the Multipart and its parts to it
	    Multipart mp = new MimeMultipart();
	    mp.addBodyPart(mbp1);
	    mp.addBodyPart(mbp2);

	    // add the Multipart to the message
	    message.setContent(mp);

	    // set the Date: header
	    message.setSentDate(new Date());

	    // send the message
	    Transport.send(message);
	}
	catch (MessagingException mex) 
	{
		mex.printStackTrace();
	}
	}
}