package com.dell.plm.agile.pc.px.globaldeviations.corevalidations;

import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.ChangeConstants;
import com.agile.api.IAgileSession;
import com.agile.api.IChange;
import com.agile.api.IDataObject;
import com.agile.api.IItem;
import com.agile.api.IRow;
import com.agile.api.ITable;
import com.agile.px.IWFChangeStatusEventInfo;
import com.dell.plm.agile.common.util.CommonAglUtil;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;

public class GDEVCCBStatusValidation extends GlobalDeviationsBO {

	private Logger log;

	public GDEVCCBStatusValidation() {
		log = GDevLogger.getLogger();
		
	}

	/**
	 * doValidation method for CCB status
	 */
	public String doValidation(IAgileSession session, IWFChangeStatusEventInfo objectEventInfo, Properties props) {

		log.info("Inside GDEVCCBStatusValidation.doValidation :: Workflow status change from SUBMITTED > CCB");
				
		log = GDevLogger.getLogger();
	
		GlobalDeviationValidation validationInfo = new GlobalDeviationValidation(
				session, objectEventInfo);
		
		IDataObject deviationChangeObj;
		boolean coreValidationFlag = false;
		Vector<Boolean> errorCheck = new Vector<Boolean>();
		boolean deviateToNull = false;

		
		try {
			deviationChangeObj = objectEventInfo.getDataObject();
			log.info("Validating for GDEV Object : "+ deviationChangeObj.getName());
			ITable table = deviationChangeObj
					.getTable(ChangeConstants.TABLE_AFFECTEDITEMS);

			Iterator itr = table.iterator();
			Integer maxTableSize = Integer.parseInt(props.getProperty(GDEVConstants.GDEV_TABLE_SIZE));
			if (table.size() <= maxTableSize && table.size() != 0) {
				
				while (itr.hasNext()) {
					IRow row = (IRow) itr.next();

					// Load the Deviate From (Item Number) & Deviate To parts
					String itemNumber = (String) row
							.getValue(ChangeConstants.ATT_AFFECTED_ITEMS_ITEM_NUMBER);

					IItem itemNumberObj = (IItem) session.getObject(
							IItem.OBJECT_TYPE, itemNumber);

					String deviateToPart = CommonAglUtil.getListValue(row,
							ChangeConstants.ATT_AFFECTED_ITEMS_LIST01);
					if(deviateToPart != null && !deviateToPart.equalsIgnoreCase("")
							&& !deviateToPart.equalsIgnoreCase("null"))
					{
						IItem deviateToObj = (IItem) session.getObject(
								IItem.OBJECT_TYPE, deviateToPart);

						validationInfo.setAllRequiredAttributes(
								deviationChangeObj, row, itemNumberObj,
								deviateToObj);
						
						
						String overrideValidateCheck = CommonAglUtil
								.getListValueFromDataObject(
										(IChange) deviationChangeObj,
										ChangeConstants.ATT_PAGE_THREE_LIST03);
						
						coreValidationFlag = validationInfo
								.isNoneToPartScenario(itemNumber);
						if (coreValidationFlag) {
							errorCheck.add(false);
						}

						// If Item number & Deviate To parts are identical
						coreValidationFlag = validationInfo
								.identicalPartsValidation(itemNumber,
										deviateToPart);
						if (!coreValidationFlag) {
							errorCheck.add(coreValidationFlag);

						}

						// Check Revision
						coreValidationFlag = validationInfo.checkRevision(
								itemNumberObj, deviateToObj);
						if (!coreValidationFlag) {
							errorCheck.add(coreValidationFlag);

						}

						// Part Type Validation
						coreValidationFlag = validationInfo.validatePartType(
								itemNumberObj, deviateToObj);

						if (!coreValidationFlag) {
							errorCheck.add(coreValidationFlag);

						}

						// CFI/non CFI part validation
						coreValidationFlag = validationInfo.isCFI(itemNumber,
								deviateToPart);
						if (!coreValidationFlag) {
							errorCheck.add(coreValidationFlag);

						}

						// Multilevel PH validation

						coreValidationFlag = validationInfo.validatePH(
								itemNumber, deviateToPart);
						if (!coreValidationFlag) {
							errorCheck.add(coreValidationFlag);

						}

						if (overrideValidateCheck != null && overrideValidateCheck.equalsIgnoreCase("No")) {
							coreValidationFlag = validationInfo
									.validateReleasedDeviations(itemNumberObj);
							if (!coreValidationFlag) {
								errorCheck.add(coreValidationFlag);

							}
						}
					}
					else{
						
						errorCheck.add(false);
						deviateToNull = true;
					
					}

				}
				
				if (errorCheck != null && errorCheck.contains(Boolean.FALSE))
				{
					log.info("ERROR CONDITION"+errorCheck);
					if(!deviateToNull)
						validationInfo.createHTMLAttachment();
					return "Validation Error";
				}
				else if (!errorCheck.contains(Boolean.FALSE) && validationInfo.getWarningFlag())
				{
					log.info("WARNING CONDITION");
					if(!deviateToNull){
						validationInfo.createHTMLAttachment();
					    GDEVSupplierAccessBO.setIsWarning(true);
					}
					return "Validation Success";
				}
				else
					return "Validation Success";
			} else {
				
				if (table.size() > maxTableSize) {
					log.error(GDEVConstants.GDEV_TABLE_SIZE_MORE_THAN_LIMIT_ERROR+ maxTableSize);
					return "Table Size more than 100";
				} else if (table.size() == 0) {
					log.error(GDEVConstants.GDEV_TABLE_IS_EMPTY_ERROR);
					return "Table is empty";
				}
				return "";
			}

		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
			return "";
		} 

	}

	
	

}
