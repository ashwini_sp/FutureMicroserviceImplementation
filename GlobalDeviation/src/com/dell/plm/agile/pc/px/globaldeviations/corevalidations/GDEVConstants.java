package com.dell.plm.agile.pc.px.globaldeviations.corevalidations;

/**
 * Global Deviation Constants
 * @author nitin_k_kumar
 *
 */
public class GDEVConstants {

	public static final String GDEV_NONE_PART_CLASS = "NONE_PART_CLASS";
	public static final String GDEV_NONE_PART_TYPE = "NONE_PART_TYPE";
	public static final String GDEV_NONE_PART_DESC = "NONE_PART_DESC";
	//NewChange Variables
	public static final String GDEV_OTHER_NONE_PART_DESC = "OTHER_NONE_PART_DESC";
	//--------------------
	public static final String GDEV_NONE_MOD_PARTS = "NONE_MOD_PARTS";
	public static final String GDEV_TABLE_SIZE = "MAX_AI_TABLE_SIZE";
	public static final String GDEV_SIGNOFF_WARNINGS = "APPROVAL_WARNINGS";
	
	public static final String GDEV_VALIDATION_ERROR = "ERROR: DETAIL WILL BE SENT TO YOUR OUTLOOK INBOX AS WELL AS ATTACHED TO THE DEVIATION OBJECT ITSELF. PLEASE CORRECT ALL ERRORS BEFORE PROCEEDING.";
	public static final String GDEV_TABLE_SIZE_MORE_THAN_LIMIT_ERROR = "ERROR: Affected Item table size cannot exceed Max number of rows i.e.: ";
	public static final String GDEV_TABLE_IS_EMPTY_ERROR = "ERROR: Affected Item table cannot be empty. Please add at least one affected items row.";
	public static final String GDEV_TABLE_MAX_LIMIT_NULL = "ERROR: Max limit for Affected items is not defined in Global Deviations Property.Please contact Agile Support";
	public static final String GDEV_ASSOCIATED_RELEASED_DEVIATION_ERROR = "Already has a Released Deviation structured to same platform";
	public static final String GDEV_CHANGE_HISTORY_TABLE_TYPE = "Global Deviation";
	public static final String GDEV_PART_NUMBER_PREFIX = "GDEV";
	public static final String GDEV_PENDING_STATUS = "Pending";
	public static final String GDEV_SUBMITTED_STATUS = "Submitted";
	public static final String GDEV_CCB_STATUS = "CCB";
	public static final String GDEV_RELEASED_STATUS = "Released";
	public static final String GDEV_EXPIRED_STATUS = "Expired";
	public static final String GDEV_CFI_PART_CLASS_PREFIX = "CFI";
	public static final String GDEV_CFI_PART_TYPE_PREFIX = "SI";
	public static final String GDEV_ATTACHMENT_ERROR_MESSAGE = "Validation status : Failed";
	public static final String GDEV_ATTACHMENT_WARNING_MESSAGE = "Validation status : Success";
	public static final String GDEV_VALID_ATTACHMENT_PATH = "VALID_ATTACHMENT_PATH";
	public static final String GDEV_SMTP_HOST = "SMTPHOST";
	public static final String GDEV_FROM_MAIL_ID = "FROM";
	public static final String GDEV_EMAIL_OPTION = "EMAILOPTION";
	public static final String GDEV_PLATFORM_HIERARCHY = "Platform Hierarchy";
	public static final String GDEV_ALL_APPROVERS = "Not all Approvers have approved";
	
	
	public static final String GDEV_AGL_SUPPORT_MAIL_ID = "SUPPORT_MAIL_ID";
	//public static final String GDEV_GLOBAL_DEVIATION_PROPERTIES = "D:\\AgileApps\\GlobalDeviation\\config\\GDEV.properties";
	public static final String GDEV_GLOBAL_DEVIATION_PROPERTIES = "/u01/agileapps/GlobalDeviation/config/GDEV.properties";
	
	public static final String PROPERTY_LOG_FILE = "LOG_FILE";
	public static final String PROPERTY_LOG_FILE_SIZE_LIMIT="LOG_FILE_SIZE_LIMIT";
	public static final String PROPERTY_LOG_FILE_MAX_COUNT="LOG_FILE_MAX_COUNT";
	
	public static final String GDEV_LOG4J_PROPERTIES = "GDEV_LOG4J_PROPERTIES";

	public static final String GDEV_PHANTOM_PART_MESSAGE = "Following Null Items are in Item Number field. These Phantom Parts are not allowed on �Deviate From� side of a Deviation Request: ";
	public static final String GDEV_IDENTICAL_PARTS_MESSAGE = "Following Item Number and respective Deviate To combination are identical. Deviate From and Deviate To information on same row cannot be the same:";

	public static final String GDEV_PARTS_REVISION_ERROR_MESSAGE = "Following affected items are not at A or X revision. Only X or A revision items are permitted on a Deviation request:  ";
	public static final String GDEV_PART_TYPE_MESSAGE = "Following Item Numbers and respective Deviate To Part Type do not match. Part Type respective of Deviate From and Deviate To items should be the same:";
	public static final String GDEV_CFI_PARTS_MESSAGE = "Following Affected Items are CFI. CFI Items are not allowed on a Deviation Request:";

	public static final String GDEV_PH_VALIDATION_MESSAGE = "Warning: Following Affected Items are not structured to respective Platforms defined in respective Platform Number Affected field:";
	public static final String GDEV_PARTS_REVISION_WARNING_MESSAGE = "Following affected items are at X revision: ";

	public static final String GDEV_DUPLICATE_RELEASED_DEVIATION_MESSAGE_PART1 = "Affected items and respective Platform Number Affected ";
	public static final String GDEV_DUPLICATE_RELEASED_DEVIATION_MESSAGE_PART2 = "matches that on the following released deviations: ";
	public static final String GDEV_DUPLICATE_RELEASED_DEVIATION_MESSAGE_PART3 = " Please insure this Deviation request is not a duplicate of the listed released deviations";
	
	public static final String GDEV_CTO_DESTINATION  = "DESTINATION";
	public static final String GDEV_CTO_FILTER  = "FILTER";
	public static final String GDEV_CTO_FORMAT_TYPE  = "FORMAT_TYPE";
	
	public static final String GDEV_AGILE_PENDING_TO_SUBMITTED_NOTIFICATION_TEMPLATE = "AGILE_PENDING_TO_SUBMITTED_NOTIFICATION_TEMPLATE";
	public static final String GDEV_AGILE_SUBMITTED_TO_CCB_NOTIFICATION_TEMPLATE = "AGILE_SUBMITTED_TO_CCB_NOTIFICATION_TEMPLATE";
	public static final String GDEV_AGILE_CCB_TO_RELEASED_NOTIFICATION_TEMPLATE = "AGILE_CCB_TO_RELEASED_NOTIFICATION_TEMPLATE";
	
	//added by Guna
	//to fix PH issue
	public static final String GDEV_PROC_URL= "BI_DB_URL";  
	public static final String GDEV_PROC_USER="BI_DB_SCHEMA_AGILEPLM_DM_RT_USERNAME";
	public static final String GDEV_PROC_PWD="BI_DB_SCHEMA_AGILEPLM_DM_RT_PASSWORD";
	public static final String GDEV_PROC="GDEV_PROC";
	public static final String GDEV_GLOBAL_DEVIATION_DB_PROPERTIES="GDEV_GLOBAL_DEVIATION_DB_PROPERTIES";
	
	/*Added by Lakhan
	 * For new AX 2012 Facility Value
	 */
	public static final String AXFACILITY_ON_OFF= "AXFACILITY_ON_OFF";
	public static final String AXFACILITYLIST_DELIMITER= "AXFACILITYLIST_DELIMITER";
	public static final String CONNECTING_DELIMITER= "CONNECTING_DELIMITER";
	public static final String AXFACILITY_NEW_VALUES_LIST= "AXFACILITY_NEW_VALUES_LIST";
	public static final String IMPLEMENTATION_TYPE="IMPLEMENTATION_TYPE";
	
	
	public static final String GDEV_SKIP_PARTTYPE_VALIDATION= "SKIP_PARTTYPE_VALIDATION";
	
}
