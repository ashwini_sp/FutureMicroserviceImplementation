package com.dell.plm.agile.pc.px.globaldeviations.corevalidations;

import java.util.Properties;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.IAgileSession;
import com.agile.api.IDataObject;
import com.agile.px.IWFChangeStatusEventInfo;
import com.dell.plm.agile.common.util.CommonAglUtil;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;
import com.dell.plm.agile.pc.px.globaldeviations.publishobjects.GDEVCreateCTOBO;

public class GDEVExpiredStatusValidation extends GlobalDeviationsBO{

	
	private Logger log;

	public GDEVExpiredStatusValidation() {
		log = GDevLogger.getLogger();
		
	}
	
	/**
	 * doValidation method for Expired Status
	 */
	public String doValidation(IAgileSession session,
			IWFChangeStatusEventInfo objectEventInfo, Properties props) {

		log.info("Inside GDEVExpiredStatusValidation.doValidation():: Workflow status change RELEASED > EXPIRED");
	
	
		IDataObject deviationChangeObj;
		//boolean coreValidationFlag = false;
		try {
			deviationChangeObj = objectEventInfo.getDataObject();
			log.info("Validating for GDEV Object : "+ deviationChangeObj.getName());
			GDEVCreateCTOBO publishObj = new GDEVCreateCTOBO(session,
					objectEventInfo);
			publishObj.createAndSendCTO(deviationChangeObj,"Workflow Status Change To Expired");
			return "Success";

		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
			return "Error";
		}

		
	}

	
	
	
}
