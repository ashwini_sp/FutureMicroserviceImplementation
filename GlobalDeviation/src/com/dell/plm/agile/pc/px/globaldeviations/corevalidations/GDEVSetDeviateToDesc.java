package com.dell.plm.agile.pc.px.globaldeviations.corevalidations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.ChangeConstants;
import com.agile.api.IAgileSession;
import com.agile.api.ICell;
import com.agile.api.IDataObject;
import com.agile.api.IItem;
import com.agile.api.INode;
import com.agile.api.IRow;
import com.agile.api.ITable;
import com.agile.api.ItemConstants;
import com.agile.px.ActionResult;
import com.agile.px.EventActionResult;
import com.agile.px.EventConstants;
import com.agile.px.IEventAction;
import com.agile.px.IEventDirtyCell;
import com.agile.px.IEventDirtyRow;
import com.agile.px.IEventDirtyRowUpdate;
import com.agile.px.IEventDirtyTable;
import com.agile.px.IEventInfo;
import com.agile.px.IUpdateTableEventInfo;
import com.dell.plm.agile.common.util.CommonAglUtil;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;

public class GDEVSetDeviateToDesc implements IEventAction {

	
	private Logger log;


	@Override
	public EventActionResult doAction(IAgileSession session, INode actionNode,
			IEventInfo ieInfo) {
		log = GDevLogger.getLogger();
		log.info("Inside GDEVSetDeviateToDesc.doAction method");
		HashMap idMap = new HashMap();
		ArrayList idList = new ArrayList();
		
		try {

			IUpdateTableEventInfo iutInfo = (IUpdateTableEventInfo) ieInfo;

			IEventDirtyTable iedTable = iutInfo.getTable();

			Iterator itrTable = iedTable.iterator();

			String partDesc = null;

			while (itrTable.hasNext()) {
				IEventDirtyRow row = (IEventDirtyRow) itrTable.next();

				if (row.getAction() == EventConstants.DIRTY_ROW_ACTION_UPDATE) {

					IEventDirtyCell[] cells = ((IEventDirtyRowUpdate) row)
							.getCells();

					for (int i = 0; i < cells.length; i++) {

						String baseId = cells[i].getAttributeId().toString();

						if (baseId.equalsIgnoreCase("2069")) {

							if (cells[i].getValue() != null
									&& !cells[i].getValue().toString()
											.equalsIgnoreCase("")) {

								IItem partObj = (IItem) session.getObject(
										IItem.OBJECT_TYPE, cells[i].getValue()
												.toString());
								partDesc = (String) partObj
										.getValue(ItemConstants.ATT_TITLE_BLOCK_DESCRIPTION);

								int rowId = ((IEventDirtyRowUpdate) row)
										.getRowId();

								idMap.put(rowId, partDesc);
								idList.add(rowId);
								// ((IEventDirtyRowUpdate) row).setCell(
								// ChangeConstants.ATT_AFFECTED_ITEMS_MULTITEXT01,
								// partDesc);
							} else {
								int rowId = ((IEventDirtyRowUpdate) row)
										.getRowId();
								idList.add(rowId);
								idMap.put(rowId, "");
							}

						}

					}

				}
			}

			setValue(iutInfo, idMap, idList);

		} catch (APIException e) {
			log.error(CommonAglUtil.exception2String(e));

		}

		return new EventActionResult(ieInfo, new ActionResult(
				ActionResult.STRING, "Success"));
	}


	
	public void setValue(IUpdateTableEventInfo iutInfo, HashMap idMap1,
			ArrayList idList) throws APIException {

		IDataObject deviationObject = iutInfo.getDataObject();

		ITable table = deviationObject
				.getTable(ChangeConstants.TABLE_AFFECTEDITEMS);

		Iterator normalTable = table.iterator();

		while (normalTable.hasNext()) {
			IRow rowNew = (IRow) normalTable.next();
			String rowIds = rowNew.getId().toString();
			String[] arrStr = rowIds.split(":");

			for (int i = 0; i < idList.size(); i++) {
				if (idList.get(i).toString().equalsIgnoreCase(arrStr[1])) {

					//log.info("ID value matches :: " + idList.get(i));

					ICell cellDesc = rowNew
							.getCell(ChangeConstants.ATT_AFFECTED_ITEMS_MULTITEXT01);

					cellDesc.setValue(idMap1.get(idList.get(i)));

					log.info("Set Decription : Value is set ");
				}
			}

		}

	}

}
