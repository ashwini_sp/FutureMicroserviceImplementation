package com.dell.plm.agile.pc.px.globaldeviations.corevalidations;

import java.util.Properties;

import org.apache.log4j.Logger;

import com.agile.api.IAgileSession;
import com.agile.px.IWFChangeStatusEventInfo;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;

public class GDEVSubmittedStatusValidation extends GlobalDeviationsBO{
	
	private Logger log;

	public GDEVSubmittedStatusValidation()
	{
		log = GDevLogger.getLogger();
	}
	
	/**
	 * doValidation method for CCB status
	 */
	public String doValidation(IAgileSession session, IWFChangeStatusEventInfo eventInfo, Properties props) {
		// TODO Auto-generated method stub
		log.info("Inside GDEVSubmittedStatusValidation.doValidation() :: Workflow status change PENDING > SUBMITTED");
		
		return super.doValidation(session, eventInfo,props);
	}


}
