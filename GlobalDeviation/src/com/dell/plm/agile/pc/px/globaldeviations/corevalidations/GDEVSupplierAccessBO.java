package com.dell.plm.agile.pc.px.globaldeviations.corevalidations;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.ChangeConstants;
import com.agile.api.IAgileList;
import com.agile.api.IAgileSession;
import com.agile.api.ICell;
import com.agile.api.IDataObject;
import com.agile.api.IItem;
import com.agile.api.IRow;
import com.agile.api.ITable;
import com.agile.api.IUserGroup;
import com.agile.api.ItemConstants;
import com.agile.px.IWFChangeStatusEventInfo;
import com.dell.plm.agile.common.util.CommonAglUtil;
import com.dell.plm.agile.common.util.HTMLAttachmentFormat;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;
import com.dell.plm.agile.pc.px.globaldeviations.publishobjects.GDEVAttributeChangeEventBO;
import com.dell.plm.agile.pc.px.globaldeviations.publishobjects.GDEVCreateCTOBO;

public class GDEVSupplierAccessBO {

	
	
	private IWFChangeStatusEventInfo objectEventInfo;
	private IAgileSession session;
	private Logger log;
	private Properties props;
	public static boolean isWarning = false;
	public GDEVSupplierAccessBO(IAgileSession session,
			IWFChangeStatusEventInfo objectEventInfo) {
		// TODO Auto-generated constructor stub
		
		this.session = session;
		this.objectEventInfo = objectEventInfo;
		log = GDevLogger.getLogger();
		props = CommonAglUtil
		.loadProperties(GDEVConstants.GDEV_GLOBAL_DEVIATION_PROPERTIES);
		if (props == null)
			log.error("ERROR: Could not load the Properties file");

	}
	
	/**
	 * Implements Global Deviation Supplier Access
	 * @param deviationChangeObj 
	 * @return
	 */
	public boolean implementGDEVSA(IDataObject deviationChangeObj)
	{
		
		try {

			
			log.info("Implementing Supplier Access");
			ITable table = deviationChangeObj
					.getTable(ChangeConstants.TABLE_AFFECTEDITEMS);
			Iterator itr = table.iterator();
			ArrayList listSA = new ArrayList();
			while (itr.hasNext()) {
				IRow row = (IRow) itr.next();
				String platformNumberAffected = CommonAglUtil.getMultiListValue(
						row, ChangeConstants.ATT_AFFECTED_ITEMS_MULTILIST01);
				log.info("Supplier access >>>  "+platformNumberAffected);
				if(platformNumberAffected != null && !platformNumberAffected.equalsIgnoreCase("")  )
				{
					
					StringTokenizer strTok = new StringTokenizer(platformNumberAffected,";");
					
					
					while (strTok.hasMoreTokens())
					{
						String platformAffected = strTok.nextToken().toString();
						//log.info("Platform Value: "+platformAffected);
						getSAValues( platformAffected, listSA);
						
					}
					
					//log.info("Final SA Values:"+listSA.toString());
				
				}
			}
			
			log.info("List value FINAL is >>> "+listSA.toString());
			ArrayList<IUserGroup> userGroupList = new ArrayList<IUserGroup>();
			for (int i =0 ;i<listSA.size();i++)
			{
				IUserGroup usrGrp = (IUserGroup) session.getObject(
						IUserGroup.OBJECT_TYPE, listSA.get(i));
				
				log.info("Usergrp name is : "+usrGrp.getName());
				userGroupList.add(usrGrp);
			}
			implementSA(deviationChangeObj,userGroupList);
		} catch (APIException e) {
			log.error(CommonAglUtil.exception2String(e));
			
		}
		return false;
	}
	
	/**
	 * Get Supplier access values
	 * @param platformAffected
	 * @param listSA
	 */
	private void getSAValues(String platformAffected, ArrayList listSA) {
		// TODO Auto-generated method stub

		try {
			IItem platformNumberObj = (IItem) session.getObject(
					IItem.OBJECT_TYPE, platformAffected);

			IAgileList list = (IAgileList) platformNumberObj
					.getValue(ItemConstants.ATT_PAGE_TWO_MULTILIST04);

			

			String existingValues = (String) list.toString();
			log.info("Existing values are: "+existingValues);
			StringTokenizer Strtoken = new StringTokenizer(existingValues, ";");
			while (Strtoken.hasMoreTokens()) {

				//usergroup = (IUserGroup) session.getObject(
						//IUserGroup.OBJECT_TYPE, Strtoken.nextToken());
				String usergroup = Strtoken.nextToken();
				log.info("usergroup: " + usergroup);
				if(!listSA.contains(usergroup))
					listSA.add(usergroup);

			}

			log.info("Supplier Access values:" + listSA.toString());

		} catch (APIException e) {
			log.error(CommonAglUtil.exception2String(e));
		}
	}

	/**
	 * Set Supplier Access value for the Global Deviation Object
	 * @param deviationChangeObj
	 * @param supplierAccessList
	 */
	private void implementSA(IDataObject deviationChangeObj,
			ArrayList supplierAccessList) {
		// TODO Auto-generated method stub
		try {

			ICell cell = deviationChangeObj
					.getCell(ChangeConstants.ATT_PAGE_TWO_MULTILIST04);

			IAgileList valuesList = cell.getAvailableValues();

			valuesList.setSelection(supplierAccessList.toArray());

			cell.setValue(valuesList);

		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
		}
	}

	/**
	 * Reset the override Validation check value to NO
	 * 
	 * @param deviationChangeObj
	 */
	public void resetOverrideValidationCheck(IDataObject deviationChangeObj) {
		// TODO Auto-generated method stub
		try {
			String workflowStatus = objectEventInfo.getToStatus().toString();
			if(!workflowStatus.equalsIgnoreCase("Released"))
			{
				log.info("Reset Overriding Validation Check attribute to NO");
				ICell overrideValidateCheckCell = deviationChangeObj
						.getCell(ChangeConstants.ATT_PAGE_THREE_LIST03);

				overrideValidateCheckCell.setValue("NO");
				
			}
			
		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
		}
	}

	/*public void changeStatusToExpiredTest(IDataObject deviationChangeObj) {
		// TODO Auto-generated method stub
		
		try {
			String deviationStatus = CommonAglUtil.getListValueFromDataObject(deviationChangeObj,ChangeConstants.ATT_PAGE_TWO_LIST03).toString();
			//String Status = deviationChangeObj.getValue(ChangeConstants.ATT_COVER_PAGE_STATUS).toString();
			ICell cell = deviationChangeObj.getCell(ChangeConstants.ATT_PAGE_TWO_LIST03);
			Date effectivityFrom = (Date)deviationChangeObj.getValue(ChangeConstants.ATT_COVER_PAGE_EFFECTIVE_FROM);
			
			Date date = new Date();
			Date effectivityTo = (Date)deviationChangeObj.getValue(ChangeConstants.ATT_COVER_PAGE_EFFECTIVE_TO);
			
			if (date.compareTo(effectivityTo) > 0) {

				cell.setValue("OFF");
				((IChange) deviationChangeObj).changeStatus(
						((IChange) deviationChangeObj).getDefaultNextStatus(),
						true, "EXPIRED", false, false, new IUser[0], null,
						null, false, "");
			}
			
		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
		}
		
		
		
	}*/

	public void sendCTO(IDataObject deviationChangeObj) throws Exception {
		try {
			String workflowStatus = objectEventInfo.getToStatus().toString();
			//Old Condition : if(workflowStatus.equalsIgnoreCase("Released") || workflowStatus.equalsIgnoreCase("Expired") )
			if(workflowStatus.equalsIgnoreCase("Released"))
			{
				GDEVAttributeChangeEventBO attChangeEventBo= new GDEVAttributeChangeEventBO();
				attChangeEventBo.attributeChangeEventBo(session, deviationChangeObj);
				
				GDEVCreateCTOBO publishObj = new GDEVCreateCTOBO(session,objectEventInfo);
				publishObj.createAndSendCTO(deviationChangeObj,"Workflow Status Change To Released");
				
				
			}
			else if(workflowStatus.equalsIgnoreCase("Expired"))
			{
				GDEVCreateCTOBO publishObj = new GDEVCreateCTOBO(session,objectEventInfo);
				publishObj.createAndSendCTO(deviationChangeObj,"Workflow Status Change To Released");
			}
			
		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
		}
		
	}
	
	/**
	 * Add Attachment to the deviation object attachment tab
	 */
	public void addAttachment(IDataObject deviationChangeObj) {

		log.info("adding Attachment in attachment tab");
		ITable attachmentTable;
		try {
			String attachment = props
					.getProperty(GDEVConstants.GDEV_VALID_ATTACHMENT_PATH)
					+ "\\" + deviationChangeObj.getName() + ".html";
			log.info("Attachment Path :" + attachment);

			if (getIsWarning()) {
				HTMLAttachmentFormat
				.generateHTMLMessage(GDEVConstants.GDEV_ATTACHMENT_WARNING_MESSAGE);
		
				attachmentTable = (ITable) deviationChangeObj
						.getTable(ChangeConstants.TABLE_ATTACHMENTS);

				String GDEVObjNumber = deviationChangeObj.getName();

				// ttChangeFiles
				HashMap attachmentMap = new HashMap();

				attachmentMap.put(ChangeConstants.ATT_ATTACHMENTS_FILE_NAME,
						attachment);
				attachmentMap.put(
						ChangeConstants.ATT_ATTACHMENTS_FILE_DESCRIPTION,
						"Warning");

				boolean isAttached = false;
				int intLoop = 1;
				IRow attachmentRow = null;
				while (intLoop < 3 && !isAttached) {
					try {
						log
								.info(GDEVObjNumber + ":"
										+ "Trying to attach file. Tentative "
										+ intLoop);
						attachmentRow = attachmentTable
								.createRow(attachmentMap);
						isAttached = true;
					} catch (APIException apie) {
						intLoop++;
						if (intLoop == 3) {
							throw apie;
						}
					}
					attachment = null;
				}
				attachmentTable = null;
				attachmentMap = null;
				log.info(GDEVObjNumber + ":" + "File attached with success.");
			}
		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
		}

	}

	public static void setIsWarning(boolean status){
		isWarning = status;
	}
	
	public static boolean getIsWarning(){
		return isWarning;
	}
}
