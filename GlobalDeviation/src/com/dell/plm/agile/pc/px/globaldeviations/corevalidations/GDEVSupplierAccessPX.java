package com.dell.plm.agile.pc.px.globaldeviations.corevalidations;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.IAgileSession;
import com.agile.api.IDataObject;
import com.agile.api.INode;
import com.agile.px.ActionResult;
import com.agile.px.EventActionResult;
import com.agile.px.IEventAction;
import com.agile.px.IEventInfo;
import com.agile.px.IWFChangeStatusEventInfo;
import com.dell.plm.agile.common.util.CommonAglUtil;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;
import com.dell.plm.agile.pc.px.globaldeviations.publishobjects.GDEVAttributeChangeEventBO;

public class GDEVSupplierAccessPX implements IEventAction {

	private Logger log;
	@Override
	public EventActionResult doAction(IAgileSession session, INode arg1,
			IEventInfo eventInfo) {
		// TODO Auto-generated method stub
		
		log = GDevLogger.getLogger();
		log.info("Inside Supplier Access PX");
		IWFChangeStatusEventInfo objectEventInfo = (IWFChangeStatusEventInfo) eventInfo;
		GDEVSupplierAccessBO SABO = new GDEVSupplierAccessBO(session,objectEventInfo);
		IDataObject deviationChangeObj = null;
		try {
			deviationChangeObj = objectEventInfo.getDataObject();

			SABO.addAttachment(deviationChangeObj);
			
			SABO.resetOverrideValidationCheck(deviationChangeObj);
			
			boolean isSAImplemented = SABO.implementGDEVSA(deviationChangeObj);

			
			SABO.sendCTO(deviationChangeObj);
			
			//if (isSAImplemented)
				return new EventActionResult(eventInfo, new ActionResult(
						ActionResult.STRING, "Implemented Suppier Access"));
		//	else
			//	return new EventActionResult(eventInfo, new ActionResult(
				//		ActionResult.STRING, "Could not implement SA"));

		} catch (APIException e) {
			log.error(CommonAglUtil.exception2String(e));
			return new EventActionResult(eventInfo, new ActionResult(
					ActionResult.EXCEPTION, e));

		}
		catch (Exception ex) {
			log.error(CommonAglUtil.exception2String(ex));
			ex.printStackTrace();
			 return new EventActionResult(eventInfo , new ActionResult(ActionResult.EXCEPTION, ex));
			
		}

	}

	
	
}
