package com.dell.plm.agile.pc.px.globaldeviations.corevalidations;

public class GDevWFFactory {

	
	public static GlobalDeviationsBO getFactoryClass(String workflowStatus)
	{
		if(workflowStatus.equalsIgnoreCase(GDEVConstants.GDEV_SUBMITTED_STATUS))
			return new GDEVSubmittedStatusValidation();
		else if(workflowStatus.equalsIgnoreCase(GDEVConstants.GDEV_CCB_STATUS))
			return new GDEVCCBStatusValidation();
		else if (workflowStatus.equalsIgnoreCase(GDEVConstants.GDEV_RELEASED_STATUS))
			return new GDEVReleasedStatusValidation();
		else if (workflowStatus.equalsIgnoreCase(GDEVConstants.GDEV_EXPIRED_STATUS))
			return new GDEVExpiredStatusValidation();
		
		return null;
	}
	
}
