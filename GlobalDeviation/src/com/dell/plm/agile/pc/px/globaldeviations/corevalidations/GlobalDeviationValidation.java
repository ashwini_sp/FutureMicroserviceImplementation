package com.dell.plm.agile.pc.px.globaldeviations.corevalidations;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import oracle.jdbc.OracleTypes;
import oracle.sql.DATE;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.ChangeConstants;
import com.agile.api.IAdmin;
import com.agile.api.IAgileClass;
import com.agile.api.IAgileSession;
import com.agile.api.IAttribute;
import com.agile.api.ICell;
import com.agile.api.IChange;
import com.agile.api.IDataObject;
import com.agile.api.IItem;
import com.agile.api.IQuery;
import com.agile.api.IRow;
import com.agile.api.ITable;
import com.agile.api.IUser;
import com.agile.api.ItemConstants;
import com.agile.api.QueryConstants;
import com.agile.px.IWFChangeStatusEventInfo;
import com.dell.plm.agile.common.util.CommonAglUtil;
import com.dell.plm.agile.common.util.HTMLAttachmentFormat;
import com.dell.plm.agile.common.util.MailUser;
import com.dell.plm.agile.dellcommon.PropertiesLoader;
import com.dell.plm.agile.dellcommon.cryptography.FileEncryptorUtil;
import com.dell.plm.agile.dellcommon.cryptography.StringEncryptor;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;



public class GlobalDeviationValidation {

	private IAgileSession session;
	private IWFChangeStatusEventInfo eventInfo;
	private Logger log;
	private StringBuffer logMessage = new StringBuffer();
	private String itemPartType;
	private String deviateToPartType;
	private String itemPartClass;
	private String deviateToPartClass;
	private String platformNumberAffected;
	private IDataObject deviationChangeObj;
	private String itemNumberDesc;
	private String deviateToDesc;
	private String deviateFFRegion;
	private Properties props;
	private String revisionError = "";
	private String partTypeError = "";
	private String revisionWarning = "";
	private String isPHStructuredWarning = "";
	private String CFIParts = "" ;
	private String duplicateReleasedDeviationCheck = "";
	private String duplicateGDEVNumber = "";
	private String globalWarningFlag = "";
	private boolean isWarning = false;
	private String globalErrorFlag = "";
	private boolean isError = false;
	private String phantomItemNumber = "";
	private String identicalParts = "";
	private boolean noneToPartFlag;
	
	//added by Guna
	private static String dbUrl = "";
	private static String dbUserName = "";
	private static String dbPassword = "";
	private static String procToGetPH = "";
	public GlobalDeviationValidation()
	{

	}

	public GlobalDeviationValidation(IAgileSession session , IWFChangeStatusEventInfo eventInfo)
	{
		log = GDevLogger.getLogger();
		this.session = session;
		this.eventInfo = eventInfo;
		loadPropertiesFile();
	}

	private void loadPropertiesFile() {
		// TODO Auto-generated method stub

		props = CommonAglUtil
				.loadProperties(GDEVConstants.GDEV_GLOBAL_DEVIATION_PROPERTIES);
		if (props == null)
			log.error("ERROR: Could not load the Properties file");
		
		//Added by Guna
		//to fix PH related issue
		//new property file is added
		try{
			FileEncryptorUtil.encryptFile(props.getProperty(GDEVConstants.GDEV_GLOBAL_DEVIATION_DB_PROPERTIES));
			PropertiesLoader.loadAbsolute(props.getProperty(GDEVConstants.GDEV_GLOBAL_DEVIATION_DB_PROPERTIES));
			
					
			dbUrl = PropertiesLoader.getProperty(GDEVConstants.GDEV_PROC_URL).trim(); 
			log.info("dbUrl:"+dbUrl);
			dbUserName = PropertiesLoader.getProperty(GDEVConstants.GDEV_PROC_USER).trim(); 
			log.info("dbUserName:"+dbUserName);
			dbPassword = PropertiesLoader.getProperty(GDEVConstants.GDEV_PROC_PWD).trim(); 
			//System.out.println("dbPassword:"+dbPassword);
			//log.info("dbPassword:"+dbPassword);
			dbPassword = StringEncryptor.decrypt(dbPassword);
			//System.out.println("dbPassword:"+dbPassword);
			//log.info("dbPassword:"+dbPassword);
			procToGetPH = props.getProperty(GDEVConstants.GDEV_PROC);
			log.info("procToGetPH:"+procToGetPH);
		}catch (Exception ex){
			log.info("Entered in Exception loadPropertiesFile() ");
			log.error(ex.toString());
			log.error(CommonAglUtil.exception2String(ex));
		}

	}


	public boolean validatePH(String itemNumberPart , String deviateToPart) {

		log.info("Where used PH validation:");
		String platformNumber = getPlatformNumberAffected();

		log.info("Platform number affected : "+ platformNumber);


		if (platformNumber == null || platformNumber.equalsIgnoreCase(""))
		{
			// Assuming that Platform Number Affected can be null only in Pending status.
			// For other statuses , configuration will not allow to trigger this PX.
			return true;
		}
		else
		{
			//commented out following lines 
			//String itemNumberTopLevelPH = getWhereUsedPlatformHierarchy(itemNumberPart);
			//String deviateToTopLevelPH = getWhereUsedPlatformHierarchy(deviateToPart);
			//Fix to solve PH related issues
			String itemNumberTopLevelPH = getWhereUsedPlatformHierarchy(itemNumberPart, true);
			String deviateToTopLevelPH = getWhereUsedPlatformHierarchy(deviateToPart, true);
			
			log.info("Item number PH:"+itemNumberTopLevelPH);
			log.info("deviate TO: "+deviateToTopLevelPH);

			boolean isItemNumberStructured = isPlatformStructured(
					platformNumber, itemNumberTopLevelPH, itemNumberPart);

			boolean isDeviateTONumberStructured = isDeviateToPlatformStructured(
					platformNumber, deviateToTopLevelPH, deviateToPart);

			/*
			 * if(isItemNumberStructured && isDeviateTONumberStructured) { //
			 * Implement Supplier Access for GD //implementSA(platformNumber);
			 * return true; } else if (!isItemNumberStructured &&
			 * isDeviateTONumberStructured) { log.error("Item Number :
			 * "+itemNumberPart+" is not structured with "+ platformNumber);
			 * return false; } else if (!isDeviateTONumberStructured &&
			 * isItemNumberStructured) { log.error("Deviate To Number :
			 * "+deviateToPart+" is not structured with "+ platformNumber);
			 * return false; } else {
			 *
			 * log.error("Item Number : "+itemNumberPart+" and Deviate To Number :
			 * "+deviateToPart+" is not structured with "+ platformNumber);
			 * return false;
			 *
			 *  }
			 */

			if (isItemNumberStructured && isDeviateTONumberStructured)
				return true;
			else {
				globalWarningFlag += "true"; // Changing it to warning msg
				return true;
			}
		}
	}

	/**
	 * check if Item number is Structured
	 * @param platformNumber
	 * @param partTopLevelPH
	 * @param itemNumberPart
	 * @return
	 */
	private boolean isPlatformStructured(String platformNumber,
			String partTopLevelPH , String itemNumberPart) {

		StringBuffer isStructured  = new StringBuffer();
		StringTokenizer tok = new StringTokenizer(platformNumber, ";");

		while (tok.hasMoreElements()) {
			String PHNumberAffected = tok.nextToken();
			if (partTopLevelPH.contains(PHNumberAffected)) {
				isStructured.append("true");
			}
			else
			{

				isStructured.append("false");
				log.error("Item Number : " + itemNumberPart
						+ " is not structured with " + PHNumberAffected);

				// writing the error to the Attachment string object
				isPHStructuredWarning += "[" + itemNumberPart
						+ " , " + PHNumberAffected + "],";



			}

		}
		if(isStructured.toString().contains("false"))
		{
			return false;
		}
		return true;
	}

	/**
	 * check if Deviate To part is structured
	 * @param platformNumber
	 * @param partTopLevelPH
	 * @param deviateToPart
	 * @return
	 */
	private boolean isDeviateToPlatformStructured(String platformNumber,
			String partTopLevelPH , String deviateToPart) {

		StringBuffer isStructured  = new StringBuffer();
		StringTokenizer tok = new StringTokenizer(platformNumber, ";");

		while (tok.hasMoreElements()) {
			String PHNumberAffected = tok.nextToken();
			if (partTopLevelPH.contains(PHNumberAffected) || isPartToNoneScenario(deviateToPart)) {
				isStructured.append("true");
			}
			else
			{

				isStructured.append("false");
				log.error("Item Number : " + deviateToPart
						+ " is not structured with " + PHNumberAffected);

				// writing the error to the Attachment string object
				isPHStructuredWarning += "[" + deviateToPart
						+ " , " + PHNumberAffected + "],";


			}

		}
		if(isStructured.toString().contains("false"))
		{
			return false;
		}
		return true;
	}
	
	/**
	 * IQuery is used to get PH items at all level 
	 * Query is taking long time and page is getting expired in production
	 * 
	 * To fix the issue, work around is done by using ADM SQL procedure
	 * 
	 * Over loaded method to get PH items
	 * 
	 * @param part
	 * @param fromDirectDB
	 * @return String with all PHs
	 * 
	 * @author Gunapati_Reddy
	 */
	
	public String getWhereUsedPlatformHierarchy(String part, boolean fromDirectDB){
		log.info("Got into new getWhereUsedPlatformHierarchy, overloaded method");
		Connection  con = null;
		StringBuffer whereUsedPH = new StringBuffer();
		try{
			//con = getDBConnection (dbUrl, dbUserName, dbPassword);
			con = getDBConnection ();
			CallableStatement stmt =con.prepareCall("{call "+procToGetPH +"(?,?)}");
			stmt.setString(1, part);
			stmt.registerOutParameter(2, OracleTypes.CURSOR);
			stmt.executeQuery();
			ResultSet rs = (ResultSet) stmt.getObject(2);
			
			int rowCount = 0;
			while(rs.next()){
				log.info(rs.getString(1));
				whereUsedPH.append(rs.getString(1));
				rowCount++;
			}
			
			log.info("ph resultset iterator sizee --->>"+rowCount);
			return whereUsedPH.toString();
		}catch(Exception exe){
			exe.printStackTrace();
			log.error(CommonAglUtil.exception2String(exe));
			return null;
			
		}
		finally{
			if(con!=null){
				try{
					log.info("Connection is closed properly");
					//if(!con.isClosed()){
						
						con.close();
					//}
					con=null;
				}catch(Exception ex){
					log.error(CommonAglUtil.exception2String(ex));
					
				}
			}
		}
		
		
	}
	
	/**
	 * Establishes the connection with ADM schema with given user name and password
	 * 
	 * @return Connection
	 * @throws SQLException
	 * 
	 * @author Gunapati_Reddy
	 */
	public Connection getDBConnection() throws SQLException
	{
		Connection conn = null;
		DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		conn = DriverManager.getConnection(dbUrl,dbUserName,dbPassword);

		return conn;
	}

	/**
	 * Get where used platforms Hierarchy
	 * @param affectedPart
	 * @return
	 */
	public String getWhereUsedPlatformHierarchy(String affectedPart)
	{

		StringBuffer whereUsedPH = new StringBuffer();
		try {

			ITable results = getWhereUsedItems(session, affectedPart);

			//log.info("WHERE USED TABLE RESULTS: " + results.size());

			results.setPageSize(results.size()+100);

			Iterator itr1 = results.iterator();

			String sPartClass = "";
			String sPartType = "";
			String sPlatCode = "";

			while (itr1.hasNext()) {
				IRow rowData = (IRow) itr1.next();

				if (rowData != null || !rowData.toString().trim().equalsIgnoreCase("")) {

					ICell[] cells = rowData.getCells();
					sPartClass = cells[3].toString();
					sPartType = cells[4].toString();
					//log.info("Where used items: " + rowData.toString());
					//log.info("Where used sPartType: " + sPartType);
					//log.info("parttyoe :"+ cells[4].toString()+" value :"+cells[0].toString());

					//log.info("Is Structured::::::::::::::::::: "+sPartType.equalsIgnoreCase(GDEVConstants.GDEV_PLATFORM_HIERARCHY));
					if (sPartType.equalsIgnoreCase(GDEVConstants.GDEV_PLATFORM_HIERARCHY) && sPartClass.equalsIgnoreCase(GDEVConstants.GDEV_PLATFORM_HIERARCHY)) {
						 whereUsedPH.append(cells[0].toString());

					}
				}
			}
			//log.info("PH Value:"+whereUsedPH.toString());
			results = null;
			return whereUsedPH.toString();
		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
			return null;

		}

	}


	/**
	 * Get Where used results for parts
	 * @param m_session
	 * @param affectedItemNumber
	 * @return
	 */
	private ITable getWhereUsedItems(IAgileSession m_session,
			String affectedItemNumber)

	{

		try {

			IQuery query =
			(IQuery) m_session.createObject(IQuery.OBJECT_TYPE,
					ItemConstants.CLASS_ITEM_BASE_CLASS);

			// Set the where-used type
			query.setSearchType(QueryConstants.WHERE_USED_ALL_LEVEL);
			query.setCaseSensitive(false);

			query.setCriteria("[Title Block.Number] == '" + affectedItemNumber
					+ "'");

			setQueryResultColumns(query);

			ITable results = query.execute();
			//log.info("Table size >> : " + results.size());
			//query.delete();
			return results;

		} catch (APIException ex) {

			log.error(CommonAglUtil.exception2String(ex));

			return null;

		}

	}

	/**
	 * Filter the Where used results
	 * @param query
	 * @throws APIException
	 */
	private void setQueryResultColumns(IQuery query) throws APIException {

		// Get Admin instance
		IAdmin admin = session.getAdminInstance();
		// Get the Part class
		IAgileClass cls = admin.getAgileClass(ItemConstants.CLASS_PART);
		// Get Part attributes
		IAttribute attr1 = cls
				.getAttribute(ItemConstants.ATT_TITLE_BLOCK_NUMBER);
		IAttribute attr2 = cls
				.getAttribute(ItemConstants.ATT_TITLE_BLOCK_DESCRIPTION);
		IAttribute attr3 = cls
				.getAttribute(ItemConstants.ATT_TITLE_BLOCK_LIFECYCLE_PHASE);
		IAttribute attr4 = cls
				.getAttribute(ItemConstants.ATT_TITLE_BLOCK_ITEM_TYPE);
		IAttribute attr5 = cls
				.getAttribute(ItemConstants.ATT_TITLE_BLOCK_ITEM_CATEGORY);
		IAttribute attr6 = cls.getAttribute(ItemConstants.ATT_TITLE_BLOCK_REV);
		IAttribute attr7 = cls
				.getAttribute(ItemConstants.ATT_TITLE_BLOCK_PRODUCT_LINES);

		IAttribute[] attrs = { attr1, attr2, attr3, attr4, attr5, attr6, attr7 };
		// Set the result attributes for the query
		query.setResultAttributes(attrs);
	}

	/**
	 * Validate parts Revision
	 * @param itemNumberPart
	 * @param deviateToPart
	 * @return
	 */
	public boolean checkRevision(IItem itemNumberPart, IItem deviateToPart) {

		try {

			//log.info("Checking Revision");

			String itemNumberRev = itemNumberPart.getRevision();
			String deviateToRev = deviateToPart.getRevision();
			Vector isError = new Vector();
			boolean errorFlag = false;

			// Validate Item number Revision. Item Number part revision is
			// already present in the AI table so passing the same.
			log.info("Validating Item Number Revision.....");

			errorFlag = validateRev(itemNumberRev, itemNumberPart);
			isError.add(errorFlag);
			// Validate Deviate To Revision.Devaite To part revision is not
			// present in the AI table so passing the the part object.
			log.info("Validating Deviate To Revision......");


			errorFlag = validateRev(deviateToRev, deviateToPart);
			isError.add(errorFlag);

			if (isError != null && isError.contains(Boolean.FALSE))
			{

				return false;
			}
			else
				return true;

		} catch (APIException e) {
			log.error(CommonAglUtil.exception2String(e));
			return false;
		}

	}

	/**
	 * Segregate the validation in error , warning and success
	 * @param partRevision
	 * @param partNumberObject
	 * @return
	 */
	private boolean validateRev(String partRevision, IItem partNumberObject) {

		boolean isValidRevision = false;
		try {
			if (partRevision.startsWith("A")) {
				log.info("Part Revision is A");
				logMessage.append("Part Revision is A");
				isValidRevision = true;

			} else if (partRevision.startsWith("X")) {
				log.warn("For " + partNumberObject.getName()
						+ " Revision is X. Do you want to continue ?");
				revisionWarning += partNumberObject.getName() + ",";
				globalWarningFlag += "true";
				isValidRevision = true;
			} else {
				log
						.error("For "
								+ partNumberObject.getName()
								+ " Revision should be A OR X. Please choose other part");
				revisionError += partNumberObject.getName() + ",";
				globalErrorFlag += "true";
				isValidRevision = false;

			}

			return isValidRevision;
		} catch (APIException e) {

			log.error(CommonAglUtil.exception2String(e));

		}
		return isValidRevision;

	}


	/**
	 * Validating Part Type
	 * @param itemNumberObj
	 * @param deviateToObj
	 * @return
	 * @throws APIException
	 */
	public boolean validatePartType(IItem itemNumberObj,
			IItem deviateToObj)
			throws APIException {
		// TODO Auto-generated method stub
		log.info("Validating Part Types....");
		// Getting the Item Number Part Type from AI table
		String itemNumberPartType = getItemNumberPartType();

		// Getting the deviate To part type from Deviate To Part object
		String deviateToType = getDeviateToPartType();

		boolean isPartTypeValid = true;

		log.info("Checking for Part to None and None to Part scenario");

		if(!getNoneToPartScenarioFlag() && !isPartToNoneScenario(deviateToObj.getName()))
		{
			//New Change  15 March 2010
			String deviateToDescription = getDeviateToDesc();
			String itemDesc = getItemNumberDesc();
			String otherNonePartDesc = props.getProperty(GDEVConstants.GDEV_OTHER_NONE_PART_DESC);
			boolean excludedDescValue=false;
			ArrayList<String> nonePartDescList = CommonAglUtil.getListFromString(otherNonePartDesc, "::");
			for(int desc=0;desc<nonePartDescList.size();desc++){
				String descValue = nonePartDescList.get(desc).trim();
				if(deviateToDescription.startsWith(descValue) ||itemDesc.startsWith(descValue) ){
					excludedDescValue=true;
				}
			}

			if(excludedDescValue==false){
				 //edits to turn off part type validations for the Fulfillment Region impacted = "North America Fulfillment Centers"
				/*if (!itemNumberPartType.equalsIgnoreCase(deviateToType)) {
					isPartTypeValid = false;
					log.error(showPartTypeError(itemNumberObj, deviateToObj));
					partTypeError += "[" + itemNumberObj.getName() + ","+deviateToObj.getName()+"],";
					globalErrorFlag += "true";
				}*/
				
				String[] region=props.getProperty(GDEVConstants.GDEV_SKIP_PARTTYPE_VALIDATION).split(";");
				
				for(int r=0;r<region.length;r++)
				{
				if(!region[r].equalsIgnoreCase(getFulfillmentRegions()))
					{
							if (!itemNumberPartType.equalsIgnoreCase(deviateToType)) 
							{
								isPartTypeValid = false;
								break;
							}
					}
				
				}			
				
				if(!isPartTypeValid)
				{
					log.error(showPartTypeError(itemNumberObj, deviateToObj));
					partTypeError += "[" + itemNumberObj.getName() + ","+deviateToObj.getName()+"],";
					globalErrorFlag += "true";
				}
				
			}
		}
		else if (isPartToNoneScenario(deviateToObj.getName()))
				{
					isPartTypeValid = true;

				}

		return isPartTypeValid;
	}

	/**
	 * Display Part Type validation error
	 * @param itemNumberObj
	 * @param deviateToObj
	 * @return
	 * @throws APIException
	 */
	private String showPartTypeError(IItem itemNumberObj, IItem deviateToObj)throws APIException {
		// TODO Auto-generated method stub
		return "ERROR: Item Number:"+itemNumberObj.getName() +"$ Deviate To : "+deviateToObj.getName()+" Part Type do not match. Validation failed!!!";
	}

	/**
	 * Validate CFI parts
	 * @param row
	 * @return
	 */
	public boolean isCFI(String itemNumber, String deviateToPart) {
		// TODO Auto-generated method stub

		log.info("Validating CFI/NON CFI Parts");

		String itemNumberPartClass = getItemNumberPartClass();
		String deviateToPartClass = getDeviateToPartClass();
		String itemNumberPartType = getItemNumberPartType();
		String deviateToPartType = getDeviateToPartType();

		Vector<Boolean> isError = new Vector<Boolean>();


		if (itemNumberPartClass
				.startsWith(GDEVConstants.GDEV_CFI_PART_CLASS_PREFIX)) {

			if (itemNumberPartType
					.startsWith(GDEVConstants.GDEV_CFI_PART_TYPE_PREFIX)) {

				log.error(itemNumber + " is a CFI part. Please remove it");
				CFIParts += itemNumber+",";
				globalErrorFlag += "true";
				isError.add(false);

			}
		}
		if (deviateToPartClass
				.startsWith(GDEVConstants.GDEV_CFI_PART_CLASS_PREFIX)) {
			if (deviateToPartType
					.startsWith(GDEVConstants.GDEV_CFI_PART_TYPE_PREFIX)) {
				CFIParts += deviateToPart+",";
				isError.add(false);
				globalErrorFlag += "true";
				log.error(deviateToPart + " is a CFI part. Please remove it");

			}
		}

		if (isError != null && isError.contains(Boolean.FALSE)) {
			return false;
		} else
			return true;

	}


	/**
	 * Set all required attributes for the validations
	 * @param deviationChangeObj
	 * @param row
	 * @param itemNumberObj
	 * @param deviateToObj
	 */
	public void setAllRequiredAttributes(IDataObject deviationChangeObj, IRow row, IItem itemNumberObj,
			IItem deviateToObj) {
		// TODO Auto-generated method stub

		setDeviationObj(deviationChangeObj);
		setItemNumberPartClass(row);
		setDeviateToPartType(deviateToObj);
		setItemNumberPartType(itemNumberObj);
		setDeviateToPartClass(deviateToObj);
		setPlatformNumberAffected(row);
		setItemNumberDesc(row);
		setDeviateToDesc(row);
		setFulFillmentRegion(deviationChangeObj);

	}

	/**
	 * Set Deviate To Part Desc
	 * @param row
	 */
	private void setDeviateToDesc(IRow row) {
		// TODO Auto-generated method stub
		try {
			deviateToDesc = row.getValue(ChangeConstants.ATT_AFFECTED_ITEMS_MULTITEXT01).toString();
		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
		}
	}

	/**
	 * Set Deviation Fulfillment Region
	 * Added for CR - 7535
	 * @param deviationChangeObj
	 */
	private void setFulFillmentRegion(IDataObject deviationChangeObj) {
		// TODO Auto-generated method stub
		try {
			deviateFFRegion = deviationChangeObj.getValue(ChangeConstants.ATT_PAGE_TWO_MULTILIST09).toString();
		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
		}
	}

	/**
	 * Set Item Number Description
	 * @param row
	 */
	private void setItemNumberDesc(IRow row) {
	// TODO Auto-generated method stub
	try {
		itemNumberDesc = row.getValue(ChangeConstants.ATT_AFFECTED_ITEMS_ITEM_DESCRIPTION).toString();
	} catch (APIException e) {
		// TODO Auto-generated catch block
		log.error(CommonAglUtil.exception2String(e));
	}
}

	/**
	 * Set Deviate To part object
	 * @param deviationChangeObj
	 */
	public void setDeviationObj(IDataObject deviationChangeObj) {
		// TODO Auto-generated method stub
		this.deviationChangeObj = deviationChangeObj;
	}

	/**
	 * Set Platform Number affected
	 * @param row
	 */
	public void setPlatformNumberAffected(IRow row) {
		// TODO Auto-generated method stub
		platformNumberAffected = CommonAglUtil.getMultiListValue(row,
				ChangeConstants.ATT_AFFECTED_ITEMS_MULTILIST01);

	}

	/**
	 * Get Platform Number Affected
	 * @return platformNumberAffected
	 */
	public String getPlatformNumberAffected() {
		return platformNumberAffected;
	}

	/**
	 * Added for CR - 7535
	 * Get Platform Number Affected
	 * @return platformNumberAffected
	 */
	public String getFulfillmentRegions() {
		return deviateFFRegion;
	}

	/**
	 * Get Item Number Part Type
	 * @return itemPartType
	 */
	public String getItemNumberPartType() {
		return itemPartType;
	}

	/**
	 * Get Deviate To Part Type
	 * @return deviateToPartType
	 */
	public String getDeviateToPartType() {
		return deviateToPartType;
	}

	/**
	 * Get Deviate To Part Type
	 * @return deviateToPartClass
	 */
	public String getDeviateToPartClass() {
		return deviateToPartClass;
	}

	/**
	 * Get Item Number part Class
	 * @return itemPartClass
	 */
	public String getItemNumberPartClass() {
		return itemPartClass;
	}

	/**
	 * Set Item Number Part Type
	 * @param row
	 */
	public void setItemNumberPartClass(IRow row) {
		itemPartClass = CommonAglUtil.getListValue(row,
				ChangeConstants.ATT_AFFECTED_ITEMS_ITEM_TYPE);
	}

	/**
	 * Set Item Number Part Class
	 * @param itemNumberObj
	 */
	public void setItemNumberPartType(IItem itemNumberObj) {
		itemPartType = CommonAglUtil.getListValueFromDataObject(itemNumberObj,
				ItemConstants.ATT_TITLE_BLOCK_ITEM_CATEGORY);

	}

	/**
	 * Set Deviate To Part Class
	 * @param deviateToObj
	 */
	public void setDeviateToPartClass(IItem deviateToObj) {
		deviateToPartClass = CommonAglUtil.getListValueFromDataObject(deviateToObj,
				ItemConstants.ATT_TITLE_BLOCK_ITEM_TYPE);
	}

	/**
	 * Set Deviate To Part Type
	 * @param deviateToObj
	 */
	public void setDeviateToPartType(IItem deviateToObj) {
		deviateToPartType = CommonAglUtil.getListValueFromDataObject(deviateToObj,
				ItemConstants.ATT_TITLE_BLOCK_ITEM_CATEGORY);
	}

	/**
	 * Validate Identical Parts in same row
	 * @param itemNumber
	 * @param deviateToPart
	 * @return boolean
	 */
	public boolean identicalPartsValidation(String itemNumber,
			String deviateToPart) {
		// TODO Auto-generated method stub
		log.info("Checking for identical parts");
		if(itemNumber.equalsIgnoreCase(deviateToPart))
		{
			log.info("Identical Parts validation failed!!! : "+itemNumber + " and " + deviateToPart + "are same");
			globalErrorFlag += "true";
			identicalParts = identicalParts + "[" + itemNumber
			+ " , " + deviateToPart + "],";
			return false;
		}
		/*else if(getNoneToPartScenarioFlag() && isPartToNoneScenario(deviateToPart))
		{
			log.error("None to None is not recommended.Please remove the parts: " +itemNumber +" and " + deviateToPart+" combination");
			globalErrorFlag += "true";
			//identicalParts = identicalParts + "[" + itemNumber
			//+ " , " + deviateToPart + "],";
			return false;
		}
			*/
		return true;

	}

	/**
	 * Get Item Number Description
	 * @return itemNumberDesc
	 */
	public String getItemNumberDesc() {
		// TODO Auto-generated method stub
		return itemNumberDesc;
	}

	/**
	 * Validate None to Part scenario
	 * @param itemNumber
	 * @return boolean
	 */
	public boolean isNoneToPartScenario(String itemNumber) {
		// TODO Auto-generated method stub

		log.info("Checking None to Part Scenario..");

		boolean flag = false;
		String itemPartClass = getItemNumberPartClass();
		String itemPartType = getItemNumberPartType();
		String itemDescription = getItemNumberDesc();

		ArrayList<String> nonePartClass = CommonAglUtil.getListFromString(props
				.getProperty(GDEVConstants.GDEV_NONE_PART_CLASS), ",");

		ArrayList<String> nonePartType = CommonAglUtil.getListFromString(props
				.getProperty(GDEVConstants.GDEV_NONE_PART_TYPE), ",");

		String nonePartDesc = props
				.getProperty(GDEVConstants.GDEV_NONE_PART_DESC);

		String noneModPart = props
		.getProperty(GDEVConstants.GDEV_NONE_MOD_PARTS);

		ArrayList<String> noneModPartList = CommonAglUtil.getListFromString(noneModPart, ",");

		if(nonePartClass.contains(itemPartClass)&& nonePartType.contains(itemPartType)&& itemDescription.startsWith(nonePartDesc))


		{
			log.error("Item Number :"+ itemNumber +" can not be a Phantom part.Please remove it");
			phantomItemNumber = phantomItemNumber  + "[" + itemNumber+ "] ,";
			flag = true;
			globalErrorFlag += "true";

		}
		else if (noneModPartList.contains(itemNumber)){
			log.error("Item Number :"+ itemNumber +" can not be a Phantom part.Please remove it");
			phantomItemNumber = phantomItemNumber  + "[" + itemNumber+ "] ,";
			flag = true;
			globalErrorFlag += "true";
		}

		//set the value so that other functions can use the value instead of calling this method everytime.
		setNoneToPartScenarioFlag (flag);
		return flag;
	}

	private void setNoneToPartScenarioFlag(boolean noneToPartFlag) {
		// TODO Auto-generated method stub
		this.noneToPartFlag = noneToPartFlag;
	}

	private boolean getNoneToPartScenarioFlag()
	{
		return noneToPartFlag;
	}

	/**
	 * Get Deviate To Description
	 * @return deviateToDesc
	 */
	private String getDeviateToDesc() {
		// TODO Auto-generated method stub
		return deviateToDesc;
	}

	/**
	 * Validate Part To None scenario
	 * @param deviateTo
	 * @return boolean
	 */
	public boolean isPartToNoneScenario(String deviateTo) {
		// TODO Auto-generated method stub

		log.info("Checking Part to None Scenario..");
		String deviateToPartClass = getDeviateToPartClass();
		String deviateToPartType = getDeviateToPartType();
		String deviateToDescription = getDeviateToDesc();

		ArrayList<String> nonePartClass = CommonAglUtil.getListFromString(props
				.getProperty(GDEVConstants.GDEV_NONE_PART_CLASS), ",");

		ArrayList<String> nonePartType = CommonAglUtil.getListFromString(props
				.getProperty(GDEVConstants.GDEV_NONE_PART_TYPE), ",");

		String nonePartDesc = props
				.getProperty(GDEVConstants.GDEV_NONE_PART_DESC);

		String noneModPart = props
		.getProperty(GDEVConstants.GDEV_NONE_MOD_PARTS);

		ArrayList<String> noneModPartList = CommonAglUtil.getListFromString(noneModPart, ",");


		//-----------------
		if(nonePartClass.contains(deviateToPartClass)&& nonePartType.contains(deviateToPartType)&& deviateToDescription.startsWith(nonePartDesc))

		{
			log.info("Deviate To Part: " + deviateTo + "is None");
			return true;
		}
		else if (noneModPartList.contains(deviateTo)){
			log.info("Deviate To Part: " + deviateTo + "is None");
			return true;
		}

		return false;
	}

	/**
	 * Validate Released Deviations
	 * @param itemNumberObj
	 * @param overrideValidateCheck
	 * @return boolean
	 * @throws APIException
	 */
	public boolean validateReleasedDeviations(IItem itemNumberObj) throws APIException{
		// TODO Auto-generated method stub
		log.info("Validating Associated Released Deviations for Item Number:");

		boolean hasReleasedDeviation = true;

		String associatedGDEVplatformNumberAffected = getAssociatedReleasedDeviationsPlatform(itemNumberObj);
		log.info("Associated (CHANGE HISTORY )GDEV Platform number is :"+ associatedGDEVplatformNumberAffected);

		String originalGDEVPlatformNumberAffected= getPlatformNumberAffected();
		//Start for CR -7535
		String originalGDEVFulfillmentRegions = getFulfillmentRegions();
		StringTokenizer strTokFF = new StringTokenizer(originalGDEVFulfillmentRegions , ";");
		//End for CR -7535

		log.info("Original GDEV Platform number is :"+ originalGDEVPlatformNumberAffected);
		log.info("Original GDEV FF region is :"+ originalGDEVFulfillmentRegions);

		//Start for CR - 7535
		while (strTokFF.hasMoreTokens())
		{
			String strFFRegions = strTokFF.nextToken().toString();

			if(associatedGDEVplatformNumberAffected != null
					&& !associatedGDEVplatformNumberAffected.equalsIgnoreCase(""))
			{
				StringTokenizer affectedPlatRegTok = new StringTokenizer(associatedGDEVplatformNumberAffected, "::");

				while (affectedPlatRegTok.hasMoreTokens())
				{
					String strAffectedPHRegion = affectedPlatRegTok.nextToken().toString();
					if(strAffectedPHRegion.contains(strFFRegions))
					{
						StringTokenizer strTok = new StringTokenizer(originalGDEVPlatformNumberAffected , ";");
						while (strTok.hasMoreTokens())
						{
							String platformAffected = (String)strTok.nextToken();


							if (strAffectedPHRegion
									.contains(platformAffected))
							{

								// Override validation check
								log.error(props.getProperty(itemNumberObj.getName()	+ " : "
										+ GDEVConstants.GDEV_ASSOCIATED_RELEASED_DEVIATION_ERROR));

								duplicateReleasedDeviationCheck += "["
										+ itemNumberObj.getName() + " , "
										+ platformAffected + ", " + strFFRegions + "],";
								globalErrorFlag += "true";
								hasReleasedDeviation = false;

							}
						}
					}
				}
			}
		}


		return hasReleasedDeviation;
	}

	/**
	 * Get valid associated Released Deviations for Item Number
	 * @param itemNumberObj
	 * @return
	 */
	public String getAssociatedReleasedDeviationsPlatform (IItem itemNumberObj)
    {

		try {
			ITable changeHistoryTable = itemNumberObj
					.getTable(ItemConstants.TABLE_CHANGEHISTORY);

			Iterator changeTableItr = changeHistoryTable.iterator();

			ArrayList<String> pendingChanges = new ArrayList<String>();
			String platformNumberAffected = "";
			while (changeTableItr.hasNext()) {
				IRow changesTableRow = (IRow) changeTableItr.next();

				String changeType = CommonAglUtil.getListValue(changesTableRow, ItemConstants.ATT_CHANGE_HISTORY_TYPE) ;
				String changeNumber = (String)changesTableRow.getValue(ItemConstants.ATT_CHANGE_HISTORY_NUMBER) ;
				String changeStatus = CommonAglUtil.getListValue(changesTableRow, ItemConstants.ATT_CHANGE_HISTORY_STATUS) ;

				if (changeType.equalsIgnoreCase(GDEVConstants.GDEV_CHANGE_HISTORY_TABLE_TYPE)
						&& changeNumber.startsWith(GDEVConstants.GDEV_PART_NUMBER_PREFIX)
						&& changeStatus.equalsIgnoreCase(GDEVConstants.GDEV_RELEASED_STATUS))
				{
					log.info("Item Number :" + itemNumberObj.getName() +" already has a released deviation associated: "+ changeNumber + " Checking for Platform Number affected:");
					IChange associatedReleasedGDEV = (IChange) session.getObject(
							IChange.OBJECT_TYPE, changeNumber);
					//Start for CR 7535
					String associatedFFRegions = associatedReleasedGDEV.getValue(ChangeConstants.ATT_PAGE_TWO_MULTILIST09).toString();
					String originalFFRegions = getFulfillmentRegions();
					//log.info("originalFFRegions: " + originalFFRegions);
					//log.info("associatedFFRegions: " + associatedFFRegions);
					boolean isFFRegionExists = false;
					StringTokenizer strFFR = new StringTokenizer (originalFFRegions, ";");
					while (strFFR.hasMoreTokens())
					{
						String strFFRegion = strFFR.nextToken().toString();
						if(associatedFFRegions.contains(strFFRegion))
							//	|| originalFFRegions.contains(associatedFFRegions))
						{
							isFFRegionExists = true;
							break;
						}
					}
					//End for CR 7535
					//Nandini
					//log.info("isFFRegionExists: " + isFFRegionExists);
					ITable table = associatedReleasedGDEV
					.getTable(ChangeConstants.TABLE_AFFECTEDITEMS);
					Iterator itr = table.iterator();

					while (itr.hasNext())
					{
						IRow row = (IRow) itr.next();
						String itemNumber = (String) row
						.getValue(ChangeConstants.ATT_AFFECTED_ITEMS_ITEM_NUMBER);


						if(itemNumberObj.getName().equalsIgnoreCase(itemNumber))
						{
							log.info("Item Number in associated Affected item list : "+itemNumber);

							String originalGDEVPlatformNumberAffected= getPlatformNumberAffected();

							String associatedGDEVPlatformNumberAffected = CommonAglUtil
							.getMultiListValue(
									row,
									ChangeConstants.ATT_AFFECTED_ITEMS_MULTILIST01);


							//Start for CR - 7535

							/*if(associatedGDEVPlatformNumberAffected.contains(originalGDEVPlatformNumberAffected)
									|| originalGDEVPlatformNumberAffected.contains(associatedGDEVPlatformNumberAffected))
							{
								duplicateGDEVNumber += changeNumber + ",";
							}

							platformNumberAffected += associatedGDEVPlatformNumberAffected;*/

							if((associatedGDEVPlatformNumberAffected.contains(originalGDEVPlatformNumberAffected)
									|| originalGDEVPlatformNumberAffected.contains(associatedGDEVPlatformNumberAffected))&& isFFRegionExists)
							{
								if(!duplicateGDEVNumber.contains(changeNumber))
									duplicateGDEVNumber += changeNumber + ",";
							}

							platformNumberAffected += associatedFFRegions + "|" + associatedGDEVPlatformNumberAffected + "::";
							//End for CR -7535

						}

					}

				}
			}
			return platformNumberAffected;

		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
			return null;
		}
	}



	/**
	 * Create HTML attachment
	 * @param phantomItemNumber
	 * @param identicalParts
	 * @throws APIException
	 */
	public void createHTMLAttachment() throws APIException
	{

		log.info("phantomItemNumber:"+phantomItemNumber);
		log.info("identicalParts:"+identicalParts);
		log.info("revisionError:"+revisionError);
		log.info("partTypeError:"+partTypeError);
		log.info("revisionWarning:"+revisionWarning);
		log.info("isPHStructuredWarning:"+isPHStructuredWarning);
		log.info("duplicateReleasedDeviationCheck:"+duplicateReleasedDeviationCheck);
		log.info("duplicateGDEVNumber:"+duplicateGDEVNumber);

		String workflowStatus = eventInfo.getToStatus().toString();
		String workflowStatusFromTo = getWorkflowStatusChangeName(workflowStatus);

		Date today = new Date();

		String title = "Workflow status change from "+workflowStatusFromTo+" for Global Deviation : "
		+ deviationChangeObj.getName();

		HTMLAttachmentFormat.start();
		HTMLAttachmentFormat.generateHTMLTitle(title);

		HTMLAttachmentFormat
		.generateHTMLMessage("Timestamp: "+today);

		if(getErrorFlag())
		{
			HTMLAttachmentFormat
			.generateHTMLMessage(GDEVConstants.GDEV_ATTACHMENT_ERROR_MESSAGE);

			HTMLAttachmentFormat.generateHTMLValidationErrorTAG();
			if (!phantomItemNumber.trim().equalsIgnoreCase("")){

				phantomItemNumber = phantomItemNumber.substring(0,phantomItemNumber.length()-1);
				HTMLAttachmentFormat
						.generateHTMLValidationError(GDEVConstants.GDEV_PHANTOM_PART_MESSAGE
								+ HTMLAttachmentFormat.bold(phantomItemNumber
										.trim()));

			}
			if (!identicalParts.trim().equalsIgnoreCase("")){
				identicalParts = identicalParts.substring(0,identicalParts.length()-1);
				log.info(identicalParts.trim());
				HTMLAttachmentFormat
						.generateHTMLValidationError(GDEVConstants.GDEV_IDENTICAL_PARTS_MESSAGE
								+ HTMLAttachmentFormat.bold(identicalParts.trim()));
			}


			if (!revisionError.trim().equalsIgnoreCase("")){
				revisionError = revisionError.substring(0,revisionError.length()-1);
				HTMLAttachmentFormat
						.generateHTMLValidationError(GDEVConstants.GDEV_PARTS_REVISION_ERROR_MESSAGE
								+HTMLAttachmentFormat.bold(revisionError.trim()));
			}

			if (!partTypeError.trim().equalsIgnoreCase("")){
				partTypeError = partTypeError.substring(0,partTypeError.length()-1);
				HTMLAttachmentFormat
						.generateHTMLValidationError(GDEVConstants.GDEV_PART_TYPE_MESSAGE
								+ HTMLAttachmentFormat.bold(partTypeError.trim()));
			}

			if (!CFIParts.trim().equalsIgnoreCase("")){
				CFIParts = CFIParts.substring(0,CFIParts.length()-1);
				HTMLAttachmentFormat
						.generateHTMLValidationError(GDEVConstants.GDEV_CFI_PARTS_MESSAGE
								+ HTMLAttachmentFormat.bold(CFIParts.trim()));
			}



			if (!duplicateReleasedDeviationCheck.trim().equalsIgnoreCase("")){
				duplicateReleasedDeviationCheck = duplicateReleasedDeviationCheck.substring(0,duplicateReleasedDeviationCheck.length()-1);
				if(!duplicateGDEVNumber.trim().equalsIgnoreCase(""))
					duplicateGDEVNumber = duplicateGDEVNumber.substring(0,duplicateGDEVNumber.length()-1);

				HTMLAttachmentFormat
						.generateHTMLValidationError(GDEVConstants.GDEV_DUPLICATE_RELEASED_DEVIATION_MESSAGE_PART1
								+ HTMLAttachmentFormat.bold(duplicateReleasedDeviationCheck.trim()) + GDEVConstants.GDEV_DUPLICATE_RELEASED_DEVIATION_MESSAGE_PART2
								+ HTMLAttachmentFormat.bold(duplicateGDEVNumber.trim()) + GDEVConstants.GDEV_DUPLICATE_RELEASED_DEVIATION_MESSAGE_PART3);
			}

			if (!isPHStructuredWarning.trim().equalsIgnoreCase("")){
				isPHStructuredWarning = isPHStructuredWarning.substring(0,isPHStructuredWarning.length()-1);

				HTMLAttachmentFormat.generateHTMLValidationWarningTAG();
				HTMLAttachmentFormat
						.generateHTMLValidationWarning(GDEVConstants.GDEV_PH_VALIDATION_MESSAGE
								+ HTMLAttachmentFormat.bold(isPHStructuredWarning.trim()));

			}

			if (!revisionWarning.trim().equalsIgnoreCase("")) {

				if (!isPHStructuredWarning.trim().equalsIgnoreCase(""))
				{
					HTMLAttachmentFormat.generateHTMLValidationWarningTAG();
				}
				revisionWarning = revisionWarning.substring(0,revisionWarning.length()-1);
				HTMLAttachmentFormat
						.generateHTMLValidationWarning(GDEVConstants.GDEV_PARTS_REVISION_WARNING_MESSAGE
								+ HTMLAttachmentFormat.bold(revisionWarning.trim()));
			}

		}
		else if(getWarningFlag() && !getErrorFlag())
		{
			HTMLAttachmentFormat
			.generateHTMLMessage(GDEVConstants.GDEV_ATTACHMENT_WARNING_MESSAGE);

			HTMLAttachmentFormat.generateHTMLValidationWarningTAG();

			if (!revisionWarning.trim().equalsIgnoreCase("")) {

				revisionWarning = revisionWarning.substring(0,revisionWarning.length()-1);
				HTMLAttachmentFormat
						.generateHTMLValidationWarning(GDEVConstants.GDEV_PARTS_REVISION_WARNING_MESSAGE
								+ HTMLAttachmentFormat.bold(revisionWarning.trim()));
			}
			if (!isPHStructuredWarning.trim().equalsIgnoreCase("")){
				isPHStructuredWarning = isPHStructuredWarning.substring(0,isPHStructuredWarning.length()-1);
				HTMLAttachmentFormat
						.generateHTMLValidationWarning(GDEVConstants.GDEV_PH_VALIDATION_MESSAGE
								+ HTMLAttachmentFormat.bold(isPHStructuredWarning.trim()));

			}


		}
		HTMLAttachmentFormat.end();
		log.info("HTML Attachment : "+HTMLAttachmentFormat.getAttachmentObject());
		if(sendMail())
			log.info("Written");


	}

	/**
	 * Send mail and add the attachment
	 * @return boolean
	 * @throws APIException
	 */
	public boolean sendMail () throws APIException
	{

		String attachment =
			props.getProperty(GDEVConstants.GDEV_VALID_ATTACHMENT_PATH)
					+ "\\"
					+ deviationChangeObj.getName()
					+ ".html";

		log.info("Attachment : "+attachment);
			String emailText =
				"Attached HTML files contains the Global Deviation Validation results: "
					+ deviationChangeObj.getName();
			boolean isWritten = false;
			try {
				isWritten = writeFileToArchive(HTMLAttachmentFormat.getAttachmentObject().toString(), attachment);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				log.error(CommonAglUtil.exception2String(e));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error(CommonAglUtil.exception2String(e));
			}
			String workflowStatus = eventInfo.getToStatus().toString();
			String workflowStatusFromTo = getWorkflowStatusChangeName(workflowStatus);

			String subject = "Validation Errors and/or Warnings regarding Workflow status change from "
				+ workflowStatusFromTo
				+ "  for Global Deviation : "
				+ deviationChangeObj.getName();

			IUser currentIUser = session.getCurrentUser();
			String currentUser = currentIUser.getName();
			ArrayList<IUser> userList = new ArrayList<IUser>();
			userList.add(currentIUser);
			String smtpHost = props.getProperty(GDEVConstants.GDEV_SMTP_HOST).toString();

			String fromMailId = props.getProperty(GDEVConstants.GDEV_FROM_MAIL_ID).toString();

			String emailOption = props.getProperty(GDEVConstants.GDEV_EMAIL_OPTION).toString();
			try {
				if(emailOption.equalsIgnoreCase("ON")){

				//Send mail to user outlook mail box
				MailUser me = new MailUser(session, currentUser, subject,
						emailText, attachment, "True", smtpHost, fromMailId);
				me.doSend();

				//Send mail to Agile Inbox

				String GDEVNotificationTemplate = getNotificationTemplate(workflowStatus);

				session
						.sendNotification(
								deviationChangeObj,GDEVNotificationTemplate,
								userList, true, "GDEV ERROR:");
				//Add attachment to the attachment tab in POST Event
				if(getErrorFlag())
					addAttachment();

				log.info("Successfully sent the email for "
						+ deviationChangeObj.getName());
			}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				log.error(CommonAglUtil.exception2String(e));
			}
			return isWritten;
	}



	/**
	 * Returns the Notification template for workflow status change
	 * @param workflowStatus
	 * @return notificationTemplateName
	 */
	private String getNotificationTemplate(String workflowStatus) {

		String notificationTemplateName = "";
		if (workflowStatus != null
				&& workflowStatus.equalsIgnoreCase("Submitted"))
			notificationTemplateName = props.getProperty(GDEVConstants.GDEV_AGILE_PENDING_TO_SUBMITTED_NOTIFICATION_TEMPLATE);
		else if (workflowStatus != null
				&& workflowStatus.equalsIgnoreCase("CCB"))
			notificationTemplateName = props.getProperty(GDEVConstants.GDEV_AGILE_SUBMITTED_TO_CCB_NOTIFICATION_TEMPLATE);
		else if (workflowStatus != null
				&& workflowStatus.equalsIgnoreCase("Released"))
			notificationTemplateName = props.getProperty(GDEVConstants.GDEV_AGILE_CCB_TO_RELEASED_NOTIFICATION_TEMPLATE);
		//else if (workflowStatus != null
			//	&& workflowStatus.equalsIgnoreCase("Expired"))
			//notificationTemplateName = "Global Deviations - Validation Error for status change from Released to Expired";

		return notificationTemplateName;
	}

	/**
	 * write contents of HTML attachment to file
	 */
	public boolean writeFileToArchive(String htmlString, String outputFilePath)
			throws FileNotFoundException, IOException {
		FileOutputStream fos = null;
		try {
			byte[] buf;

			/* Define FilePath and Streams for transfer */

			File outputFile = new File(outputFilePath);
			if (outputFile.exists()) {
				outputFile.delete();
				outputFile = new File(outputFilePath);
			}
			fos = new FileOutputStream(outputFile);
			log.info("WRITING TO FILE ");
			/* Dump the data from the String to the file. */
			buf = new byte[htmlString.length() + 10];
			buf = htmlString.getBytes();
			fos.write(buf, 0, htmlString.length());

		} catch (IOException e) {
			log.error(CommonAglUtil.exception2String(e));
			return false;
		} finally {
			if (fos != null) {
				fos.close(); // always close the file.
			}
		}
		htmlString = null; // release the big message.
		return true;
	}

	/**
	 * returns warning flag
	 * @return warningFlag
	 */
	public boolean getWarningFlag()
	{
		log.info("warning flag is : ....    "+globalWarningFlag);
		if(globalWarningFlag.contains("true"))
			isWarning = true;
		else
			isWarning = false;

		return isWarning;
	}

	/**
	 * returns error flag
	 * @return errorFlag
	 */
	public boolean getErrorFlag(){
		log.info("Error flag is : ....    "+globalErrorFlag);
		if(globalErrorFlag.contains("true"))
			isError = true;
		else isError = false;
		return isError;
	}

	/**
	 * Based on the target workflow status , return the status change
	 * @return
	 * @throws APIException
	 */
	public String getWorkflowStatusChangeName (String workflowStatus) throws APIException
	{

		String workflowStatusChange = "";
		if(workflowStatus != null && workflowStatus.equalsIgnoreCase("Submitted"))
			workflowStatusChange = "Pending to Submitted";
		else if(workflowStatus != null && workflowStatus.equalsIgnoreCase("CCB"))
			workflowStatusChange = "Submitted to CCB";
		else if(workflowStatus != null && workflowStatus.equalsIgnoreCase("Released"))
			workflowStatusChange = "CCB to Released";
		else if(workflowStatus != null && workflowStatus.equalsIgnoreCase("Expired"))
			workflowStatusChange = "Released to Expired";

		return workflowStatusChange;
	}

	/**
	 * Add Attachment to the deviation object attachment tab
	 */
	private void addAttachment() {

		log.info("adding Attachment in attachment tab");
		ITable attachmentTable;
		try {
			attachmentTable = (ITable) deviationChangeObj
					.getTable(ChangeConstants.TABLE_ATTACHMENTS);

			String GDEVObjNumber = deviationChangeObj.getName();
			String attachment = props
					.getProperty(GDEVConstants.GDEV_VALID_ATTACHMENT_PATH)
					+ "\\" + deviationChangeObj.getName() + ".html";
			log.info("Attachment Path :" + attachment);
			// ttChangeFiles
			HashMap attachmentMap = new HashMap();

			attachmentMap.put(ChangeConstants.ATT_ATTACHMENTS_FILE_NAME, attachment);
			attachmentMap.put(ChangeConstants.ATT_ATTACHMENTS_FILE_DESCRIPTION, "Errors");

			boolean isAttached = false;
			int intLoop = 1;
			IRow attachmentRow = null;
			while (intLoop < 3 && !isAttached) {
				try {
					log.info(GDEVObjNumber + ":"
							+ "Trying to attach file. Tentative " + intLoop);
					attachmentRow = attachmentTable.createRow(attachmentMap);
					isAttached = true;
				} catch (APIException apie) {
					intLoop++;
					if (intLoop == 3) {
						throw apie;
					}
				}
				attachment = null;
			}
			attachmentTable = null;
			attachmentMap = null;
			log.info(GDEVObjNumber + ":" + "File attached with success.");

		} catch (APIException e) {
			// TODO Auto-generated catch block
			log.error(CommonAglUtil.exception2String(e));
		}

	}

}
