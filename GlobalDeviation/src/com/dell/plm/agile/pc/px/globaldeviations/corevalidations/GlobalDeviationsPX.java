package com.dell.plm.agile.pc.px.globaldeviations.corevalidations;

import java.util.Properties;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.IAgileSession;
import com.agile.api.INode;
import com.agile.px.ActionResult;
import com.agile.px.EventActionResult;
import com.agile.px.IEventAction;
import com.agile.px.IEventInfo;
import com.agile.px.IWFChangeStatusEventInfo;
import com.dell.plm.agile.common.exception.BaseException;
import com.dell.plm.agile.common.util.CommonAglUtil;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;

public class GlobalDeviationsPX implements IEventAction {

	private Logger log;
	
	@Override
	public EventActionResult doAction(IAgileSession session, INode arg1,
			IEventInfo eventInfo) {
		// TODO Auto-generated method stub

		IWFChangeStatusEventInfo objectEventInfo = (IWFChangeStatusEventInfo) eventInfo;
		String workflowStatus = null;
		log = GDevLogger.getLogger();
		Properties props = CommonAglUtil
				.loadProperties(GDEVConstants.GDEV_GLOBAL_DEVIATION_PROPERTIES);
		if (props == null)
		 log.error("GlobalDeviationsPX.doAction : ERROR: Could not load the Properties file");
		
		
		try {
			workflowStatus = objectEventInfo.getToStatus().toString();

			GlobalDeviationsBO businessObj = GDevWFFactory
					.getFactoryClass(workflowStatus);

			
			Integer maxTableSize = Integer.parseInt(props.getProperty(GDEVConstants.GDEV_TABLE_SIZE)); 
			if (maxTableSize != null)
			{
				String validationResult = businessObj.doValidation(session,
						objectEventInfo, props);
				log.info("Validation result is ::::::::::  "+ validationResult);

				if (validationResult.equalsIgnoreCase("Validation Error")) {
					log.info("ERROR: PLEASE CORRECT ERRORS FIRST");
					throw new BaseException(GDEVConstants.GDEV_VALIDATION_ERROR);
				} else if (validationResult
						.equalsIgnoreCase("Table Size more than 100")) {
					throw new BaseException(
							GDEVConstants.GDEV_TABLE_SIZE_MORE_THAN_LIMIT_ERROR);
				} else if (validationResult.equalsIgnoreCase("Table is empty")) {
					throw new BaseException(
							GDEVConstants.GDEV_TABLE_IS_EMPTY_ERROR);
				} else if (validationResult.equalsIgnoreCase("Not approved")){
					log.info("In BaseException: Not Approved"); 
					throw new BaseException(
							GDEVConstants.GDEV_ALL_APPROVERS);
				}
				
			}
			else {
				throw new BaseException(GDEVConstants.GDEV_TABLE_MAX_LIMIT_NULL);
			}
			
			
			return new EventActionResult(eventInfo, new ActionResult(
					ActionResult.STRING, "Global Deviations PX : Success"));
			
		} catch (BaseException e) {
			log.error(CommonAglUtil.exception2String(e));
			return new EventActionResult(eventInfo, new ActionResult(
					ActionResult.EXCEPTION, e));

		} catch (APIException e) {
			log.error(CommonAglUtil.exception2String(e));
			return new EventActionResult(eventInfo, new ActionResult(
					ActionResult.EXCEPTION, e));
		}
		

	}
	
	

}
