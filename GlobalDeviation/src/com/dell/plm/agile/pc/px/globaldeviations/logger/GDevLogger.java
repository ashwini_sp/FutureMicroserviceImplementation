package com.dell.plm.agile.pc.px.globaldeviations.logger;



import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.dell.plm.agile.common.util.CommonAglUtil;
import com.dell.plm.agile.pc.px.globaldeviations.corevalidations.GDEVConstants;

/**
 * 
 * Global Deviations Logger
 *
 */
public class GDevLogger {

	static final Logger log = Logger.getLogger(getCallingClass());
	private static Properties props;

	private GDevLogger() {

	}

	/**
	 * Initialize Logger
	 */
	private static void initializeLogger() {

		try {
			props = CommonAglUtil
			.loadProperties(GDEVConstants.GDEV_GLOBAL_DEVIATION_PROPERTIES);
		
			PropertyConfigurator.configure(props.getProperty(GDEVConstants.GDEV_LOG4J_PROPERTIES));

		} catch (Exception e) {
			throw new RuntimeException("Unable to load logging property ");
		}
	}
	
	
	
	/*public  void  configureLog(String fileName, int logFileSizelimit, int logFileCount) throws Exception {
        log = Logger.getLogger(fileName);
        FileHandler fileHandler = null;
        try {
        	System.out.println("Before Configuring Logging");
        	 fileHandler = new FileHandler(fileName ,logFileSizelimit,logFileCount, true);
            
            System.out.println("After Configuring Logging");
        } catch (IOException ioException) {
        	System.err.println(" Configure Log Exception :"+commonagexception2String(ioException));
        	System.out.println(" Configure Log Exception :"+exception2String(ioException));
            throw new Exception("PX: Unable to configure the log file :", ioException);
        }
        if (fileHandler != null)
            log.addHandler(fileHandler);
        else 
            throw new Exception("PX: Unable to configire the log file ");
        
    }*/

	/**
	 * Return the logger object
	 * @return log
	 */
	public static Logger getLogger() {
		initializeLogger();
		return log;
	}

	/**
	 *  Returns the calling class
	 * @return element.getClassName()
	 */
	private static String getCallingClass() {
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		StackTraceElement element = stackTrace[3];
		if (element == null) {
			return "";
		}
		return element.getClassName();
	}

}
