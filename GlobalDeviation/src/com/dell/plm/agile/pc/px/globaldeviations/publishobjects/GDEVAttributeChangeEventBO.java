package com.dell.plm.agile.pc.px.globaldeviations.publishobjects;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.agile.api.ChangeConstants;
import com.agile.api.IAgileSession;
import com.agile.api.IChange;
import com.agile.api.IDataObject;
import com.dell.plm.agile.common.util.CommonAglUtil;
import com.dell.plm.agile.pc.px.globaldeviations.corevalidations.GDEVConstants;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;

public class GDEVAttributeChangeEventBO 
{
	Logger log=null;
	 Properties props=null;
	
	public GDEVAttributeChangeEventBO() 
	{
		  log = GDevLogger.getLogger();
		  props = CommonAglUtil.loadProperties(GDEVConstants.GDEV_GLOBAL_DEVIATION_PROPERTIES);
	}

	public void attributeChangeEventBo(IAgileSession session, IDataObject deviationChangeObj) throws Exception
	{
		String implementationType=CommonAglUtil
				.getListValueFromDataObject(
						(IChange) deviationChangeObj,
						ChangeConstants.ATT_PAGE_THREE_LIST02);
		
		log.info(" Implementation Type is: "+implementationType);
		if(implementationType!=null && !implementationType.trim().isEmpty() && implementationType.equalsIgnoreCase(props.getProperty(GDEVConstants.IMPLEMENTATION_TYPE)))
		{
				log.info("Starting the process to set the NEW AX Fulfillment Facility Affected Values.");
				setFulfillmentFacilityAffValue(session,(IChange)deviationChangeObj);
				log.info("Successfully completed.");
		}
		else
		{
			log.info("New Facility values will be set only for Implementation type:OFS Applied");
		}
	}
	
	
	public void setFulfillmentFacilityAffValue(IAgileSession session, IChange devChangeObj) throws Exception
	{
		String AXFACILITY_ON_OFF= props.getProperty(GDEVConstants.AXFACILITY_ON_OFF);
		Map<String, String> ffaValuesMAp=null;
		List<String> ffaList= new LinkedList<String>();
		log.info("Inside the Set Fulfillment method");
		
		
		if(AXFACILITY_ON_OFF !=null && !AXFACILITY_ON_OFF.trim().isEmpty()&& AXFACILITY_ON_OFF.equalsIgnoreCase("ON"))
		{
			log.info("New AX 2012 Facility functionality is ON");
			ffaValuesMAp= getAXFacilityList();
			
			log.debug("Mapping of the Old and New AX 2012 Facility Values : "+ffaValuesMAp);
			log.info("Invoking the getMultiListValueFromDataObject method to get the current Fulfillment Facility Affected attribute values from Agile.");
			String attFFAListValue= CommonAglUtil.getMultiListValueFromDataObject(devChangeObj, ChangeConstants.ATT_PAGE_THREE_MULTILIST01);
			
			log.info("Current Fulfillment Facility Affected attribute value list: "+attFFAListValue);
			
			if(attFFAListValue !=null && !attFFAListValue.trim().isEmpty())
			{
			for(String attFFAValues:attFFAListValue.split(";"))
			{
			//Using non circuit operator to check null and empty conditions for attFFAValues & ffaValuesMAp
			if((attFFAValues!=null && !attFFAValues.trim().isEmpty()) & (ffaValuesMAp!=null && !ffaValuesMAp.isEmpty()))
			{
					if(ffaValuesMAp.containsKey(attFFAValues) || ffaValuesMAp.containsValue(attFFAValues))
					{
						String value= ffaValuesMAp.get(attFFAValues);
				
						if(value!=null && !value.trim().isEmpty())
						{
							log.debug("Found the value based on the input key: "+attFFAValues+" Value: "+value);
							ffaList.add(value);
//							ffaList.add(attFFAValues);
						}
						else if(value==null)
						{
							log.debug("Value is null: "+value);
							for(Object obj: ffaValuesMAp.keySet())
							{
								if(ffaValuesMAp.get(obj).equals(attFFAValues))
								{
									log.debug("Foung the key based on the input Value : "+attFFAValues+" Key: "+obj);
									ffaList.add(obj.toString());
//									ffaList.add(attFFAValues);
								}
							}
						}
					}
				
			}
		}	//Below condition is checking if both old and new values has been selected by user, so don't invoke the setMultiListValueFromDataObject
			if(Arrays.asList(attFFAListValue.split(";")).containsAll(new HashSet<String>(ffaList)))
			{
				log.debug("New AX Facility values of Fulfillment Facility Affected attribute is already selected by user:"+ffaList);
				ffaValuesMAp=null;
			}
			else
			{
				log.debug("List of New AX Facility values to set the Fulfillment Facility Affected attribute :"+ffaList);
				CommonAglUtil.setMultiListValueFromDataObject(session, devChangeObj, ChangeConstants.ATT_PAGE_THREE_MULTILIST01, ffaList);
			}
		}
			
			
		}
		else
		{
			log.info("Functionality for New AX 2012 Facility is OFF");
		}
	
	}
	
	
	
	/*Getting AX facility list from the prop. file.
	 *Splitting the list with delimiter in key value pair.
	 *Store the key value pair in map.
	 *return Map.
	 * */
	
	public  Map<String,String> getAXFacilityList()
	{
		log.info("Inside the method of getAXFacilityList to get the list of NEW AX 2012 Facility values from the property file.");
		String delimiter= props.getProperty(GDEVConstants.AXFACILITYLIST_DELIMITER);
		String connectingDelimeter=props.getProperty(GDEVConstants.CONNECTING_DELIMITER);
		String  facilityListValue= props.getProperty(GDEVConstants.AXFACILITY_NEW_VALUES_LIST);
		Map<String, String> ffaValues= new LinkedHashMap<String, String>();
		
		log.debug("Facility list values: "+facilityListValue);
		log.debug("Delimiter to split the list values: "+delimiter);
		if((delimiter!=null && !delimiter.trim().isEmpty()) && (facilityListValue!=null && !facilityListValue.trim().isEmpty()))
		{
			StringTokenizer token=new StringTokenizer(facilityListValue,delimiter);
			while(token.hasMoreElements())
			{
				String values= token.nextToken();
				if(connectingDelimeter!=null && !connectingDelimeter.trim().isEmpty())
				{
					String valueArr[]= values.split(connectingDelimeter);
					String key=valueArr[0];
					String value=valueArr[1];
					log.debug("Splitted key and value pair of list to put into the Map: key= "+key+"   Value= "+value);
					ffaValues.put(key, value);
				}
				
			}
			
		}
		log.info("Returning the map of AX Facility values");
		return ffaValues;
	}

	
}
