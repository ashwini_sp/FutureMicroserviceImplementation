package com.dell.plm.agile.pc.px.globaldeviations.publishobjects;

import java.util.Properties;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.ChangeConstants;
import com.agile.api.IAgileSession;
import com.agile.api.IChange;
import com.agile.api.IDataObject;
import com.agile.api.INode;
import com.agile.px.ActionResult;
import com.agile.px.EventActionResult;
import com.agile.px.IEventAction;
import com.agile.px.IEventDirtyCell;
import com.agile.px.IEventInfo;
import com.agile.px.IUpdateTitleBlockEventInfo;
import com.dell.plm.agile.common.util.CommonAglUtil;
import com.dell.plm.agile.pc.px.globaldeviations.corevalidations.GDEVConstants;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;

public class GDEVAttributeChangeEventPX implements IEventAction{

	private boolean isObjectPublished;
	

	@Override
	public EventActionResult doAction(IAgileSession session, INode arg1,
			IEventInfo eventInfo) {
		// TODO Auto-generated method stub
		
		  Logger log = GDevLogger.getLogger();
		 
		  
		  IUpdateTitleBlockEventInfo titleEventInfo = (IUpdateTitleBlockEventInfo) eventInfo;
		  IDataObject deviationChangeObj = null;
		  String CTONumber = null;
		  String workflowStatus = null;
		  try {
			log.info("Inside Attribute change Event class");
			IEventDirtyCell deviationStatusAttr = titleEventInfo.getCell(ChangeConstants.ATT_PAGE_TWO_LIST03);
				
			if (deviationStatusAttr != null) {
				log.info("Cell is not empty");
				deviationChangeObj = titleEventInfo.getDataObject();

				//log.info("DATA Object name is : "+ deviationChangeObj.getName());

				workflowStatus = ((IChange) deviationChangeObj).getStatus()
						.getName();
				//log.info("Workflow status is : " + workflowStatus);
			
			
			
			if (workflowStatus != null
					&& workflowStatus.equalsIgnoreCase("Released")) {
				log.info("Perfect candidate>> ");
				
				/*GDEVAttributeChangeEventBO attChangeEventBo= new GDEVAttributeChangeEventBO();
				attChangeEventBo.attributeChangeEventBo(session, deviationChangeObj,titleEventInfo);*/
				
				GDEVCreateCTOBO publishObj = new GDEVCreateCTOBO(session,
						titleEventInfo);
				
				
				isObjectPublished = publishObj
						.createAndSendCTO(deviationChangeObj,"Deviation Status :Attribute Change Event");

				CTONumber = publishObj.getCTONumber();
				/*if (isObjectPublished) {
					return new EventActionResult(eventInfo, new ActionResult(
							ActionResult.STRING,
							"Published Objects Successfully"));
				} else {
					return new EventActionResult(eventInfo, new ActionResult(
							ActionResult.STRING,
							"Publishing Object failed.CTO number is :"
									+ CTONumber));
				}*/
			
			}
			return new EventActionResult(eventInfo, new ActionResult(
					ActionResult.STRING,
					"Published Objects Successfully"));
			}
			else {
				return new EventActionResult(eventInfo, new ActionResult(
						ActionResult.STRING, ""));
			}
		} catch (APIException e) {
			e.printStackTrace();
			 return new EventActionResult(eventInfo , new ActionResult(ActionResult.EXCEPTION, e));
			
		}
		  catch (Exception e) {
				e.printStackTrace();
				 return new EventActionResult(eventInfo , new ActionResult(ActionResult.EXCEPTION, e));
				
			}
	}

	
	
}
