package com.dell.plm.agile.pc.px.globaldeviations.publishobjects;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.IAdmin;
import com.agile.api.IAgileClass;
import com.agile.api.IAgileList;
import com.agile.api.IAgileSession;
import com.agile.api.IAutoNumber;
import com.agile.api.IChange;
import com.agile.api.IDataObject;
import com.agile.api.IRow;
import com.agile.api.ITable;
import com.agile.api.ITransferOrder;
import com.agile.api.IUser;
import com.agile.api.TransferOrderConstants;
import com.agile.px.IEventInfo;
import com.dell.plm.agile.common.util.CommonAglUtil;
import com.dell.plm.agile.common.util.MailUser;
import com.dell.plm.agile.pc.px.globaldeviations.corevalidations.GDEVConstants;
import com.dell.plm.agile.pc.px.globaldeviations.logger.GDevLogger;

public class GDEVCreateCTOBO {

	private IAgileSession session;

	private Logger log;
	private String autoNumber;
	private Properties props;

	public GDEVCreateCTOBO(IAgileSession session, IEventInfo eventInfo) {
		this.session = session;
		log = GDevLogger.getLogger();
		props = CommonAglUtil.loadProperties(GDEVConstants.GDEV_GLOBAL_DEVIATION_PROPERTIES);
		if(props == null)
			log.error("GDEVCreateCTOBO.doAction : ERROR: Could not load the Properties file");

	}

	/**
	 * Create and Send CTO . If CTO creation is failed then a mail will be sent to Support to create the CTO manually
	 * @param deviationChangeObj
	 * @return boolean
	 */
	public boolean createAndSendCTO(IDataObject deviationChangeObj , String eventInfo) {

		String deviationObjectName = "";
		try {
			deviationObjectName = deviationChangeObj.getName();
			// Creating Autonumber for CTO
			String autoNumber = getAutoNumber();
			setAutoNumber(autoNumber);
			log.info("CTO AUTONUMBER :" + autoNumber);
			
			// Creating CTO for Global Deviation
			ITransferOrder cto = (ITransferOrder) session.createObject(
					TransferOrderConstants.CLASS_CTO, autoNumber);
			
			// Setting the only workflow status
			cto.setWorkflow(cto.getWorkflows()[0]);

			ITable content = cto
					.getTable(TransferOrderConstants.TABLE_SELECTEDOBJECTS);
			
			// Adding Global Deviation Object to the CTO content
			content.add((IChange) session.getObject(IChange.OBJECT_TYPE,
					deviationObjectName));
			
			// Getting Where Sent table ref
			ITable destination = cto
					.getTable(TransferOrderConstants.TABLE_WHERESENT);

			HashMap params = new HashMap();

			// Adding Destination
			params.put(TransferOrderConstants.ATT_WHERE_SENT_DESTINATION,
					props.getProperty(GDEVConstants.GDEV_CTO_DESTINATION));
			
			// Adding Filters
			
			// String filters[] ={"Global Deviation Filter","GDev Part Filter"};

			 String filter  = props.getProperty(GDEVConstants.GDEV_CTO_FILTER);
			 
			 ArrayList<String> filterList = CommonAglUtil.getListFromString(filter, ",");
			 String[] filterArr = new String[filterList.size()];
			 filterList.toArray(filterArr);
							 
			 params.put(TransferOrderConstants.ATT_WHERE_SENT_FILTERS,
					 filterArr);
		 
			 
		     //params.put(TransferOrderConstants.ATT_WHERE_SENT_FILTERS, filters);
			
			//params.put(TransferOrderConstants.ATT_WHERE_SENT_FILTERS,
					//props.getProperty(GDEVConstants.GDEV_CTO_FILTER));

			
			// Adding Format
			params.put(TransferOrderConstants.ATT_WHERE_SENT_DATA_FORMAT,
					props.getProperty(GDEVConstants.GDEV_CTO_FORMAT_TYPE));

			destination.createRow(params);

			// Change status to Review
			cto.changeStatus(cto.getDefaultNextStatus(), true, "Reviewed CTO",
					false, false, new IUser[0], null, null, false, "");

			log.info("Changed CTO " + autoNumber + " status to REVIEWED");

			// Change status to Approved

			cto.changeStatus(cto.getDefaultNextStatus(), true, "Approved CTO",
					false, false, new IUser[0], null, null, false, "");

			log.info("Changed CTO " + autoNumber + " status to APPROVED");
			/*try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			//boolean isSuccess = isPublishingSuccessful(destination);
			
			return true;

		} catch (APIException e) {
			
			String subject = "CTO CREATION FAILED FOR :"+deviationObjectName+". Please create CTO manually and publish "+deviationObjectName+ " again";
			String fromMailId = props.getProperty(GDEVConstants.GDEV_FROM_MAIL_ID).toString();
			String toMailId = props.getProperty(GDEVConstants.GDEV_AGL_SUPPORT_MAIL_ID).toString();
			String emailText =
				"CTO CREATION FAILED FOR Global Deviation Number: "
					+ deviationObjectName
					+ ".Caused by Event : "
					+ eventInfo
					+ ". The CTO number created is : " + autoNumber;
			
			log.error(emailText +" --- Caused by :"+CommonAglUtil.exception2String(e));
			
			IUser currentIUser;
			String currentUser;
			try {
				//currentIUser = session.getCurrentUser();
				//currentUser = currentIUser.getName();
				//log.info("Cureent user is :"+currentUser);
				String smtpHost = props.getProperty(GDEVConstants.GDEV_SMTP_HOST).toString();
				
				MailUser me = new MailUser(session,"",subject,
						emailText, "", "false", smtpHost, fromMailId);
				me.doSend(toMailId);
			} catch (APIException e1) {
				// TODO Auto-generated catch block
				log.error(CommonAglUtil.exception2String(e1));
			} catch (SQLException e2) {
				// TODO Auto-generated catch block
				log.error(CommonAglUtil.exception2String(e2));
			}
			
			return false;
		}

	}

	/**
	 *  Check if publishing successful . Need to check if we can handle this in Agile using SDK
	 * @param destination
	 * @return
	 */
	private boolean isPublishingSuccessful(ITable destination) {
		
		Iterator itr = destination.iterator();
		while (itr.hasNext()) {
			IRow row = (IRow) itr.next();
			
				String transmissionStatus = CommonAglUtil
						.getListValue(
								row,
								TransferOrderConstants.ATT_WHERE_SENT_TRANSMISSION_STATUS);
				log.info("transmissionStatus : " + transmissionStatus);
				
				if (transmissionStatus.equalsIgnoreCase("Failure")) {
					return false;
				}
			
		}
		return true;
	}

	/**
	 * Set CTO Number generated
	 * @param autoNumber
	 */
	private void setAutoNumber(String autoNumber) {
		// TODO Auto-generated method stub
		this.autoNumber = autoNumber;
	}

	/**
	 * return CTO number
	 * @return autoNumber
	 */
	public String getCTONumber() {
		return autoNumber;
	}

	/**
	 * Return a new random , next sequence CTO number
	 * @return nextAvailableAutoNumber
	 * @throws APIException
	 */
	private String getAutoNumber() throws APIException {
		//Get the Admin instance
		IAdmin admin = session.getAdminInstance();
		//Get the Part class
		IAgileClass cls = admin.getAgileClass(TransferOrderConstants.CLASS_CTO);
		//Check if AutoNumber is required

		IAutoNumber[] numSources = cls.getAutoNumberSources();
		// Get the next available AutoNumber using the first autonumber source 
		String nextAvailableAutoNumber = numSources[0].getNextNumber(cls);

		return nextAvailableAutoNumber;
	}
}
