package com.dell.plm.agile.pqm.bean;

public class PQMJobBean {
	
	private int jobid;
	private String snapshotdate;
	private String status;
	private String errormessage;
	private String processStartTime;
	
	public String getProcessStartTime() {
		return processStartTime;
	}
	public void setProcessStartTime(String processStartTime) {
		this.processStartTime = processStartTime;
	}
	public int getJobid() {
		return jobid;
	}
	public void setJobid(int jobid) {
		this.jobid = jobid;
	}
	public String getSnapshotdate() {
		return snapshotdate;
	}
	public void setSnapshotdate(String snapshotdate) {
		this.snapshotdate = snapshotdate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrormessage() {
		return errormessage;
	}
	public void setErrormessage(String errormessage) {
		this.errormessage = errormessage;
	}

}
