package com.dell.plm.agile.pqm.bl;

import org.apache.log4j.Logger;

public class FoxconnQueryBuilder implements IQueryBuilder{
	
	private String foxconnQueryDM;
	private String foxconnQueryRegular;
	
	public String getFoxconnQueryDM() {
		return foxconnQueryDM;
	}

	public void setFoxconnQueryDM(String foxconnQueryDM) {
		this.foxconnQueryDM = foxconnQueryDM;
	}

	public String getFoxconnQueryRegular() {
		return foxconnQueryRegular;
	}

	public void setFoxconnQueryRegular(String foxconnQueryRegular) {
		this.foxconnQueryRegular = foxconnQueryRegular;
	}

	static Logger log = Logger.getLogger(FoxconnQueryBuilder.class);

	public String getQueryString(String timeStamp,String processStartTime) {
		
		log.info("## Building Foxconn Query ##");
		
		String foxconnQuery="";
		
		if (   timeStamp != null  ) 
		{
			if ( (timeStamp.equalsIgnoreCase("01/01/1990 05:55:26 AM")) ) 
			{
				foxconnQuery=getFoxconnQueryDM();
			} 
			else 
			{
				foxconnQuery= getFoxconnQueryRegular();
				foxconnQuery=foxconnQuery.replaceFirst("#", timeStamp);
				foxconnQuery=foxconnQuery.replaceFirst("#", processStartTime);
			}
		}
		return foxconnQuery;
	}
	
public String getQueryString(String timeStamp) {
		
		log.info("## Building Foxconn Query ##");
		
		String foxconnQuery="";
		
		if (   timeStamp != null  ) 
		{
			if ( (timeStamp.equalsIgnoreCase("01/01/1990 05:55:26 AM")) ) 
			{
				foxconnQuery=getFoxconnQueryDM();
			} 
			else 
			{
				foxconnQuery= getFoxconnQueryRegular();
				foxconnQuery=foxconnQuery.replaceFirst("#", timeStamp);
				
			}
		}
		return foxconnQuery;
	}

}
