package com.dell.plm.agile.pqm.bl;

public interface IQueryBuilder {

	public String getQueryString(String timeStamp , String processStartTime);
	public String getQueryString(String timeStamp);
}
