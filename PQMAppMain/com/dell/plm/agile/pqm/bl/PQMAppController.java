package com.dell.plm.agile.pqm.bl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.security.auth.login.LoginException;

import org.apache.log4j.Logger;
import org.springframework.jdbc.CannotGetJdbcConnectionException;

import com.agile.api.APIException;
import com.agile.api.IAgileSession;
import com.agile.api.IServiceRequest;
import com.dell.plm.agile.pqm.bean.PQMJobBean;
import com.dell.plm.agile.pqm.provider.AgilePQMDAO;
import com.dell.plm.agile.pqm.provider.JMSProvider;
import com.dell.plm.agile.pqm.provider.PQMSDKProvider;
import com.dell.plm.agile.pqm.util.CommonUtil;

public class PQMAppController {

	private FoxconnQueryBuilder foxconnQueryBuilder;
	private CommonUtil commonUtil;
	private AgilePQMDAO pqmDao;
	private PQMSDKProvider sdkProvider;
	private JMSProvider jmsProvider;
	private int numberOfBitsPerXML;
	private int retryCounter;
	private int retryDelay;

	public int getRetryDelay() {
		return retryDelay;
	}
	public void setRetryDelay(int retryDelay) {
		this.retryDelay = retryDelay;
	}
	public int getRetryCounter() {
		return retryCounter;
	}
	public void setRetryCounter(int retryCounter) {
		this.retryCounter = retryCounter;
	}

	static Logger log = Logger.getLogger(PQMAppController.class);

	public FoxconnQueryBuilder getFoxconnQueryBuilder() {
		return foxconnQueryBuilder;
	}
	public void setFoxconnQueryBuilder(FoxconnQueryBuilder foxconnQueryBuilder) {
		this.foxconnQueryBuilder = foxconnQueryBuilder;
	}
	public CommonUtil getCommonUtil() {
		return commonUtil;
	}
	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}
	public AgilePQMDAO getPqmDao() {
		return pqmDao;
	}
	public void setPqmDao(AgilePQMDAO pqmDao) {
		this.pqmDao = pqmDao;
	}
	public PQMSDKProvider getSdkProvider() {
		return sdkProvider;
	}
	public void setSdkProvider(PQMSDKProvider sdkProvider) {
		this.sdkProvider = sdkProvider;
	}
	public JMSProvider getJmsProvider() {
		return jmsProvider;
	}
	public void setJmsProvider(JMSProvider jmsProvider) {
		this.jmsProvider = jmsProvider;
	}
	public int getNumberOfBitsPerXML() {
		return numberOfBitsPerXML;
	}
	public void setNumberOfBitsPerXML(int numberOfBitsPerXML) {
		this.numberOfBitsPerXML = numberOfBitsPerXML;
	}


	public void schedulerControl()  throws APIException, FileNotFoundException, IOException, LoginException, SecurityException, ConnectException, CannotGetJdbcConnectionException {
		
		IAgileSession agileSession = null;
		String queryString = "";
		String storeid = null;
		int countAgile=0;
		int jobid = 0 ;
		
		
		
		/*get last successful Job time */
		
		log.info("## Getting Last successful job runtime ##");
		String processStartTime = "1/1/1990 5:55:26.738277 AM";
		Date processStartDateTime = null;
		String timeStamp  = null;
		
		try{
				
			PQMJobBean pqmJobBean = pqmDao.getLastSuccessfulJob();
			if (pqmJobBean != null) {
				timeStamp = pqmJobBean.getSnapshotdate();					 
			}
			
			processStartDateTime = pqmDao.getProcessStartTime();
			
			if (processStartDateTime != null) {
				processStartTime = commonUtil.formatTime(processStartDateTime);
				 if ( ! processStartTime.equalsIgnoreCase("01/01/1990 05:55:26 AM")) {
					 processStartTime = processStartTime + " GMT";
				 }
				 
				 if (pqmJobBean != null) {
						pqmJobBean.setProcessStartTime(processStartTime);
					}
			}			
			
			
		}
		catch (SQLException se ) 
		{
			log.error("Error in connecting to DB Fetching Last Successfull Date :: SQLException :: " + commonUtil.exception2String(se));
		} 
		catch (CannotGetJdbcConnectionException ce)
		{
			log.error("Error in connecting to DB Fetching Last Successfull Date :: CannotGetJdbcConnectionException :: " + commonUtil.exception2String(ce));
		} 
		catch (Exception e)
		{
			log.error("Problem in getting  Last successful Job status ");
			log.error("## Error in processing :: Exception :: "+  commonUtil.exception2String(e));
		}
		
		/*Building IQuery for Foxconn */
		
		queryString = foxconnQueryBuilder.getQueryString(timeStamp,processStartTime);
		
		//queryString = foxconnQueryBuilder.getQueryString(timeStamp);
		
		log.info("## Max. Retry Count in case of Failure :: "+getRetryCounter() + " ##");
		log.info("## Time Delay between rety :: " +getRetryDelay()/1000 +" seconds ##");
		
		/*Getting Agile Session*/
		
		
		log.info("## Getting Agile Session ##");
		
		while(countAgile < getRetryCounter())
		{
			try{
				log.info("## No. of retries for getting Agile Session :: "+countAgile);
				agileSession = commonUtil.getAgileSession();
				break;
			} 
			catch (APIException ae) 
			{
				/*if (((Integer)ae.getErrorCode()).intValue() == 60062) 
				{
					log.error("## User Name / Pwd Error  in processing :: Agile Session :: "+  commonUtil.exception2String(ae));
				} 
				else if (((Integer) ae.getErrorCode()).intValue() == 60089) 
				{
					log.error("## Agile server down :: "+  commonUtil.exception2String(ae));
				}*/
				ae.printStackTrace();
				log.error("## Error while getting Agile Session :: APIException :: "+  commonUtil.exception2String(ae));
				countAgile++;
				if(countAgile<getRetryCounter()){
					try {
						Thread.sleep(getRetryDelay());
					} catch (NumberFormatException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (InterruptedException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
			} 
			
		}
		if (agileSession != null) {
			
		/*Retrieve list of PSR objects based on the query using SDK call*/
		//store time here and then call query with two parameter.	
		HashMap<String,ArrayList <IServiceRequest>> hmPSR = sdkProvider.getPSRObjects(queryString, agileSession);
		ArrayList <IServiceRequest> cfiListPSR = new ArrayList<IServiceRequest>();
		ArrayList <IServiceRequest> btoListPSR = new ArrayList<IServiceRequest>();
		
		if(!hmPSR.isEmpty()){	
			
			int countLastSuccessTime=0;
			
			/*Insert Jobid into Job table */
			
			while(countLastSuccessTime<getRetryCounter())
			{
				try
				{
					log.info("No. of retries while inserting new record in PQM_PSR_JOBS Table :: "+countLastSuccessTime);
					jobid = pqmDao.insertPQMJob(processStartDateTime); // Added new parameter processStartTime
					log.info("## Job ID :: " +jobid +" ##");
					break;
				} 
				catch(Exception e)
				{
					log.error("## Error generating JOBID :: Exception :: "+ commonUtil.exception2String(e));
					e.printStackTrace();
					countLastSuccessTime++;
					if(countLastSuccessTime<getRetryCounter()){
						try {
							Thread.sleep(getRetryDelay());
						} catch (NumberFormatException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (InterruptedException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
					}
					
				}	
			}
			
			log.info("## PSR Objects : "+hmPSR + " ##");
			
			if(jobid!=0){
				
				if (hmPSR.containsKey("CFI")){
					cfiListPSR = hmPSR.get("CFI");
					log.info("## CFI Objects : "+cfiListPSR + " ##");
				}
				if (hmPSR.containsKey("BTO")){
					btoListPSR = hmPSR.get("BTO");
					log.info("## BTO Objects : "+btoListPSR + " ##");
				}
				if (!cfiListPSR.isEmpty() ) {
					sendPSRtoQueue(cfiListPSR, agileSession, jobid,"CFI PR");
				}
				if (!btoListPSR.isEmpty() ) {
					sendPSRtoQueue(btoListPSR, agileSession, jobid,"BTO/FACE");
				}							
			}
			else
				log.info("## JobID not genarated..... :: JOBID :: "+jobid + " ##");
			
			
		} else {						
			log.info("## There are no modified or newly created PSR objects in Agile ##");
			storeid="";
			int noDataCounter=0;
			
			try{
				jobid = pqmDao.insertPQMJob(processStartDateTime);
				log.info("## Job ID :: " +jobid +" ##");
			}
			catch(Exception e){
				log.error("Error Inserting JobID in DB when there are no PSR :: " +commonUtil.exception2String(e));
				e.printStackTrace();
			}
			
			
			while(noDataCounter<getRetryCounter()){
				
				try {
					log.info("## No. of retries pushing JMS message when there is no data :: "+countAgile);
					String jmsXmlString = commonUtil.buildXML(jobid, storeid);
					jmsProvider.sendMessage(jmsXmlString);
					log.info("JMS Message pushed for Job ID :: "+jobid + " :: Store ID :: "+storeid);
					break;
				} catch (Exception e){
					log.error("## Error pushing JMS message when there are no messages to push ##" +commonUtil.exception2String(e));
					e.printStackTrace();
					noDataCounter++;
					if(noDataCounter<getRetryCounter()){
						try {
							Thread.sleep(getRetryDelay());
						} catch (NumberFormatException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (InterruptedException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
					}
					}
				}
			
			}
		} 
		
		else
			log.error(" Agile Session is Null :: AgileSeesion :: "+agileSession);
	}

	
	
	@SuppressWarnings("rawtypes")
	private void sendPSRtoQueue(ArrayList <IServiceRequest> listPSR, IAgileSession session, int jobid, String psrType ){
		

			List<List<IServiceRequest>> parts = chopped(listPSR, getNumberOfBitsPerXML());
			for (Iterator iterator = parts.iterator(); iterator.hasNext();) {
				String storeid = null;
				int countStoredProc = 0;
				int countJMS = 0;
				
				while(countStoredProc < getRetryCounter()){
					
					try{
						log.info("No. of retries when processing :: "+psrType + " :: " +countStoredProc);
						@SuppressWarnings("unchecked")
						List<IServiceRequest> psrExport = (List<IServiceRequest>) iterator.next();
						byte[] msgAgile = sdkProvider.getExport(session, psrExport);
						String msgUnzippedString = commonUtil.unZipper(msgAgile);
						storeid =pqmDao.processBITsToDB(msgUnzippedString,jobid);
						log.info("## PSR :: "+psrExport + " || Store ID :: " +storeid +" ##");
						/*String jmsXmlString = commonUtil.buildXML(jobid, storeid);
						jmsProvider.sendMessage(jmsXmlString);
						log.info("## JMS Message Pushed for...."+psrExport+ " ##");*/
						break;
					}
					catch(Exception e)
					{
						log.error("## Error while processing :: " +psrType +" :: "+ commonUtil.exception2String(e));
						e.printStackTrace();
						countStoredProc++;
						if(countStoredProc<getRetryCounter()){
							try {
								Thread.sleep(getRetryDelay());
							} catch (NumberFormatException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (InterruptedException e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}
						}
						
					}
					
				}
				if(storeid!=null && storeid!=""){
					while(countJMS<getRetryCounter()){
						try{
							log.info("No. of retries when pushing JMS Message :: " +countJMS);
							String jmsXmlString = commonUtil.buildXML(jobid, storeid);
							jmsProvider.sendMessage(jmsXmlString);
							log.info("JMS Message pushed for Job ID :: "+jobid + " :: Store ID :: "+storeid);
							break;
						}
						catch(Exception e){
							log.error("## Error while pushing JMS Message ##" +commonUtil.exception2String(e));
							e.printStackTrace();
							countJMS++;	
							if(countJMS<getRetryCounter()){
								try {
									Thread.sleep(getRetryDelay());
								} catch (NumberFormatException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								} catch (InterruptedException e2) {
									// TODO Auto-generated catch block
									e2.printStackTrace();
								}
							}
							
						}
					}
				}
				
			}
	}


	/*PSR XML Splitting happens here*/
	
	private List<List<IServiceRequest>> chopped(List<IServiceRequest> list, int n) {
		List<List<IServiceRequest>> parts = new ArrayList<List<IServiceRequest>>();
	    final int k = list.size();
	    for (int i = 0; i < k; i += n) {
	        parts.add(new ArrayList<IServiceRequest>(
	            list.subList(i, Math.min(k, i + n)))
	        );
	    }
	    return parts;
	}

}
