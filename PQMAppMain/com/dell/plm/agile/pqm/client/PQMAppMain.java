package com.dell.plm.agile.pqm.client;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.agile.api.APIException;
import com.dell.plm.agile.pqm.bl.PQMAppController;

public class PQMAppMain {

	static Logger log = Logger.getLogger(PQMAppMain.class);

	public static void main(String args[]) {
		
		try{
			log.info("## Intializing Application Main starts ##");
			
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("ApplicationContext.xml");
			PQMAppController SchedulerDAO = (PQMAppController) context.getBean("PQMController");
			SchedulerDAO.schedulerControl();
		} 
		catch ( java.net.ConnectException ce) {
			log.error("## Connection Exception : "+ ce);
		} 
		catch ( org.springframework.beans.factory.BeanCreationException bce) {
			log.error("## BeanCreation Exception : "+ bce);
		} 
		catch ( java.lang.SecurityException securityException) {
			log.error("## Security Exception : "+ securityException);
		} 
		catch ( javax.security.auth.login.LoginException loginException) {
			log.error("## Login Exception : "+ loginException);
		} 
		catch ( APIException apiException) {
			if (apiException.getErrorCode().toString() == "60089"){
					System.out.println("## Failed to connect to Agile : ");
				}
			log.error("## APIException  : "+ apiException);		
		} 
		catch (Exception e) {
			log.error("## Exception : "+ e);
			e.printStackTrace();
		}		
	}
}
