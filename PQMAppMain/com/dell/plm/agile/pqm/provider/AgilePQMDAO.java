package com.dell.plm.agile.pqm.provider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.incrementer.OracleSequenceMaxValueIncrementer;

import com.dell.plm.agile.pqm.bean.PQMJobBean;
import com.dell.plm.agile.pqm.util.CommonUtil;

public class AgilePQMDAO{
	
	private CommonUtil commonUtil;
	private JdbcTemplate jdbcTemplate;
	private OracleSequenceMaxValueIncrementer pqmBitsIncrementer;
	private PQMDataSource pqmDataSource;
	private ProcessBITsToDBSP sproc;
	private static String procName;
	private String processStartTime;
	
	static Logger log = Logger.getLogger(AgilePQMDAO.class);
	private static final String GET_JOB_START_TIME = "select sys_extract_utc(systimestamp)as ProcessStartTime from dual";
	private static final String GET_SUCCESS_JOB_TIME =	" SELECT * FROM ( SELECT * FROM PQM_PSR_JOBS WHERE STATUS='C' ORDER BY SNAPSHOTDATE DESC ) WHERE ROWNUM=1 ";		
	//private static final String INSERT_JOB = "insert into PQM_PSR_JOBS values(?,current_timestamp,?,?)";
	//private static final String INSERT_JOB = "insert into PQM_PSR_JOBS values(?,sys_extract_utc(systimestamp),?,?)"; old
	private static final String INSERT_JOB = "insert into PQM_PSR_JOBS values(?,?,?,?)";
	
	public String getProcName() {
		return procName;
	}

	public void setProcName(String procName) {
		AgilePQMDAO.procName = procName;
	}

	public PQMDataSource getPqmDataSource() {
		return pqmDataSource;
	}

	public void setPqmDataSource(PQMDataSource pqmDataSource) {
		this.jdbcTemplate = new JdbcTemplate(pqmDataSource);
		this.sproc = new ProcessBITsToDBSP(jdbcTemplate.getDataSource());
	}

	public OracleSequenceMaxValueIncrementer getPqmBitsIncrementer() {
		return pqmBitsIncrementer;
	}

	public void setPqmBitsIncrementer(
			OracleSequenceMaxValueIncrementer pqmBitsIncrementer) {
		this.pqmBitsIncrementer = pqmBitsIncrementer;
	}

	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}
	
	public String processBITsToDB(String messageData, int jobId) throws Exception,SQLException{
		log.info("## Inside processBITsToDB procedure ##");
		Map<String, Object> returnMap = sproc.execute(messageData, jobId);
		String returnStoreId = (String) returnMap.get("Return_Store_Id");
		return returnStoreId;
	}
	public Date getProcessStartTime() throws Exception{
				
		return jdbcTemplate.query(GET_JOB_START_TIME, new Object[] {}, new PQMStartTimeRowMapper()).get(0);
	}	
		
	public PQMJobBean getLastSuccessfulJob() throws Exception{
		return jdbcTemplate.query(GET_SUCCESS_JOB_TIME, new Object[] {}, new PQMRowMapper()).get(0);
		
	}
	
	public int insertPQMJob(Date processStartDateTime) throws Exception{
		log.info("## Inserting the current job status as 'N' in PQM_PSR_JOBS table :: insertPQMJob method ##");
		int jobId = pqmBitsIncrementer.nextIntValue();		
		jdbcTemplate.update(INSERT_JOB, new Object[]{	jobId,processStartDateTime,"N",""});
		return jobId;
	}
	
	private final class PQMStartTimeRowMapper implements RowMapper<Date>{
		
		public Date mapRow(ResultSet rs, int rowNum) throws SQLException{
			Date moddate;
			//String dateString ="1/1/1990 5:55:26.738277 AM";
			
			moddate = rs.getTimestamp("ProcessStartTime");
			/*if (moddate != null) {
				 dateString = commonUtil.formatTime(moddate);
				 if ( ! dateString.equalsIgnoreCase("01/01/1990 05:55:26 AM")) {
				 dateString = dateString + " GMT";
				 }
			}*/			
			return moddate;
		}
	}
	
	private final class PQMRowMapper implements RowMapper<PQMJobBean>{

		public PQMJobBean mapRow(ResultSet rs, int rowNum) throws SQLException{
			Date moddate;
			String dateString ="1/1/1990 5:55:26.738277 AM";
			PQMJobBean pqmBean = new PQMJobBean();
			pqmBean.setJobid(rs.getInt("JOBID"));
			moddate = rs.getTimestamp("SNAPSHOTDATE");
			if (moddate != null) {
				 dateString = commonUtil.formatTime(moddate);
				 if ( ! dateString.equalsIgnoreCase("01/01/1990 05:55:26 AM")) {
				 dateString = dateString + " GMT";
				 }
			}
			pqmBean.setSnapshotdate(dateString);
			pqmBean.setStatus(rs.getString("STATUS"));
			return pqmBean;
		}
	}
	
	static class ProcessBITsToDBSP extends StoredProcedure {
		public ProcessBITsToDBSP(DataSource datasource) {
			super(datasource, procName);
			log.info("## setting IN Parameters of ObsoleteReport's Store Procedure ##");
			declareParameter(new SqlParameter("Fxn_Xml", Types.CLOB));
			declareParameter(new SqlParameter("Stage", Types.VARCHAR));
			declareParameter(new SqlParameter("Job_Id", Types.INTEGER));
			declareParameter(new SqlParameter("Store_Id", Types.VARCHAR));
			declareParameter(new SqlParameter("Fault_Var", Types.CLOB));
			declareParameter(new SqlOutParameter("Return_Store_Id", Types.VARCHAR));
			compile();
		}

		public Map<String, Object> execute(String messageData,int jobId) {
			Map<String, Object> returnMap =  super.execute(messageData, "SCH", jobId, null, null);
			return returnMap;
		}
	}
}	
			
