package com.dell.plm.agile.pqm.provider;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

public class JMSProvider {
	
	/*private Destination destination;
    private JmsTemplate jmsTemplate;
    
	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}*/
	
	/*public void sendMessage(String Msg){
	final String xmlMsg = Msg;
	
	MessageCreator creator = new MessageCreator() {
		public Message createMessage(Session session) {
			TextMessage message = null;
			try {
				message = session.createTextMessage(xmlMsg);
				// message.setStringProperty("text", "Hello World");
			} catch (JMSException e) {
				e.printStackTrace();
			}
			return message;
		}
	};

	jmsTemplate.send(destination, creator);
	
}*/
	


	//public final static String JNDI_FACTORY="weblogic.jndi.WLInitialContextFactory";

	 // Defines the JMS context factory.
	 //public final static String JMS_FACTORY="jms.agile9ods.cf.XAConnectionFactory";

	 // Defines the queue.
	 //public final static String QUEUE="Foxconn";

	 private QueueConnectionFactory qconFactory;
	 private QueueConnection qcon;
	 private QueueSession qsession;
	 private QueueSender qsender;
	 private Queue queue;
	 private TextMessage msg;
	 
	 private String weblogicJNDI;
	 private String weblogicConnectionFactory;
	 private String weblogicJMSQueue;
	 private String weblogicServerURL;
	 private String jmsMessageFlag;
	 
	 public String getJmsMessageFlag() {
		return jmsMessageFlag;
	}

	public void setJmsMessageFlag(String jmsMessageFlag) {
		this.jmsMessageFlag = jmsMessageFlag;
	}

	public String getWeblogicConnectionFactory() {
		return weblogicConnectionFactory;
	}

	public void setWeblogicConnectionFactory(String weblogicConnectionFactory) {
		this.weblogicConnectionFactory = weblogicConnectionFactory;
	}

	public String getWeblogicJMSQueue() {
		return weblogicJMSQueue;
	}

	public void setWeblogicJMSQueue(String weblogicJMSQueue) {
		this.weblogicJMSQueue = weblogicJMSQueue;
	}

	public String getWeblogicServerURL() {
		return weblogicServerURL;
	}

	public void setWeblogicServerURL(String weblogicServerURL) {
		this.weblogicServerURL = weblogicServerURL;
	}

	public String getWeblogicJNDI() {
		return weblogicJNDI;
	}

	public void setWeblogicJNDI(String weblogicJNDI) {
		this.weblogicJNDI = weblogicJNDI;
	}

	static Logger log = Logger.getLogger(JMSProvider.class);

	public void sendMessage(String Msg) throws NamingException, JMSException, FileNotFoundException, IOException, Exception {
			
		
		//System.out.println("jmsMessageFlow :: " +getJmsMessageFlag());
		
		if(getJmsMessageFlag().equalsIgnoreCase("FALSE"))
		{
			log.info("## JMS Messges are stopped.... ##");
		}
		else{
			InitialContext ic = getInitialContext(getWeblogicServerURL(),getWeblogicJNDI());
			JMSProvider qs = new JMSProvider();
		    qs.init(ic, getWeblogicJMSQueue(),getWeblogicConnectionFactory());
		    qs.send(Msg);
		    qs.close();
		}
		
	}
	 
	 public void init(Context ctx, String queueName, String connectionFactory)
			    throws NamingException, JMSException
			 {
			    qconFactory = (QueueConnectionFactory) ctx.lookup(connectionFactory);
			    qcon = qconFactory.createQueueConnection();
			    qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			    queue = (Queue) ctx.lookup(queueName);
			    qsender = qsession.createSender(queue);
			    msg = qsession.createTextMessage();
			    qcon.start();
			 }
	 
	 public void send(String message) throws JMSException {
		    msg.setText(message);
		    qsender.send(msg);
		 }
	 
	 public void close() throws JMSException {
		    qsender.close();
		    qsession.close();
		    qcon.close();
		 }
	 
	 private InitialContext getInitialContext(String url, String jndiFactory)
			    throws NamingException
			 {
			    Hashtable env = new Hashtable();
			    env.put(Context.INITIAL_CONTEXT_FACTORY, jndiFactory);
			    env.put(Context.PROVIDER_URL, url);
			    return new InitialContext(env);
			 }
}
