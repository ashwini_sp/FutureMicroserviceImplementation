package com.dell.plm.agile.pqm.provider;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.dell.plm.agile.aic.common.cryptography.StringEncryptor;

public class PQMDataSource extends DriverManagerDataSource{
	
	public String getPassword(){
		String password = super.getPassword();
		//System.out.println("The encrypted password is "+password);
		String decryptedPassword = StringEncryptor.decrypt(password);
		return decryptedPassword;
	}

}
