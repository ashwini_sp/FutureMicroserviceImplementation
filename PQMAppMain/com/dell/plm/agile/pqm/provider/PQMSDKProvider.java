package com.dell.plm.agile.pqm.provider;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.ExportConstants;
import com.agile.api.IAgileSession;
import com.agile.api.IDataObject;
import com.agile.api.IExportManager;
import com.agile.api.IQuery;
import com.agile.api.IRow;
import com.agile.api.IServiceRequest;
import com.agile.api.ITable;
import com.dell.plm.agile.pqm.client.PQMAppMain;
import com.dell.plm.agile.pqm.util.CommonUtil;

public class PQMSDKProvider {
	
	static Logger log = Logger.getLogger(PQMAppMain.class);

    private CommonUtil commonUtil;
    private String psrType;
    private String cfiObject;
    private String btoObject;
    private String exportFilter;

	public String getExportFilter() {
		return exportFilter;
	}

	public void setExportFilter(String exportFilter) {
		this.exportFilter = exportFilter;
	}

	public String getCfiObject() {
		return cfiObject;
	}

	public void setCfiObject(String cfiObject) {
		this.cfiObject = cfiObject;
	}

	public String getBtoObject() {
		return btoObject;
	}

	public void setBtoObject(String btoObject) {
		this.btoObject = btoObject;
	}

	public String getPsrType() {
		return psrType;
	}

	public void setPsrType(String psrType) {
		this.psrType = psrType;
	}

	public CommonUtil getCommonUtil() {
		return commonUtil;
	}

	public void setCommonUtil(CommonUtil commonUtil) {
		this.commonUtil = commonUtil;
	}

	public HashMap<String,ArrayList <IServiceRequest>> getPSRObjects(String psrQuery,IAgileSession session) throws APIException, FileNotFoundException, IOException{

		IQuery query = (IQuery)session.createObject(IQuery.OBJECT_TYPE, getPsrType());
		query.setCaseSensitive(false);
		System.out.println("query = " + psrQuery);
		query.setCriteria(psrQuery);
		//ITable iTable = query.execute(new Object[] {"CFI PR","BTO/FACE"});
		ITable iTable = query.execute(new Object[] {getCfiObject(),getBtoObject()});
		System.out.println("Size = "+ iTable.size());
		Iterator<?> iterator = iTable.iterator();
		HashMap<String,ArrayList <IServiceRequest>> hmPSR = new HashMap<String,ArrayList <IServiceRequest>>();
		ArrayList <IServiceRequest> cfiListPSR = new ArrayList<IServiceRequest>();
		ArrayList <IServiceRequest> btoListPSR = new ArrayList<IServiceRequest>();
		while(iterator.hasNext()){
			IRow row = (IRow)iterator.next();
			IServiceRequest serviceRequest = (IServiceRequest) row.getReferent();
			if (serviceRequest.getName() != null ) {
				 if (serviceRequest.getName().indexOf("CFI") >= 0  ){
					 cfiListPSR.add(serviceRequest);
				} else {
					btoListPSR.add(serviceRequest);
				}
			}	
				
			log.info("## Values in Table : " +serviceRequest.getName() + " ##");
			//listPSR.add(serviceRequest);
		}
	
		if (cfiListPSR.size() > 0 ){
			hmPSR.put("CFI", cfiListPSR);
		}
		if ( btoListPSR.size() > 0){ 
			hmPSR.put("BTO", btoListPSR);
		}
		return hmPSR;
	}
	
	public byte[] getExport (IAgileSession session, List<IServiceRequest> parts) throws APIException{
		
		String[] filters = {getExportFilter()};
		IExportManager eMgr = (IExportManager) session.getManager(IExportManager.class);
		IDataObject[] exObject_temp = new IDataObject[parts.size()];
		if(!parts.isEmpty())
		{		for(int i=0;i<parts.size();i++){
				exObject_temp[i]=(IDataObject) parts.get(i);
				}
		}
		byte[] exportData = eMgr.exportData(exObject_temp, ExportConstants.EXPORT_FORMAT_AXML, filters);
		return exportData;				
	}
	


}
