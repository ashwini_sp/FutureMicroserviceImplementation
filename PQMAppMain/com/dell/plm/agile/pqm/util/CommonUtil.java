package com.dell.plm.agile.pqm.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.security.auth.login.LoginException;

import org.apache.log4j.Logger;

import com.agile.api.APIException;
import com.agile.api.AgileSessionFactory;
import com.agile.api.IAgileSession;
import com.dell.plm.agile.aic.common.cryptography.StringEncryptor;

public class CommonUtil {
	
	public static IAgileSession session=null;
	
	static Logger log = Logger.getLogger(CommonUtil.class);
	private String xml;
	private String agileUrl;
	private String agileUsername;
	private String agilePassword;

	public String getAgileUrl() {
		return agileUrl;
	}

	public void setAgileUrl(String agileUrl) {
		this.agileUrl = agileUrl;
	}

	public String getAgileUsername() {
		return agileUsername;
	}

	public void setAgileUsername(String agileUsername) {
		this.agileUsername = agileUsername;
	}

	public String getAgilePassword() {
		return agilePassword;
	}

	public void setAgilePassword(String agilePassword) {
		this.agilePassword = agilePassword;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String exception2String(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);
        pw.flush();
        pw.close();
        return sw.toString();
    }
	
	public String buildXML(int jobid, String storeid) throws Exception{
		String xmlData = getXml();	
		xmlData=xmlData.replaceFirst("#", new Integer(jobid).toString());	
		xmlData=xmlData.replaceFirst("#", storeid);
		xmlData=xmlData.replaceFirst("#", "<Destination>FXN</Destination>");
		return xmlData;
	}
	
/*	public String getCurretDate(String timestamp){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		//calendar.add(Calendar.HOUR, -4);
		Date date = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat(DateFormat);
		dateFormat.setTimeZone(TimeZone.getTimeZone(TimeZoneGMT));
		String CurrentDate = dateFormat.format(date);
		CurrentDate = CurrentDate.concat(TimeZoneVariable);
		return CurrentDate;
	}*/
	
	public String formatTime(Date moddate){
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		String dateString = format.format(moddate);
		return dateString;
	}
	@SuppressWarnings("unchecked")
	public IAgileSession getAgileSession() throws APIException, FileNotFoundException, IOException, LoginException, SecurityException {

			String AgileURL = getAgileUrl().trim();
			log.info("## Agile URL : "+AgileURL + " ##");
			String AgileUser = getAgileUsername().trim();
			log.info("## Agile URL : "+AgileUser + " ##");
			String AgilePassword = getAgilePassword().trim();
			AgilePassword = StringEncryptor.decrypt(AgilePassword);
			@SuppressWarnings("rawtypes")
			HashMap param = new HashMap();
			AgileSessionFactory factory = AgileSessionFactory.getInstance(AgileURL);
			param.put(AgileSessionFactory.USERNAME, AgileUser);
			param.put(AgileSessionFactory.PASSWORD, AgilePassword);
			session = factory.createSession(param);
			return session;
	}
	
	public String unZipper(byte[] exportData) throws IOException, Exception{
		
		String sExtractedString = null;
		if(exportData!=null){				
			int n;
			byte[] buf = new byte[2*exportData.length];
			ByteArrayInputStream oByteArrayIS = null;
			ZipInputStream oZipInputStream = null;
			ZipEntry oZipEntry = null;				
			oByteArrayIS = new ByteArrayInputStream(exportData);
			oZipInputStream = new ZipInputStream(oByteArrayIS);
			oZipEntry = oZipInputStream.getNextEntry(); 
			while (oZipEntry != null) { 
	    		//String entryName = oZipEntry.getName();
	    		//System.out.println("EntryName : "+entryName);
	    		ByteArrayOutputStream oByteArrayStream = new ByteArrayOutputStream();
	    		while ((n = oZipInputStream.read(buf, 0,2*exportData.length )) > -1) {					
	    			oByteArrayStream.write(buf, 0, n);					
	    		}
	    		sExtractedString = oByteArrayStream.toString();
	    		oByteArrayStream.close();
	    		oZipInputStream.closeEntry();
	    		oZipEntry = oZipInputStream.getNextEntry();
	    	}
	    	oZipInputStream.close();
			//System.out.println("After Convert -- "+sExtractedString);
		}
		return sExtractedString;
	}
}
